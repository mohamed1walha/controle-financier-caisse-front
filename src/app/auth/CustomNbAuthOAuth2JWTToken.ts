import { NbAuthOAuth2JWTToken } from '@nebular/auth';

export class CustomNbAuthOAuth2JWTToken extends NbAuthOAuth2JWTToken {
  override isValid(): boolean {
    return (
      !!this.getValue() &&
      this.accessTokenPayload &&
      (!this.getTokenExpDate() || new Date() < this.getTokenExpDate())
    );
  }
  override getTokenExpDate(): any {
    if (!this.token.hasOwnProperty('expires_in')) {
      return null;
    }
    if (this.createdAt) {
      return new Date(
        this.createdAt.getTime() + Number(this.token.expires_in - 30) * 1000
      );
    }
  }
}
