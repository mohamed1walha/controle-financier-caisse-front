import {NgModule} from '@angular/core';

import {SharedModule} from '../shared/shared.module';
import {AuthRoutingModule} from './auth-routing.module';
import {IndividualConfig, ToastrService} from "ngx-toastr";
import {NbLayoutModule, NbToastrModule} from "@nebular/theme";
import {NbAuthModule} from "@nebular/auth";

const toastrService = {
  success: (
    message?: string,
    title?: string,
    override?: Partial<IndividualConfig>
  ) => {
  },
  error: (
    message?: string,
    title?: string,
    override?: Partial<IndividualConfig>
  ) => {
  },
};
@NgModule({
  declarations: [
    ...AuthRoutingModule.components,
  ],
  imports: [
    SharedModule,
    AuthRoutingModule,
    NbToastrModule.forRoot(),
    NbLayoutModule,
    NbAuthModule,

  ],
  providers: [
    {provide: ToastrService, useValue: toastrService}],
})
export class AuthModule { }
