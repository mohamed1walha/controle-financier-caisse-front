import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {finalize} from 'rxjs/operators';

import {DashboardComponent} from '../../dashboard/dashboard.component';
import {AuthenticationService} from 'src/app/core/services/authentication.service';
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  form!: FormGroup;
  message!: string;
  loginLoading = false;
  static path = () => ['login'];
  submitted: boolean | undefined;
  error: any;

  constructor(
    private authenticationService: AuthenticationService,
    public formBuilder: FormBuilder,
    private router: Router,
    private toastService: NbToastrService
  ) {
    this.initFormBuilder();
  }

  ngOnInit() {
  }

  loginUser(formValue: NgForm) {
    this.loginLoading = true;
    this.submitted = true;

    this.authenticationService.login(formValue)
        .pipe(finalize(() => this.loginLoading = false))
      .subscribe(
        data => {
          this.router.navigate(DashboardComponent.path());
        },
        error => {

          this.toastService.danger($localize`UserName ou mot de passe incorrect`, $localize`Erreur`, {

            duration: 3000, position: NbGlobalPhysicalPosition.BOTTOM_RIGHT
          });

        }
      );

  }





  private initFormBuilder() {
    this.form = this.formBuilder.group({
      username: ['', [
        Validators.required]],
      password: ['', Validators.required]
    });
  }

}
