import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {
  ChauffeurDTO,
  EmetteurDTO,
  EtablissementDTO,
  JoursCaiseDTO,
  JoursCaisseService,
  PiedecheDTO,
  StatusDTO,
} from '../../../../../swagger-api';
import {
  ConfirmationService,
  FilterMatchMode,
  FilterMetadata,
  FilterService,
  MessageService,
  SelectItem,
} from 'primeng/api';
import { Table } from 'primeng/table';
import { debounceTime, Observable, skipWhile, takeWhile } from 'rxjs';
import {
  fromBlade,
  fromChauffeur,
  fromEmetteur,
  fromEtablissement,
  fromJoursCaisse,
  fromLastSyncApp,
  fromModePaiement,
  fromPiedeche,
  fromStatus,
} from '../../../shared/reducers';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import {
  BladeActions,
  ChauffeurActions,
  EmetteurActions,
  EtablissementActions,
  JoursCaisseActions,
  LastSyncAppActions,
  PiedecheActions,
  StatusActions,
} from '../../../shared/actions';
import { map } from 'rxjs/operators';
import { setFilterBy } from '../../../shared/actions/joursCaisse.actions';
import jsPDF from 'jspdf';
import autoTable, { RowInput } from 'jspdf-autotable';
import { ExportService } from '../../../core/services/export.service';
import { DatePipe } from '@angular/common';
import { deepCopy } from '../../../shared/utils/deep-copy';
import { NbDialogService } from '@nebular/theme';
import { Calendar } from 'primeng/calendar';
import { LocalService } from '../../../core/services/locale.service';

Calendar.prototype.getDateFormat = () => 'dd/mm/yy';

@Component({
  selector: 'app-enveloppe-non-receptionne',
  templateUrl: './enveloppe-non-receptionne.component.html',
  styles: [
    `
      :host ::ng-deep .p-dialog .product-image {
        width: 150px;
        margin: 0 auto 2rem auto;
        display: block;
      }
    `,
  ],
  providers: [ExportService, NbDialogService],
  styleUrls: ['./enveloppe-non-receptionne.component.scss'],
})
export class EnveloppeNonReceptionneComponent implements OnInit, OnDestroy {
  @ViewChild('dt') dt: Table;
  headerRow: {
    displayName: string;
    key: string;
    hasSort: boolean;
    type: string;
  }[] = [
    {
      displayName: $localize`Etablissement`,
      key: 'gjcEtablissement.etLibelle',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Caisse`,
      key: 'gjcCaisse',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Référence Z`,
      key: 'gjcNumzcaisse',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Date d'ouverture`,
      key: 'gjcDateouv',
      hasSort: true,
      type: 'date',
    },
    {
      displayName: $localize`Date Fermeture`,
      key: 'gjcDateferme',
      hasSort: true,
      type: 'date',
    },
    {
      displayName: $localize`Statut`,
      key: 'statusReceptionObj.statuslibelle',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Ref externe`,
      key: 'refExterne',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Montant Total`,
      key: 'montantCaisse',
      hasSort: true,
      type: 'text',
    },
  ];
  _alive: boolean = true;
  joursEnAttentes$: Observable<any> = this.store$.select(
    fromJoursCaisse.selectData
  );
  status$: Observable<StatusDTO[]> = this.store$.select(
    fromStatus.selectStatuss
  );
  statuses: StatusDTO[];
  loading$: Observable<any> = this.store$.select(fromJoursCaisse.selectLoading);
  piedeche$: Observable<PiedecheDTO[]> = this.store$.select(
    fromPiedeche.selectPiedeches
  );
  joursEnAttentes: JoursCaiseDTO[];
  selectedJourCaisses: JoursCaiseDTO[];
  piedeche: PiedecheDTO[];
  joursEnAttente: JoursCaiseDTO = {};
  matchModeOptions: SelectItem[];
  matchModeOptionsDate: SelectItem[];
  etablissement$: Observable<EtablissementDTO[]> = this.store$.select(
    fromEtablissement.selectEtablissements
  );
  etablissements: EtablissementDTO[];
  dateForm: FormGroup;
  selectedjoursCaisses: JoursCaiseDTO[];
  loading: boolean = false;
  etablissement: EtablissementDTO;
  chauffeurs$: Observable<ChauffeurDTO[]> = this.store$.select(
    fromChauffeur.selectChauffeurs
  );
  emetteurs$: Observable<EmetteurDTO[]> = this.store$.select(
    fromEmetteur.selectEmetteurs
  );
  chauffeurs: ChauffeurDTO[];
  emetteurs: EmetteurDTO[];
  selectedChauffeur: ChauffeurDTO | undefined;
  selectedEmetteur: EmetteurDTO | undefined;
  lastSync$ = this.store$.select(fromLastSyncApp.getDateSync);
  selectedProducts: JoursCaiseDTO[] = [];
  lastDateSync: Date;
  searchControl = new FormControl('');
  columns = [
    { title: $localize`Etablissement`, dataKey: 'gjcEtablissementlabel' },
    { title: $localize`Période`, dataKey: 'periode' },
    { title: $localize`Nbre d'enveloppe`, dataKey: 'nbrEnv' },
    { title: $localize`Remarques`, dataKey: 'rqs' },
    { title: $localize`Nom Emetteur`, dataKey: 'nomEm' },
    { title: $localize`Signature Emetteur`, dataKey: 'signEm' },
    { title: $localize`Cachet Emetteur`, dataKey: 'cachEm' },
    { title: $localize`Nom Chauffeur`, dataKey: 'nomChauf' },
    { title: $localize`Signature Chauffeur`, dataKey: 'signChauf' },
    { title: $localize`Date Reception`, dataKey: 'receipDate' },
  ];
  montantTot: number = 0;

  receptionDialog: boolean;
  private submitted: boolean;
  receptionForm: FormGroup;

  currentLanguage = this.localStore.getData('language');

  constructor(
    private store$: Store<
      fromBlade.State &
        fromJoursCaisse.State &
        fromPiedeche.State &
        fromEtablissement.State &
        fromChauffeur.State &
        fromEmetteur.State &
        fromModePaiement.State
    >,
    private filterService: FilterService,
    private localStore: LocalService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private dialogService: NbDialogService,
    private joursCaisseService: JoursCaisseService,
    public snackBar: MatSnackBar,
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private exportService: ExportService,
    private datePipe: DatePipe
  ) {}

  ngOnDestroy(): void {
    this.store$.dispatch(JoursCaisseActions.resetDataTableStore());
    this._alive = false;
  }

  get dateFermeFrom() {
    return this.dateForm.get('dateFermeFrom');
  }

  get dateFermeTo() {
    return this.dateForm.get('dateFermeTo');
  }
  createForm() {
    this.dateForm = this.fb.group({
      dateFermeFrom: [new Date(Date.now() - 86400000*7), Validators.compose([Validators.required])],
      dateFermeTo: [new Date(), Validators.compose([Validators.required])],
    });
  }

  ngOnInit(): void {
    //this.store$.dispatch(LastSyncAppActions.fetchlastSync());
    this.store$.dispatch(StatusActions.fetchStatuss());

    this.createForm();
    this.status$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive),
        map((x) =>
          x.filter(
            (x) =>
              x.code == 'ECR' ||
              x.code == 'NONE' ||
              x.code == 'EAT' ||
              x.code == 'EATI' ||
              x.code == 'RCP' ||
              x.code == 'VAL' ||
              x.code == 'ESPC'
          )
        )
      )
      .subscribe((x) => {
        return (this.statuses = deepCopy(x));
      });

    this.lastSync$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => {
        if (x) {
          this.lastDateSync = new Date(x);
        }
      });
    this.store$.dispatch(ChauffeurActions.fetchChauffeurs());
    this.chauffeurs$.pipe().subscribe((x) => {
      this.chauffeurs = x;
    });
    this.store$.dispatch(EmetteurActions.fetchEmetteurs());
    this.emetteurs$.pipe().subscribe((x) => {
      this.emetteurs = x;
    });

    //this.store$.dispatch(JoursCaisseActions.fetchAllDataStatusEnAttente());
    this.rechercheEnveloppe();

    this.store$.dispatch(EtablissementActions.fetchEtablissements());
    this.etablissement$.pipe().subscribe((x) => {
      this.etablissements = x;
    });
    this.matchModeOptions = [
      { label: $localize`Début avec`, value: FilterMatchMode.STARTS_WITH },
      { label: $localize`Contient`, value: FilterMatchMode.CONTAINS },
    ];
    this.matchModeOptionsDate = [
      { label: $localize`Date supérieur`, value: FilterMatchMode.DATE_AFTER },
      { label: $localize`Date inférieur`, value: FilterMatchMode.DATE_BEFORE },
      { label: $localize`Date Egale`, value: FilterMatchMode.DATE_IS },
    ];
    //this.store$.dispatch(JoursCaisseActions.setData({data: this.data}));
    this.joursEnAttentes$
      .pipe(
        skipWhile((x) => x === null),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => {
        this.joursEnAttentes = deepCopy(x);
        this.joursEnAttentes.forEach((joursCaisses) => {
          joursCaisses.dateCreate = new Date(joursCaisses.dateCreate!);
          joursCaisses.dateComptage = new Date(joursCaisses.dateComptage!);
          joursCaisses.gjcDateferme = new Date(joursCaisses.gjcDateferme!);
          joursCaisses.receptionDate = new Date(joursCaisses.receptionDate!);
          joursCaisses.gjcDateouv = new Date(joursCaisses.gjcDateouv!);
          joursCaisses.validationDateDebut = new Date(
            joursCaisses.validationDateDebut!
          );
          joursCaisses.validationDateFin = new Date(
            joursCaisses.validationDateFin!
          );
          this.montantTot += joursCaisses?.montantCaisse!;
          // this.montantTot.toFixed(3)
          // joursCaisses?.montantCaisse != joursCaisses?.montantCaisse?.toFixed(3)
        });
      });

    this.searchControl.valueChanges
      .pipe(
        debounceTime(1000),
        map((query) => query?.toLowerCase())
      )
      .subscribe((query) => {
        this.store$.dispatch(
          setFilterBy({
            filters: {
              filterBy: [
                'gjcEtablissement.etLibelle',
                'gjcNumzcaisse',
                'gjcCaisse',
              ],
              query,
            },
          })
        );
      });
  }

  editProduct(joursCaise: JoursCaiseDTO) {
    this.joursEnAttente = { ...joursCaise };
    this.store$.dispatch(
      PiedecheActions.fetchPiedecheByJoursCaisse({
        id: joursCaise.id,
      })
    );
    this.piedeche$
      .pipe(
        skipWhile((x) => x === null),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => (this.piedeche = x));
  }

  handleFilter(
    filteredData: any[],
    filters: { [p: string]: FilterMetadata | FilterMetadata[] }
  ) {
    this.montantTot = 0;
    filteredData?.forEach((data) => {
      this.montantTot += data?.montantCaisse;
    });
  }

  clear(value: any) {}

  onchange(
    etEtablissement: any,
    gjcEtablissementLabel: string,
    equals: string
  ) {
    if (etEtablissement != null) {
      this.dt.filter(etEtablissement.etLibelle, gjcEtablissementLabel, equals);
    } else {
      this.dt.filter({}, gjcEtablissementLabel, equals);
    }
  }

  rechercheEnveloppe() {
    let dateFermeTo = this.datePipe.transform(
      this.dateFermeTo?.value || new Date(),
      'yyyyMMdd'
    );
    let dateFermeFrom = this.datePipe.transform(
      this.dateFermeFrom?.value || new Date(Date.now() - 86400000*7),
      'yyyyMMdd'
    );

    if (dateFermeTo !== null && dateFermeFrom !== null) {
      this.loading = true;
      this.montantTot = 0;

      this.joursCaisseService
        .chargerJoursCaisseFiltreEnveloppeEnAttentePost(
          dateFermeFrom,
          dateFermeTo
        )
        .pipe(map((x) => x))
        .subscribe((data) => {
          if (data != null) {
            this.store$.dispatch(JoursCaisseActions.setData({ data }));
            this.loading = false;
          }
        });
    }
  }

  exportxPdf() {
    const doc = new jsPDF();
    const columns = [
      [
        $localize`Etablissement`,
        $localize`Caisse`,
        $localize`Référence Z`,
        $localize`Date d'ouverture`,
        $localize`Heure d'ouverture`,
        $localize`Date de fermeture`,
        $localize`Heure de fermeture`,
        $localize`User Fermer`,
        $localize`Montant Total`,
      ],
    ];

    const data = [] as RowInput[];

    autoTable(doc, {
      head: columns,
      body: data,
      didDrawPage: (dataArg) => {
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.text(
          $localize`Jours Caisses`,
          dataArg.settings.margin.left,
          this.joursEnAttentes.length
        );
      },
    });
    doc.save('table.pdf');
  }

  getValueFilter(event: any): string {
    return event.target.value;
  }

  test() {
    this.store$.dispatch(BladeActions.openBlade());

    this.router.navigate(
      [
        './',
        {
          outlets: {
            blade: ['test'],
          },
        },
      ],
      { relativeTo: this.activatedRoute }
    );
  }

  getEmetteurLabel(emetteurId: any) {
    return <string>(
      this.emetteurs.find((x) => x.id === emetteurId)?.emetteurLibelle
    );
  }

  getChauffeurLabel(cahuffeurId: any) {
    return <string>(
      this.chauffeurs.find((x) => x.id === cahuffeurId)?.chauffeurLibelle
    );
  }

  deleteSelectedProducts() {
    // this.dialogService.open(recepDialog,{})
    this.receptionDialog = true;
  }

  hideDialog() {
    this.receptionDialog = false;
    this.submitted = false;
  }

  saveMp() {
    this.submitted = true;
    this.receptionDialog = false;

    this.confirmationService.confirm({
      message: $localize`Voulez vous confirmer la réception?`,
      header: $localize`Confirmer`,
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.montantTot = 0;
        this.store$.dispatch(
          JoursCaisseActions.receptionEnveloppes({
            enveloppes: this.selectedProducts,
            chauffeurId: this.selectedChauffeur?.id,
            emetteurId: this.selectedEmetteur?.id,
          })
        );
        this.selectedProducts = [];
        this.messageService.add({
          severity: 'success',
          summary: $localize`Succée`,
          detail: $localize`Réception terminé`,
          life: 3000,
        });
      },
    });

    /*if (this.reception.id) {
      this.reception.statusId=this.selectedStatus?.id;
      this.reception.cahuffeurId=this.selectedChauffeur?.id;
      this.reception.emetteurId=this.selectedEmetteur?.id;
      this.store$.dispatch(ReceptionActions.EditReception({reception: this.reception}));

    }*/
  }

  exportExcel() {
    this.exportService.exportExcelCustom(
      this.dt,
      this.selectedJourCaisses,
      this.selectedProducts,
      this.columns
    );
    if (this.selectedProducts) {
      this.changeStatus(this.selectedProducts);
    }
  }

  exportPdf() {
    this.exportService.exportPdfReception(
      this.dt,
      this.selectedJourCaisses,
      this.selectedProducts,
      this.columns /*, dateFrom, dateTo*/
    );
    if (this.selectedProducts) {
      this.changeStatus(this.selectedProducts);
    }
  }

  changeStatus(joursCaisse: JoursCaiseDTO[]) {
    this.montantTot = 0;
    this.store$.dispatch(JoursCaisseActions.changeStatus({ joursCaisse }));
  }

  checkForm() {
    if (
      this.selectedEmetteur === undefined ||
      this.selectedChauffeur === undefined
    ) {
      return true;
    } else {
      return false;
    }
  }

  onSelectionChange(event: any) {
    this.selectedProducts = event;
  }
}
