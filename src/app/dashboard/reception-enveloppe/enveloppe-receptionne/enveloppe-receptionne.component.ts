import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {
  ConfirmationService,
  FilterMatchMode,
  FilterMetadata,
  FilterService,
  MessageService,
  SelectItem,
} from 'primeng/api';
import { Table } from 'primeng/table';
import { debounceTime, Observable, skipWhile, takeWhile } from 'rxjs';
import {
  fromBlade,
  fromChauffeur,
  fromEmetteur,
  fromEtablissement,
  fromJoursCaisse,
  fromLastSyncApp,
  fromModePaiement,
  fromMotifresync,
  fromPiedeche,
  fromStatus,
} from '../../../shared/reducers';
import {
  ChauffeurDTO,
  EmetteurDTO,
  EtablissementDTO,
  JoursCaiseDTO,
  JoursCaisseService,
  MotifresyncDTO,
  PiedecheDTO,
  StatusDTO,
} from '../../../../../swagger-api';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import {
  ChauffeurActions,
  EmetteurActions,
  EtablissementActions,
  JoursCaisseActions,
  LastSyncAppActions,
  MotifresyncActions,
  PiedecheActions,
  StatusActions,
} from '../../../shared/actions';
import { map } from 'rxjs/operators';
import { setFilterBy } from '../../../shared/actions/joursCaisse.actions';
import { ExportService } from '../../../core/services/export.service';
import { DatePipe } from '@angular/common';
import { deepCopy } from '../../../shared/utils/deep-copy';
import { Calendar } from 'primeng/calendar';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { LocalService } from '../../../core/services/locale.service';

Calendar.prototype.getDateFormat = () => 'dd/mm/yy';

@Component({
  selector: 'app-enveloppe-receptionne',
  templateUrl: './enveloppe-receptionne.component.html',
  styles: [
    `
      :host ::ng-deep .p-dialog .product-image {
        width: 150px;
        margin: 0 auto 2rem auto;
        display: block;
      }
    `,
  ],
  providers: [ExportService, DatePipe],
  styleUrls: ['./enveloppe-receptionne.component.scss'],
})
export class EnveloppeReceptionneComponent implements OnInit, OnDestroy {
  @ViewChild('dt') dt: Table;
  headerRow: {
    displayName: string;
    key: string;
    hasSort: boolean;
    type: string;
  }[] = [
    {
      displayName: $localize`Etablissement`,
      key: 'gjcEtablissement.etLibelle',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Caisse`,
      key: 'gjcCaisse',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Référence Z`,
      key: 'gjcNumzcaisse',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Emetteur`,
      key: 'emetteurId',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Chauffeur`,
      key: 'cahuffeurId',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Date ouverture`,
      key: 'gjcDateouv',
      hasSort: true,
      type: 'date',
    },
    {
      displayName: $localize`Date de fermeture`,
      key: 'gjcDateferme',
      hasSort: true,
      type: 'date',
    },
    {
      displayName: $localize`Date de réception`,
      key: 'receptionDate',
      hasSort: true,
      type: 'date',
    },
    {
      displayName: $localize`Motif`,
      key: 'motifId',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Commentaire`,
      key: 'comments',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Statut`,
      key: 'statusReceptionObj.statuslibelle',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Ref externe`,
      key: 'refExterne',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Montant Total`,
      key: 'montantCaisse',
      hasSort: true,
      type: 'text',
    },
  ];
  montantTot: number = 0;
  show = false;
  _alive: boolean = true;
  selectedJourCaisses: JoursCaiseDTO[];

  joursReceptionnes$: Observable<any> = this.store$.select(
    fromJoursCaisse.selectData
  );
  loading$: Observable<any> = this.store$.select(fromJoursCaisse.selectLoading);
  piedeche$: Observable<PiedecheDTO[]> = this.store$.select(
    fromPiedeche.selectPiedeches
  );
  joursReceptionnes: JoursCaiseDTO[];
  piedeche: PiedecheDTO[];

  chauffeurs$: Observable<ChauffeurDTO[]> = this.store$.select(
    fromChauffeur.selectChauffeurs
  );
  emetteurs$: Observable<EmetteurDTO[]> = this.store$.select(
    fromEmetteur.selectEmetteurs
  );
  motifs$: Observable<MotifresyncDTO[]> = this.store$.select(
    fromMotifresync.selectMotifresyncs
  );
  chauffeurs: ChauffeurDTO[];
  emetteurs: EmetteurDTO[];

  joursReceptionne: JoursCaiseDTO;
  matchModeOptions: SelectItem[];
  matchModeOptionsDate: SelectItem[];
  etablissement$: Observable<EtablissementDTO[]> = this.store$.select(
    fromEtablissement.selectEtablissements
  );
  etablissements: EtablissementDTO[];

  lastDateSync: Date;
  dateForm: FormGroup;
  loading: boolean = false;
  lastSync$ = this.store$.select(fromLastSyncApp.getDateSync);

  etablissement: EtablissementDTO;
  searchControl = new FormControl('');
  columns = [
    { title: $localize`Etablissement`, dataKey: 'gjcEtablissementlabel' },
    { title: $localize`Période`, dataKey: 'periode' },
    { title: $localize`Nbre d'enveloppe`, dataKey: 'nbrEnv' },
    { title: $localize`Nom Emetteur`, dataKey: 'nomEm' },
    { title: $localize`Nom Chauffeur`, dataKey: 'nomChauf' },
    { title: $localize`Date Réception`, dataKey: 'receipDate' },
  ];
  decimalSeparator = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .decimalSeparator;
  digitsAfterDecimal = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .digitsAfterDecimal;

  receptionDialog: boolean;
  selectedProducts: JoursCaiseDTO[] = [];
  selectedChauffeur: ChauffeurDTO | undefined;
  selectedEmetteur: EmetteurDTO | undefined;
  private submitted: boolean;
  comments: string = '';
  selectedMotif: MotifresyncDTO | undefined;
  motifs: MotifresyncDTO[];
  currentLanguage = this.localStore.getData('language');
  status$: Observable<StatusDTO[]> = this.store$.select(
    fromStatus.selectStatuss
  );
  statuses: StatusDTO[];

  constructor(
    private localStore: LocalService,
    private store$: Store<
      fromBlade.State &
        fromChauffeur.State &
        fromEmetteur.State &
        fromMotifresync.State &
        fromJoursCaisse.State &
        fromPiedeche.State &
        fromEtablissement.State &
        fromModePaiement.State
    >,
    private filterService: FilterService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private toastService: NbToastrService,
    private joursCaisseService: JoursCaisseService,
    public snackBar: MatSnackBar,
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private exportService: ExportService,
    private datePipe: DatePipe
  ) {}

  ngOnDestroy(): void {
    this.store$.dispatch(JoursCaisseActions.resetDataTableStore());
    this._alive = false;
  }

  get dateFermeFrom() {
    return this.dateForm.get('dateFermeFrom');
  }

  get dateFermeTo() {
    return this.dateForm.get('dateFermeTo');
  }
  createForm() {
    this.dateForm = this.fb.group({
      dateFermeFrom: [new Date(Date.now() - 86400000*7), Validators.compose([Validators.required])],
      dateFermeTo: [new Date(), Validators.compose([Validators.required])],
    });
  }
  ngOnInit(): void {
    this.store$.dispatch(StatusActions.fetchStatuss());

    //this.store$.dispatch(LastSyncAppActions.fetchlastSync());
    this.createForm();
    this.status$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive),
        map((x) =>
          x.filter(
            (x) =>
              x.code == 'ECR' ||
              x.code == 'NONE' ||
              x.code == 'EAT' ||
              x.code == 'EATI' ||
              x.code == 'RCP' ||
              x.code == 'VAL' ||
              x.code == 'ESPC'
          )
        )
      )
      .subscribe((x) => {
        return (this.statuses = deepCopy(x));
      });
    this.lastSync$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => {
        if (x) {
          this.lastDateSync = new Date(x);
          if (this.dateFermeFrom) {
            //   this.store$.dispatch(JoursCaisseActions.fetchDataByDate({date: this.lastDateSync}));
          }
        }
      });
    //this.store$.dispatch(JoursCaisseActions.fetchAllDataStatusReceptionne());


    this.store$.dispatch(ChauffeurActions.fetchChauffeurs());
    this.chauffeurs$.pipe().subscribe((x) => {
      this.chauffeurs = x;
    });
    this.store$.dispatch(MotifresyncActions.fetchMotifresyncs());

    this.motifs$.pipe().subscribe((x) => {
      this.motifs = x;
    });
    this.store$.dispatch(EmetteurActions.fetchEmetteurs());
    this.emetteurs$.pipe().subscribe((x) => {
      this.emetteurs = x;
    });

    //this.store$.dispatch(LastSyncAppActions.fetchlastSync());

    //this.store$.dispatch(JoursCaisseActions.fetchAllDataStatusReceptionne());

    this.rechercheEnveloppe();


    this.store$.dispatch(EtablissementActions.fetchEtablissements());
    this.etablissement$.pipe().subscribe((x) => {
      this.etablissements = x;
    });
    this.matchModeOptions = [
      { label: $localize`Début avec`, value: FilterMatchMode.STARTS_WITH },
      { label: $localize`Contient`, value: FilterMatchMode.CONTAINS },
    ];
    this.matchModeOptionsDate = [
      { label: $localize`Date supérieur`, value: FilterMatchMode.DATE_AFTER },
      { label: $localize`Date inférieur`, value: FilterMatchMode.DATE_BEFORE },
      { label: $localize`Date Equals`, value: FilterMatchMode.DATE_IS },
    ];
    this.joursReceptionnes$
      .pipe(
        skipWhile((x) => x === null),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => {
        this.joursReceptionnes = deepCopy(x);
        this.joursReceptionnes.forEach((joursCaisses) => {
          joursCaisses.dateCreate = new Date(joursCaisses.dateCreate!);
          joursCaisses.dateComptage = new Date(joursCaisses.dateComptage!);
          joursCaisses.gjcDateferme = new Date(joursCaisses.gjcDateferme!);
          joursCaisses.receptionDate = new Date(joursCaisses.receptionDate!);
          joursCaisses.gjcDateouv = new Date(joursCaisses.gjcDateouv!);
          joursCaisses.validationDateDebut = new Date(
            joursCaisses.validationDateDebut!
          );
          joursCaisses.validationDateFin = new Date(
            joursCaisses.validationDateFin!
          );
          this.montantTot += joursCaisses?.montantCaisse!;
          // this.montantTot.toFixed(3)
        });
        this.show = false;
      });

    this.searchControl.valueChanges
      .pipe(
        debounceTime(1000),
        map((query) => query?.toLowerCase())
      )
      .subscribe((query) => {
        this.store$.dispatch(
          setFilterBy({
            filters: {
              filterBy: [
                'gjcEtablissement.etLibelle',
                'gjcNumzcaisse',
                'gjcCaisse',
              ],
              query: query,
            },
          })
        );
      });
  }
  editProduct(joursCaise: JoursCaiseDTO) {
    this.joursReceptionne = { ...joursCaise };
    this.store$.dispatch(
      PiedecheActions.fetchPiedecheByJoursCaisse({
        id: joursCaise.id,
      })
    );
    this.piedeche$
      .pipe(
        skipWhile((x) => x === null),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => (this.piedeche = x));
  }

  rechercheEnveloppe() {
    let dateFermeTo = this.datePipe.transform(
      this.dateFermeTo?.value || new Date(),
      'yyyyMMdd'
    );
    let dateFermeFrom = this.datePipe.transform(
      this.dateFermeFrom?.value || new Date(Date.now() - 86400000*7),
      'yyyyMMdd'
    );


    if (dateFermeTo !== null && dateFermeFrom !== null) {
      this.loading = true;
      this.montantTot = 0;
      this.joursCaisseService
        .chargerJoursCaisseFiltreEnveloppeReceptionnePost(
          dateFermeFrom,
          dateFermeTo
        )
        .pipe(map((x) => x))
        .subscribe((data) => {
          if (data != null) {
            this.store$.dispatch(JoursCaisseActions.setData({ data }));
            this.loading = false;
            //this.show=false;
          }
        });
    }
  }

  handleFilter(
    filteredData: any[],
    filters: { [p: string]: FilterMetadata | FilterMetadata[] }
  ) {
    this.montantTot = 0;
    filteredData?.forEach((data) => {
      this.montantTot += data?.montantCaisse;
    });
  }

  onchange(
    etEtablissement: any,
    gjcEtablissementLabel: string,
    equals: string
  ) {
    if (etEtablissement != null) {
      this.dt.filter(etEtablissement.etLibelle, gjcEtablissementLabel, equals);
    } else {
      this.dt.filter({}, gjcEtablissementLabel, equals);
    }
  }

  exportExcel() {
    this.exportService.exportExcel(
      this.dt,
      this.selectedJourCaisses,
      this.joursReceptionnes
    );
  }

  exportPdf() {
    let dateFrom: any;
    let dateTo: any;
    dateFrom = this.datePipe.transform(this.dateFermeFrom?.value, 'dd/MM/yyyy');
    dateTo = this.datePipe.transform(this.dateFermeTo?.value, 'dd/MM/yyyy');
    this.exportService.exportPdfReception(
      this.dt,
      this.selectedJourCaisses,
      this.joursReceptionnes,
      this.columns /*,dateFrom,dateTo*/
    );
  }

  getValueFilter(event: any): string {
    return event.target.value;
  }

  getEmetteurLabel(emetteurId: any) {
    return <string>(
      this.emetteurs.find((x) => x.id === emetteurId)?.emetteurLibelle
    );
  }

  getChauffeurLabel(cahuffeurId: any) {
    return <string>(
      this.chauffeurs.find((x) => x.id === cahuffeurId)?.chauffeurLibelle
    );
  }

  getMotifLabel(motifId: any) {
    return <string>(
      this.motifs.find((x) => x.id === motifId)?.motifReSyncDescription
    );
  }

  deleteSelectedProducts() {
    // this.dialogService.open(recepDialog,{})
    this.receptionDialog = true;
  }

  hideDialog() {
    this.receptionDialog = false;
    this.submitted = false;
  }

  saveMp() {
    let selectedFiltredProducts: JoursCaiseDTO[] = this.selectedProducts;
    // @ts-ignore
    selectedFiltredProducts = selectedFiltredProducts.filter(
      (x) => x.statusComtage === 2
    );

    let selectedToastProducts: JoursCaiseDTO[] = this.selectedProducts;
    selectedToastProducts = selectedToastProducts.filter(
      (x) => x.statusComtage === 4 || x.statusComtage === 3
    );

    if (
      selectedToastProducts.length === 1 &&
      this.selectedProducts.length === 1
    ) {
      if (selectedToastProducts[0].statusComtage === 4) {
        this.toastService.danger(
          $localize`Journée validés`,
          $localize`Erreur`,
          {
            duration: 3000,
            position: NbGlobalPhysicalPosition.BOTTOM_RIGHT,
          }
        );
      }
      if (selectedToastProducts[0].statusComtage === 3) {
        this.toastService.danger(
          $localize`Journée en cours de comptage`,
          $localize`Erreur`,
          {
            duration: 3000,
            position: NbGlobalPhysicalPosition.BOTTOM_RIGHT,
          }
        );
      }
    }

    this.show = true;
    this.montantTot = 0;
    this.store$.dispatch(
      JoursCaisseActions.receptionReSyncEnveloppes({
        enveloppes: selectedFiltredProducts,
        motifId: this.selectedMotif?.id,
        comments: this.comments,
      })
    );
    this.selectedProducts = [];
    this.receptionDialog = false;
    this.submitted = true;

    this.resetData();
  }

  onSelectionChange(event: any) {
    this.selectedProducts = event;
  }
  checkForm() {
    if (
      /*this.selectedEmetteur === undefined || this.selectedChauffeur === undefined ||*/ this
        .selectedMotif === undefined
    ) {
      return true;
    } else {
      return false;
    }
  }

  resetData() {
    /*
     this.selectedChauffeur= undefined;
     this.selectedEmetteur= undefined;
*/
    this.selectedMotif = undefined;
    this.comments = '';
  }
}
