import {LOCALE_ID, NgModule} from '@angular/core';
import {SharedModule} from 'src/app/shared/shared.module';

import {ReceptionEnveloppeRoutingModule} from './reception-enveloppe-routing.module';
import {DatePipe, registerLocaleData} from "@angular/common";

import localeFr from '@angular/common/locales/fr';
import {NbCardModule, NbDatepickerModule, NbInputModule, NbSelectModule} from "@nebular/theme";
import {RippleModule} from "primeng/ripple";
import {NbDateFnsDateModule} from "@nebular/date-fns";
import {NbMomentDateModule} from "@nebular/moment";
import {DatePickerModule} from "@progress/kendo-angular-dateinputs";
import {NgxLoaderModule} from "@tusharghoshbd/ngx-loader";

registerLocaleData(localeFr);
@NgModule({
  declarations: [
    ...ReceptionEnveloppeRoutingModule.components,
  ],
    imports: [
        SharedModule,
        ReceptionEnveloppeRoutingModule,
        NbCardModule,
        RippleModule,
        NbSelectModule,
        NbDatepickerModule,
        NbMomentDateModule,
        NbInputModule,
        NbDateFnsDateModule.forChild({format: 'dd-MM-yyyy'}),
        NbDateFnsDateModule.forRoot({
            format: 'dd-MM-yyyy',
            parseOptions: {useAdditionalWeekYearTokens: true, useAdditionalDayOfYearTokens: true},
            formatOptions: {useAdditionalWeekYearTokens: true, useAdditionalDayOfYearTokens: true},
        }),
        DatePickerModule,
        NgxLoaderModule
    ],
  providers: [DatePipe, {provide: LOCALE_ID, useValue: "fr-FR"}]
})
export class ReceptionEnveloppeModule { }
