import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ReceptionComponent} from "./reception/reception.component";
import {EnveloppeReceptionneComponent} from "./enveloppe-receptionne/enveloppe-receptionne.component";
import {EnveloppeNonReceptionneComponent} from "./enveloppe-non-receptionne/enveloppe-non-receptionne.component";

const routes: Routes = [
  {
    path: 'reception',
    component: ReceptionComponent
  },
  {
    path: 'enveloppe-receptionne',
    component: EnveloppeReceptionneComponent
  },
  {
    path: 'enveloppe-non-receptionne',
    component: EnveloppeNonReceptionneComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ReceptionEnveloppeRoutingModule {
  static components = [
    ReceptionComponent,
    EnveloppeReceptionneComponent,
    EnveloppeNonReceptionneComponent
  ];
}
