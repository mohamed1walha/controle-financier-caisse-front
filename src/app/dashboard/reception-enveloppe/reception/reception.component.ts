import {Component, OnInit} from '@angular/core';
import {PrimeNGConfig} from "primeng/api";
import {fromChauffeur, fromEmetteur, fromEtablissement, fromJoursCaisse} from "../../../shared/reducers";
import {Store} from "@ngrx/store";
import {ChauffeurDTO, EmetteurDTO, EtablissementDTO, StatusDTO} from "../../../../../swagger-api";
import {ChauffeurActions, EmetteurActions, EtablissementActions, JoursCaisseActions,} from "../../../shared/actions";
import {Observable} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-reception',
  templateUrl: './reception.component.html',
  styleUrls: ['./reception.component.scss']
})
export class ReceptionComponent implements OnInit {

  _alive = true;

  etablissement$: Observable<EtablissementDTO[]> = this.store$.select(fromEtablissement.selectEtablissements);
  chauffeurs$: Observable<ChauffeurDTO[]> = this.store$.select(fromChauffeur.selectChauffeurs);
  emetteurs$: Observable<EmetteurDTO[]> = this.store$.select(fromEmetteur.selectEmetteurs);
  // statusOfReception$: Observable<StatusDTO[]> = this.store$.select(fromReception.selectStatusOfReception);
  etablissements: EtablissementDTO[];
  numZs: number[];
  generateReception: any
  caisses: string[];
  selectedEtablissement: EtablissementDTO;
  selectedNumZ: number;
  selectedCaisse: string;
  selectedEtabCode: string;
  selectedEtabId: number;
  chauffeurs: ChauffeurDTO[];
  emetteurs: EmetteurDTO[];
  statusOfReception: StatusDTO[];
  selectedChauffeur:ChauffeurDTO;
  selectedEmetteur:EmetteurDTO;
  selectedStatus:any;
  showPanel: boolean=false;



  constructor(
    public snackBar: MatSnackBar,
    private store$: Store<fromEtablissement.State & fromChauffeur.State & fromEmetteur.State>,
    private primengConfig: PrimeNGConfig) { }

  ngOnInit(): void {
    this.store$.dispatch(EtablissementActions.fetchEtablissements());
    this.etablissement$.pipe().subscribe(x => {
      this.etablissements = x;
    });
    this.store$.dispatch(ChauffeurActions.fetchChauffeurs());
    this.chauffeurs$.pipe().subscribe(x => {
      this.chauffeurs = x;
    });
    this.store$.dispatch(EmetteurActions.fetchEmetteurs());
    this.emetteurs$.pipe().subscribe(x => {
      this.emetteurs = x;
    });
    // this.store$.dispatch(ReceptionActions.fetchStatusOfReceptions());
    // this.statusOfReception$.pipe().subscribe(x => {
    //   this.statusOfReception = x;
    // });
  }

  getNumZAndCaisse() {
    this.showPanel=false;
    this.generateReception={};
    if (this.selectedEtablissement?.id)
      this.selectedEtabId=this.selectedEtablissement.id;
    this.store$.dispatch(JoursCaisseActions.fetchNumZByEtablissement({etabId:this.selectedEtabId}));
    this.store$.select(fromJoursCaisse.selectNumZByEtablissement).pipe().subscribe(x => {
      this.numZs = x;
    });
    this.store$.dispatch(JoursCaisseActions.fetchCaisseByEtablissement({etabId:this.selectedEtabId}));
    this.store$.select(fromJoursCaisse.selectCaisseByEtablissement).pipe().subscribe(x => {
      this.caisses = x;
    });
  }

  generateEnveloppe() {
    this.selectedStatus = this.statusOfReception.find(x => x.code === "EAT");
    if (this.selectedEtablissement?.etEtablissement)
      this.selectedEtabCode = this.selectedEtablissement.etEtablissement;
    // this.store$.dispatch(ReceptionActions.fetchEnveloppe({numZ:this.selectedNumZ, codeEtab: this.selectedEtabCode, codeCaisse: this.selectedCaisse}));
    //  this.store$.select(fromReception.selectEnveloppe).pipe().subscribe(x => {
    //    this.generateReception = x;
    //    if(this.generateReception.etEtablissement!==undefined){
    //      this.showPanel=true;
    //      this.snackBar.open('Success !', 'Enveloppe Créer', {
    //        duration: 3000,
    //        horizontalPosition: 'end',
    //        verticalPosition: 'top'
    //      });
    //    }else{
    //      this.showPanel=false;
    //      this.generateReception={};
    //      this.snackBar.open('Failed !', 'Pas d"enveloppe', {
    //        duration: 3000,
    //        horizontalPosition: 'end',
    //        verticalPosition: 'top'
    //      });
    //    }
    //  });
  }

  createReception() {
    // const receptionE: ReceptionDTO={};
    // receptionE.statusId=this.selectedStatus?.id;
    // receptionE.jourcaisseId=this.generateReception?.jourcaisseId;
    // receptionE.cahuffeurId=this.selectedChauffeur?.id;
    // receptionE.emetteurId=this.selectedEmetteur?.id;
    // this.store$.dispatch(ReceptionActions.CreateReception({reception: receptionE}));
    // this.resetForm();
  }

  resetForm(){
    this.selectedStatus=null;
    this.selectedChauffeur={};
    this.selectedEmetteur={};
    this.generateReception={};
    this.showPanel=false;
  }
}
