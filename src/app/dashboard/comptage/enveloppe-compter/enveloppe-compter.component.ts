import { Component, OnInit, ViewChild } from '@angular/core';
import { Table } from 'primeng/table';
import {
  ChauffeurDTO,
  EmetteurDTO,
  EtablissementDTO,
  JoursCaiseDTO,
  JoursCaisseService,
  PiedecheDTO,
  StatusDTO,
} from '../../../../../swagger-api';
import { debounceTime, Observable, skipWhile, takeWhile } from 'rxjs';
import {
  fromBlade,
  fromChauffeur,
  fromEmetteur,
  fromEtablissement,
  fromJoursCaisse,
  fromModePaiement,
  fromPiedeche,
  fromStatus,
} from '../../../shared/reducers';
import {
  ConfirmationService,
  FilterMatchMode,
  FilterMetadata,
  FilterService,
  SelectItem,
} from 'primeng/api';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ExportService } from '../../../core/services/export.service';
import { DatePipe } from '@angular/common';
import {
  ChauffeurActions,
  EmetteurActions,
  EtablissementActions,
  JoursCaisseActions,
  LastSyncAppActions,
  PiedecheActions,
  StatusActions,
} from '../../../shared/actions';
import { deepCopy } from '../../../shared/utils/deep-copy';
import { map } from 'rxjs/operators';
import { setFilterBy } from '../../../shared/actions/joursCaisse.actions';
import { Calendar } from 'primeng/calendar';
import { LocalService } from '../../../core/services/locale.service';
import {MessagesService} from "../ComptageLigne/comptage-ligne/message.service";

Calendar.prototype.getDateFormat = () => 'dd/mm/yy';

@Component({
  selector: 'app-enveloppe-compter',
  templateUrl: './enveloppe-compter.component.html',
  styleUrls: ['./enveloppe-compter.component.scss'],
  styles: [
    `
      :host ::ng-deep .p-dialog .product-image {
        width: 150px;
        margin: 0 auto 2rem auto;
        display: block;
      }
    `,
  ],
  providers: [ExportService, DatePipe],
})
export class EnveloppeCompterComponent implements OnInit {
  @ViewChild('dt') dt: Table;
  headerRow: {
    displayName: string;
    key: string;
    hasSort: boolean;
    type: string;
  }[] = [
    {
      displayName: $localize`Etablissement`,
      key: 'gjcEtablissement.etLibelle',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Caisse`,
      key: 'gjcCaisse',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Référence Z`,
      key: 'gjcNumzcaisse',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Emetteur`,
      key: 'emetteurId',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Chauffeur`,
      key: 'cahuffeurId',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Date de reception`,
      key: 'receptionDate',
      hasSort: true,
      type: 'date',
    },
    {
      displayName: $localize`Date d'ouverture`,
      key: 'gjcDateouv',
      hasSort: true,
      type: 'date',
    },
    {
      displayName: $localize`Statut`,
      key: 'statusComptageObj.statuslibelle',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Ref externe`,
      key: 'refExterne',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Montant Total`,
      key: 'montantCaisse',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Ecart`,
      key: 'ecart',
      hasSort: false,
      type: 'text',
    },
  ];
  _alive: boolean = true;
  selectedJourCaisses: JoursCaiseDTO[];

  joursReceptionnes$: Observable<any> = this.store$.select(
    fromJoursCaisse.selectData
  );
  loading$: Observable<any> = this.store$.select(fromJoursCaisse.selectLoading);
  piedeche$: Observable<PiedecheDTO[]> = this.store$.select(
    fromPiedeche.selectPiedeches
  );
  status$: Observable<StatusDTO[]> = this.store$.select(
    fromStatus.selectStatuss
  );
  statuses: StatusDTO[];

  joursReceptionnes: JoursCaiseDTO[];
  piedeche: PiedecheDTO[];

  chauffeurs$: Observable<ChauffeurDTO[]> = this.store$.select(
    fromChauffeur.selectChauffeurs
  );
  emetteurs$: Observable<EmetteurDTO[]> = this.store$.select(
    fromEmetteur.selectEmetteurs
  );
  chauffeurs: ChauffeurDTO[];
  emetteurs: EmetteurDTO[];

  joursReceptionne: JoursCaiseDTO;
  matchModeOptions: SelectItem[];
  matchModeOptionsDate: SelectItem[];
  etablissement$: Observable<EtablissementDTO[]> = this.store$.select(
    fromEtablissement.selectEtablissements
  );
  etablissements: EtablissementDTO[];

  lastDateSync: string;
  dateForm: FormGroup;

  etablissement: EtablissementDTO;
  searchControl = new FormControl('');
  columns = [
    { title: $localize`Etablissement`, dataKey: 'gjcEtablissementlabel' },
    { title: $localize`Caisse`, dataKey: 'gjcCaisse' },
    { title: $localize`Référence Z`, dataKey: 'gjcNumzcaisse' },
    { title: $localize`Date d'ouverture`, dataKey: 'gjcDateouv' },
    { title: $localize`Heure d'ouverture`, dataKey: 'gjcHeureouv' },
    { title: $localize`Date de fermeture`, dataKey: 'gjcDateferme' },
    { title: $localize`Heure de fermeture`, dataKey: 'gjcHeureferme' },
    { title: $localize`Date creation`, dataKey: 'dateCreate' },
    { title: $localize`User Fermer`, dataKey: 'gjcUserferme' },
    { title: $localize`Statut`, dataKey: 'statusReceptionObj' },
    { title: $localize`Montant`, dataKey: 'montantCaisse' },
  ];
  sidebar$: Observable<boolean> = this.store$.select(
    fromPiedeche.selectSideBar
  );
  sidebar: boolean;
  montantTot: number = 0;
  currentLanguage = this.localStore.getData('language');
  loading: boolean = false;

  constructor(
    private store$: Store<
      fromBlade.State &
        fromChauffeur.State &
        fromEmetteur.State &
        fromJoursCaisse.State &
        fromPiedeche.State &
        fromEtablissement.State &
        fromModePaiement.State
    >,
    private filterService: FilterService,
    private localStore: LocalService,
    private confirmationService: ConfirmationService,
    private joursCaisseService: JoursCaisseService,
    public snackBar: MatSnackBar,
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private exportService: ExportService,
    private datePipe: DatePipe,
    private messageService: MessagesService
  ) {
    this.messageService.message$.subscribe(message => {
      if (message===true){
        this.rechercheEnveloppe();
      }
    });
  }

  createForm() {
    this.dateForm = this.fb.group({
      dateFermeFrom: [new Date(Date.now() - 86400000*7), Validators.compose([Validators.required])],
      dateFermeTo: [new Date(), Validators.compose([Validators.required])],
    });
  }


  ngOnDestroy(): void {
    this.store$.dispatch(JoursCaisseActions.resetDataTableStore());
    this._alive = false;
  }

  ngOnInit(): void {
    this.store$.dispatch(ChauffeurActions.fetchChauffeurs());
    this.store$.dispatch(StatusActions.fetchStatuss());

    this.chauffeurs$.pipe().subscribe((x) => {
      this.chauffeurs = x;
    });
    this.store$.dispatch(EmetteurActions.fetchEmetteurs());
    this.emetteurs$.pipe().subscribe((x) => {
      this.emetteurs = x;
    });

    //this.store$.dispatch(LastSyncAppActions.fetchlastSync());

/*
    this.store$.dispatch(JoursCaisseActions.fetchAllDataStatusCompter());
*/

    this.createForm();
    this.rechercheEnveloppe();


    this.store$.dispatch(EtablissementActions.fetchEtablissements());
    this.etablissement$.pipe().subscribe((x) => {
      this.etablissements = x;
    });
    this.matchModeOptions = [
      { label: $localize`Début avec`, value: FilterMatchMode.STARTS_WITH },
      { label: $localize`Contient`, value: FilterMatchMode.CONTAINS },
    ];
    this.matchModeOptionsDate = [
      { label: $localize`Date supérieur`, value: FilterMatchMode.DATE_AFTER },
      { label: $localize`Date inférieur`, value: FilterMatchMode.DATE_BEFORE },
      { label: $localize`Date Egale`, value: FilterMatchMode.DATE_IS },
    ];
    this.status$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive),
        map((x) =>
          x.filter(
            (x) =>
              x.code == 'ECR' ||
              x.code == 'NONE' ||
              x.code == 'EAT' ||
              x.code == 'EATI' ||
              x.code == 'RCP' ||
              x.code == 'VAL' ||
              x.code == 'ESPC'
          )
        )
      )
      .subscribe((x) => {
        return (this.statuses = deepCopy(x));
      });
    this.sidebar$
      .pipe(
        skipWhile((x) => x === null),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => (this.sidebar = x));
    this.joursReceptionnes$
      .pipe(
        skipWhile((x) => x === null),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => {
        this.joursReceptionnes = deepCopy(x);
        this.joursReceptionnes.forEach((joursCaisses) => {
          joursCaisses.dateCreate = new Date(joursCaisses.dateCreate!);
          joursCaisses.dateComptage = new Date(joursCaisses.dateComptage!);
          joursCaisses.gjcDateferme = new Date(joursCaisses.gjcDateferme!);
          joursCaisses.receptionDate = new Date(joursCaisses.receptionDate!);
          joursCaisses.gjcDateouv = new Date(joursCaisses.gjcDateouv!);
          joursCaisses.validationDateFin = new Date(
            joursCaisses.validationDateFin!
          );
          joursCaisses.validationDateDebut = new Date(
            joursCaisses.validationDateDebut!
          );
          this.montantTot += joursCaisses?.montantCaisse!;
          // this.montantTot.toFixed(3)
        });
      });

    this.searchControl.valueChanges
      .pipe(
        debounceTime(1000),
        map((query) => query?.toLowerCase())
      )
      .subscribe((query) => {
        this.store$.dispatch(
          setFilterBy({
            filters: {
              filterBy: [
                'gjcEtablissement.etLibelle',
                'gjcNumzcaisse',
                'gjcCaisse',
              ],
              query,
            },
          })
        );
      });
  }

  editProduct(joursCaise: JoursCaiseDTO) {
    this.joursReceptionne = { ...joursCaise };
    this.store$.dispatch(
      PiedecheActions.fetchPiedecheByJoursCaisse({
        id: joursCaise.id,
      })
    );
    this.piedeche$
      .pipe(
        skipWhile((x) => x === null),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => (this.piedeche = x));
  }

  handleFilter(
    filteredData: any[],
    filters: { [p: string]: FilterMetadata | FilterMetadata[] }
  ) {
    this.montantTot = 0;
    filteredData?.forEach((data) => {
      this.montantTot += data?.montantCaisse;
    });
  }

  onchange(
    etEtablissement: any,
    gjcEtablissementLabel: string,
    equals: string
  ) {
    if (etEtablissement != null) {
      this.dt.filter(etEtablissement.etLibelle, gjcEtablissementLabel, equals);
    } else {
      this.dt.filter({}, gjcEtablissementLabel, equals);
    }
  }

  exportExcel() {
    this.exportService.exportExcel(
      this.dt,
      this.selectedJourCaisses,
      this.joursReceptionnes
    );
  }

  exportPdf() {
    this.exportService.exportPdf(
      this.dt,
      this.selectedJourCaisses,
      this.joursReceptionnes,
      this.columns
    );
  }

  getValueFilter(event: any): string {
    return event.target.value;
  }

  getEmetteurLabel(emetteurId: any) {
    return <string>(
      this.emetteurs.find((x) => x.id === emetteurId)?.emetteurLibelle
    );
  }

  getChauffeurLabel(cahuffeurId: any) {
    return <string>(
      this.chauffeurs.find((x) => x.id === cahuffeurId)?.chauffeurLibelle
    );
  }

  compter(cmpt: any) {
    this.router.navigate(
      [
        './',
        {
          outlets: {
            blade: ['view'],
          },
        },
      ],
      { relativeTo: this.activatedRoute }
    );
    this.store$.dispatch(
      JoursCaisseActions.setJoursCaisse({ joursCaisse: cmpt })
    );
  }


  rechercheEnveloppe() {
    let dateFermeTo = this.datePipe.transform(
      this.dateFermeTo?.value || new Date(),
      'yyyyMMdd'
    );
    let dateFermeFrom = this.datePipe.transform(
      this.dateFermeFrom?.value || new Date(Date.now() - 86400000*7),
      'yyyyMMdd'
    );

    if (dateFermeTo !== null && dateFermeFrom !== null) {
      this.loading = true;
      this.montantTot = 0;

      this.joursCaisseService
        .joursCaisseFiltreGetAllByStatusCompterPost(
          dateFermeFrom,
          dateFermeTo
        )
        .pipe(map((x) => x))
        .subscribe((data) => {
          if (data != null) {
            this.store$.dispatch(JoursCaisseActions.setData({ data }));
            this.loading = false;
          }
        });
    }
  }

  get dateFermeFrom() {
    return this.dateForm.get('dateFermeFrom');
  }

  get dateFermeTo() {
    return this.dateForm.get('dateFermeTo');
  }


}
