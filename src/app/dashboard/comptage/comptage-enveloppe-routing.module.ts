import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EnveloppeCompterComponent } from './enveloppe-compter/enveloppe-compter.component';
import { EnveloppeNonCompterComponent } from './enveloppe-non-compter/enveloppe-non-compter.component';
import { ComptageLigneComponent } from './ComptageLigne/comptage-ligne/comptage-ligne.component';
import { RapportComponent } from './rapport/rapport.component';
import { RapportBilletsComponent } from './rapport-billets/rapport-billets.component';

const routes: Routes = [
  {
    path: 'enveloppe-compter',
    children: [
      {
        path: '',
        component: EnveloppeCompterComponent,
      },
      {
        path: 'view',
        outlet: 'blade',
        component: ComptageLigneComponent,
      },
    ],
  },
  {
    path: 'rapport',
    children: [
      {
        path: '',
        component: RapportComponent,
      },
    ],
  },
  {
    path: 'billets',
    children: [
      {
        path: '',
        component: RapportBilletsComponent,
      },
    ],
  },
  {
    path: 'enveloppe-non-compter',
    children: [
      {
        path: '',
        component: EnveloppeNonCompterComponent,
      },
      {
        path: 'create/:id',
        outlet: 'blade',
        component: ComptageLigneComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ComptageEnveloppeRoutingModule {
  static components = [
    EnveloppeCompterComponent,
    EnveloppeNonCompterComponent,
    ComptageLigneComponent,
    RapportComponent,
    RapportBilletsComponent,
  ];
}
