import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  private messageSource = new Subject<boolean>();
  message$ = this.messageSource.asObservable();

  constructor() { }

  sendMessage(message: boolean) {
    this.messageSource.next(message);
  }
}
