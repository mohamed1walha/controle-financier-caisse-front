import {
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Store } from '@ngrx/store';
import {
  fromBlade,
  fromEnteteDepense,
  fromEntetePrelevement,
  fromJoursCaisse,
  fromModePaiement,
  fromMotif,
  fromPiedeche,
  fromStatus,
} from '../../../../shared/reducers';
import { ConfirmationService, MessageService } from 'primeng/api';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NotEmptyPipe } from '../../../../shared/utils/noEmpty';
import {
  BilletDetailComptageDTO,
  BilletDetailComptageService,
  EnteteDepenseDTO,
  EntetePrelevementDTO,
  JoursCaiseDTO,
  ModepaieDTO,
  MotifDTO,
  PiedecheDTO,
  StatusDTO,
} from '../../../../../../swagger-api';
import { Observable, skipWhile, takeWhile } from 'rxjs';
import {
  BladeActions,
  EnteteDepenseActions,
  EntetePrelevementActions,
  JoursCaisseActions,
  ModePaiementActions,
  MotifActions,
  PiedecheActions,
  StatusActions,
} from '../../../../shared/actions';
import { EditableRow, Table } from 'primeng/table';
import { deepCopy } from '../../../../shared/utils/deep-copy';
import { map, take } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import {
  NbDialogService,
  NbGlobalPhysicalPosition,
  NbToastrService,
} from '@nebular/theme';
import { SaveEditableRow } from 'primeng/table/table';
import { DialogCalculatePromptComponent } from '../../../../shared/components/dialog-calculate-prompt/dialog-calculate-prompt.component';
import { LocalService } from '../../../../core/services/locale.service';
import { SeparateurPipe } from '../../../../shared/utils/separateur.pipe';
import {MessagesService} from "./message.service";

@Component({
  selector: 'app-comptage-ligne',
  templateUrl: './comptage-ligne.component.html',
  providers: [EditableRow],
  styleUrls: ['./comptage-ligne.component.scss'],
})
export class ComptageLigneComponent
  implements OnInit, OnDestroy, AfterViewInit
{
  @ViewChild('table') table: ElementRef;
  depenseMontant: number = 0;
  comptageForm: FormGroup;
  piedecheForm: FormGroup;
  selectedComptage: JoursCaiseDTO;
  piedeches$: Observable<PiedecheDTO[]> = this.store$.select(
    fromPiedeche.selectPiedeches
  );
  piedeches: PiedecheDTO[] = [];
  piedeche$: Observable<PiedecheDTO> = this.store$.select(
    fromPiedeche.selectPiedeche
  );
  selectedComptage$: Observable<JoursCaiseDTO> = this.store$.select(
    fromJoursCaisse.selectJoursCaisse
  );
  piedecheDTO: PiedecheDTO;
  status$: Observable<StatusDTO[]> = this.store$.select(
    fromStatus.selectStatuss
  );
  statusJourCaisse$: Observable<StatusDTO> = this.store$.select(
    fromStatus.selectStatus
  );
  entetePrelevement$: Observable<EntetePrelevementDTO[]> = this.store$.select(
    fromEntetePrelevement.selectEntetePrelevements
  );
  enteteDepense$: Observable<EnteteDepenseDTO[]> = this.store$.select(
    fromEnteteDepense.selectEnteteDepenses
  );
  isLoading$: Observable<boolean> = this.store$.select(
    fromJoursCaisse.selectIsLoading
  );
  _alive: boolean = true;
  compter: boolean = false;
  isDepenseValue: boolean = false;
  @ViewChild('dtpiedeche') dtpiedeche: Table;
  modePaiements$ = this.store$.select(fromModePaiement.selectTableData);
  mps: ModepaieDTO[];
  motifs$ = this.store$.select(fromMotif.selectMotifs);
  dateSys: Date = new Date(Date.now());
  motifs: MotifDTO[];
  statuss: StatusDTO[];
  statusJourCaisse: StatusDTO;
  sidebar$: Observable<boolean> = this.store$.select(
    fromPiedeche.selectSideBar
  );
  digitsAfterDecimal = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .digitsAfterDecimal;
  loading: boolean;
  motifVide: MotifDTO = { id: 0, libelle: 'Aucun' };
  currentUser = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .currentUser;
  piedeche: PiedecheDTO={};
  piedechePrev: PiedecheDTO;
  jourcaisseParamsID: number;
  totMontant: number = 0;
  totMontantCompter: number = 0;
  totMontantDepence: number = 0;
  totMontantEspece: number = 0;
  totMontantVerse: number = 0;
  totMontantTotal: number = 0;
  totMontantEcart: number = 0;
  value: number;
  statusComptageObj: string | null | undefined = '';
  dateV: Date | null | undefined;
  saveEditableRow: SaveEditableRow;
  isSource: boolean = false;
  input: boolean = false;
  calculette: any;
  showMontant: boolean = false;
  AllCalculetteByPiedechIdGet: BilletDetailComptageDTO[] = [];

  clonedCars: { [s: string]: PiedecheDTO } = {};
  currentLanguage = this.localStore.getData('language');
  constructor(
    private store$: Store<
      fromPiedeche.State &
        fromBlade.State &
        fromModePaiement.State &
        fromStatus.State &
        fromMotif.State &
        fromJoursCaisse.State
    >,
    private numberPipe: SeparateurPipe,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private BilletDetailComptageService: BilletDetailComptageService,
    private fb: FormBuilder,
    private dialogService: NbDialogService,
    private noEmpty: NotEmptyPipe,
    private toastService: NbToastrService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public editableRow: EditableRow,
    private localStore: LocalService,
    private messagesService: MessagesService
  ) {}

  get id() {
    return this.piedecheForm.get('id')?.value;
  }

  set id(value) {
    this.piedecheForm.get('value')?.setValue(value);
  }

  get dateCreation() {
    return this.piedecheForm.get('dateCreation');
  }

  get montantCompte() {
    return this.piedecheForm.get('montantCompte');
  }

  set montantCompte(value) {
    this.piedecheForm.get('montantCompte')?.setValue(value);
  }

  get especeVerse() {
    return this.piedecheForm.get('especeVerse');
  }

  get especeCash() {
    return this.piedecheForm.get('especeCash');
  }

  get ecart() {
    return this.piedecheForm.get('ecart');
  }

  get source() {
    return this.piedecheForm.get('source');
  }

  get commentaire() {
    return this.piedecheForm.get('commentaire');
  }

  get montant() {
    return this.piedecheForm.get('montant');
  }

  get motif() {
    return this.piedecheForm.get('motif');
  }

  get depence() {
    return this.piedecheForm.get('depence');
  }

  get status() {
    return this.piedecheForm.get('status');
  }

  get jourcaisseId() {
    return this.piedecheForm.get('jourcaisseId');
  }

  get montantTotal() {
    return this.piedecheForm.get('montantTotal');
  }

  set montantTotal(value: any) {
    this.piedecheForm.get('montantTotal')?.setValue(value);
  }

  get agentComptageP() {
    return this.piedecheForm?.get('agentComptage');
  }

  set agentComptageP(value: any) {
    this.piedecheForm.get('agentComptage')?.setValue(value);
  }

  get modepaie() {
    return this.piedecheForm.get('modepaie');
  }

  get f() {
    return this.piedecheForm.controls;
  }

  get statusComtage() {
    return this.comptageForm?.get('statusComtage');
  }

  set statusComtage(value) {
    this.comptageForm?.get('statusComtage')?.setValue(value);
  }

  get commentaireJC() {
    return this.comptageForm?.get('commentaireJC');
  }

  get receptionDate() {
    return this.comptageForm?.get('receptionDate');
  }

  get gjcEtablissementlabel() {
    return this.comptageForm?.get('gjcEtablissementlabel');
  }

  get gjcCaisse() {
    return this.comptageForm?.get('gjcCaisse');
  }

  get idJourcaisse() {
    return this.comptageForm?.get('id');
  }

  get gjcUserferme() {
    return this.comptageForm?.get('gjcUserferme');
  }

  get agentComptage() {
    return this.comptageForm?.get('agentComptage');
  }

  get gjcEtablissementId() {
    return this.comptageForm?.get('gjcEtablissementId');
  }

  get validationDateDebut() {
    return this.comptageForm?.get('validationDateDebut');
  }

  get validationDateFin() {
    return this.comptageForm?.get('validationDateFin');
  }

  get statusComtageLibelle() {
    return this.comptageForm?.get('statusComtageLibelle');
  }

  set statusComtageLibelle(value) {
    this.comptageForm?.get('statusComtageLibelle')?.setValue(value);
  }

  get emetteurlibelle() {
    return this.comptageForm?.get('emetteurlibelle');
  }

  get chauffeurlibelle() {
    return this.comptageForm?.get('chauffeurlibelle');
  }

  get montantCaisse() {
    return this.comptageForm?.get('montantCaisse');
  }

  get gjcNumzcaisse() {
    return this.comptageForm?.get('gjcNumzcaisse');
  }

  ngAfterViewInit() {
    // this.cdref.detectChanges();
    this.activatedRoute.params
      .pipe(
        takeWhile(() => this._alive),
        take(1)
      )
      .subscribe((params) => {
        if (params['id']) {
          this.store$.dispatch(
            JoursCaisseActions.fetchByIdParams({ id: params['id'] })
          );

          this.store$.dispatch(StatusActions.fetchStatus({ id: params['id'] }));

          this.jourcaisseParamsID == params['id'];
        }
      });
  }

  creatPiedecheForm() {
    this.piedecheForm = this.fb.group({
      id: [''],
      montantCompte: ['', Validators.required],
      especeVerse: [''],
      especeCash: [''],
      montantTotal: [''],
      ecart: [''],
      montant: [''],
      commentaire: [''],
      modepaie: ['', Validators.required],
      status: ['', Validators.required],
      motif: [''],
      //motif: ['',this.ecart?.value==null?Validators.required:Validators.nullValidator],
      depence: [''],
      jourcaisseId: [''],
      dateCreation: [''],
      source: [''],
      agentComptage: [''],
    });
  }

  ngOnInit(): void {
    setTimeout(() => {
      this.compter = document.URL.includes('view');
      this.store$.dispatch(ModePaiementActions.fetchData());
      this.store$.dispatch(StatusActions.fetchStatuss());
      console.log('currentLanguage ' + this.currentLanguage);
      this.isLoading$
        .pipe(
          skipWhile((x) => x === null || x === undefined),
          takeWhile(() => this._alive)
        )
        .subscribe((x) => {
          if (!x) {
            // this.store$.dispatch(PiedecheActions.fetchPiedecheByJoursCaisse({id: this.jourcaisseParamsID}));
          }
          return (this.loading = x);
        });
      this.entetePrelevement$
        .pipe(
          skipWhile((x) => x.length === 0 || x === undefined),
          takeWhile(() => this._alive),
          take(1)
        )
        .subscribe((x) => {
          if (x) {
            x.forEach((entete) => {
              this.depenseMontant += entete?.montantTotal!;
            });
          }
        });
      this.enteteDepense$
        .pipe(
          skipWhile((x) => x.length === 0 || x === undefined),
          takeWhile(() => this._alive),
          take(1)
        )
        .subscribe((x) => {
          if (x) {
            x.forEach((entete) => {
              this.depenseMontant += entete?.totalTtc!;
            });
          }
        });
      this.modePaiements$
        .pipe(
          skipWhile((x) => x === null || x === undefined),
          takeWhile(() => this._alive)
        )
        .subscribe((x) => (this.mps = x));
      this.store$.dispatch(MotifActions.fetchMotifs());
      this.motifs$
        .pipe(
          skipWhile((x) => x === null || x === undefined),
          takeWhile(() => this._alive)
        )
        .subscribe((x) => {
          this.motifs = [];
          this.motifs.push(this.motifVide);
          this.motifs.push(...deepCopy(x));
        });
      this.digitsAfterDecimal = this.digitsAfterDecimal
        ? this.digitsAfterDecimal
        : 2;
      this.status$
        .pipe(
          skipWhile((x) => x === null || x === undefined),
          takeWhile(() => this._alive),
          map((x) => x.filter((x) => x.code == 'VAL' || x.code == 'ECR'))
        )
        .subscribe((x) => {
          return (this.statuss = deepCopy(x));
        });
      this.selectedComptage$
        .pipe(
          skipWhile((x) => x === null || x.id === undefined),
          takeWhile(() => this._alive)
        )
        .subscribe((x) => {
          //  console.log('selectedComptage$ ' + x?.id);

          this.selectedComptage = deepCopy(x);
          this.store$.dispatch(
            EnteteDepenseActions.fetchAllEnteteDepensesWithEtabAndNumZRefAndcaisseId(
              {
                etabId: this.selectedComptage?.gjcEtablissementId!,
                numZRef: this.selectedComptage?.gjcNumzcaisse!,
                caisseId: this.selectedComptage?.gjcCaisse!,
              }
            )
          );
          this.store$.dispatch(
            EntetePrelevementActions.fetchAllEntetePrelevementsWithEtabAndNumZRefAndcaisseId(
              {
                etabId: this.selectedComptage?.gjcEtablissementId!,
                numZRef: this.selectedComptage?.gjcNumzcaisse!,
                caisseId: this.selectedComptage?.gjcCaisse!,
              }
            )
          );
          if (this.selectedComptage) {
            this.dateV = new Date(this.selectedComptage?.validationDateDebut!);
            if (this.selectedComptage?.validationDateFin) {
              this.validationDateFin?.setValue(
                new Date(this.selectedComptage.validationDateFin)
              );
            }
            if (this.selectedComptage) {
              this.comptageForm?.patchValue({
                ...this.selectedComptage,
                emetteurlibelle:
                  this.selectedComptage.emetteur?.emetteurLibelle,
                chauffeurlibelle:
                  this.selectedComptage.chauffeur?.chauffeurLibelle,
              });
              const datee = new Date(
                this.selectedComptage?.validationDateDebut!
              );
              if (
                this.selectedComptage &&
                (datee.getFullYear() !== undefined ||
                  datee.getFullYear() !== 1970)
              ) {
                this.dateV = new Date(this.validationDateDebut?.value);
              }
            }
          }
        });
      //this.store$.dispatch(JoursCaisseActions.fetchById({id:this.selectedComptage?.id}));

      this.createForm();
      this.creatPiedecheForm();
      this.store$.dispatch(BladeActions.openBlade());
      this.store$.dispatch(BladeActions.setBladeWidth({ width: 95 }));

      this.statusJourCaisse$
        .pipe(
          skipWhile((x) => x === null || x.id === undefined),
          takeWhile(() => this._alive)
        )
        .subscribe((x) => {
          //  console.log("statusJourCaisse$ " + x?.id)
          if (x) {
            this.statusComptageObj = x?.code;
            this.statusComtage?.setValue(x?.id);
            this.statusComtageLibelle?.setValue(
              this.currentLanguage === 'fr'
                ? x?.statuslibelle
                : x?.statuslibelleen
            );
            return (this.statusJourCaisse = deepCopy(x));
          }
        });

      this.piedeches$
        .pipe(
          skipWhile((x) => x === null || x === undefined),
          takeWhile(() => this._alive)
        )
        .subscribe((x) => {
          this.piedeches = deepCopy(x);
        });
      this.piedeche$
        .pipe(
          skipWhile((x) => x === null || x === undefined),
          takeWhile(() => this._alive)
        )
        .subscribe((x) => {
          this.piedeche = deepCopy(x);
          this.dtpiedeche?.value.push(this.piedeche);
        });
    });
  }

  createForm() {
    this.comptageForm = this.fb.group({
      id: [''],
      gjcNumzcaisse: [''],
      gjcCaisse: [''],
      gjcHeureouv: [''],
      gjcDateferme: [''],
      gjcHeureferme: [''],
      gjcUserferme: [''],
      gjcEtat: [''],
      gjcDateouv: [''],
      gjcEtablissementcode: [''],
      gjcEtablissementlabel: [''],
      montantCaisse: [''],
      gjcEtablissementId: [''],
      receptionDate: [''],
      validationDateDebut: [''],
      validationDateFin: [''],
      emetteurId: [''],
      cahuffeurId: [''],
      statusReception: [''],
      dateComptage: [''],
      agentComptage: [''],
      commentaireJC: [''],
      commentaire: [''],
      statusComtage: [''],
      dateCreate: [''],
      gjcEtablissement: [],
      statusReceptionObj: [],
      statusComptageObj: [],
      chauffeurlibelle: [''],
      emetteurlibelle: [''],
      statusComtageLibelle: [''],
      status: [''],
    });
  }

  onRowEditInit(event, piedeche: PiedecheDTO, ri: any) {
    this.BilletDetailComptageService.billetDetailComptageGetAllByPiedechIdGet(
      piedeche.id
    ).subscribe((allCalculettes) => {
      this.AllCalculetteByPiedechIdGet = allCalculettes;
    });
    this.clonedCars[piedeche.id!] = { ...piedeche };

    if (piedeche.source == 'CEGID') {
      this.isSource = true;
    }

    this.piedechePrev = deepCopy(piedeche);
  }

  convertCalculetteExp(calculette: any) {
    let billetDetailComptageDTO: BilletDetailComptageDTO[] = [];
    if (calculette?.items.length > 0) {
      calculette.items.forEach((ligne) => {
        let billet: BilletDetailComptageDTO = {
          billetId: ligne.billetId,
          quantite: Number(ligne.quantite),
          montant: ligne.montant,
          total: Number(ligne.quantite) * ligne.montant,
          piedecheId: 0,
        };
        billetDetailComptageDTO.push(billet);
      });
    }
    return billetDetailComptageDTO;
  }

  onRowEditSave(piedeche: PiedecheDTO, ri: any, editing: any) {
    // editableRow: new EditableRow(this.table);

    // saveEditableRow: new SaveEditableRow(this.dtpiedeche, this.editableRow);

    if (
      piedeche.status == null &&
      piedeche.modepaie == null &&
      piedeche.motif == null &&
      piedeche.montantCompte == null &&
      piedeche.especeCash == null &&
      piedeche.depence == null &&
      piedeche.especeVerse == null &&
      piedeche.montant == null &&
      piedeche.status == null &&
      piedeche.commentaire == null &&
      piedeche.montantTotal == null &&
      piedeche.jourcaisseId == null &&
      piedeche.id == null &&
      piedeche.ecart == null
    ) {
      this.dtpiedeche.value.splice(ri, 1);
    }

    if (piedeche.motif?.id == 0) {
      this.motif?.setValue(null);
    }
    this.piedecheForm.patchValue({ ...piedeche });
    if (
      this.ecart?.value != null &&
      this.ecart?.value != 0 &&
      this.motif?.value == null
    ) {
      this.piedecheForm.controls['motif'].addValidators([Validators.required]);
      this.piedecheForm.controls['motif'].updateValueAndValidity();
    }
    if (this.montantTotal?.value == null || this.montantTotal?.value == '') {
      this.piedecheForm.controls['montantTotal'].addValidators([
        Validators.required,
      ]);
      this.piedecheForm.controls['montantTotal'].updateValueAndValidity();
    }
    if (this.piedecheForm.valid) {
      if (this.id == 0) {
        this.jourcaisseId?.setValue(this.selectedComptage.id);
        this.dateCreation?.setValue(new Date(Date.now()));
        this.source?.setValue('APP');
      }
      this.agentComptageP = this.currentUser;
      if (
        this.ecart?.value != null &&
        this.ecart?.value != 0 &&
        (this.motif?.value == null || this.motif?.value?.id == 0)
      ) {
        this.toastService.warning(
          $localize`S'il vous plait choisi un motif`,
          $localize`Erreur`,
          {
            duration: 3000,
            position: NbGlobalPhysicalPosition.BOTTOM_RIGHT,
          }
        );
      } else {
        let piedeche: PiedecheDTO = this.piedecheForm.value as PiedecheDTO;
        if (piedeche?.motif?.id === 0) {
          piedeche.motifId = null;
          piedeche.motif = {};
        }
        piedeche.billetDetailComptageDTO = this.convertCalculetteExp(
          this.calculette
        );
        this.store$.dispatch(PiedecheActions.updatepiedeche({ piedeche }));
        this.store$.dispatch(JoursCaisseActions.fetchById());

        this.piedecheForm.controls['motif'].clearValidators();
        this.piedecheForm.controls['motif'].updateValueAndValidity();

        // this.editableRow.el=this.table ;
        // this.editableRow.data=piedeche ;
        // this.editableRow.pEditableRowDisabled=false;
        if (this.piedeche && piedeche.id != undefined) {
          this.dtpiedeche.cancelRowEdit(piedeche);

          // this.dtpiedeche.value.push(...this.piedeches);
          this.piedeche = {};
          this.calculette = null;
          this.showMontant = false;
        }
      }
    } else {
      if (this.piedecheForm.invalid) {
        this.toastService.danger(
          $localize`S'il vous plait Verifier les champs`,
          $localize`Erreur`,
          {
            duration: 3000,
            position: NbGlobalPhysicalPosition.BOTTOM_RIGHT,
          }
        );
      }
    }
  }

  onRowEditCancel(piedeche: any, ri: any) {
    this.AllCalculetteByPiedechIdGet = [];
    if (piedeche?.id == null) {
      this.dtpiedeche.value.splice(ri, 1);
    } else {
      this.dtpiedeche.value.splice(ri, 1);
      this.dtpiedeche.value.push(this.piedechePrev);
    }
  }

  trackByFn(index, row) {
    return index;
  }

  ngOnDestroy() {
    this.piedeche={}
    this.depenseMontant=0
    this.store$.dispatch(
      EntetePrelevementActions.setEntetePrelevements({ entetePrelevements: [] })
    );
    this.store$.dispatch(
      EnteteDepenseActions.setEnteteDepenses({ enteteDepenses: [] })
    );
    this.store$.dispatch(BladeActions.closeBlade());
    console.log(this.compter)
    if (!this.compter) {
      this.messagesService.sendMessage(true);

      /*this.store$.dispatch(
        JoursCaisseActions.fetchAllDataStatusReceptionneNonValide()
      );*/
    } else {
      this.messagesService.sendMessage(true);

     // this.store$.dispatch(JoursCaisseActions.fetchAllDataStatusCompter());
    }
  }

  onclose() {

    this.store$.dispatch(BladeActions.closeBlade());
  }

  onchangeMontantTotal(event: any, piedeche: any, ri: any, champ: string) {
    let x = 0;
    switch (champ) {
      case 'depence':
        x =
          Number(piedeche?.montantCompte) +
          Number(piedeche?.especeCash) +
          Number(piedeche?.especeVerse) +
          Number(event);

        break;

      case 'montantCompte':
        x =
          Number(event) +
          Number(piedeche?.especeCash) +
          Number(piedeche?.especeVerse) +
          Number(piedeche?.depence);

        break;
      case 'especeCash':
        x =
          Number(piedeche?.montantCompte) +
          Number(event) +
          Number(piedeche?.especeVerse) +
          Number(piedeche?.depence);
        break;
      case 'especeVerse':
        x =
          Number(piedeche?.montantCompte) +
          Number(piedeche?.especeCash) +
          Number(event) +
          Number(piedeche?.depence);
        break;
    }
    // this.dtpiedeche.value[ri].montantTotal = this.precise(x, Math.trunc(x).toString().length + 2);
    // this.dtpiedeche.value[ri].ecart = this.precise(this.precise(piedeche.montant, Math.trunc(piedeche.montant).toString().length + 2) - piedeche.montantTotal,
    //   Math.trunc(piedeche.montant - piedeche.montantTotal).toString().length + 2);
    //

    this.dtpiedeche.value[ri].montantTotal = this.numberPipe.transform(x);
    this.dtpiedeche.value[ri].ecart = this.precise(
      this.precise(
        piedeche.montant,
        Math.trunc(piedeche.montant).toString().length + 3
      ) - piedeche.montantTotal,
      Math.trunc(piedeche.montant - piedeche.montantTotal).toString().length + 3
    );
    //this.dtpiedeche.value[ri].ecart = this.dtpiedeche.value[ri].ecart
    //   this.dtpiedeche.value[ri].montantTotal=Math.round(this.dtpiedeche.value[ri].montantTotal * 1000) / 1000
    // console.log(this.dtpiedeche.value[ri].ecart);
    const xx = Number(this.dtpiedeche.value[ri].ecart);
    this.dtpiedeche.value[ri].ecart = this.numberPipe.transform(xx);
    if (this.dtpiedeche.value[ri].ecart == piedeche.montant) {
      this.dtpiedeche.value[ri].ecart = 0;
    }
  }

  precise(x, y) {
    return x.toPrecision(y);
  }

  newRow() {
    return {
      status: null,
      modepaie: null,
      motif: null,
      montantCompte: null,
      especeCash: null,
      depence: null,
      especeVerse: null,
      montant: null,
      statuslibelle: null,
      commentaire: null,
      montantTotal: null,
      jourcaisseId: null,
      id: 0,
      ecart: null,
    };
  }

  onSubmit() {}

  sumbitJourCaisse() {
    this.comptageForm?.get('agentComptage')?.setValue(this.currentUser);
    this.comptageForm
      ?.get('statusComtage')
      ?.setValue(this.statusJourCaisse?.id);
    //this.comptageForm?.get("validationDateFin")?.setValue(new Date(Date.now()));

    this.store$.dispatch(
      JoursCaisseActions.UpdateJoursCaisse({
        jourCaisse: this.comptageForm.value,
      })
    );
  }

  gettotMontant() {
    let totMontant: number = 0;
    for (let p of this.piedeches) {
      totMontant = totMontant + p?.montant!;
    }
    return this.numberPipe.transform(totMontant);
  }

  gettotMontantDepence() {
    let totMontantDepence: number = 0;
    for (let p of this.piedeches) {
      totMontantDepence += Number(p?.depence!);
    }
    return totMontantDepence;
  }

  gettotMontantEcart() {
    let totMontantEcart: number = 0;
    for (let p of this.piedeches) {
      if (p?.ecart != null && p?.ecart != 0) {
        totMontantEcart += Number.parseFloat(p.ecart.toString());
      }
    }
    return totMontantEcart;
  }

  gettotMontantTotal() {
    let totMontantTotal = 0;
    for (let p of this.piedeches) {
      if (p?.montantTotal != null) {
        totMontantTotal += Number.parseFloat(p.montantTotal.toString());
      }
    }
    return totMontantTotal;
  }

  gettotMontantEspece(): number {
    let totMontantEspece = 0;
    for (let p of this.piedeches) {
      totMontantEspece += Number(p?.especeCash!);
    }
    return totMontantEspece;
  }

  gettotMontantVerse(): number {
    let totMontantVerse = 0;
    for (let p of this.piedeches) {
      totMontantVerse += Number(p?.especeVerse!);
    }
    return totMontantVerse;
  }

  gettotMontantCompter(): number {
    let totMontantCompter = 0;
    for (let p of this.piedeches) {
      totMontantCompter += Number(p?.montantCompte!);
    }
    return totMontantCompter;
  }

  pressNum(event: any, piedeche: any, ri: any) {
    let num = event.target.value;
    let lastChar = num.indexOf('.');

    if (lastChar + 4 >= num.length) {
      // console.log("lastChar " + lastChar);
      // console.log("num.length " + num.length);
      // this.dtpiedeche.value[ri].montantCompte=   num;
    } else if (lastChar != -1) {
      // console.log("num "+num);
      // console.log("montantc "+this.dtpiedeche.value[ri].montantCompte);
      // console.log("num sub "+num.substr(0, num.length - 1));
      switch (event.target.name) {
        case 'depence':
          this.dtpiedeche.value[ri].depence = num = Number(
            num.substr(0, num.length - 1)
          );

          break;

        case 'montantCompte':
          this.dtpiedeche.value[ri].montantCompte = num = Number(
            num.substr(0, num.length - 1)
          );

          break;
        case 'especeCash':
          this.dtpiedeche.value[ri].especeCash = num = Number(
            num.substr(0, num.length - 1)
          );

          break;
        case 'especeVerse':
          this.dtpiedeche.value[ri].especeVerse = num = Number(
            num.substr(0, num.length - 1)
          );

          break;
      }
    }
  }
  openCalculate(
    event: any,
    piedeche: any,
    ri: any,
    champ: string,
    AllCalculetteByPiedechIdGet: BilletDetailComptageDTO[]
  ) {
    this.dialogService
      .open(DialogCalculatePromptComponent, {
        context: {
          calculette: this.calculette,
          calculComptage: AllCalculetteByPiedechIdGet,
        },
      })
      .onClose.subscribe((calculette) => {
        if (
          Number(calculette?.somme) !== 0 &&
          calculette?.somme !== undefined
        ) {
          this.calculette = calculette;
          piedeche.montantCompte = Number(calculette?.somme);
          this.showMontant = true;
        } else if (Number(calculette?.somme) === 0) {
          this.showMontant = false;
          this.calculette = calculette;
          piedeche.montantCompte = Number(calculette?.somme);
        }

        this.onchangeMontantTotal(
          piedeche.montantCompte,
          piedeche,
          ri,
          'montantCompte'
        );
        // let montantVerse = piedeche.montantVerse === undefined ? 0 : piedeche.montantVerse;
        // let montantCompte = piedeche.montantCompte === undefined ? 0 : piedeche.montantCompte;
        // let depence = piedeche.depence === undefined ? 0 : piedeche.depence;
        // let especeVerse = piedeche.especeVerse === undefined ? 0 : piedeche.especeVerse;
        // piedeche.montantTotal = (montantCompte + depence + especeVerse + montantVerse);
        // piedeche.ecart = piedeche.montant-(montantCompte + depence + especeVerse + montantVerse);
      });
  }

  isDepense(ri: any): boolean {
    const v = this.dtpiedeche.value[ri]?.modepaie?.id;
    this.isDepenseValue = this.mps?.some(
      (x) => x?.id == v && x?.isCash == true
    );
    if (this.isDepenseValue) {
      this.dtpiedeche.value[ri].depence = this.depenseMontant;
    }
    return this.isDepenseValue;
  }

  openDepense(piedeche: PiedecheDTO) {
    let newRelativeUrl = this.router.createUrlTree(['dashboard/depense']);
    let baseUrl = window.location.href.replace(this.router.url, '');
    window.open(
      baseUrl +
        newRelativeUrl +
        '/' +
        this.gjcEtablissementId?.value +
        '/' +
        this.gjcCaisse?.value +
        '/' +
        this.gjcNumzcaisse?.value,
      '_blank'
    );
  }
}
