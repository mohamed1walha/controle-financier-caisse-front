import {Component, OnInit} from '@angular/core';
import {ConfirmationService, MessageService} from "primeng/api";
import {FormBuilder} from "@angular/forms";


@Component({
  selector: 'app-comptage',
  templateUrl: './comptage.component.html',
  styles: [`
    :host ::ng-deep .p-dialog {
      width: 150px;
      margin: 0 auto 2rem auto;
      display: block;
    }
  `],
  styleUrls: ['./comptage.component.scss']
})
export class ComptageComponent implements OnInit {
  // receptionDialog: boolean;
  // allReceptions$ = this.store$.select(
  //   fromReception.selectReceptions
  // );
  //
  //
  // allReceptions: ReceptionDTO[] = [];
  // reception: ReceptionDTO;
  // selectedReceptions: ReceptionDTO[];
  // submitted: boolean;
  // actifs: any;
  // _alive = true;
  // selectedReception: ReceptionDTO;

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private fb: FormBuilder
  ) {
  }

  ngOnInit(): void {
    // this.store$.dispatch(ReceptionActions.fetchReceptions({statusCode: 'RCP'}));
    // this.allReceptions$.pipe(
    //   skipWhile((x) => x === null || x === undefined),
    //   takeWhile(() => this._alive)
    // ).subscribe(x =>
    //     this.allReceptions = x,
    //   //console.log(x)
    // );

  }

  //
  // exportPdf() {
  //   const doc = new jsPDF();
  //   const columns = [["First Column", "Second Column", "Third Column"]];
  //   const data = [
  //     ["Data 1", "Data 2", "Data 3"],
  //     ["Data 1", "Data 2", "Data 3"]
  //   ];
  //
  //   autoTable(doc, {
  //     head: columns,
  //     body: data,
  //     didDrawPage: dataArg => {
  //       doc.setFontSize(20);
  //       doc.setTextColor(40);
  //       doc.text("PAGE", dataArg.settings.margin.left, 10);
  //     }
  //   });
  //   doc.save("table.pdf");
  // }
  //
  // exportExcel() {
  //   import("xlsx").then(xlsx => {
  //     const worksheet = xlsx.utils.json_to_sheet(this.allReceptions);
  //     const workbook = {Sheets: {'data': worksheet}, SheetNames: ['data']};
  //     const excelBuffer: any = xlsx.write(workbook, {bookType: 'xlsx', type: 'array'});
  //     this.saveAsExcelFile(excelBuffer, "mps");
  //   });
  // }
  //
  // saveAsExcelFile(buffer: any, fileName: string): void {
  //   let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
  //   let EXCEL_EXTENSION = '.xlsx';
  //   const data: Blob = new Blob([buffer], {
  //     type: EXCEL_TYPE
  //   });
  //   FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  // }
  //
  //
  // editMp(reception: ReceptionDTO) {
  //   this.reception = {...reception};
  //   this.receptionDialog = true;
  // }
  //
  // deleteMp(reception: ReceptionDTO) {
  //   this.confirmationService.confirm({
  //     message: 'Etes-vous sûr que vous voulez supprimer ?',
  //     header: 'Confirmer',
  //     icon: 'pi pi-exclamation-triangle',
  //     accept: () => {
  //       this.store$.dispatch(ReceptionActions.DeleteReception({reception: reception}));
  //     }
  //   })
  // }
  //
  // hideDialog() {
  //   this.receptionDialog = false;
  //   this.submitted = false;
  // }
  //
  // saveMp() {
  //   this.submitted = true;
  //   if (this.reception.id) {
  //     this.store$.dispatch(ReceptionActions.EditReception({reception: this.reception}));
  //     this.receptionDialog = false;
  //   }
  // }
  //
  // getValueFilter(event: any): string {
  //   return event.target.value;
  // }
  //
  // compter(reception: any) {
  //   this.receptionDialog = true;
  //   this.selectedReception = reception;
  //
  // }
}
