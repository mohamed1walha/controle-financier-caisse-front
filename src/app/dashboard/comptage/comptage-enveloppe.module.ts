import { LOCALE_ID, NgModule } from '@angular/core';
import { DatePipe } from '@angular/common';

import { ComptageEnveloppeRoutingModule } from './comptage-enveloppe-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { SidebarModule } from 'primeng/sidebar';
import {
  NbCardModule,
  NbDatepickerModule,
  NbIconModule,
  NbInputModule,
} from '@nebular/theme';
import { RippleModule } from 'primeng/ripple';
import { NbMomentDateModule } from '@nebular/moment';
import { NbDateFnsDateModule } from '@nebular/date-fns';
import { DatePickerModule } from '@progress/kendo-angular-dateinputs';
import { ToggleButtonModule } from 'primeng/togglebutton';

@NgModule({
  declarations: [...ComptageEnveloppeRoutingModule.components],
  imports: [
    SharedModule,
    ComptageEnveloppeRoutingModule,
    SidebarModule,
    NbCardModule,
    RippleModule,
    NbInputModule,
    NbDatepickerModule,
    NbMomentDateModule,
    NbDateFnsDateModule.forChild({ format: 'dd-MM-yyyy' }),
    NbDateFnsDateModule.forRoot({
      format: 'dd-MM-yyyy',
      parseOptions: {
        useAdditionalWeekYearTokens: true,
        useAdditionalDayOfYearTokens: true,
      },
      formatOptions: {
        useAdditionalWeekYearTokens: true,
        useAdditionalDayOfYearTokens: true,
      },
    }),
    DatePickerModule,
    NbIconModule,
    ToggleButtonModule,
  ],
  providers: [DatePipe, { provide: LOCALE_ID, useValue: 'fr-FR' }],
})
export class ComptageEnveloppeModule {}
