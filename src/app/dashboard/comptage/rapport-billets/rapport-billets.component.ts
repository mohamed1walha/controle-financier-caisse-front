import {
  AfterViewChecked,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Table } from 'primeng/table';
import {
  EtablissementDTO,
  ParcaisseDTO,
  ReportBilletDTO,
  ReportBilletRequestDTO,
} from '../../../../../swagger-api';
import { Observable, skipWhile, takeWhile } from 'rxjs';
import {
  fromBlade,
  fromEtablissement,
  fromParcaisse,
  fromReportBillet,
} from '../../../shared/reducers';
import { FilterService } from 'primeng/api';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { ExportService } from '../../../core/services/export.service';
import { DatePipe } from '@angular/common';
import {
  EtablissementActions,
  ParcaisseActions,
  ReportBilletActions,
} from '../../../shared/actions';
import { deepCopy } from '../../../shared/utils/deep-copy';
import { Calendar } from 'primeng/calendar';
import { MultiSelect } from 'primeng/multiselect';

Calendar.prototype.getDateFormat = () => 'dd/mm/yy';

@Component({
  selector: 'app-rapport-billets',
  templateUrl: './rapport-billets.component.html',
  styleUrls: ['./rapport-billets.component.scss'],
  styles: [
    `
      :host ::ng-deep .p-dialog .product-image {
        width: 150px;
        margin: 0 auto 2rem auto;
        display: block;
      }
    `,
  ],
  providers: [ExportService, DatePipe],
})
export class RapportBilletsComponent implements OnInit, AfterViewChecked {
  headerRow: {
    title: string;
    displayName: string;
    key: string;
    type: string;
  }[] = [
    {
      title: $localize`Quantité`,
      displayName: $localize`Quantité`,
      key: 'quantiteTotal',
      type: 'text',
    },
    {
      title: $localize`Notes`,
      displayName: $localize`Notes`,
      key: 'note',
      type: 'text',
    },
    {
      title: $localize`Totales`,
      displayName: $localize`Totales`,
      key: 'montantTotal',
      type: 'text',
    },
  ];

  dateForm: FormGroup;
  @ViewChild('dt') dt: Table;

  _alive: boolean = true;
  etablissement$: Observable<EtablissementDTO[]> = this.store$.select(
    fromEtablissement.selectEtablissements
  );
  etablissements: EtablissementDTO[];
  totalRecords: number = 0;

  reports: ReportBilletDTO[] = [];
  rapports$: Observable<ReportBilletDTO[]> = this.store$.select(
    fromReportBillet.selectReportBillets
  );
  loading$ = this.store$.select(fromReportBillet.selectIsLoading);
  loading: boolean;

  checked: boolean = false;

  parcaisses: ParcaisseDTO[];
  parcaisses$: Observable<ParcaisseDTO[]> = this.store$.select(
    fromParcaisse.selectParcaisses
  );
  selectedReport: ReportBilletDTO[];
  filter: ReportBilletRequestDTO;
  total: number = 0;

  constructor(
    private store$: Store<
      fromBlade.State &
        fromReportBillet.State &
        fromEtablissement.State &
        fromParcaisse.State
    >,
    private filterService: FilterService,
    private changeDetector: ChangeDetectorRef,
    private datePipe: DatePipe,
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private exportService: ExportService
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.store$.dispatch(EtablissementActions.fetchEtablissements());
    this.rapports$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => {
        this.reports = x;
        this.reports.forEach((x) => (this.total += x?.montantTotal!));
      });
    this.etablissement$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => {
        this.etablissements = x;
      });
    this.loading$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => {
        this.loading = x;
      });
    this.parcaisses$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => {
        this.parcaisses = x;
      });
  }

  createForm() {
    this.dateForm = this.fb.group({
      startDate: [],
      endDate: [],
      etablissmentIds: [],
      caisseIds: [],
    });
  }

  rechercheEnveloppe() {
    this.total = 0;
    let dateTransform = this.datePipe.transform(
      this.startDate?.value,
      'yyyyMMdd'
    );
    let startDate: string = '';
    if (dateTransform !== null) {
      startDate = dateTransform;
    }

    let dateTransformTo = this.datePipe.transform(
      this.endDate?.value,
      'yyyyMMdd'
    );
    let endDate: string = '';
    if (dateTransformTo !== null) {
      endDate = dateTransformTo;
    }
    var filter = this.dateForm.getRawValue() as ReportBilletRequestDTO;
    if (filter.etablissmentIds?.length == 0) {
      filter.etablissmentIds = null;
    }
    this.filter = filter;
    this.store$.dispatch(
      ReportBilletActions.fetchReportBillets({
        reportBillets: { ...filter, startDate: startDate, endDate: endDate },
      })
    );
  }

  ngOnDestroy(): void {
    this._alive = false;
  }

  get startDate() {
    return this.dateForm.get('startDate');
  }

  get endDate() {
    return this.dateForm.get('endDate');
  }

  get etablissmentIds() {
    return this.dateForm.get('etablissmentIds');
  }
  get caisseIds() {
    return this.dateForm.get('caisseIds');
  }

  exportExcel() {
    let dateTransform = this.datePipe.transform(
      this.startDate?.value,
      'dd-MM-yyyy'
    );
    let dateOuverture: string = '';
    if (dateTransform !== null) {
      dateOuverture = dateTransform;
    }

    let dateTransformTo = this.datePipe.transform(
      this.endDate?.value,
      'dd-MM-yyyy'
    );
    let dateOuvertureTo: string = '';
    if (dateTransformTo !== null) {
      dateOuvertureTo = dateTransformTo;
    }
    const etablissementIds = this.filter?.etablissmentIds!?.map(
      (etablissementId) => {
        const etablissement = this.etablissements.find(
          (x) => x?.etEtablissement === etablissementId
        );
        if (etablissement) {
          return `${etablissement.etEtablissement} - ${etablissement.etLibelle}`;
        }
        return null;
      }
    );
    this.dt.filteredValue === null
      ? (this.selectedReport = this.reports)
      : (this.selectedReport = deepCopy(this.dt.filteredValue));
    this.exportService.exportExcelReportBillet(
      this.reports,
      this.filter,
      etablissementIds,

      dateOuverture,
      dateOuvertureTo,
      this.headerRow
    );
  }

  ngAfterViewChecked(): void {}

  getTotal(state: any): number {
    let tot = 0;
    if (
      state.totalRecords > state.rows &&
      state.totalRecords > state.rows * (state.page + 1)
    ) {
      tot = state.rows * (state.page + 1);
    } else {
      tot = state.totalRecords;
    }
    return tot;
  }

  onChangeEtablissement(value: any) {
    console.log(value.value);
    this.caisseIds?.setValue(null);
    this.store$.dispatch(
      ParcaisseActions.fetchParcaissesByEtabIds({ etabIds: value.value })
    );
  }
}
