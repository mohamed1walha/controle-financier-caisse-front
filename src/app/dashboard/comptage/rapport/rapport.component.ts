import {
  AfterViewChecked,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Table } from 'primeng/table';
import {
  EtablissementDTO,
  JoursCaisseService,
  ReportDTO,
} from '../../../../../swagger-api';
import { Observable, skipWhile, takeWhile } from 'rxjs';
import {
  fromBlade,
  fromChauffeur,
  fromEmetteur,
  fromEtablissement,
  fromJoursCaisse,
  fromModePaiement,
  fromPiedeche,
  fromReports,
} from '../../../shared/reducers';
import {
  ConfirmationService,
  FilterMatchMode,
  FilterService,
  SelectItem,
} from 'primeng/api';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { ExportService } from '../../../core/services/export.service';
import { DatePipe } from '@angular/common';
import { EtablissementActions, ReportActions } from '../../../shared/actions';
import { deepCopy } from '../../../shared/utils/deep-copy';
import { Calendar } from 'primeng/calendar';
import { LocalService } from '../../../core/services/locale.service';

Calendar.prototype.getDateFormat = () => 'dd/mm/yy';

@Component({
  selector: 'app-rapport',
  templateUrl: './rapport.component.html',
  styleUrls: ['./rapport.component.scss'],
  styles: [
    `
      :host ::ng-deep .p-dialog .product-image {
        width: 150px;
        margin: 0 auto 2rem auto;
        display: block;
      }
    `,
  ],
  providers: [ExportService, DatePipe],
})
export class RapportComponent implements OnInit, AfterViewChecked {
  headerRow: {
    title: string;
    displayName: string;
    key: string;
    type: string;
  }[] = [
    {
      title: $localize`Etablissement`,
      displayName: $localize`Etablissement`,
      key: 'etablissement',
      type: 'text',
    },
    {
      title: $localize`Caisse`,
      displayName: $localize`Caisse`,
      key: 'caisse',
      type: 'text',
    },
    {
      title: $localize`Reference Z`,
      displayName: $localize`Reference Z`,
      key: 'referenceZ',
      type: 'text',
    },
    {
      title: $localize`Date Ouverture`,
      displayName: $localize`Date Ouverture`,
      key: 'dateOuverture',
      type: 'text',
    },
    {
      title: $localize`Heure Ouverture`,
      displayName: $localize`Heure Ouverture`,
      key: 'heureOuverture',
      type: 'text',
    },
    {
      title: $localize`Mode Paiement`,
      displayName: $localize`Mode Paiement`,
      key: 'modePaiement',
      type: 'text',
    },
    {
      title: $localize`Montant Total`,
      displayName: $localize`Montant Total`,
      key: 'montantTotal',
      type: 'text',
    },
    {
      title: $localize`Montant Comptée`,
      displayName: $localize`Montant Comptée`,
      key: 'montantCompte',
      type: 'text',
    },
    {
      title: $localize`Montant Versée`,
      displayName: $localize`Montant Versée`,
      key: 'montantVerse',
      type: 'text',
    },
    {
      title: $localize`Dépense`,
      displayName: $localize`Dépense`,
      key: 'depense',
      type: 'text',
    },
    {
      title: $localize`Ecart`,
      displayName: $localize`Ecart`,
      key: 'ecart',
      type: 'text',
    },
    {
      title: $localize`Statut`,
      displayName: $localize`Statut`,
      key: 'statut',
      type: 'text',
    },
  ];

  dateForm: FormGroup;
  @ViewChild('dt') dt: Table;

  _alive: boolean = true;
  etablissement$: Observable<EtablissementDTO[]> = this.store$.select(
    fromEtablissement.selectEtablissements
  );
  etablissements: EtablissementDTO[];
  matchModeOptions: SelectItem[];
  matchModeOptionsDate: SelectItem[];
  totalRecords: number = 0;

  reports: ReportDTO[] = [];
  rapports$ = this.store$.select(fromReports.selectReports);
  loading$ = this.store$.select(fromReports.selectIsLoading);
  loading: boolean;
  MontantTotal: any;
  MontantCompte: any;
  Ecart: any;
  MontantVerse: any;
  Depense: any;

  selectedReport: ReportDTO[];
  selectedRow: any;
  sum: number = 0;

  currentLanguage = this.localStore.getData('language');

  constructor(
    private store$: Store<
      fromBlade.State &
        fromChauffeur.State &
        fromEmetteur.State &
        fromJoursCaisse.State &
        fromReports.State &
        fromPiedeche.State &
        fromEtablissement.State &
        fromModePaiement.State
    >,
    private filterService: FilterService,
    private changeDetector: ChangeDetectorRef,
    private confirmationService: ConfirmationService,
    private joursCaisseService: JoursCaisseService,
    private datePipe: DatePipe,
    public snackBar: MatSnackBar,
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private exportService: ExportService,
    private localStore: LocalService
  ) {}

  ngOnInit(): void {
    this.matchModeOptions = [
      { label: $localize`Contient`, value: FilterMatchMode.CONTAINS },
      { label: $localize`Début avec`, value: FilterMatchMode.STARTS_WITH },
    ];
    this.matchModeOptionsDate = [
      { label: $localize`Date supérieur`, value: FilterMatchMode.DATE_AFTER },
      { label: $localize`Date inférieur`, value: FilterMatchMode.DATE_BEFORE },
      { label: $localize`Date Equals`, value: FilterMatchMode.DATE_IS },
    ];
    this.createForm();
    this.store$.dispatch(EtablissementActions.fetchEtablissements());
    this.etablissement$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => {
        this.etablissements = x;
      });
    this.loading$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => {
        this.loading = x;
      });
  }

  createForm() {
    this.dateForm = this.fb.group({
      dateOuverture: [],
      dateOuvertureTo: [],
      etablissementId: [],
    });
  }

  rechercheEnveloppe() {
    let dateTransform = this.datePipe.transform(
      this.dateOuverture?.value,
      'yyyyMMdd'
    );
    let dateOuverture: string = '';
    if (dateTransform !== null) {
      dateOuverture = dateTransform;
    }

    let dateTransformTo = this.datePipe.transform(
      this.dateOuvertureTo?.value,
      'yyyyMMdd'
    );
    let dateOuvertureTo: string = '';
    if (dateTransformTo !== null) {
      dateOuvertureTo = dateTransformTo;
    }

    this.store$.dispatch(
      ReportActions.fetchReports({
        dateOuverture,
        dateOuvertureTo,
        etablissementId: this.etablissementId?.value,
      })
    );
    this.rapports$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => {
        this.reports = deepCopy(x);
        this.totalRecords = x?.length;
        this.MontantTotal = x
          .map((item) =>
            item.montantTotal !== undefined ? item.montantTotal : 0
          )
          .reduce((prev, curr) => prev + curr, 0);
        this.MontantCompte = x
          .map((item) =>
            item.montantCompte !== undefined ? item.montantCompte : 0
          )
          .reduce((prev, curr) => prev + curr, 0);
        this.MontantVerse = x
          .map((item) =>
            item.montantVerse !== undefined ? item.montantVerse : 0
          )
          .reduce((prev, curr) => prev + curr, 0);
        this.Depense = x
          .map((item) => (item.depense !== undefined ? item.depense : 0))
          .reduce((prev, curr) => prev + curr, 0);
        this.Ecart = x
          .map((item) => (item.ecart !== undefined ? item.ecart : 0))
          .reduce((prev, curr) => prev + curr, 0);
      });
  }
  calculateCustomerTotal(name): number {
    let total = 0;

    if (this.reports) {
      for (let customer of this.reports) {
        if (customer.referenceZ === name) {
          total++;
          console.log(name);
        }
      }
    }

    return total;
  }
  save(event) {
    //console.log(event)
  }

  ngOnDestroy(): void {
    this._alive = false;
  }

  get dateOuverture() {
    return this.dateForm.get('dateOuverture');
  }

  get dateOuvertureTo() {
    return this.dateForm.get('dateOuvertureTo');
  }

  get etablissementId() {
    return this.dateForm.get('etablissementId');
  }

  exportExcel() {
    let dateTransform = this.datePipe.transform(
      this.dateOuverture?.value,
      'dd-MM-yyyy'
    );
    let dateOuverture: string = '';
    if (dateTransform !== null) {
      dateOuverture = dateTransform;
    }

    let dateTransformTo = this.datePipe.transform(
      this.dateOuvertureTo?.value,
      'dd-MM-yyyy'
    );
    let dateOuvertureTo: string = '';
    if (dateTransformTo !== null) {
      dateOuvertureTo = dateTransformTo;
    }

    this.dt.filteredValue === null
      ? (this.selectedReport = this.reports)
      : (this.selectedReport = deepCopy(this.dt.filteredValue));
    this.exportService.exportExcelReport(
      this.selectedReport,
      dateOuverture,
      dateOuvertureTo,
      this.headerRow,
      this.MontantTotal,
      this.MontantCompte,
      this.MontantVerse,
      this.Depense,
      this.Ecart
    );
  }

  exportPdf() {
    let dateTransform = this.datePipe.transform(
      this.dateOuverture?.value,
      'dd-MM-yyyy'
    );
    let dateOuverture: string = '';
    if (dateTransform !== null) {
      dateOuverture = dateTransform;
    }

    let dateTransformTo = this.datePipe.transform(
      this.dateOuvertureTo?.value,
      'dd-MM-yyyy'
    );
    let dateOuvertureTo: string = '';
    if (dateTransformTo !== null) {
      dateOuvertureTo = dateTransformTo;
    }
    this.dt.filteredValue === null
      ? (this.selectedReport = this.reports)
      : (this.selectedReport = deepCopy(this.dt.filteredValue));
    this.exportService.exportReportPdf(
      this.selectedReport,
      dateOuverture,
      dateOuvertureTo,
      this.headerRow,
      this.MontantTotal,
      this.MontantCompte,
      this.MontantVerse,
      this.Depense,
      this.Ecart
    );
  }

  ngAfterViewChecked(): void {
    if (this.reports.length > 0) {
      this.dt.filteredValue === null
        ? (this.selectedReport = this.reports)
        : (this.selectedReport = deepCopy(this.dt.filteredValue));
      this.MontantTotal = this.selectedReport
        .map((item) =>
          item.montantTotal !== undefined ? item.montantTotal : 0
        )
        .reduce((prev, curr) => prev + curr, 0);
      this.MontantCompte = this.selectedReport
        .map((item) =>
          item.montantCompte !== undefined ? item.montantCompte : 0
        )
        .reduce((prev, curr) => prev + curr, 0);
      this.MontantVerse = this.selectedReport
        .map((item) =>
          item.montantVerse !== undefined ? item.montantVerse : 0
        )
        .reduce((prev, curr) => prev + curr, 0);
      this.Depense = this.selectedReport
        .map((item) => (item.depense !== undefined ? item.depense : 0))
        .reduce((prev, curr) => prev + curr, 0);
      this.Ecart = this.selectedReport
        .map((item) => (item.ecart !== undefined ? item.ecart : 0))
        .reduce((prev, curr) => prev + curr, 0);
      this.changeDetector.detectChanges();
    }
  }

  getTotal(state: any): number {
    let tot = 0;
    if (
      state.totalRecords > state.rows &&
      state.totalRecords > state.rows * (state.page + 1)
    ) {
      tot = state.rows * (state.page + 1);
    } else {
      tot = state.totalRecords;
    }
    return tot;
  }
}
