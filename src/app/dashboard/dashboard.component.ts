import {Component, ElementRef, OnDestroy, OnInit, ViewChildren} from '@angular/core';
import {MENU_ITEMS} from "./dashboard-menu";
import {
  NbIconLibraries,
  NbMediaBreakpointsService,
  NbMenuService,
  NbSidebarService,
  NbThemeService
} from "@nebular/theme";
import {Store} from "@ngrx/store";
import {fromBlade} from "../shared/reducers";
import {AuthenticationService} from "../core/services/authentication.service";
import {Router} from "@angular/router";
import {LayoutService} from "../shared/utils/layout.service";
import {map} from "rxjs/operators";
import {takeWhile} from "rxjs";
import {BladeActions} from "../shared/actions";

const WIDTH_FOR_RESPONSIVE = 1280;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  currentUser = (JSON?.parse(localStorage?.getItem('currentUser') || '{}')).data?.currentUser;
  menu = MENU_ITEMS;
  isExpanded = true;
  @ViewChildren('nbSidebarComponent') nbSidebarComponent;
  isBladeExpanded = true;
  bladeWithDirective = 60.5;
  isActive: boolean = true;

  constructor(
    private sidebarService: NbSidebarService,
    private store$: Store<fromBlade.State>,
    private breakpointService: NbMediaBreakpointsService,
    private themeService: NbThemeService,
    private elementRef: ElementRef,
    private authenticationService: AuthenticationService,
    private router: Router,
    private layoutService: LayoutService,
    private menuService: NbMenuService,
    private iconLibraries: NbIconLibraries
  ) {
    this.iconLibraries.registerSvgPack('ic', {
      'emetteur': '<svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/>' +
        '<path d="M9 12c1.93 0 3.5-1.57 3.5-3.5S10.93 5 9 5 5.5 6.57 5.5 8.5 7.07 12 9 12zm0-5c.83 0 1.5.67 1.5 1.5S9.83 10 9 10s-1.5-.67-1.5-1.5S8.17 7 9 7zm.05 10H4.77c.99-.5 2.7-1 4.23-1 .11 0 .23.01.34.01.34-.73.93-1.33 1.64-1.81-.73-.13-1.42-.2-1.98-.2-2.34 0-7 1.17-7 3.5V19h7v-1.5c0-.17.02-.34.05-.5zm7.45-2.5c-1.84 0-5.5 1.01-5.5 3V19h11v-1.5c0-1.99-3.66-3-5.5-3zm1.21-1.82c.76-.43 1.29-1.24 1.29-2.18C19 9.12 17.88 8 16.5 8S14 9.12 14 10.5c0 .94.53 1.75 1.29 2.18.36.2.77.32 1.21.32s.85-.12 1.21-.32z"/></svg>',
      'motif': '<svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M18 9l-1.41-1.42L10 14.17l-2.59-2.58L6 13l4 4zm1-6h-4.18C14.4 1.84 13.3 1 12 1c-1.3 0-2.4.84-2.82 2H5c-.14 0-.27.01-.4.04-.39.08-.74.28-1.01.55-.18.18-.33.4-.43.64-.1.23-.16.49-.16.77v14c0 .27.06.54.16.78s.25.45.43.64c.27.27.62.47 1.01.55.13.02.26.03.4.03h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2zm-7-.25c.41 0 .75.34.75.75s-.34.75-.75.75-.75-.34-.75-.75.34-.75.75-.75zM19 19H5V5h14v14z"/></svg>',
      'chauffeur': '<svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#000000">' +
        '<path d="M0 0h24v24H0V0z" fill="none"/>' +
        '<path d="M12 6c1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2 .9-2 2-2m0 9c2.7 0 5.8 1.29 6 2v1H6v-.99c.2-.72 3.3-2.01 6-2.01m0-11C9.79 '
        +
        '4 8 5.79 8 8s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm0 9c-2.67 0-8 1.34-8 4v3h16v-3c0-2.66-5.33-4-8-4z"/></svg>',
      'cash': '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-cash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">\n' +
        '  <path fill-rule="evenodd" d="M15 4H1v8h14V4zM1 3a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h14a1 1 0 0 0 1-1V4a1 1 0 0 0-1-1H1z"/>\n' +
        '  <path d="M13 4a2 2 0 0 0 2 2V4h-2zM3 4a2 2 0 0 1-2 2V4h2zm10 8a2 2 0 0 1 2-2v2h-2zM3 12a2 2 0 0 0-2-2v2h2zm7-4a2 2 0 1 1-4 0 2 2 0 0 1 4 0z"/>\n' +
        '</svg>',
      'declaration': '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-card-list" fill="currentColor" xmlns="http://www.w3.org/2000/svg">\n' +
        '  <path fill-rule="evenodd" d="M14.5 3h-13a.5.5 0 0 0-.5.5v9a.5.5 0 0 0 .5.5h13a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z"/>\n' +
        '  <path fill-rule="evenodd" d="M5 8a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7A.5.5 0 0 1 5 8zm0-2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm0 5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5z"/>\n' +
        '  <circle cx="3.5" cy="5.5" r=".5"/>\n' +
        '  <circle cx="3.5" cy="8" r=".5"/>\n' +
        '  <circle cx="3.5" cy="10.5" r=".5"/>\n' +
        '</svg>',
      'validation': '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-card-checklist" fill="currentColor" xmlns="http://www.w3.org/2000/svg">\n' +
        '  <path fill-rule="evenodd" d="M14.5 3h-13a.5.5 0 0 0-.5.5v9a.5.5 0 0 0 .5.5h13a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z"/>\n' +
        '  <path fill-rule="evenodd" d="M7 5.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm-1.496-.854a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 1 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0zM7 9.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5zm-1.496-.854a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0l-.5-.5a.5.5 0 0 1 .708-.708l.146.147 1.146-1.147a.5.5 0 0 1 .708 0z"/>\n' +
        '</svg>',

      // ...
    });
  }

  static path = () => ['dashboard'];

  ngOnInit() {
    this.iconLibraries.registerFontPack('font-awesome', {iconClassPrefix: 'fa'});
    this.iconLibraries.registerFontPack('material-icons', {
      ligature: true,
    });
    this.menuService.onItemClick()
      .subscribe((event) => {
        this.onContecxtItemSelection(event.item.title);
      });
    const {xl} = this.breakpointService.getBreakpointsMap();
    this.themeService
      .onMediaQueryChange()
      .pipe(map(([, currentBreakpoint]) => currentBreakpoint.width < xl))
      .subscribe((isLessThanXl: boolean) => {
        isLessThanXl ? (this.isExpanded = false) : (this.isExpanded = true);
      });
    this.store$
      .select(fromBlade.selectBladeIsExpanded)
      .pipe(takeWhile(() => this.isActive))
      .subscribe((value) => {
        this.isBladeExpanded = value;
      });
    this.store$
      .select(fromBlade.selectBladeWidth)
      .pipe(takeWhile(() => this.isActive))
      .subscribe((value) => {
        this.bladeWithDirective = value;
      });
  }

  closeBlade() {
    this.store$.dispatch(BladeActions.closeBlade());
  }

  ngOnDestroy(): void {
    this.isActive = false;
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['auth/login']);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    this.layoutService.changeLayoutSize();
    this.isExpanded = !this.isExpanded;
    return false;
  }

  onContecxtItemSelection(title) {
    if (title === 'Log out') {
      this.logout();
    }
    if (title === 'Profile') {
      this.profile();
    }
  }

  profile() {
    this.router.navigate(['dashboard/profile']);
  }
}



