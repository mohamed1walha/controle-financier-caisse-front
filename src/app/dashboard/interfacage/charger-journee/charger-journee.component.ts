import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { debounceTime, Observable, skipWhile, takeWhile } from 'rxjs';
import * as FileSaver from 'file-saver';

// NgRx
import { Store } from '@ngrx/store';
import {
  fromBlade,
  fromEtablissement,
  fromJoursCaisse,
  fromModePaiement,
  fromPiedeche,
} from '../../../shared/reducers';
import {
  EtablissementActions,
  JoursCaisseActions,
  LastSyncAppActions,
  PiedecheActions,
} from '../../../shared/actions';
import {
  EtablissementDTO,
  JoursCaiseDTO,
  JoursCaisseService,
} from '../../../../../swagger-api';
import {
  ConfirmationService,
  FilterMatchMode,
  FilterMetadata,
  FilterService,
  SelectItem,
} from 'primeng/api';
import { Table } from 'primeng/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import { map } from 'rxjs/operators';
import { setFilterBy } from '../../../shared/actions/joursCaisse.actions';
import { PiedecheDTO } from '../../../../../swagger-api/model/piedecheDTO';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { deepCopy } from '../../../shared/utils/deep-copy';
import { Calendar } from 'primeng/calendar';
import { SeparateurPipe } from '../../../shared/utils/separateur.pipe';

Calendar.prototype.getDateFormat = () => 'dd/mm/yy';

@Component({
  selector: 'app-charger-journee',
  templateUrl: './charger-journee.component.html',
  styles: [
    `
      :host ::ng-deep .p-dialog .product-image {
        width: 150px;
        margin: 0 auto 2rem auto;
        display: block;
      }
    `,
  ],
  styleUrls: ['./charger-journee.component.scss'],
  providers: [DatePipe],
})
export class ChargerJourneeComponent implements OnInit, OnDestroy {
  @ViewChild('dt') dt: Table;
  headerRow: {
    displayName: string;
    key: string;
    hasSort: boolean;
    type: string;
  }[] = [
    /*
    {displayName: "Référence", key: 'id', hasSort: true, type: 'text'},
*/
    {
      displayName: $localize`Etablissement`,
      key: 'gjcEtablissement.etLibelle',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Caisse`,
      key: 'gjcCaisse',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Référence Z`,
      key: 'gjcNumzcaisse',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Date d'ouverture`,
      key: 'gjcDateouv',
      hasSort: true,
      type: 'date',
    },
    {
      displayName: $localize`Heure d'ouverture`,
      key: 'gjcHeureouv',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Date de fermeture`,
      key: 'gjcDateferme',
      hasSort: true,
      type: 'date',
    },
    {
      displayName: $localize`Heure de fermeture`,
      key: 'gjcHeureferme',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Date Creation`,
      key: 'dateCreate',
      hasSort: true,
      type: 'date',
    },
    {
      displayName: $localize`User Fermer`,
      key: 'gjcUserferme',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Ref externe`,
      key: 'refExterne',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Montant Total`,
      key: 'montantCaisse',
      hasSort: true,
      type: 'text',
    },
  ];
  _alive: boolean = true;
  joursCaisses$: Observable<JoursCaiseDTO[]> = this.store$.select(
    fromJoursCaisse.selectTableData
  );
  loading$: Observable<any> = this.store$.select(fromJoursCaisse.selectLoading);
  piedeche$: Observable<PiedecheDTO[]> = this.store$.select(
    fromPiedeche.selectPiedeches
  );
  joursCaisses: JoursCaiseDTO[];
  selectedjoursCaisses: JoursCaiseDTO[];
  piedeche: PiedecheDTO[];
  joursCaisse: JoursCaiseDTO;
  matchModeOptions: SelectItem[];
  matchModeOptionsDate: SelectItem[];
  etablissement$: Observable<EtablissementDTO[]> = this.store$.select(
    fromEtablissement.selectEtablissements
  );
  etablissements: EtablissementDTO[];
  montantTot: number = 0;
  lastDateSync: Date;
  dateForm: FormGroup;
  etablissement: EtablissementDTO;
  loading: boolean = false;

  searchControl = new FormControl('');
  columns = [
    { title: $localize`Référence`, dataKey: 'id' },
    { title: $localize`Etablissement`, dataKey: 'gjcEtablissementlabel' },
    { title: $localize`Caisse`, dataKey: 'gjcCaisse' },
    { title: $localize`Référence Z`, dataKey: 'gjcNumzcaisse' },
    { title: $localize`Date d'ouverture`, dataKey: 'gjcDateouv' },
    { title: $localize`Heure d'ouverture`, dataKey: 'gjcHeureouv' },
    { title: $localize`Date de fermeture`, dataKey: 'gjcDateferme' },
    { title: $localize`Heure de fermeture`, dataKey: 'gjcHeureferme' },
    { title: $localize`Date creation`, dataKey: 'dateCreate' },
    { title: $localize`User Fermer`, dataKey: 'gjcUserferme' },
    { title: $localize`Ref externe`, dataKey: 'refExterne' },
    { title: $localize`Montant`, dataKey: 'montantCaisse' },
  ];

  joursCaissePartielle: JoursCaiseDTO = {};
  partielleDialog: boolean;
  submitted: boolean;
  selectedEtablissement: EtablissementDTO;
  modeUpdatePartielle: boolean;

  constructor(
    private store$: Store<
      fromBlade.State &
        fromJoursCaisse.State &
        fromPiedeche.State &
        fromEtablissement.State &
        fromModePaiement.State
    >,
    private numberPipe: SeparateurPipe,
    private filterService: FilterService,
    private confirmationService: ConfirmationService,
    private joursCaisseService: JoursCaisseService,
    public snackBar: MatSnackBar,
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private datePipe: DatePipe
  ) {}

  ngOnDestroy(): void {
    this.store$.dispatch(JoursCaisseActions.resetDataTableStore());
    this._alive = false;
  }

  ngOnInit(): void {
    this.fetchdata();
  }
  createForm() {
    this.dateForm = this.fb.group({
      dateFermeFrom: [new Date(Date.now() - 86400000*7), Validators.compose([Validators.required])],
      dateFermeTo: [new Date(), Validators.compose([Validators.required])],
    });
  }

  fetchdata() {
    //this.store$.dispatch(LastSyncAppActions.fetchlastSync());
    //this.store$.dispatch(JoursCaisseActions.fetchAllDataStatusCpmtNone());
    this.store$.dispatch(EtablissementActions.fetchEtablissements());
    this.createForm();
    this.rechercheEnveloppe();
    this.etablissement$
      .pipe(skipWhile((x) => x === null || x === undefined))
      .subscribe((x) => {
        this.etablissements = x;
      });
    this.matchModeOptions = [
      { label: $localize`Début avec`, value: FilterMatchMode.STARTS_WITH },
      { label: $localize`Contient`, value: FilterMatchMode.CONTAINS },
    ];
    this.matchModeOptionsDate = [
      { label: $localize`Date supérieur`, value: FilterMatchMode.DATE_AFTER },
      { label: $localize`Date inférieur`, value: FilterMatchMode.DATE_BEFORE },
      { label: $localize`Date Egale`, value: FilterMatchMode.DATE_IS },
    ];
    //this.store$.dispatch(JoursCaisseActions.setData({data: this.data}));
    this.joursCaisses$
      .pipe(
        skipWhile((x) => x === null),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => {
        this.joursCaisses = deepCopy(x);
        //this.selectedjoursCaisses=this.joursCaisses
        //  this.joursCaisses={...this.joursCaisses,}
        this.joursCaisses.forEach((joursCaisses) => {
          joursCaisses.dateCreate = new Date(joursCaisses.dateCreate!);
          joursCaisses.dateComptage = new Date(joursCaisses.dateComptage!);
          joursCaisses.gjcDateferme = new Date(joursCaisses.gjcDateferme!);
          joursCaisses.receptionDate = new Date(joursCaisses.receptionDate!);
          joursCaisses.gjcDateouv = new Date(joursCaisses.gjcDateouv!);
          joursCaisses.validationDateFin = new Date(
            joursCaisses.validationDateFin!
          );
          joursCaisses.validationDateDebut = new Date(
            joursCaisses.validationDateDebut!
          );

          this.montantTot += joursCaisses?.montantCaisse!;
          // this.montantTot.toFixed(3)
        });
      });

    this.searchControl.valueChanges
      .pipe(
        debounceTime(1000),
        map((query) => query?.toLowerCase())
      )
      .subscribe((query) => {
        this.store$.dispatch(
          setFilterBy({
            filters: {
              filterBy: [
                'gjcEtablissement.etLibelle',
                'gjcNumzcaisse',
                'gjcCaisse',
              ],
              query: query,
            },
          })
        );
      });
  }

  editProduct(joursCaise: JoursCaiseDTO) {
    this.joursCaisse = { ...joursCaise };
    this.store$.dispatch(
      PiedecheActions.fetchPiedecheByJoursCaisse({
        id: joursCaise.id,
      })
    );
    this.piedeche$
      .pipe(
        skipWhile((x) => x === null),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => (this.piedeche = x));
  }

  handleFilter(
    filteredData: any[],
    filters: { [p: string]: FilterMetadata | FilterMetadata[] }
  ) {
    this.montantTot = 0;
    filteredData?.forEach((data) => {
      this.montantTot += data?.montantCaisse;
    });
  }

  clear(value: any) {}

  // onchange(etEtablissement: any, gjcEtablissementLabel: string, equals: string) {
  //   console.log(etEtablissement);
  //   if (etEtablissement != null) {
  //     this.dt.filter(etEtablissement.etLibelle, gjcEtablissementLabel, equals);
  //   } else {
  //     this.dt.filter({}, gjcEtablissementLabel, equals);
  //
  //
  //   }
  // }

  exportExcel() {
    import('xlsx').then((xlsx) => {
      this.dt.filteredValue === null
        ? (this.selectedjoursCaisses = this.joursCaisses)
        : (this.selectedjoursCaisses = deepCopy(this.dt.filteredValue));

      const worksheet = xlsx.utils.json_to_sheet(this.selectedjoursCaisses);
      const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, {
        bookType: 'xlsx',
        type: 'array',
      });
      this.saveAsExcelFile(excelBuffer, 'JoursCaisses');
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE =
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    FileSaver.saveAs(
      data,
      fileName +
        '_export_' +
        this.datePipe.transform(new Date(), 'YYYYMMdd hh:mm:ss') +
        EXCEL_EXTENSION
    );
  }

  exportPdf() {
    var mappedItems = [] as any;
    var foot = [] as any;
    let sum = 0;
    this.dt.filteredValue === null
      ? (this.selectedjoursCaisses = this.joursCaisses)
      : (this.selectedjoursCaisses = deepCopy(this.dt.filteredValue));
    this.selectedjoursCaisses.forEach((item, key) => {
      if (item.montantCaisse != null) {
        sum += item.montantCaisse!;
      }
      mappedItems.push({
        gjcEtablissementlabel: item.gjcEtablissement?.etLibelle,
        gjcCaisse: item.gjcCaisse,
        gjcNumzcaisse: item.gjcNumzcaisse,
        gjcDateouv: this.datePipe.transform(item.gjcDateouv, 'dd-MMM-YYYY'),
        gjcHeureouv: item.gjcHeureouv,
        gjcDateferme: this.datePipe.transform(item.gjcDateferme, 'dd-MMM-YYYY'),
        gjcHeureferme: item.gjcHeureferme,
        gjcUserferme: item.gjcUserferme,
        dateCreate: this.datePipe.transform(item.dateCreate, 'dd-MMM-YYYY'),
        montantCaisse: this.numberPipe.transform(item.montantCaisse),
      });
    });

    foot.push({
      gjcEtablissementLabel: '',
      gjcCaisse: 'Montant Total:',
      gjcNumzcaisse: '',
      gjcDateouv: '',
      gjcHeureouv: '',
      gjcDateferme: '',
      gjcHeureferme: '',
      gjcUserferme: '',
      dateCreate: '',
      montantCaisse: this.numberPipe.transform(sum),
    });
    const doc = new jsPDF('p');
    autoTable(doc, {
      columns: this.columns,
      body: mappedItems,
      foot: foot,
      margin: { top: 15, bottom: 10 },
      pageBreak: 'auto',
      showHead: 'everyPage',
      columnStyles: {
        0: { cellWidth: 'auto' },
        1: { cellWidth: 'auto' },
        2: { cellWidth: 'auto' },
        3: { cellWidth: 'auto' },
        4: { cellWidth: 'auto' },
        5: { cellWidth: 'auto' },
        6: { cellWidth: 'auto' },
        7: { cellWidth: 'auto' },
        8: { cellWidth: 'auto' },
        9: { cellWidth: 'auto', halign: 'right' },
      },

      headStyles: { fontSize: 7, fontStyle: 'bolditalic' },
      rowPageBreak: 'auto',
      footStyles: {
        fontSize: 10,
        fontStyle: 'bolditalic',
        lineColor: '',
        halign: 'right',
      },
      tableWidth: 'auto',
      styles: { overflow: 'linebreak', fontStyle: 'normal', fontSize: 7 },

      didDrawPage: (dataArg) => {
        doc.setFontSize(12);
        doc.setTextColor(20);
        doc.text('Jours Caisses', dataArg.settings.margin.left, 10);
      },
    });
    doc.save(
      'JoursCaisses_export_' +
        this.datePipe.transform(new Date(), 'YYYYMMdd hh:mm:ss') +
        '.pdf'
    );
  }

  collectePartielle() {
    this.joursCaissePartielle = {};
    this.submitted = false;
    this.partielleDialog = true;
    this.modeUpdatePartielle = false;
  }

  hideDialog() {
    this.joursCaissePartielle = {};
    this.partielleDialog = false;
    this.submitted = false;
    this.selectedEtablissement = {};
  }

  savePartielle() {
    this.submitted = true;
    this.joursCaissePartielle.gjcEtablissementId =
      this.selectedEtablissement.id;

    /* this.joursCaisseService.chargerJoursCaisseSearchByCodeGet(this.joursCaissePartielle.gjcNumzcaisse,this.selectedEtablissement.id,this.joursCaissePartielle.gjcCaisse).subscribe(res=>{

      if(res){

      }else{

      }
    })*/

    if (this.modeUpdatePartielle === false) {
      this.joursCaissePartielle.refExterne =
        this.joursCaissePartielle.gjcNumzcaisse;
      this.joursCaissePartielle.gjcNumzcaisse =
        Number(this.joursCaissePartielle.gjcNumzcaisse) + 10000;

      this.joursCaisseService
        .joursCaisseCreatePost(this.joursCaissePartielle)
        .subscribe((x) => {
          this.joursCaissePartielle = {};
          this.partielleDialog = false;
          this.submitted = false;
          this.selectedEtablissement = {};
          this.modeUpdatePartielle = false;
          this.fetchdata();
        });
    } else {
      if (Number(this.joursCaissePartielle.gjcNumzcaisse) > 10000) {
        this.joursCaissePartielle.refExterne =
          Number(this.joursCaissePartielle.gjcNumzcaisse) - 10000;
      }
      this.joursCaisseService
        .joursCaisseUpdatePut(this.joursCaissePartielle)
        .subscribe((x) => {
          this.joursCaissePartielle = {};
          this.partielleDialog = false;
          this.submitted = false;
          this.selectedEtablissement = {};
          this.modeUpdatePartielle = false;
          this.fetchdata();
        });
    }
  }

  editPartielle(jourCaisse: any) {
    this.joursCaissePartielle = { ...jourCaisse };
    this.partielleDialog = true;
    this.modeUpdatePartielle = true;
    this.selectedEtablissement = this.etablissements.find(
      (element) => element.id === jourCaisse.gjcEtablissementId
    ) as EtablissementDTO;
  }

  deletePartielle(jourCaisse: any) {
    this.joursCaisseService
      .joursCaisseDeleteDelete(jourCaisse.id)
      .subscribe(() => {
        this.fetchdata();
      });
  }

  rechercheEnveloppe() {
    let dateFermeTo = this.datePipe.transform(
      this.dateFermeTo?.value || new Date(),
      'yyyyMMdd'
    );
    let dateFermeFrom = this.datePipe.transform(
      this.dateFermeFrom?.value || new Date(Date.now() - 86400000*7),
      'yyyyMMdd'
    );

    if (dateFermeTo !== null && dateFermeFrom !== null) {
      this.loading = true;
      this.montantTot = 0;

      this.joursCaisseService
        .joursCaisseFiltreGetAllByStatusCmptNonePost(
          dateFermeFrom,
          dateFermeTo
        )
        .pipe(map((x) => x))
        .subscribe((data) => {
          if (data != null) {
            this.store$.dispatch(JoursCaisseActions.setData({ data }));
            this.loading = false;
          }
        });
    }
  }

  get dateFermeFrom() {
    return this.dateForm.get('dateFermeFrom');
  }

  get dateFermeTo() {
    return this.dateForm.get('dateFermeTo');
  }

}
