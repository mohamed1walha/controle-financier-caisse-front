import {FilterMetadata} from "primeng/api";

export class FilterMetadataModel {

  name: any;
  filter: {
    [s: string]: FilterMetadata;
  };
}
