import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ChargerJourneeComponent} from "./charger-journee/charger-journee.component";
import {SynchroniserJourneeComponent} from "./synchroniser-journee/synchroniser-journee.component";

const routes: Routes = [
    {
      path: 'charger-journee',
      component: ChargerJourneeComponent,
    },
    {
      path: 'synchroniser-journee',
      component: SynchroniserJourneeComponent,
    }


  ]
;

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class InterfacageRoutingModule {
  static components = [
    ChargerJourneeComponent,

  ];
}
