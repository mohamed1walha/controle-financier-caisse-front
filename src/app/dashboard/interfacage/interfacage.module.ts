import {NgModule} from '@angular/core';
import {SharedModule} from 'src/app/shared/shared.module';
import {InterfacageRoutingModule} from "./interfacage-routing.module";
import {DataTableComponent} from "./components/data-table/data-table.component";
import {SpinnerComponent} from "./components/spinner/spinner.component";
import {ErrorComponent} from "./components/error/error.component";
import {CommonModule} from "@angular/common";
import {ReactiveComponentModule} from "@ngrx/component";
import {SynchroniserJourneeComponent} from './synchroniser-journee/synchroniser-journee.component';
import {ProgressSpinnerModule} from "primeng/progressspinner";
import {NgxSpinnerModule} from "ngx-spinner";
import {NbCardModule, NbDatepickerModule} from "@nebular/theme";
import {NbMomentDateModule} from "@nebular/moment";
import {NbDateFnsDateModule} from "@nebular/date-fns";
import {fr} from 'date-fns/locale';
import {DatePickerModule} from "@progress/kendo-angular-dateinputs";
import {IntlModule} from "@progress/kendo-angular-intl";
import {NgxLoaderModule} from "@tusharghoshbd/ngx-loader";
import {RippleModule} from "primeng/ripple";

@NgModule({
  declarations: [
    ...InterfacageRoutingModule.components,
    DataTableComponent,
    SpinnerComponent,
    ErrorComponent,
    SynchroniserJourneeComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    InterfacageRoutingModule,
    ReactiveComponentModule,
    ProgressSpinnerModule,
    NgxSpinnerModule,
    NbCardModule,
    NbDatepickerModule,
    NbMomentDateModule,
    NbDateFnsDateModule.forChild({
      format: 'DD.MM.YYYY',
      parseOptions: {locale: fr, useAdditionalWeekYearTokens: true, useAdditionalDayOfYearTokens: true},
      formatOptions: {locale: fr, useAdditionalWeekYearTokens: true, useAdditionalDayOfYearTokens: true}
    }),

    NbDateFnsDateModule.forRoot({
      format: 'DD.MM.YYYY',

      parseOptions: {locale: fr, useAdditionalWeekYearTokens: true, useAdditionalDayOfYearTokens: true},
      formatOptions: {locale: fr, useAdditionalWeekYearTokens: true, useAdditionalDayOfYearTokens: true},
    }),
    DatePickerModule,
    IntlModule,
    NgxLoaderModule,
    RippleModule,

  ],
  providers: []

})
export class InterfacageModule {
}
