import {
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import {
  fromEtablissement,
  fromJoursCaisse,
  fromLastSyncApp,
  fromModePaiement,
  fromPiedeche,
} from '../../../shared/reducers';
import {
  EtablissementActions,
  JoursCaisseActions,
  LastSyncAppActions,
  PiedecheActions,
} from '../../../shared/actions';
import { Observable, skipWhile, takeWhile } from 'rxjs';
import { FilterMatchMode, FilterMetadata, SelectItem } from 'primeng/api';
import { map } from 'rxjs/operators';
import { Table } from 'primeng/table';
import {
  EtablissementDTO,
  JoursCaiseDTO,
  JoursCaisseService,
  PiedecheDTO,
} from '../../../../../swagger-api';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import * as FileSaver from 'file-saver';
import { DatePipe } from '@angular/common';
import { deepCopy } from '../../../shared/utils/deep-copy';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { Calendar } from 'primeng/calendar';
import { SeparateurPipe } from '../../../shared/utils/separateur.pipe';

Calendar.prototype.getDateFormat = () => 'dd/mm/yy';

@Component({
  selector: 'app-synchroniser-journee',
  templateUrl: './synchroniser-journee.component.html',
  styleUrls: ['./synchroniser-journee.component.scss'],
  providers: [DatePipe],
  encapsulation: ViewEncapsulation.None,
})
export class SynchroniserJourneeComponent implements OnInit, OnDestroy {
  @ViewChild('dt') dt: Table;
  headerRow: {
    displayName: string;
    key: string;
    hasSort: boolean;
    type: string;
  }[] = [
    {
      displayName: $localize`Etablissement`,
      key: 'gjcEtablissement.etLibelle',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Caisse`,
      key: 'gjcCaisse',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Référence Z`,
      key: 'gjcNumzcaisse',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Date d'ouverture`,
      key: 'gjcDateouv',
      hasSort: true,
      type: 'date',
    },
    {
      displayName: $localize`Heure d'ouverture`,
      key: 'gjcHeureouv',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Date de fermeture`,
      key: 'gjcDateferme',
      hasSort: true,
      type: 'date',
    },
    {
      displayName: $localize`Heure de fermeture`,
      key: 'gjcHeureferme',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Date Creation`,
      key: 'dateCreate',
      hasSort: true,
      type: 'date',
    },
    {
      displayName: $localize`User Fermer`,
      key: 'gjcUserferme',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Montant Total`,
      key: 'montantCaisse',
      hasSort: true,
      type: 'text',
    },
  ];
  _alive: boolean = true;
  joursCaisses$: Observable<any> = this.store$.select(
    fromJoursCaisse.selectData
  );
  loading$: Observable<any> = this.store$.select(fromJoursCaisse.selectLoading);
  piedeche$: Observable<PiedecheDTO[]> = this.store$.select(
    fromPiedeche.selectPiedeches
  );
  joursCaisses: JoursCaiseDTO[];
  piedeche: PiedecheDTO[];
  selectedjoursCaisses: JoursCaiseDTO[];

  joursCaisse: JoursCaiseDTO;
  matchModeOptions: SelectItem[];
  matchModeOptionsDate: SelectItem[];
  etablissement$: Observable<EtablissementDTO[]> = this.store$.select(
    fromEtablissement.selectEtablissements
  );
  etablissements: EtablissementDTO[];
  etablissement: EtablissementDTO;

  lastSync$ = this.store$.select(fromLastSyncApp.getDateSync).pipe(
    skipWhile((x) => x === null || x === undefined),
    takeWhile(() => this._alive)
  );
  lastDateSync = new Date();
  showLastSync: boolean = false;
  dateForm: FormGroup;
  loading: boolean = false;
  data$: Observable<any[]> = this.store$.select(fromJoursCaisse.selectData);
  columns = [
    { title: $localize`Etablissement`, dataKey: 'gjcEtablissement.etLibelle' },
    { title: $localize`Caisse`, dataKey: 'gjcCaisse' },
    { title: $localize`Référence Z`, dataKey: 'gjcNumzcaisse' },
    { title: $localize`Date d'ouverture`, dataKey: 'gjcDateouv' },
    { title: $localize`Heure d'ouverture`, dataKey: 'gjcHeureouv' },
    { title: $localize`Date de fermeture`, dataKey: 'gjcDateferme' },
    { title: $localize`Heure de fermeture`, dataKey: 'gjcHeureferme' },
    { title: $localize`Date Creation`, dataKey: 'dateCreate' },
    { title: $localize`User Fermer`, dataKey: 'gjcUserferme' },
    { title: $localize`Montant`, dataKey: 'montantCaisse' },
  ];
  currentUser = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .currentUser;
  public showTable: boolean = false;
  montantTot: number = 0;
  jours: JoursCaiseDTO[];
  show = false;

  constructor(
    private numberPipe: SeparateurPipe,
    private store$: Store<
      fromJoursCaisse.State &
        fromPiedeche.State &
        fromEtablissement.State &
        fromModePaiement.State
    >,
    private joursCaisseService: JoursCaisseService,
    private fb: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private datePipe: DatePipe,
    private toastService: NbToastrService
  ) {}

  get dateFermeFrom() {
    return this.dateForm.get('dateFermeFrom');
  }

  get dateFermeTo() {
    return this.dateForm.get('dateFermeTo');
  }

  get etablissementsForm() {
    return this.dateForm.get('etablissements');
  }

  ngOnDestroy(): void {
    this.store$.dispatch(JoursCaisseActions.resetDataTableStore());
    this._alive = false;
  }

  ngOnInit(): void {
    this.store$.dispatch(LastSyncAppActions.fetchlastSync());

    this.lastSync$
      .pipe(skipWhile((x) => x === null || x === undefined))
      .subscribe((x) => {
        if (x) {
          this.lastDateSync = new Date(x);
          this.createForm();
          //this.dateFermeFrom?.setValue(this.lastDateSync);
        }
      });

    this.store$.dispatch(EtablissementActions.fetchEtablissements());
    this.etablissement$
      .pipe(skipWhile((x) => x === null || x === undefined))
      .subscribe((x) => {
        this.etablissements = x;
      });
    this.matchModeOptions = [
      { label: $localize`Début avec`, value: FilterMatchMode.STARTS_WITH },
      { label: $localize`Contient`, value: FilterMatchMode.CONTAINS },
    ];
    this.matchModeOptionsDate = [
      { label: $localize`Date supérieur`, value: FilterMatchMode.DATE_AFTER },
      { label: $localize`Date inférieur`, value: FilterMatchMode.DATE_BEFORE },
      { label: $localize`Date Egale`, value: FilterMatchMode.DATE_IS },
    ];
    //this.store$.dispatch(JoursCaisseActions.setData({data: this.data}));
    this.joursCaisses$
      .pipe(
        skipWhile((x) => x === null),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => {
        this.joursCaisses = deepCopy(x);
        this.joursCaisses.forEach((joursCaisses) => {
          joursCaisses.dateCreate = new Date(joursCaisses.dateCreate!);
          joursCaisses.dateComptage = new Date(joursCaisses.dateComptage!);
          joursCaisses.gjcDateferme = new Date(joursCaisses.gjcDateferme!);
          joursCaisses.receptionDate = new Date(joursCaisses.receptionDate!);
          joursCaisses.gjcDateouv = new Date(joursCaisses.gjcDateouv!);
          joursCaisses.validationDateFin = new Date(
            joursCaisses.validationDateFin!
          );
          joursCaisses.validationDateDebut = new Date(
            joursCaisses.validationDateDebut!
          );
          this.montantTot += joursCaisses?.montantCaisse!;
          // this.montantTot.toFixed(3)
        });
      });
  }

  createForm() {
    this.dateForm = this.fb.group({
      dateFermeFrom: [
        this.lastDateSync !== null ? this.lastDateSync : new Date(),
        Validators.required,
      ],
      dateFermeTo: ['', Validators.required],
      etablissements: [[]],
    });
  }

  copierDataBase() {
    let dateFermeDe = this.datePipe.transform(
      this.dateFermeFrom?.value,
      'dd-MM-yyyy'
    );
    let dateFermeA = this.datePipe.transform(
      this.dateFermeTo?.value,
      'dd-MM-yyyy'
    );

    if (dateFermeDe !== null && dateFermeA !== null) {
      this.loading = true;
      this.show = true;
      this.joursCaisseService
        .chargerJoursCaisseCopieDatabasePost(
          dateFermeDe,
          dateFermeA,
          this.currentUser,
          this.etablissementsForm?.value
        )
        .pipe(map((x) => x))
        .subscribe((data) => {
          console.log(data);
          if (data.data != null && !data.errorOccured) {
            this.store$.dispatch(
              JoursCaisseActions.setData({
                data: data?.data as JoursCaiseDTO[],
              })
            );
            this.jours = data?.data as JoursCaiseDTO[];
            this.loading = false;
            if (this.jours?.length > 0) {
              this.showTable = true;
              this.show = false;
            } else if (this.jours?.length === 0) {
              this.show = false;
            }

            this.toastService.success(
              this.jours?.length +
                $localize`Journées synchronisé pour cette période`,
              $localize`Information`,
              {
                duration: 5000,
                position: NbGlobalPhysicalPosition.BOTTOM_RIGHT,
              }
            );
          } else if (data?.data == null || data.errorOccured) {
            this.show = false;
            this.toastService.danger(
              'Erreur de synchronisation',
              data.message,
              {
                duration: 5000,
                position: NbGlobalPhysicalPosition.BOTTOM_RIGHT,
              }
            );
          }
        });
    } else {
      if (dateFermeDe === null || dateFermeA === null) {
        this.toastService.danger(
          $localize`Veuillez bien choisir les dates de début et de fin`,
          $localize`Information`,
          {
            duration: 5000,
            position: NbGlobalPhysicalPosition.BOTTOM_RIGHT,
          }
        );
      }
    }
  }

  getValueFilter(event: any): string {
    return event.target.value;
  }

  handleFilter(
    event: any,
    filters: { [p: string]: FilterMetadata | FilterMetadata[] }
  ) {}

  getPiedeches(joursCaise: JoursCaiseDTO) {
    this.joursCaisse = { ...joursCaise };
    this.store$.dispatch(
      PiedecheActions.fetchPiedecheByJoursCaisse({
        id: joursCaise.id,
      })
    );
    this.piedeche$
      .pipe(
        skipWhile((x) => x === null),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => (this.piedeche = x));
  }

  exportExcel() {
    import('xlsx').then((xlsx) => {
      this.dt.filteredValue === null
        ? (this.selectedjoursCaisses = this.joursCaisses)
        : (this.selectedjoursCaisses = deepCopy(this.dt.filteredValue));

      const worksheet = xlsx.utils.json_to_sheet(this.selectedjoursCaisses);
      const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, {
        bookType: 'xlsx',
        type: 'array',
      });
      this.saveAsExcelFile(excelBuffer, 'JoursCaisses');
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE =
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    FileSaver.saveAs(
      data,
      fileName +
        '_export_' +
        this.datePipe.transform(new Date(), 'YYYYMMdd hh:mm:ss') +
        EXCEL_EXTENSION
    );
  }

  exportPdf() {
    var mappedItems = [] as any;
    var foot = [] as any;
    let sum = 0;
    this.dt.filteredValue === null
      ? (this.selectedjoursCaisses = this.joursCaisses)
      : (this.selectedjoursCaisses = deepCopy(this.dt.filteredValue));
    this.selectedjoursCaisses.forEach((item, key) => {
      if (item.montantCaisse != null) {
        sum += item.montantCaisse!;
      }
      mappedItems.push({
        gjcEtablissementlabel: item.gjcEtablissement?.etLibelle,
        gjcCaisse: item.gjcCaisse,
        gjcNumzcaisse: item.gjcNumzcaisse,
        gjcDateouv: this.datePipe.transform(item.gjcDateouv, 'dd-MMM-YYYY'),
        gjcHeureouv: item.gjcHeureouv,
        gjcDateferme: this.datePipe.transform(item.gjcDateferme, 'dd-MMM-YYYY'),
        gjcHeureferme: item.gjcHeureferme,
        gjcUserferme: item.gjcUserferme,
        dateCreate: this.datePipe.transform(item.dateCreate, 'dd-MMM-YYYY'),
        montantCaisse: this.numberPipe.transform(item.montantCaisse),
      });
    });

    foot.push({
      gjcEtablissementLabel: '',
      gjcCaisse: 'Montant Total:',
      gjcNumzcaisse: '',
      gjcDateouv: '',
      gjcHeureouv: '',
      gjcDateferme: '',
      gjcHeureferme: '',
      gjcUserferme: '',
      dateCreate: '',
      montantCaisse: this.numberPipe.transform(sum),
    });
    const doc = new jsPDF('p');
    autoTable(doc, {
      columns: this.columns,
      body: mappedItems,
      foot: foot,
      margin: { top: 15, bottom: 10 },
      pageBreak: 'auto',
      showHead: 'everyPage',
      columnStyles: {
        0: { cellWidth: 'auto' },
        1: { cellWidth: 'auto' },
        2: { cellWidth: 'auto' },
        3: { cellWidth: 'auto' },
        4: { cellWidth: 'auto' },
        5: { cellWidth: 'auto' },
        6: { cellWidth: 'auto' },
        7: { cellWidth: 'auto' },
        8: { cellWidth: 'auto' },
        9: { cellWidth: 'auto', halign: 'right' },
      },

      headStyles: { fontSize: 7, fontStyle: 'bolditalic' },
      rowPageBreak: 'auto',
      footStyles: {
        fontSize: 10,
        fontStyle: 'bolditalic',
        lineColor: '',
        halign: 'right',
      },
      tableWidth: 'auto',
      styles: { overflow: 'linebreak', fontStyle: 'normal', fontSize: 7 },

      didDrawPage: (dataArg) => {
        doc.setFontSize(12);
        doc.setTextColor(20);
        doc.text('Jours Caisses', dataArg.settings.margin.left, 10);
      },
    });
    doc.save(
      'JoursCaisses_export_' +
        this.datePipe.transform(new Date(), 'YYYYMMdd hh:mm:ss') +
        '.pdf'
    );
  }

  onChangeEtab(value) {
    console.log('edfdf  ' + value);
  }
}
