import {Component, OnInit, ViewChild} from '@angular/core';
import {Observable} from 'rxjs';

// NgRx
import {Store} from '@ngrx/store';
import {
  fromEtablissement,
  fromJoursCaisse,
  fromLastSyncApp,
  fromModePaiement,
  fromPiedeche
} from "../../../../shared/reducers";
import {EtablissementDTO, JoursCaiseDTO, JoursCaisseService} from "../../../../../../swagger-api";
import {ConfirmationService, FilterService, SelectItem} from "primeng/api";
import {Table} from "primeng/table";
import {MatSnackBar} from "@angular/material/snack-bar";
import {FormBuilder, FormControl, FormGroup} from "@angular/forms";
import {PiedecheDTO} from "../../../../../../swagger-api/model/piedecheDTO";

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styles: [`
    :host ::ng-deep .p-dialog .product-image {
      width: 150px;
      margin: 0 auto 2rem auto;
      display: block;
    }
  `],
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {
  @ViewChild("dt") dt: Table;
  // @Input() data!: any[];
  // @Input() headerRow;
  date1: Date;
  headerRow: { displayName: string; key: string; hasSort: boolean, type: string }[] = [
    {displayName: $localize`Etablissement`, key: 'gjcEtablissementlabel', hasSort: true, type: 'text'},
    {displayName: $localize`Caisse`, key: 'gjcCaisse', hasSort: true, type: 'text'},
    {displayName: $localize`Référence Z`, key: 'gjcNumzcaisse', hasSort: true, type: 'text'},
    {displayName: $localize`Date d'ouverture`, key: 'gjcDateouv', hasSort: true, type: 'date'},
    {displayName: $localize`Heure d'ouverture`, key: 'gjcHeureouv', hasSort: true, type: 'text'},
    {displayName: $localize`Date de fermeture`, key: 'gjcDateferme', hasSort: true, type: 'date'},
    {displayName: $localize`Heure de fermeture`, key: 'gjcHeureferme', hasSort: true, type: 'text'},
    {displayName: $localize`User Fermer`, key: 'gjcUserferme', hasSort: true, type: 'text'},
    {displayName: $localize`Montant Total`, key: 'montantCaisse', hasSort: true, type: 'text'},

  ];
  productDialog: boolean;
  _alive: boolean = true;
  joursCaisses$: Observable<any> = this.store$.select(fromJoursCaisse.selectData);
  piedeche$: Observable<PiedecheDTO[]> = this.store$.select(fromPiedeche.selectPiedeches);
  joursCaisses: JoursCaiseDTO[];
  piedeche: PiedecheDTO[];
  jrs: JoursCaiseDTO[] = [];

  joursCaisse: JoursCaiseDTO;
  matchModeOptions: SelectItem[];
  matchModeOptionsDate: SelectItem[];
  etablissement$: Observable<EtablissementDTO[]> = this.store$.select(fromEtablissement.selectEtablissements);
  etablissements: EtablissementDTO[];
  lastSync$ = this.store$.select(
    fromLastSyncApp.getDateSync
  );
  defaultDate: Date;
  lastDateSync: string;
  dateForm: FormGroup;
  //selectedProducts: Product[];

  submitted: boolean;
  Delete: "Delete";
  etablissement: EtablissementDTO;
  searchControl = new FormControl('')
  data$: Observable<any[]> = this.store$.select(fromJoursCaisse.selectData);

  constructor(private store$: Store<fromJoursCaisse.State & fromPiedeche.State & fromEtablissement.State & fromModePaiement.State>
    , private filterService: FilterService
    , private confirmationService: ConfirmationService,
              private joursCaisseService: JoursCaisseService,
              public snackBar: MatSnackBar, private fb: FormBuilder) {
  }

  get dateC() {
    return this.dateForm.get("dateC");
  }

  ngOnInit(): void {


  }

}
