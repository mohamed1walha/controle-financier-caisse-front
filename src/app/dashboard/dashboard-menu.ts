import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: $localize`Dashboard`,
    icon: 'home-outline',
    link: '/dashboard/home',
  },
  {
    title: $localize`Echange des données`,
    group: true,
  },
  {
    title: $localize`Echange des données`,
    icon: 'layout-outline',
    children: [
      {
        title: $localize`Synchroniser Journées`,
        icon: 'sync-outline',
        link: '/dashboard/interfacage/synchroniser-journee',
      },
      {
        title: $localize`Liste des journées`,
        icon: 'swap-outline',
        link: '/dashboard/interfacage/charger-journee',
      },
    ],
  },
  {
    title: $localize`Réception des Journées`,
    icon: 'grid-outline',
    children: [
      {
        title: $localize`Journées en attente`,
        icon: 'repeat-outline',
        link: '/dashboard/reception-enveloppe/enveloppe-non-receptionne',
      },
      {
        title: $localize`Journées réceptionnées`,
        icon: 'checkmark-circle-outline',

        link: '/dashboard/reception-enveloppe/enveloppe-receptionne',
      },
    ],
  },
  {
    title: $localize`Comptage et vérification`,
    icon: 'keypad-outline',

    children: [
      {
        title: $localize`Journées non comptées`,
        icon: 'close-circle-outline',
        link: '/dashboard/comptage/enveloppe-non-compter',
      },
      {
        title: $localize`Journées comptées`,
        icon: { icon: 'validation', pack: 'ic' },
        link: '/dashboard/comptage/enveloppe-compter',
      },
      {
        title: $localize`Rapport`,
        icon: { icon: 'validation', pack: 'ic' },
        children: [
          {
            title: $localize`Etat des journées`,
            link: '/dashboard/comptage/rapport',
          },
          {
            title: $localize`Détails des billets`,
            link: '/dashboard/comptage/billets',
          },
        ],
      },
    ],
  },
  {
    title: $localize`Paramètrage`,
    icon: 'edit-2-outline',
    children: [
      {
        title: $localize`Emetteurs`,
        link: '/dashboard/parametrage/emetteur',
      },
      {
        title: $localize`Chauffeurs`,
        link: '/dashboard/parametrage/chauffeur',
      },
      {
        title: $localize`Motifs de resynchronisation`,
        link: '/dashboard/parametrage/motifresync',
      },
      {
        title: $localize`Motifs d"ecart`,
        link: '/dashboard/parametrage/motif',
      },
      {
        title: $localize`Modes de paiement`,
        link: '/dashboard/parametrage/parametrage-paiement',
      },
      {
        title: $localize`Devise`,
        link: '/dashboard/parametrage/devise',
      },
      {
        title: $localize`Paramétrage des billet`,
        link: '/dashboard/parametrage/param-billet',
      },
    ],
  },
  {
    title: $localize`Coffres`,
    group: true,
  },
  {
    title: $localize`Gestion des coffres`,
    icon: 'browser-outline',
    children: [
      {
        title: $localize`Paramètrage`,
        link: '/dashboard/coffre/parametrage',
      },
      {
        title: $localize`Alimentation`,
        link: '/dashboard/coffre/alimentation',
      },
      {
        title: $localize`Sortie`,
        link: '/dashboard/coffre/sortie',
      },
      {
        title: $localize`Solde et mouvements`,
        link: '/dashboard/coffre/solde',
      },
    ],
  },
  {
    title: $localize`Utilisateurs`,
    group: true,
  },
  {
    title: $localize`Gestion des utilisateurs`,
    icon: 'people',
    children: [
      {
        title: $localize`Utilisateurs`,
        link: '/dashboard/users/users',
        icon: 'person',
      },
      // {
      //   title: $localize`Roles`,
      //   link: '/dashboard/users/roles',
      //   icon: 'funnel'      },
    ],
  },

  {
    title: $localize`Dépenses`,
    group: true,
  },
  {
    title: $localize`Dépenses`,
    icon: 'browser-outline',
    children: [
      {
        title: $localize`Dépenses`,
        link: '/dashboard/depense/depenses',
      },
      {
        title: $localize`Prélevements`,
        link: '/dashboard/depense/prelevements',
      },
      {
        title: $localize`Paramètrage`,
        link: '/dashboard/depense/parametrage',
      },
    ],
  },
];
