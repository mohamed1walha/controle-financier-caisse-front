import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {
  FilterMatchMode,
  FilterMetadata,
  PrimeNGConfig,
  SelectItem,
} from 'primeng/api';
import { ActivatedRoute, Router } from '@angular/router';

import { Store } from '@ngrx/store';
import { Table } from 'primeng/table';
import {
  EnteteDepenseDTO,
  EtablissementDTO,
  StatusDTO,
  UserDTO,
} from 'swagger-api';
import { Observable, skipWhile, takeWhile } from 'rxjs';
import {
  fromArticle,
  fromCompteDepense,
  fromEnteteDepense,
  fromEtablissement,
  fromFournisseur,
  fromParcaisse,
  fromStatus,
  fromUsers,
} from '../../../shared/reducers';
import {
  EnteteDepenseActions,
  EtablissementActions,
  FournisseurActions,
  StatusActions,
  UsersActions,
} from '../../../shared/actions';
import { deepCopy } from '../../../shared/utils/deep-copy';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-depense-list',
  templateUrl: './depense-list.component.html',
  styleUrls: ['./depense-list.component.scss'],
})
export class DepenseListComponent implements OnInit, OnDestroy {
  @ViewChild('dt') dt: Table;
  headerRow: {
    displayName: string;
    key: string;
    hasSort: boolean;
    type: string;
  }[] = [
    /*
        {displayName: "Référence", key: 'id', hasSort: true, type: 'text'},
    */
    {
      displayName: $localize`Etablissement`,
      key: 'gjcEtablissement.etLibelle',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Utilisateur`,
      key: 'userCreatedFullName',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Validateur`,
      key: 'userValidatorFullName',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Type de dépense`,
      key: 'typeDepense',
      hasSort: true,
      type: 'boolean',
    },
    {
      displayName: $localize`Date dépense`,
      key: 'dateDepense',
      hasSort: true,
      type: 'date',
    },
    {
      displayName: $localize`Date de creation`,
      key: 'createDate',
      hasSort: true,
      type: 'date',
    },
    {
      displayName: $localize`Montant Total`,
      key: 'totalTtc',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Status`,
      key: 'status.statuslibelle',
      hasSort: true,
      type: 'text',
    },
  ];
  matchModeOptions: SelectItem[];
  matchModeOptionsDate: SelectItem[];
  enteteDepense: EnteteDepenseDTO;
  currentUser = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .currentUser;
  compteDepenseId = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .compteDepenseId;
  etablissementId = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .etablissementId;

  loading: boolean = true;
  enteteDepenses: EnteteDepenseDTO[] = [];
  enteteDepense$: Observable<EnteteDepenseDTO[]> = this.store$.select(
    fromEnteteDepense.selectEnteteDepenses
  );
  etablissements$: Observable<EtablissementDTO[]> = this.store$.select(
    fromEtablissement.selectEtablissements
  );
  montantTot: number = 0;

  user$: Observable<UserDTO> = this.store$.select(fromUsers.selectUser);
  user: UserDTO;
  status$: Observable<StatusDTO[]> = this.store$.select(
    fromStatus.selectStatuss
  );
  statuses: StatusDTO[];

  etablissements: EtablissementDTO[] = [];
  isActive: boolean = true;
  validation: boolean = false;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private primengConfig: PrimeNGConfig,
    private store$: Store<
      fromUsers.State &
        fromEtablissement.State &
        fromCompteDepense.State &
        fromEnteteDepense.State &
        fromArticle.State &
        fromParcaisse.State &
        fromFournisseur.State
    >
  ) {}

  ngOnDestroy(): void {
    this.isActive = false;
    this.store$.dispatch(
      EnteteDepenseActions.setEnteteDepenses({ enteteDepenses: [] })
    );
  }

  ngOnInit() {
    this.validation = this.router.url.includes('validation-depense');
    this.matchModeOptions = [
      { label: 'Début avec', value: FilterMatchMode.STARTS_WITH },
      { label: 'Contient', value: FilterMatchMode.CONTAINS },
    ];
    this.matchModeOptionsDate = [
      { label: 'Date supérieur', value: FilterMatchMode.DATE_AFTER },
      { label: 'Date inférieur', value: FilterMatchMode.DATE_BEFORE },
      { label: 'Date Egale', value: FilterMatchMode.DATE_IS },
    ];

    this.primengConfig.ripple = true;
    this.loading = false;
    this.store$.dispatch(StatusActions.fetchStatuss());

    this.store$.dispatch(
      UsersActions.fetchUserByUserName({ userName: this.currentUser })
    );
    this.user$
      .pipe(
        skipWhile((x) => x === null || x.id === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.user = x;

        this.store$.dispatch(EtablissementActions.fetchEtablissements());
        if (!this.validation) {
          this.store$.dispatch(
            EnteteDepenseActions.fetchAllEnteteDepensesWithEtabAndUserId({
              etablissementId: this.etablissementId,
              userId: x.id!,
            })
          );
        } else {
          this.store$.dispatch(
            EnteteDepenseActions.fetchAllEnteteDepensesWithValidatorId({
              userId: x.id!,
            })
          );
        }
      });

    this.status$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive),
        map((x) =>
          x.filter((x) => x.code == 'DMD' || x.code == 'RJT' || x.code == 'ACP')
        )
      )
      .subscribe((x) => {
        return (this.statuses = deepCopy(x));
      });
    this.store$.dispatch(FournisseurActions.fetchFournisseurs());
    this.etablissements$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.etablissements = x;
      });
    this.enteteDepense$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.montantTot = 0;
        this.enteteDepenses = deepCopy(x);
        this.enteteDepenses.forEach((entete) => {
          this.montantTot += entete?.totalTtc!;
        });
      });
  }

  navigate(value: string) {
    this.router.navigate(['create'], { relativeTo: this.activatedRoute });
  }

  handleFilter(
    filteredData: any[],
    filters: { [p: string]: FilterMetadata | FilterMetadata[] }
  ) {
    this.montantTot = 0;
    filteredData?.forEach((data) => {
      this.montantTot += data?.totalTtc;
    });
  }

  selectEntete(entete: any) {
    // console.log("erssssrrrrrrrrrrrrrrrrrrrssssssrrrrrrrr")
    //
    //
    // this.router.navigate(['edit', entete?.id], {
    //   relativeTo: this.activatedRoute.parent,
    // });
  }
}
