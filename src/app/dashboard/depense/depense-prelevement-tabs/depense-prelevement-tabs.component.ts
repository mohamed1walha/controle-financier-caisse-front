import { Component, OnInit } from '@angular/core';
import {
  EnteteDepenseActions,
  EntetePrelevementActions,
} from '../../../shared/actions';
import { Observable, takeWhile } from 'rxjs';
import {
  fromArticle,
  fromCompteDepense,
  fromEntetePrelevement,
  fromEtablissement,
  fromFournisseur,
  fromParcaisse,
  fromUsers,
} from '../../../shared/reducers';
import { Store } from '@ngrx/store';
import { UserDTO } from '../../../../../swagger-api';
import { ActivatedRoute, Router } from '@angular/router';
import { NbTabComponent, NbTabsetComponent } from '@nebular/theme';

@Component({
  selector: 'app-depense-prelevement-tabs',
  templateUrl: './depense-prelevement-tabs.component.html',
  styleUrls: ['./depense-prelevement-tabs.component.scss'],
})
export class DepensePrelevementTabsComponent implements OnInit {
  tabId = '0';
  etabId: any;
  numZRef: any;
  caisseId: any;
  user$: Observable<UserDTO> = this.store$.select(fromUsers.selectUser);
  user: UserDTO;
  currentUser = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .currentUser;
  isActive: boolean = true;
  etablissementId = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .etablissementId;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private store$: Store<
      fromUsers.State &
        fromEtablissement.State &
        fromCompteDepense.State &
        fromEntetePrelevement.State &
        fromArticle.State &
        fromParcaisse.State &
        fromFournisseur.State
    >
  ) {}

  ngOnInit(): void {
    this.activatedRoute.params
      .pipe(takeWhile(() => this.isActive))
      .subscribe((params) => {
        this.etabId = params['etabId'];
        this.caisseId = params['caisseId'];
        this.numZRef = params['numZRef'];
      });
    // this.store$.dispatch(
    //   UsersActions.fetchUserByUserName({ userName: this.currentUser })
    // );
    // this.user$
    //   .pipe(
    //     skipWhile((x) => x === null || x.id === undefined),
    //     takeWhile(() => this.isActive)
    //   )
    //   .subscribe((x) => {
    //     this.user = x;
    //     console.log(x.id);
    //     this.store$.dispatch(
    //       EntetePrelevementActions.fetchAllEntetePrelevementsWithEtabAndUserId({
    //         etablissementId: this.etablissementId,
    //         userId: this.user.id!,
    //       })
    //     );
    //   });
  }

  changeTab(value: NbTabComponent) {
    this.store$.dispatch(
      EntetePrelevementActions.setEntetePrelevements({ entetePrelevements: [] })
    );
    this.tabId = value.tabId;
    if (value.tabId == '0') {
      this.store$.dispatch(
        EntetePrelevementActions.fetchAllEntetePrelevementsWithEtabAndNumZRefAndcaisseId(
          {
            etabId: this.etabId,
            numZRef: this.numZRef,
            caisseId: this.caisseId,
          }
        )
      );
    } else {
      this.store$.dispatch(
        EnteteDepenseActions.fetchAllEnteteDepensesWithEtabAndNumZRefAndcaisseId(
          {
            etabId: this.etabId,
            numZRef: this.numZRef,
            caisseId: this.caisseId,
          }
        )
      );
    }
  }
}
