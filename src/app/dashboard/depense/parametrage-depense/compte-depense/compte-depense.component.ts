import { Component, OnInit } from '@angular/core';
import {
  fromBlade,
  fromCompteDepense,
  fromEtablissement,
} from '../../../../shared/reducers';
import { Store } from '@ngrx/store';
import {
  ConfirmationService,
  FilterMatchMode,
  MessageService,
  SelectItem,
} from 'primeng/api';
import { Observable, skipWhile, takeWhile } from 'rxjs';
import {
  CompteDepenseDTO,
  EtablissementDTO,
} from '../../../../../../swagger-api';
import {
  CompteDepenseActions,
  EtablissementActions,
} from '../../../../shared/actions';

@Component({
  selector: 'app-compte-depense',
  templateUrl: './compte-depense.component.html',
  styleUrls: ['./compte-depense.component.scss'],
})
export class CompteDepenseComponent implements OnInit {
  compteDepenses$ = this.store$.select(fromCompteDepense.selectCompteDepenses);
  compteDepenses: CompteDepenseDTO[];
  selectCompteDepense: CompteDepenseDTO;
  compteDepense: CompteDepenseDTO;
  compteDepenseDialog: boolean;
  submitted: boolean;
  _alive: boolean = true;
  matchModeOptions: SelectItem[];
  matchModeOptionsVide: SelectItem[];
  headerRow: {
    displayName: string;
    key: string;
    hasSort: boolean;
    type: string;
  }[] = [
    {
      displayName: $localize`Libelle`,
      key: 'libelle',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Compte Caisse G`,
      key: 'compteCaisseG',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Solde`,
      key: 'solde',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Montant Limite`,
      key: 'montantLimite',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Journal`,
      key: 'journal',
      hasSort: true,
      type: 'text',
    },
  ];
  etablissement$: Observable<EtablissementDTO[]> = this.store$.select(
    fromEtablissement.selectEtablissements
  );
  etablissements: EtablissementDTO[];

  constructor(
    private store$: Store<
      fromCompteDepense.State & fromBlade.State & fromEtablissement.State
    >,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.store$.dispatch(EtablissementActions.fetchEtablissements());
    this.etablissement$
      .pipe(skipWhile((x) => x === null || x === undefined))
      .subscribe((x) => {
        this.etablissements = x;
      });
    this.store$.dispatch(CompteDepenseActions.fetchCompteDepenses());
    this.compteDepenses$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => (this.compteDepenses = x));
    this.matchModeOptions = [
      { label: $localize`Début avec`, value: FilterMatchMode.STARTS_WITH },
      { label: $localize`Contient`, value: FilterMatchMode.CONTAINS },
    ];
  }

  openNew() {
    this.compteDepense = { solde: 0 };
    this.submitted = false;
    this.compteDepenseDialog = true;
  }

  getValueFilter(event: any): string {
    return event.target.value;
  }

  deleteCompteDepense(compteDepense: CompteDepenseDTO) {
    this.confirmationService.confirm({
      message:
        $localize`Etes-vous sûr que vous voulez supprimer ` +
        compteDepense.libelle +
        '?',
      header: $localize`Confirmer`,
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.store$.dispatch(
          CompteDepenseActions.DeleteCompteDepense({
            compteDepense: compteDepense,
          })
        );
      },
      reject: () => {
        this.confirmationService.close();
      },
    });
  }

  hideDialog() {
    this.compteDepenseDialog = false;
    this.submitted = false;
  }

  editCompteDepense(compteDepense: CompteDepenseDTO) {
    this.compteDepense = { ...compteDepense };
    this.compteDepenseDialog = true;
  }

  saveCompteDepense() {
    this.submitted = true;
    if (this.compteDepense.id) {
      this.store$.dispatch(
        CompteDepenseActions.EditCompteDepense({
          compteDepense: this.compteDepense,
        })
      );
      this.compteDepenseDialog = false;
    } else {
      this.store$.dispatch(
        CompteDepenseActions.CreateCompteDepense({
          compteDepense: this.compteDepense,
        })
      );
      this.compteDepenseDialog = false;
    }
  }
}
