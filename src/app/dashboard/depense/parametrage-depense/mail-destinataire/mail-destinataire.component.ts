import {Component, OnInit} from '@angular/core';
import {fromBlade, fromMailDestinataire} from "../../../../shared/reducers";
import {Store} from "@ngrx/store";
import {ConfirmationService, FilterMatchMode, MessageService, SelectItem} from "primeng/api";
import {skipWhile, takeWhile} from "rxjs";
import {MailDestinataireDTO} from "../../../../../../swagger-api";
import {MailDestinataireActions} from "../../../../shared/actions";

@Component({
  selector: 'app-mail-destinataire',
  templateUrl: './mail-destinataire.component.html',
  styleUrls: ['./mail-destinataire.component.scss']
})
export class MailDestinataireComponent implements OnInit {
  mailDestinataires$ = this.store$.select(
    fromMailDestinataire.selectMailDestinataires
  );
  mailDestinataires: MailDestinataireDTO[];
  selectMailDestinataire: MailDestinataireDTO
  mailDestinataire: MailDestinataireDTO;
  mailDestinataireDialog: boolean;
  submitted: boolean;
  _alive: boolean = true;
  matchModeOptions: SelectItem[];
  matchModeOptionsVide: SelectItem[];
  headerRow: { displayName: string; key: string; hasSort: boolean, type: string }[] = [
    {displayName: $localize`Libelle`, key: 'libelle', hasSort: true, type: 'text'},
    {displayName: $localize`Adresse Mail`, key: 'adresseMail', hasSort: true, type: 'text'},


  ];

  constructor(private store$: Store<fromMailDestinataire.State & fromBlade.State>, private confirmationService: ConfirmationService,
              private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.store$.dispatch(MailDestinataireActions.fetchMailDestinataires());
    this.mailDestinataires$.pipe(
      skipWhile((x) => x === null || x === undefined),
      takeWhile(() => this._alive)
    ).subscribe(x =>
      this.mailDestinataires = x
    );
    this.matchModeOptions = [
      {label: $localize`Début avec`, value: FilterMatchMode.STARTS_WITH},
      {label: $localize`Contient`, value: FilterMatchMode.CONTAINS}
    ];

  }

  openNew() {
    this.mailDestinataire = {};
    this.submitted = false;
    this.mailDestinataireDialog = true;
  }

  getValueFilter(event: any): string {
    return event.target.value;
  }

  deleteMailDestinataire(mailDestinataire: MailDestinataireDTO) {
    this.confirmationService.confirm({
      message: $localize`Etes-vous sûr que vous voulez supprimer ` + mailDestinataire.libelle + '?',
      header: $localize`Confirmer`,
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.store$.dispatch(MailDestinataireActions.DeleteMailDestinataire({mailDestinataire: mailDestinataire}));
      },
      reject: () => {
        this.confirmationService.close();
      }
    });
  }

  hideDialog() {
    this.mailDestinataireDialog = false;
    this.submitted = false;
  }

  editMailDestinataire(mailDestinataire: MailDestinataireDTO) {
    this.mailDestinataire = {...mailDestinataire};
    this.mailDestinataireDialog = true;
  }

  saveMailDestinataire() {
    this.submitted = true;
    if (this.mailDestinataire.id) {
      this.store$.dispatch(MailDestinataireActions.EditMailDestinataire({mailDestinataire: this.mailDestinataire}));
      this.mailDestinataireDialog = false;
    } else {
      this.store$.dispatch(MailDestinataireActions.CreateMailDestinataire({mailDestinataire: this.mailDestinataire}));
      this.mailDestinataireDialog = false;
    }
  }
}

