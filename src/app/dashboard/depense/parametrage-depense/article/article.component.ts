import {Component, OnInit} from '@angular/core';
import {fromArticle, fromBlade} from "../../../../shared/reducers";
import {Store} from "@ngrx/store";
import {ConfirmationService, FilterMatchMode, MessageService, SelectItem} from "primeng/api";
import {skipWhile, takeWhile} from "rxjs";
import {ArticleDTO} from "../../../../../../swagger-api";
import {ArticleActions} from "../../../../shared/actions";

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss']
})
export class ArticleComponent implements OnInit {
  articles$ = this.store$.select(
    fromArticle.selectArticles
  );
  articles: ArticleDTO[];
  selectArticle: ArticleDTO
  article: ArticleDTO;
  articleDialog: boolean;
  submitted: boolean;
  _alive: boolean = true;
  matchModeOptions: SelectItem[];
  headerRow: { displayName: string; key: string; hasSort: boolean, type: string }[] = [
    {displayName: $localize`Code`, key: 'code', hasSort: true, type: 'text'},
    {displayName: $localize`Designation`, key: 'designation', hasSort: true, type: 'text'},
    {displayName: $localize`Prix HT`, key: 'prixHT', hasSort: true, type: 'text'},
    {displayName: $localize`Compte C`, key: 'compteC', hasSort: true, type: 'text'},
    {displayName: $localize`Taux TVA`, key: 'tauxTVA', hasSort: true, type: 'text'},
    {displayName: $localize`Compte TVA`, key: 'compteTVA', hasSort: true, type: 'text'},
    {displayName: $localize`Type Tva`, key: 'typeTva', hasSort: true, type: 'text'},
    {displayName: $localize`Controle Solde`, key: 'controleSolde', hasSort: false, type: 'boolean'},

  ];

  constructor(private store$: Store<fromArticle.State & fromBlade.State>, private confirmationService: ConfirmationService,
              private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.store$.dispatch(ArticleActions.fetchArticles());
    this.articles$.pipe(
      skipWhile((x) => x === null || x === undefined),
      takeWhile(() => this._alive)
    ).subscribe(x =>
      this.articles = x
    );
    this.matchModeOptions = [
      {label: $localize`Début avec`, value: FilterMatchMode.STARTS_WITH},
      {label: $localize`Contient`, value: FilterMatchMode.CONTAINS}
    ];
  }

  openNew() {
    this.article = {};
    this.submitted = false;
    this.articleDialog = true;
  }

  getValueFilter(event: any): string {
    return event.target.value;
  }

  deleteArticle(article: ArticleDTO) {
    this.confirmationService.confirm({
      message: $localize`Etes-vous sûr que vous voulez supprimer ` + article.code + '?',
      header: $localize`Confirmer`,
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.store$.dispatch(ArticleActions.DeleteArticle({article: article}));
      },
      reject: () => {
        this.confirmationService.close();
      }
    });
  }

  hideDialog() {
    this.articleDialog = false;
    this.submitted = false;
  }

  editArticle(article: ArticleDTO) {
    this.article = {...article};
    this.articleDialog = true;
  }

  saveArticle() {
    this.submitted = true;
    if (this.article.id) {
      this.store$.dispatch(ArticleActions.EditArticle({article: this.article}));
      this.articleDialog = false;
    } else {
      this.store$.dispatch(ArticleActions.CreateArticle({article: this.article}));
      this.articleDialog = false;
    }
  }
}

