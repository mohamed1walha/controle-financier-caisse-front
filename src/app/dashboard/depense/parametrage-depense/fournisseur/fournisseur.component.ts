import {Component, OnInit} from '@angular/core';
import {fromBlade, fromFournisseur} from "../../../../shared/reducers";
import {Store} from "@ngrx/store";
import {ConfirmationService, FilterMatchMode, MessageService, SelectItem} from "primeng/api";
import {skipWhile, takeWhile} from "rxjs";
import {FournisseurDTO} from "../../../../../../swagger-api";
import {FournisseurActions} from "../../../../shared/actions";

@Component({
  selector: 'app-fournisseur',
  templateUrl: './fournisseur.component.html',
  styleUrls: ['./fournisseur.component.scss']
})
export class FournisseurComponent implements OnInit {
  fournisseurs$ = this.store$.select(
    fromFournisseur.selectFournisseurs
  );
  fournisseurs: FournisseurDTO[];
  selectFournisseur: FournisseurDTO
  fournisseur: FournisseurDTO;
  fournisseurDialog: boolean;
  submitted: boolean;
  _alive: boolean = true;
  matchModeOptions: SelectItem[];
  matchModeOptionsVide: SelectItem[];
  headerRow: { displayName: string; key: string; hasSort: boolean, type: string }[] = [
    {displayName: $localize`Code`, key: 'code', hasSort: true, type: 'text'},
    {displayName: $localize`Libelle`, key: 'libelle', hasSort: true, type: 'text'},
    {displayName: $localize`CompteC`, key: 'compteC', hasSort: true, type: 'text'},
    {displayName: $localize`Est Assujeti ?`, key: 'assujeti', hasSort: true, type: 'boolean'},
    {displayName: $localize`Par Défaut?`, key: 'isDefault', hasSort: true, type: 'boolean'},

  ];

  constructor(private store$: Store<fromFournisseur.State & fromBlade.State>, private confirmationService: ConfirmationService,
              private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.store$.dispatch(FournisseurActions.fetchFournisseurs());
    this.fournisseurs$.pipe(
      skipWhile((x) => x === null || x === undefined),
      takeWhile(() => this._alive)
    ).subscribe(x =>
      this.fournisseurs = x
    );
    this.matchModeOptions = [
      {label: $localize`Début avec`, value: FilterMatchMode.STARTS_WITH},
      {label: $localize`Contient`, value: FilterMatchMode.CONTAINS}
    ];

  }

  openNew() {
    this.fournisseur = {};
    this.submitted = false;
    this.fournisseurDialog = true;
  }

  getValueFilter(event: any): string {
    return event.target.value;
  }

  deleteFournisseur(fournisseur: FournisseurDTO) {
    this.confirmationService.confirm({
      message: $localize`Etes-vous sûr que vous voulez supprimer ` + fournisseur.code + '?',
      header: $localize`Confirmer`,
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.store$.dispatch(FournisseurActions.DeleteFournisseur({fournisseur: fournisseur}));
      },
      reject: () => {
        this.confirmationService.close();
      }
    });
  }

  hideDialog() {
    this.fournisseurDialog = false;
    this.submitted = false;
  }

  editFournisseur(fournisseur: FournisseurDTO) {
    this.fournisseur = {...fournisseur};
    this.fournisseurDialog = true;
  }

  saveFournisseur() {
    this.submitted = true;
    if (this.fournisseur.id) {
      this.store$.dispatch(FournisseurActions.EditFournisseur({fournisseur: this.fournisseur}));
      this.fournisseurDialog = false;
    } else {
      this.store$.dispatch(FournisseurActions.CreateFournisseur({fournisseur: this.fournisseur}));
      this.fournisseurDialog = false;
    }
  }
}

