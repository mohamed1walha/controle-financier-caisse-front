import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';
import {
  NbButtonModule,
  NbCardModule,
  NbIconModule,
  NbInputModule,
  NbSelectModule,
  NbTabsetModule,
} from '@nebular/theme';
import { RippleModule } from 'primeng/ripple';
import { NgxFileDropModule } from 'ngx-file-drop';
import { DepenseRoutingModule } from './depense-routing.module';
import { ArticleComponent } from './parametrage-depense/article/article.component';
import { FournisseurComponent } from './parametrage-depense/fournisseur/fournisseur.component';
import { CheckboxModule } from 'primeng/checkbox';
import { CompteDepenseComponent } from './parametrage-depense/compte-depense/compte-depense.component';
import { MailDestinataireComponent } from './parametrage-depense/mail-destinataire/mail-destinataire.component';
import { DepensePrelevementTabsComponent } from './depense-prelevement-tabs/depense-prelevement-tabs.component';
import { PrelevementComponent } from './prelevement-list/prelevement.component';
import { DepenseListComponent } from './depense-list/depense-list.component';
import { PrelevementsComponent } from './prelevements/prelevements.component';
import { DepensesComponent } from './depenses/depenses.component';
import { PrelevementCreateModalComponent } from './prelevements/prelevement-create-modal/prelevement-create-modal.component';
import { DepenseCreateModalComponent } from './depenses/depense-create-modal/depense-create-modal.component';
import { DatePickerModule } from '@progress/kendo-angular-dateinputs';
import { ToggleButtonModule } from 'primeng/togglebutton';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatBadgeModule} from "@angular/material/badge";

@NgModule({
  declarations: [
    ...DepenseRoutingModule.components,
    ArticleComponent,
    FournisseurComponent,
    CompteDepenseComponent,
    MailDestinataireComponent,
    DepensePrelevementTabsComponent,
    PrelevementComponent,
    DepenseListComponent,
    DepenseCreateModalComponent,
    PrelevementsComponent,
    DepensesComponent,
    PrelevementCreateModalComponent,
  ],
    imports: [
        SharedModule,
        DepenseRoutingModule,
        NbCardModule,
        NbTabsetModule,
        NbButtonModule,
        NbInputModule,
        NbSelectModule,
        RippleModule,
        NgxFileDropModule,
        CheckboxModule,
        NbIconModule,
        DatePickerModule,
        ToggleButtonModule,
        MatCheckboxModule,
        MatBadgeModule
    ],
})
export class DepenseModule {}
