import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ParametrageDepenseComponent } from './parametrage-depense/parametrage-depense.component';
import { DepensePrelevementTabsComponent } from './depense-prelevement-tabs/depense-prelevement-tabs.component';
import { DepensesComponent } from './depenses/depenses.component';
import { PrelevementsComponent } from './prelevements/prelevements.component';
import { PrelevementCreateModalComponent } from './prelevements/prelevement-create-modal/prelevement-create-modal.component';
import { DepenseCreateModalComponent } from './depenses/depense-create-modal/depense-create-modal.component';

const routes: Routes = [
  {
    path: 'parametrage',
    component: ParametrageDepenseComponent,
  },
  {
    path: '',
    children: [
      {
        path: 'depenses',
        children: [
          {
            path: '',
            component: DepensesComponent,
          },
          {
            path: 'edit/:id',
            component: DepenseCreateModalComponent,
          },
        ],
      },
      {
        path: 'prelevements',
        children: [
          {
            path: '',
            component: PrelevementsComponent,
          },
          {
            path: 'edit/:id',
            component: PrelevementCreateModalComponent,
          },
        ],
      },
      {
        path: ':etabId/:caisseId/:numZRef',
        component: DepensePrelevementTabsComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DepenseRoutingModule {
  static components = [ParametrageDepenseComponent];
}
