import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import {
  FilterMatchMode,
  FilterMetadata,
  PrimeNGConfig,
  SelectItem,
} from 'primeng/api';
import { ActivatedRoute, Router } from '@angular/router';
import {
  EntetePrelevementDTO,
  EtablissementDTO,
  StatusDTO,
  UserDTO,
} from '../../../../../swagger-api';
import { Store } from '@ngrx/store';
import {
  fromArticle,
  fromCompteDepense,
  fromEntetePrelevement,
  fromEtablissement,
  fromFournisseur,
  fromParcaisse,
  fromStatus,
  fromUsers,
} from '../../../shared/reducers';
import {
  EntetePrelevementActions,
  EtablissementActions,
  FournisseurActions,
  StatusActions,
  UsersActions,
} from '../../../shared/actions';
import { Observable, skipWhile, takeWhile } from 'rxjs';
import { deepCopy } from '../../../shared/utils/deep-copy';
import { Table } from 'primeng/table';
import { map } from 'rxjs/operators';
import { NbDialogService } from '@nebular/theme';

@Component({
  selector: 'app-prelevement',
  templateUrl: './prelevement.component.html',
  styleUrls: ['./prelevement.component.scss'],
})
export class PrelevementComponent implements OnInit, OnDestroy {
  @ViewChild('dt') dt: Table;
  headerRow: {
    displayName: string;
    key: string;
    hasSort: boolean;
    type: string;
  }[] =
    [
    /*
        {displayName: "Référence", key: 'id', hasSort: true, type: 'text'},
    */
    {
      displayName: $localize`Etablissement`,
      key: 'gjcEtablissement.etLibelle',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Utilisateur`,
      key: 'userCreatedFullName',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Validateur`,
      key: 'userValidatorFullName',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Type de prélevement`,
      key: 'typePrelevement',
      hasSort: true,
      type: 'boolean',
    },
    {
      displayName: $localize`Date prélevement`,
      key: 'datePrelevement',
      hasSort: true,
      type: 'date',
    },
    {
      displayName: $localize`Date de creation`,
      key: 'createDate',
      hasSort: true,
      type: 'date',
    },

    {
      displayName: $localize`Montant Total`,
      key: 'montantTotal',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Status`,
      key: 'status.statuslibelle',
      hasSort: true,
      type: 'text',
    },
  ];
  matchModeOptions: SelectItem[];
  matchModeOptionsDate: SelectItem[];
  entetePrelevement: EntetePrelevementDTO;
  currentUser = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .currentUser;
  comptePrelevementId = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .comptePrelevementId;
  etablissementId = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .etablissementId;
  roles = JSON.parse(localStorage.getItem('currentUser') || '{}').roles;

  loading: boolean = true;
  entetePrelevements: EntetePrelevementDTO[] = [];
  entetePrelevement$: Observable<EntetePrelevementDTO[]> = this.store$.select(
    fromEntetePrelevement.selectEntetePrelevements
  );
  etablissements$: Observable<EtablissementDTO[]> = this.store$.select(
    fromEtablissement.selectEtablissements
  );
  validation: boolean = false;

  montantTot: number = 0;

  user$: Observable<UserDTO> = this.store$.select(fromUsers.selectUser);
  user: UserDTO;
  status$: Observable<StatusDTO[]> = this.store$.select(
    fromStatus.selectStatuss
  );
  statuses: StatusDTO[];

  etablissements: EtablissementDTO[] = [];
  isActive: boolean = true;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private primengConfig: PrimeNGConfig,
    private dialog: NbDialogService,
    private store$: Store<
      fromUsers.State &
        fromEtablissement.State &
        fromCompteDepense.State &
        fromEntetePrelevement.State &
        fromArticle.State &
        fromParcaisse.State &
        fromFournisseur.State
    >
  ) {}

  handleFilter(
    filteredData: any[],
    filters: { [p: string]: FilterMetadata | FilterMetadata[] }
  ) {
    this.montantTot = 0;
    filteredData?.forEach((data) => {
      this.montantTot += data?.montantTotal;
    });
  }

  ngOnInit() {
    this.validation = this.router.url.includes('validation-prelevement');

    this.matchModeOptions = [
      { label: 'Début avec', value: FilterMatchMode.STARTS_WITH },
      { label: 'Contient', value: FilterMatchMode.CONTAINS },
    ];
    this.matchModeOptionsDate = [
      { label: 'Date supérieur', value: FilterMatchMode.DATE_AFTER },
      { label: 'Date inférieur', value: FilterMatchMode.DATE_BEFORE },
      { label: 'Date Egale', value: FilterMatchMode.DATE_IS },
    ];

    this.primengConfig.ripple = true;
    this.loading = false;
    this.store$.dispatch(StatusActions.fetchStatuss());

    this.store$.dispatch(
      UsersActions.fetchUserByUserName({ userName: this.currentUser })
    );

    this.status$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive),
        map((x) =>
          x.filter((x) => x.code == 'DMD' || x.code == 'RJT' || x.code == 'ACP')
        )
      )
      .subscribe((x) => {
        return (this.statuses = deepCopy(x));
      });
    this.store$.dispatch(FournisseurActions.fetchFournisseurs());
    this.etablissements$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.etablissements = x;
      });
    this.entetePrelevement$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.montantTot = 0;
        this.entetePrelevements = deepCopy(x);
        this.entetePrelevements.forEach((entete) => {
          this.montantTot += entete?.montantTotal!;
        });
      });
  }

  ngOnDestroy(): void {
    this.isActive = false;
    this.store$.dispatch(
      EntetePrelevementActions.setEntetePrelevements({ entetePrelevements: [] })
    );
  }

  navigate(value: string) {
    this.router.navigate([value], { relativeTo: this.activatedRoute });
  }

  selectEntete(entete: any) {
    //
    // if(this.tabId===0) {
    //
    //   this.router.navigate(['edit', entete?.id], {
    //     relativeTo: this.activatedRoute.parent,
    //   });
    // }else{
    //   this.router.navigate(['editP', entete?.id, this.tabId], {
    //     relativeTo: this.activatedRoute.parent,
    //   });
    // }
  }
}
