import {Component, OnInit, ViewChild} from '@angular/core';
import {Table} from 'primeng/table';
import {FilterMatchMode, FilterMetadata, LazyLoadEvent, PrimeNGConfig, SelectItem,} from 'primeng/api';
import {
  EnteteDepenseDTO,
  EnteteDepenseService,
  EtablissementDTO,
  FilterDepancesDTO,
  LigneDepenseDTO,
  StatusDTO,
  UserDTO,
} from '../../../../../swagger-api';
import {Observable, skipWhile, takeWhile} from 'rxjs';
import {
  fromArticle,
  fromCompteDepense,
  fromEnteteDepense,
  fromEtablissement,
  fromFournisseur,
  fromLigneDepense,
  fromParcaisse,
  fromStatus,
  fromUsers,
} from '../../../shared/reducers';
import {ActivatedRoute, Router} from '@angular/router';
import {NbDialogService} from '@nebular/theme';
import {Store} from '@ngrx/store';
import {
  EnteteDepenseActions,
  EtablissementActions,
  FournisseurActions,
  LigneDepenseActions,
  StatusActions,
  UsersActions,
} from '../../../shared/actions';
import {map} from 'rxjs/operators';
import {deepCopy} from '../../../shared/utils/deep-copy';
import * as FileSaver from 'file-saver';
import {DatePipe} from "@angular/common";
import {RequestOptions} from "../services";

@Component({
  selector: 'app-depenses',
  templateUrl: './depenses.component.html',
  styleUrls: ['./depenses.component.scss'],
})
export class DepensesComponent implements OnInit {
  @ViewChild('dt') dt: Table;
  headerRow: {
    displayName: string;
    key: string;
    hasSort: boolean;
    type: string;
  }[] =
    [
    {
      displayName: $localize`Etablissement`,
      key: 'gjcEtablissement.etLibelle',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Utilisateur`,
      key: 'userCreatedFullName',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Validateur`,
      key: 'userValidatorFullName',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Type de dépense`,
      key: 'typeDepense',
      hasSort: true,
      type: 'boolean',
    },
    {
      displayName: $localize`Date dépense`,
      key: 'dateDepense',
      hasSort: true,
      type: 'date',
    },
    {
      displayName: $localize`Date de creation`,
      key: 'createDate',
      hasSort: true,
      type: 'date',
    },
    {
      displayName: $localize`Montant Total`,
      key: 'totalTtc',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Status`,
      key: 'status.statuslibelle',
      hasSort: true,
      type: 'text',
    },
  ];
  matchModeOptions: SelectItem[];
  matchModeOptionsDate: SelectItem[];
  enteteDepense: EnteteDepenseDTO;
  currentUser = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .currentUser;
  compteDepenseId = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .compteDepenseId;
  etablissementId = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .etablissementId;
  roles = JSON.parse(localStorage.getItem('currentUser') || '{}').roles;
  ligneDepense$: Observable<LigneDepenseDTO[]> = this.store$.select(
    fromLigneDepense.selectLigneDepenses
  );
  isLoading$: Observable<boolean> = this.store$.select(
    fromEnteteDepense.selectIsLoading
  );
  loading: boolean = false;
  enteteDepenses: any;
  ligneDepenses: LigneDepenseDTO[] = [];
  etablissements$: Observable<EtablissementDTO[]> = this.store$.select(
    fromEtablissement.selectEtablissements
  );
  users$: Observable<UserDTO[]> = this.store$.select(fromUsers.selectUsersSimpleData);

  montantTot: number = 0;

  user$: Observable<UserDTO> = this.store$.select(fromUsers.selectUser);
  user: UserDTO;
  status$: Observable<StatusDTO[]> = this.store$.select(
    fromStatus.selectStatuss
  );
  statuses: StatusDTO[];

  etablissements: EtablissementDTO[] = [];

  typeDepances: any[] = [{value : false , label : 'Direct'},{value : true , label : 'Fond de dépense'}];

  isActive: boolean = true;
  selectedDepanses: EnteteDepenseDTO[] = [];
  totalRecords: any;
  showPaginator = true;

  filter: FilterDepancesDTO = { };
  users: UserDTO[] = [];
  currentEventGrid: LazyLoadEvent = {};
  changeFilter: boolean = false;
  checked: boolean = true;
  showExportCompta: boolean =false;

  constructor(
    private enteteDepenseService: EnteteDepenseService,

    private router: Router,
    private activatedRoute: ActivatedRoute,
    private primengConfig: PrimeNGConfig,
    private dialog: NbDialogService,
    private datePipe: DatePipe,
    private store$: Store<
      fromUsers.State &
        fromEtablissement.State &
        fromCompteDepense.State &
        fromEnteteDepense.State &
        fromArticle.State &
        fromParcaisse.State &
        fromFournisseur.State &
        fromLigneDepense.State
    >
  ) {}

  ngOnInit() {
    this.filter.isCompta = false;
    this.filter.dateDepanceFrom=new Date(Date.now() - 86400000*7);
    this.filter.dateDepanceTo=new Date();

    this.matchModeOptions = [
      { label: 'Début avec', value: FilterMatchMode.STARTS_WITH },
      { label: 'Contient', value: FilterMatchMode.CONTAINS },
    ];
    this.matchModeOptionsDate = [
      { label: 'Date supérieur', value: FilterMatchMode.DATE_AFTER },
      { label: 'Date inférieur', value: FilterMatchMode.DATE_BEFORE },
      { label: 'Date Egale', value: FilterMatchMode.DATE_IS },
    ];

    this.primengConfig.ripple = true;
    this.loading = false;

    this.store$.dispatch(UsersActions.fetchUsersSimpleData());
    this.store$.dispatch(
      UsersActions.fetchUserByUserName({ userName: this.currentUser })
    );
    this.store$.dispatch(StatusActions.fetchStatuss());
    this.store$.dispatch(EtablissementActions.fetchEtablissements());

    this.user$
      .pipe(
        skipWhile((x) => x === null || x.id === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.user = x;
      });
    this.users$
      .subscribe(res=>{
        if (res){
          this.users=res;
        }
      });

    this.isLoading$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.loading = x;
      });

    this.status$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive),
        map((x) =>
          x.filter((x) => x.code == 'DMD' || x.code == 'RJT' || x.code == 'ACP')
        )
      )
      .subscribe((x) => {
        if (x){
          this.filter.statutId = x.find(element=>element.code==='ACP')?.id;
          this.showExportCompta = true;
          return (this.statuses = deepCopy(x));
        }
      });
    this.store$.dispatch(FournisseurActions.fetchFournisseurs());
    this.etablissements$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.etablissements = x;
      });
    this.ligneDepense$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.ligneDepenses = x;
      });
  }
  ngOnDestroy(): void {
    this.isActive = false;
    this.store$.dispatch(
      EnteteDepenseActions.setEnteteDepenses({ enteteDepenses: [] })
    );
  }
  navigate(entete) {
    this.router.navigate(['edit', entete?.id], {
      relativeTo: this.activatedRoute.parent,
    });
  }
  handleFilter(
    filteredData: any[],
    filters: { [p: string]: FilterMetadata | FilterMetadata[] }
  ) {
    this.montantTot = 0;
    filteredData?.forEach((data) => {
      this.montantTot += data?.totalTtc;
    });
  }
  selectEntete(entete: any) {
    this.store$.dispatch(
      LigneDepenseActions.fetchLigneDepensesWithEnteteId({
        enteteId: entete?.id,
      })
    );
  }
  generer() {
    this.enteteDepenseService.enteteDepenseGenerateComptablePost(
      this.selectedDepanses
    ).subscribe((res)=>{
      if(res?.data !== null) {
        this.selectedDepanses=[];
        let files = res?.data as string[];
        let i =1;
        if (files.length=== 1){
          i=3;
        }
        files.forEach(element=>{
          if(element){
            const base64String = element;
            const byteCharacters = atob(base64String);
            const byteNumbers = new Array(byteCharacters.length);
            for (let i = 0; i < byteCharacters.length; i++) {
              byteNumbers[i] = byteCharacters.charCodeAt(i);
            }
            const byteArray = new Uint8Array(byteNumbers);
            const blob = new Blob([byteArray], { type: 'application/octet-stream' });
            FileSaver.saveAs(blob, 'ANNEXE '+i +' '+this.datePipe.transform(new Date(), 'dd-MM-yyyy HH-mm-ss') + '.tra');
            i++;
          }
        })
      }
      this.recherche();
    });
  }
  onLazyLoad(event: LazyLoadEvent): void {

    if (this.filter.dateDepanceFrom instanceof Date) {
      this.filter.dateDepanceFrom.setHours(0, 0, 0, 0);
    }

    if (this.filter.dateDepanceTo instanceof Date) {
      this.filter.dateDepanceTo.setHours(0, 0, 0, 0);
    }


    if (this.filter.dateCreationFrom instanceof Date) {
      this.filter.dateCreationFrom.setHours(0, 0, 0, 0);
    }

    if (this.filter.dateCreationTo instanceof Date) {
      this.filter.dateCreationTo.setHours(0, 0, 0, 0);
    }


    this.currentEventGrid=event;
    this.loading = true;
    this.enteteDepenses = [];
    const grid = new RequestOptions(event,this.filter);
    if(this.changeFilter===true){
      grid.page=1
      this.changeFilter=false;
    }
    this.enteteDepenseService.enteteDepenseGetAllLazyPost(grid)
      .subscribe(results => {
        this.totalRecords = results.totalRecords;
        this.loading = false;
        this.montantTot = 0;
        this.enteteDepenses = deepCopy(results?.records);
        this.enteteDepenses.forEach((entete) => {
          this.montantTot += entete?.totalTtc!;
        });
      });
  }
  recherche() {
    this.changeFilter=true;
    this.onLazyLoad(this.currentEventGrid);
  }
  onTabSelectionChange(event) {
    this.selectedDepanses=[];
    const selectedTabIndex = event.index;
    if (this.statuses[selectedTabIndex].id){
      this.filter.statutId = this.statuses[selectedTabIndex].id;
      if(this.statuses[selectedTabIndex].code === 'ACP'){
        this.showExportCompta = true;
      }else{
        this.showExportCompta = false;
      }
      this.recherche();
    }
  }

  rechercheCheckbox() {
    this.selectedDepanses=[];
    this.recherche();
  }
}
