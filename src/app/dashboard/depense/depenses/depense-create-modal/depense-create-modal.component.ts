import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { Observable, skipWhile, takeWhile } from 'rxjs';

import { ConfirmationService, PrimeNGConfig } from 'primeng/api';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import {
  ArticleActions,
  CompteDepenseActions,
  EnteteDepenseActions,
  EtablissementActions,
  FournisseurActions, ParcaisseActions,
} from '../../../../shared/actions';
import {
  ArticleDTO,
  CompteDepenseDTO,
  EnteteDepenseDTO,
  EtablissementDTO,
  FournisseurDTO, ParcaisseDTO,
  UserDTO,
} from 'swagger-api';
import {
  fromArticle,
  fromCompteDepense,
  fromEnteteDepense,
  fromEtablissement,
  fromFournisseur,
  fromParcaisse,
  fromUsers,
} from 'src/app/shared/reducers';
import { environment } from '../../../../../environments/environment';
import { deepCopy } from '../../../../shared/utils/deep-copy';

@Component({
  selector: 'app-prelevement-create-modal',
  templateUrl: './depense-create-modal.component.html',
  styleUrls: ['./depense-create-modal.component.scss'],
})
export class DepenseCreateModalComponent implements OnInit, OnDestroy {
  edit: boolean = false;
  id: number;
  currentUser = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .currentUser;
  compteDepenseId = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .compteDepenseId;
  etablissementId = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .etablissementId;
  roles: string[] = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .roles;
  etablissement$: Observable<EtablissementDTO> = this.store$.select(
    fromEtablissement.selectEtablissement
  );
  enteteDepense$: Observable<EnteteDepenseDTO> = this.store$.select(
    fromEnteteDepense.selectEnteteDepense
  );

  articles$: Observable<ArticleDTO[]> = this.store$.select(
    fromArticle.selectArticles
  );

  fournisseur$: Observable<FournisseurDTO> = this.store$.select(
    fromFournisseur.selectFournisseur
  );
  fournisseurs$: Observable<FournisseurDTO[]> = this.store$.select(
    fromFournisseur.selectFournisseurs
  );
  parcaisses$: Observable<ParcaisseDTO[]> = this.store$.select(
    fromParcaisse.selectParcaisses
  );
  etablissement: EtablissementDTO;
  fournisseur: FournisseurDTO;
  fournisseurs: FournisseurDTO[];
  user: UserDTO;
  enteteDepense: EnteteDepenseDTO;
  usersValidators: UserDTO[];
  compteDepense: CompteDepenseDTO;
  articles: ArticleDTO[];
  parcaisses: ParcaisseDTO[];
  isActive: boolean = true;
  enteteDepenseForm: FormGroup;
  url = environment.apiUrl;
  isChangingTauxTVA: boolean = false;
  isChangingMontantHT: boolean = false;
  isChangingMontantTVA: boolean = false;
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private confirmationService: ConfirmationService,
    private store$: Store<
      fromUsers.State &
        fromEtablissement.State &
        fromCompteDepense.State &
        fromEnteteDepense.State &
        fromArticle.State &
        fromParcaisse.State &
        fromFournisseur.State
    >,
    private primengConfig: PrimeNGConfig,
    private toastService: NbToastrService,
    private dialog: NbDialogService
  ) {}

  get ligneDepenses(): FormArray {
    return this.enteteDepenseForm.get('ligneDepenses') as FormArray;
  }

  get totalHt() {
    return this.enteteDepenseForm.get('totalHt')?.value;
  }

  get etablissementIdForm() {
    return this.enteteDepenseForm.get('etablissementId')?.value;
  }

  set etablissementIdForm(value) {
    this.enteteDepenseForm.get('etablissementId')?.setValue(value);
  }

  get statutLabel() {
    return this.enteteDepenseForm.get('statutLabel')?.value;
  }

  get totalTva() {
    return this.enteteDepenseForm.get('totalTva')?.value;
  }

  get totalTtc() {
    return this.enteteDepenseForm.get('totalTtc')?.value;
  }

  get fournisseurId() {
    return this.enteteDepenseForm.get('fournisseurId');
  }

  set fournisseurId(value) {
    this.enteteDepenseForm.get('fournisseurId')?.setValue(value);
  }

  get dateDepense() {
    return this.enteteDepenseForm.get('dateDepense')?.value;
  }

  get compteSource() {
    return this.enteteDepenseForm.get('compteSource')?.value;
  }

  set compteSource(value) {
    this.enteteDepenseForm.get('compteSource')?.setValue(value);
  }

  get dateReponse() {
    return this.enteteDepenseForm.get('dateReponse')?.value;
  }

  get pieceJointe() {
    return this.enteteDepenseForm.get('pieceJointe')?.value;
  }

  set pieceJointe(value) {
    this.enteteDepenseForm.get('pieceJointe')?.setValue(value);
  }

  get typeDepense() {
    return this.enteteDepenseForm.get('typeDepense');
  }

  get fileBase64() {
    return this.enteteDepenseForm.get('fileBase64')?.value;
  }

  set fileBase64(value) {
    this.enteteDepenseForm.get('fileBase64')?.setValue(value);
  }

  get validateurUserId() {
    return this.enteteDepenseForm.get('validateurUserId');
  }

  set validateurUserId(value) {
    this.enteteDepenseForm.get('validateurUserId')?.setValue(value);
  }

  ngOnInit(): void {
    this.createFrom();
    this.activatedRoute.params
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((params) => {
        if (params['id']) {
          this.edit = true;
          this.id = params['id'];
          this.store$.dispatch(
            EnteteDepenseActions.fetchEnteteDepense({ id: this.id })
          );
          this.enteteDepenseForm.controls['typeDepense'].disable();
        } else {
          this.edit = false;
          this.store$.dispatch(ArticleActions.fetchArticles());
        }
      });
    this.primengConfig.ripple = true;
    // this.store$.dispatch(ParcaisseActions.fetchParcaisses());

    this.store$.dispatch(FournisseurActions.fetchFournisseurs());
    this.store$.dispatch(
      CompteDepenseActions.fetchCompteDepenseById({ id: this.compteDepenseId })
    );
    this.parcaisses$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.parcaisses = x;
      });
    this.enteteDepense$
      .pipe(
        skipWhile((x) => x.id === null || x.id === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.ligneDepenses.clear();

        this.enteteDepense = deepCopy(x);
        this.store$.dispatch(
          EtablissementActions.fetchEtablissementById({
            etablissementId: this.enteteDepense?.etablissementId!,
          })
        );
        if (this.enteteDepense?.typeDepense) {
          this.store$.dispatch(ArticleActions.fetchArticlesWithControlSolde());
        } else {
          this.store$.dispatch(ArticleActions.fetchArticles());
        }

        this.enteteDepense?.ligneDepenses?.forEach((ligne) => {
          return (
            this.enteteDepenseForm.get('ligneDepenses') as FormArray
          ).push(this.createItem());
        });
        this.enteteDepenseForm.patchValue({
          ...this.enteteDepense,
          dateDepense:
            this.enteteDepense.dateDepense != null
              ? new Date(this.enteteDepense?.dateDepense!)
              : new Date(),
        });

        this.enteteDepenseForm.disable();
        this.disableInputs();
      });
    this.etablissement$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.etablissement = x;
        this.store$.dispatch(
          ParcaisseActions.fetchParcaissesByEtabId({
            etabId: this.etablissement?.etEtablissement,
          })
        );
      });
    this.fournisseur$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.fournisseur = x;
        this.fournisseurId?.setValue(this.fournisseur?.id);
      });
    this.fournisseurs$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.fournisseurs = x;
      });

    this.articles$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.articles = x;
      });
  }

  ngOnDestroy() {
    this.isActive = false;
    this.store$.dispatch(
      EnteteDepenseActions.setEnteteDepense({ enteteDepense: {} })
    );
    this.enteteDepenseForm.reset();
    this.ligneDepenses.reset();
  }

  createFrom() {
    this.enteteDepenseForm = this.formBuilder.group({
      id: 0,
      fournisseurId: [0],
      etablissementId: [null, Validators.required],
      validateurUserId: [null],
      typeDepense: false,
      statutId: [null],
      statutLabel: [''],
      dateReponse: [],
      dateDepense: [new Date()],
      compteSource: [''],
      commentaire: [''],
      pieceJointe: [''],
      fileBase64: [null],
      codeJournal: [''],
      totalHt: [0],
      totalTva: [0],
      totalTtc: [0],
      ligneDepenses: this.formBuilder.array([]),
    });
  }

  createItem(): FormGroup {
    return this.formBuilder.group({
      id: [0],
      depenseId: [0],
      article: [null, Validators.required],
      montantHt: [''],
      tauxTva: [''],
      compteTva: [''],
      montantTva: [''],
      montantTtc: [''],
      caisse: [null, Validators.required],
      numZref: [null],
    });
  }

  navigate() {
    this.router.navigate(['/dashboard/depense/depenses']);
  }

  changeArticle(articleValue, i: number) {
    const article = articleValue.value as ArticleDTO;
    const montantTVA = (article?.prixHt! * article?.tauxTva!) / 100;
    const montantTTC = montantTVA + article?.prixHt!;
    this.ligneDepenses.controls[i].get('montantHt')?.setValue(article?.prixHt),
      this.ligneDepenses.controls[i].get('tauxTva')?.setValue(article?.tauxTva),
      this.ligneDepenses.controls[i].get('montantTva')?.setValue(montantTVA),
      this.ligneDepenses.controls[i].get('montantTtc')?.setValue(montantTTC),
      this.ligneDepenses.controls[i]
        .get('compteTva')
        ?.setValue(article?.compteTva);
  }

  changeTTC(): number {
    let ttc: number = 0;
    if (this.ligneDepenses?.length > 0) {
      for (let i = 0; i < this.ligneDepenses?.length; i++) {
        ttc += this.ligneDepenses.controls[i].get('montantTtc')?.value;
      }
    }
    return ttc;
  }

  changeHT(): number {
    let ht: number = 0;
    if (this.ligneDepenses?.length > 0) {
      for (let i = 0; i < this.ligneDepenses?.length; i++) {
        ht += this.ligneDepenses.controls[i].get('montantHt')?.value;
      }
    }
    return ht;
  }

  changeTVA(): number {
    let tva: number = 0;
    if (this.ligneDepenses?.length > 0) {
      for (let i = 0; i < this.ligneDepenses?.length; i++) {
        tva += this.ligneDepenses.controls[i].get('montantTva')?.value;
      }
    }
    return tva;
  }

  changeMontantHT(i: number) {
    // Ajoutez un drapeau pour éviter les appels récursifs
    if (!this.isChangingMontantHT) {
      this.isChangingMontantHT = true;

      this.ligneDepenses.controls[i]
        .get('montantTtc')
        ?.setValue(
          this.ligneDepenses.controls[i].get('montantHt')?.value +
            this.ligneDepenses.controls[i].get('montantTva')?.value
        );
      this.changeTauxTVA(i);

      // Remettez le drapeau à false après avoir terminé
      this.isChangingMontantHT = false;
    }
  }

  changeMontantTVA(i: number) {
    // Ajoutez un drapeau pour éviter les appels récursifs
    if (!this.isChangingMontantTVA) {
      this.isChangingMontantTVA = true;

      this.changeMontantHT(i);

      // Remettez le drapeau à false après avoir terminé
      this.isChangingMontantTVA = false;
    }
  }

  changeTauxTVA(i: number) {
    // Ajoutez un drapeau pour éviter les appels récursifs
    if (!this.isChangingTauxTVA) {
      this.isChangingTauxTVA = true;

      this.ligneDepenses.controls[i]
        .get('montantTva')
        ?.setValue(
          (this.ligneDepenses.controls[i].get('montantHt')?.value *
            this.ligneDepenses.controls[i].get('tauxTva')?.value) /
            100
        );
      this.changeMontantHT(i);

      // Remettez le drapeau à false après avoir terminé
      this.isChangingTauxTVA = false;
    }
  }
  disableInputs() {
    (<FormArray>this.enteteDepenseForm.get('ligneDepenses')).controls.forEach(
      (control) => {
        console.log('control ' + control);
        control.disable();
      }
    );
  }
}
