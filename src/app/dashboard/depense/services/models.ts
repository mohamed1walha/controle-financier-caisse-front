import {LazyLoadEvent} from 'primeng/api';


export class RequestOptions {
  page = 1;
  pageSize = 10;
  sortField = '';
  sortDirection: 'asc' | 'desc' = 'asc';
  searchTerm = '';
  filters={};
  //filters: { [field: string]: any; } | null = null;

  constructor(event?: LazyLoadEvent, extraFilters?:any) {
    if (event && event.rows) {
      if(event.first){
        this.page = (event.first / event.rows) + 1;
      }else{
        this.page =  1;
      }
      this.pageSize = event.rows;
      this.sortField = event.sortField ?? '';
      this.sortDirection = event.sortOrder === -1 ? 'desc' : 'asc';
      this.searchTerm = event.globalFilter ?? '';
      this.filters=extraFilters;
      //this.filters = {};

      /*for (const key in event.filters) {
        if (Object.prototype.hasOwnProperty.call(event.filters, key)) {
          this.filters[key] = event.filters[key][0].value;
        }
      }*/
    }
  }
}
