import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { Observable, skipWhile, takeWhile } from 'rxjs';

import { ConfirmationService, PrimeNGConfig } from 'primeng/api';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { NbToastrService } from '@nebular/theme';
import {
  CompteDepenseDTO,
  EntetePrelevementDTO,
  EtablissementDTO,
  ParcaisseDTO,
  UserDTO,
} from 'swagger-api';
import {
  fromCompteDepense,
  fromEntetePrelevement,
  fromEtablissement,
  fromParcaisse,
  fromUsers,
} from '../../../../shared/reducers';
import { environment } from 'src/environments/environment';
import {
  CompteDepenseActions,
  EntetePrelevementActions,
  EtablissementActions,
  ParcaisseActions,
} from '../../../../shared/actions';
import { deepCopy } from '../../../../shared/utils/deep-copy';

@Component({
  selector: 'app-prelevement-create-modal',
  templateUrl: './prelevement-create-modal.component.html',
  styleUrls: ['./prelevement-create-modal.component.scss'],
  providers: [ConfirmationService],
})
export class PrelevementCreateModalComponent implements OnInit, OnDestroy {
  edit: boolean = false;
  id: number;
  currentUser = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .currentUser;
  compteDepenseId = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .compteDepenseId;
  etablissementId = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .etablissementId;
  roles: string[] = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .roles;
  etablissement$: Observable<EtablissementDTO> = this.store$.select(
    fromEtablissement.selectEtablissement
  );
  entetePrelevement$: Observable<EntetePrelevementDTO> = this.store$.select(
    fromEntetePrelevement.selectEntetePrelevement
  );

  tabId: number = 0;

  parcaisses$: Observable<ParcaisseDTO[]> = this.store$.select(
    fromParcaisse.selectParcaisses
  );
  compteDepense$: Observable<CompteDepenseDTO> = this.store$.select(
    fromCompteDepense.selectCompteDepense
  );

  etablissement: EtablissementDTO;
  user: UserDTO;
  entetePrelevement: EntetePrelevementDTO;
  compteDepense: CompteDepenseDTO;
  parcaisses: ParcaisseDTO[];
  isActive: boolean = true;
  entetePrelevementForm: FormGroup;
  url = environment.apiUrl;
  solde: number = 0;
  montantTotalExeptionnel: number = 0;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private confirmationService: ConfirmationService,
    private store$: Store<
      fromUsers.State &
        fromEtablissement.State &
        fromCompteDepense.State &
        fromEntetePrelevement.State &
        fromParcaisse.State
    >,
    private primengConfig: PrimeNGConfig,
    private toastService: NbToastrService
  ) {}

  get lignePrelevements(): FormArray {
    return this.entetePrelevementForm.get('lignePrelevements') as FormArray;
  }

  get etablissementIdForm() {
    return this.entetePrelevementForm.get('etablissementId')?.value;
  }

  set etablissementIdForm(value) {
    this.entetePrelevementForm.get('etablissementId')?.setValue(value);
  }

  get statutLabel() {
    return this.entetePrelevementForm.get('statutLabel')?.value;
  }

  get montantTotal() {
    return this.entetePrelevementForm.get('montantTotal');
  }

  get datePrelevement() {
    return this.entetePrelevementForm.get('datePrelevement')?.value;
  }

  get dateReponse() {
    return this.entetePrelevementForm.get('dateReponse')?.value;
  }

  get pieceJointe() {
    return this.entetePrelevementForm.get('pieceJointe')?.value;
  }

  set pieceJointe(value) {
    this.entetePrelevementForm.get('pieceJointe')?.setValue(value);
  }

  get typePrelevement() {
    return this.entetePrelevementForm.get('typePrelevement');
  }

  get fileBase64() {
    return this.entetePrelevementForm.get('fileBase64')?.value;
  }

  set fileBase64(value) {
    this.entetePrelevementForm.get('fileBase64')?.setValue(value);
  }

  get validateurUserId() {
    return this.entetePrelevementForm.get('validateurUserId');
  }

  set validateurUserId(value) {
    this.entetePrelevementForm.get('validateurUserId')?.setValue(value);
  }

  ngOnInit(): void {
    this.createFrom();
    this.activatedRoute.params
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((params) => {
        if (params['id']) {
          this.tabId = params['tabId'];
          this.edit = true;
          this.id = params['id'];
          this.store$.dispatch(
            EntetePrelevementActions.fetchEntetePrelevement({ id: this.id })
          );
          this.entetePrelevementForm.controls['typePrelevement'].disable();
        } else {
          this.edit = false;
        }
      });
    this.primengConfig.ripple = true;
    this.store$.dispatch(
      CompteDepenseActions.fetchCompteDepenseById({ id: this.compteDepenseId })
    );

    this.entetePrelevement$
      .pipe(
        skipWhile((x) => x.id === null || x.id === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.lignePrelevements.clear();
        this.entetePrelevement = deepCopy(x);
        console.log(this.entetePrelevement?.etablissementId);

        this.store$.dispatch(
          EtablissementActions.fetchEtablissementById({
            etablissementId: this.entetePrelevement?.etablissementId!,
          })
        );
        this.montantTotalExeptionnel = this.entetePrelevement?.montantTotal!;

        this.entetePrelevement?.lignePrelevements?.forEach((ligne) => {
          if (ligne) {
            (
              this.entetePrelevementForm.get('lignePrelevements') as FormArray
            ).push(this.createItem());
          }
        });
        this.entetePrelevementForm.patchValue({
          ...this.entetePrelevement,
          datePrelevement:
            this.entetePrelevement.datePrelevement != null
              ? new Date(this.entetePrelevement?.datePrelevement!)
              : new Date(),
        });

        this.entetePrelevementForm.disable();
        this.disableInputs();
      });
    this.etablissement$
      .pipe(
        skipWhile((x) => x === null || x.etEtablissement === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.etablissement = x;
        console.log('ddddddddd   ' + x);
        this.store$.dispatch(
          ParcaisseActions.fetchParcaissesByEtabId({
            etabId: this.etablissement?.etEtablissement,
          })
        );
      });

    this.compteDepense$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.solde = 0;
        this.compteDepense = x;
        this.solde = x?.montantLimite! - x?.solde!;
      });

    this.parcaisses$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.parcaisses = x;
      });
  }

  ngOnDestroy() {
    this.isActive = false;
    this.store$.dispatch(
      EntetePrelevementActions.setEntetePrelevement({ entetePrelevement: {} })
    );
    this.entetePrelevementForm.reset();
    this.lignePrelevements.reset();
  }

  createFrom() {
    this.entetePrelevementForm = this.formBuilder.group({
      id: 0,
      etablissementId: [null, Validators.required],
      validateurUserId: [null],
      typePrelevement: false,
      statutId: [null],
      statutLabel: [''],
      dateReponse: [],
      datePrelevement: [new Date()],
      commentaire: [''],
      pieceJointe: [''],
      fileBase64: [null],
      codeJournal: [''],
      montantTotal: [0],
      lignePrelevements: this.formBuilder.array([]),
    });
  }

  createItem(): FormGroup {
    return this.formBuilder.group({
      id: [0],
      prelevementId: [0],
      parcaisse: [null],
      parcaisseId: [null],
      montant: [''],
      numZref: [''],
    });
  }

  navigate() {
    this.router.navigate(['/dashboard/depense/prelevements']);
  }

  getFileAttachements(file: any) {
    console.log(file);
    this.pieceJointe = file[0]?.fileName;
    this.fileBase64 = file[0]?.fileBase64;
  }

  changeTTC(): number {
    let ttc: number = 0;
    if (this.lignePrelevements?.length > 0) {
      for (let i = 0; i < this.lignePrelevements?.length; i++) {
        ttc += this.lignePrelevements.controls[i].get('montant')?.value;
      }
    }
    this.montantTotal?.setValue(ttc);
    return ttc;
  }

  disableInputs() {
    (<FormArray>(
      this.entetePrelevementForm.get('lignePrelevements')
    )).controls.forEach((control) => {
      console.log('control ' + control);
      control.disable();
    });
  }
}
