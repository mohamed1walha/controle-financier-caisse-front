import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {FilterMatchMode, FilterMetadata, LazyLoadEvent, PrimeNGConfig, SelectItem,} from 'primeng/api';
import {ActivatedRoute, Router} from '@angular/router';
import {
  EntetePrelevementDTO, EntetePrelevmentService,
  EtablissementDTO,
  FilterPrelevementDTO,
  LignePrelevementDTO,
  StatusDTO,
  UserDTO,
} from '../../../../../swagger-api';
import {Store} from '@ngrx/store';
import {
  fromArticle,
  fromCompteDepense,
  fromEntetePrelevement,
  fromEtablissement,
  fromFournisseur,
  fromLignePrelevement,
  fromParcaisse,
  fromStatus,
  fromUsers,
} from '../../../shared/reducers';
import {
  EntetePrelevementActions,
  EtablissementActions,
  LignePrelevementActions,
  StatusActions,
  UsersActions,
} from '../../../shared/actions';
import {Observable, skipWhile, takeWhile} from 'rxjs';
import {deepCopy} from '../../../shared/utils/deep-copy';
import {Table} from 'primeng/table';
import {map} from 'rxjs/operators';
import {NbDialogService} from '@nebular/theme';
import * as FileSaver from 'file-saver';
import {DatePipe} from "@angular/common";
import {RequestOptions} from "../services";

@Component({
  selector: 'app-prelevements',
  templateUrl: './prelevements.component.html',
  styleUrls: ['./prelevements.component.scss'],
})
export class PrelevementsComponent implements OnInit, OnDestroy {
  @ViewChild('dt') dt: Table;
  headerRow: {
    displayName: string;
    key: string;
    hasSort: boolean;
    type: string;
  }[] = [
    /*
        {displayName: "Référence", key: 'id', hasSort: true, type: 'text'},
    */
    {
      displayName: $localize`Etablissement`,
      key: 'etablissement.etLibelle',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Utilisateur`,
      key: 'userCreatedFullName',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Validateur`,
      key: 'userValidatorFullName',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Type de prélevement`,
      key: 'typePrelevement',
      hasSort: true,
      type: 'boolean',
    },
    {
      displayName: $localize`Date prélevement`,
      key: 'datePrelevement',
      hasSort: true,
      type: 'date',
    },
    {
      displayName: $localize`Date de creation`,
      key: 'createDate',
      hasSort: true,
      type: 'date',
    },

    {
      displayName: $localize`Montant Total`,
      key: 'montantTotal',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Status`,
      key: 'status.statuslibelle',
      hasSort: true,
      type: 'text',
    },
  ];
  matchModeOptions: SelectItem[];
  matchModeOptionsDate: SelectItem[];
  entetePrelevement: EntetePrelevementDTO;
  currentUser = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .currentUser;
  comptePrelevementId = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .comptePrelevementId;
  etablissementId = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .etablissementId;
  roles = JSON.parse(localStorage.getItem('currentUser') || '{}').roles;
  lignePrelevement$: Observable<LignePrelevementDTO[]> = this.store$.select(
    fromLignePrelevement.selectLignePrelevements
  );
  isLoading$: Observable<boolean> = this.store$.select(
    fromEntetePrelevement.selectIsLoading
  );
  users$: Observable<UserDTO[]> = this.store$.select(fromUsers.selectUsersSimpleData);
  users: UserDTO[] = [];
  loading: boolean = true;
  entetePrelevements: EntetePrelevementDTO[] = [];
  lignePrelevements: LignePrelevementDTO[] = [];
  entetePrelevement$: Observable<EntetePrelevementDTO[]> = this.store$.select(
    fromEntetePrelevement.selectEntetePrelevements
  );
  etablissements$: Observable<EtablissementDTO[]> = this.store$.select(
    fromEtablissement.selectEtablissements
  );
  filter: FilterPrelevementDTO = { };
  datePrelevementFrom = null;
  montantTot: number = 0;
  selectedPrelevements: EntetePrelevementDTO[] = [];
  checked: boolean = true;
  prelevementDialog: boolean = false;
  codeJournal: string = '';
  user$: Observable<UserDTO> = this.store$.select(fromUsers.selectUser);
  user: UserDTO;
  status$: Observable<StatusDTO[]> = this.store$.select(
    fromStatus.selectStatuss
  );
  statuses: StatusDTO[];

  etablissements: EtablissementDTO[] = [];
  isActive: boolean = true;

  files$: Observable<any> = this.store$.select(
    fromEntetePrelevement.selectAnnexesFile
  );

  showExportCompta: boolean =false;
  typePrelevements: any[] = [{value : false , label : 'Normal'},{value : true , label : 'Exceptionnel'}];
  currentEventGrid: LazyLoadEvent = {};
  changeFilter: boolean = false;
  totalRecords: any;
  showPaginator = true;


  constructor(
    private entetePrelevementService: EntetePrelevmentService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private primengConfig: PrimeNGConfig,
    private dialog: NbDialogService,
    private datePipe: DatePipe,
    private store$: Store<
      fromUsers.State &
        fromEtablissement.State &
        fromCompteDepense.State &
        fromCompteDepense.State &
        fromEntetePrelevement.State &
        fromArticle.State &
        fromParcaisse.State &
        fromFournisseur.State &
        fromLignePrelevement.State
    >
  ) {}

  ngOnInit() {
    this.filter.isCompta = false;
    this.filter.datePrelevementFrom=new Date(Date.now() - 86400000*7);
    this.filter.datePrelevementTo=new Date();

    this.matchModeOptions = [
      { label: 'Début avec', value: FilterMatchMode.STARTS_WITH },
      { label: 'Contient', value: FilterMatchMode.CONTAINS },
    ];
    this.matchModeOptionsDate = [
      { label: 'Date supérieur', value: FilterMatchMode.DATE_AFTER },
      { label: 'Date inférieur', value: FilterMatchMode.DATE_BEFORE },
      { label: 'Date Egale', value: FilterMatchMode.DATE_IS },
    ];
    this.users$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => (this.users = x));
    this.primengConfig.ripple = true;
    this.store$.dispatch(StatusActions.fetchStatuss());

    this.store$.dispatch(UsersActions.fetchUsersSimpleData());
    this.store$.dispatch(
      UsersActions.fetchUserByUserName({ userName: this.currentUser })
    );
    this.user$
      .pipe(
        skipWhile((x) => x === null || x.id === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.user = x;
        this.store$.dispatch(EtablissementActions.fetchEtablissements());
        this.store$.dispatch(
          EntetePrelevementActions.fetchAllEntetePrelevements()
        );
      });

    this.status$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive),
        map((x) =>
          x.filter((x) => x.code == 'DMD' || x.code == 'RJT' || x.code == 'ACP')
        )
      )
      .subscribe((x) => {
        if (x){
          this.filter.statutId = x.find(element=>element.code==='ACP')?.id;
          this.showExportCompta = true;
          return (this.statuses = deepCopy(x));
        }
      });
    this.etablissements$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.etablissements = x;
      });
    this.lignePrelevement$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.lignePrelevements = x;
      });
    this.isLoading$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.loading = x;
      });
    this.entetePrelevement$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this.isActive)
      )
      .subscribe((x) => {
        this.montantTot = 0;
        this.entetePrelevements = deepCopy(x);
        this.entetePrelevements.forEach((entete) => {
          this.montantTot += entete?.montantTotal!;
        });
      });
  }

  ngOnDestroy(): void {
    this.isActive = false;
    this.store$.dispatch(
      EntetePrelevementActions.setEntetePrelevements({ entetePrelevements: [] })
    );
  }

  navigate(entete) {
    this.router.navigate(['edit', entete?.id], {
      relativeTo: this.activatedRoute.parent,
    });
  }

  selectEntete(entete: any) {
    this.store$.dispatch(
      LignePrelevementActions.fetchLignePrelevementsWithEnteteId({
        enteteId: entete?.id,
      })
    );
  }

  generer() {
    this.entetePrelevementService.entetePrelevementGenerateComptablePost(
      this.selectedPrelevements
    ).subscribe((res)=>{
      if(res?.data !== null) {
        this.selectedPrelevements=[];
        let files = res?.data as string[];
        let i =1;
        files.forEach(element=>{
          if(element){
            const base64String = element;
            const byteCharacters = atob(base64String);
            const byteNumbers = new Array(byteCharacters.length);
            for (let i = 0; i < byteCharacters.length; i++) {
              byteNumbers[i] = byteCharacters.charCodeAt(i);
            }
            const byteArray = new Uint8Array(byteNumbers);
            const blob = new Blob([byteArray], { type: 'application/octet-stream' });
            FileSaver.saveAs(blob, 'ANNEXE '+i +' '+this.datePipe.transform(new Date(), 'dd-MM-yyyy HH-mm-ss') + '.tra');
            i++;
          }
        })
      }
      this.recherche();
    });
  }

  handleFilter(
    filteredData: any[],
    filters: { [p: string]: FilterMetadata | FilterMetadata[] }
  ) {
    this.montantTot = 0;
    filteredData?.forEach((data) => {
      this.montantTot += data?.montantTotal;
    });
  }



  onSelectionChange(event: any) {
    this.selectedPrelevements = event;
  }

  hideDialog() {
    this.prelevementDialog = false;
  }

  saveEntete() {}

  onLazyLoad(event: LazyLoadEvent): void {
    if (this.filter.datePrelevementFrom instanceof Date) {
      this.filter.datePrelevementFrom.setHours(0, 0, 0, 0);
    }

    if (this.filter.datePrelevementTo instanceof Date) {
      this.filter.datePrelevementTo.setHours(0, 0, 0, 0);
    }


    if (this.filter.dateCreationFrom instanceof Date) {
      this.filter.dateCreationFrom.setHours(0, 0, 0, 0);
    }

    if (this.filter.dateCreationTo instanceof Date) {
      this.filter.dateCreationTo.setHours(0, 0, 0, 0);
    }

    this.currentEventGrid=event;
    this.loading = true;
    this.entetePrelevements = [];
    const grid = new RequestOptions(event,this.filter);
    if(this.changeFilter===true){
      grid.page=1
      this.changeFilter=false;
    }
    this.entetePrelevementService.entetePrelevementGetAllLazyPost(grid)
      .subscribe(results => {
        this.totalRecords = results.totalRecords;
        this.loading = false;
        this.montantTot = 0;
        this.entetePrelevements = deepCopy(results?.records);
        this.entetePrelevements.forEach((entete) => {
          this.montantTot += entete?.montantTotal!;
        });
      });
  }
  recherche() {
    this.changeFilter=true;
    this.onLazyLoad(this.currentEventGrid);
  }
  onTabSelectionChange(event) {
    this.selectedPrelevements=[];
    const selectedTabIndex = event.index;
    if (this.statuses[selectedTabIndex].id){
      this.filter.statutId = this.statuses[selectedTabIndex].id;
      if(this.statuses[selectedTabIndex].code === 'ACP'){
        this.showExportCompta = true;
      }else{
        this.showExportCompta = false;
      }
      this.recherche();
    }
  }

  rechercheCheckbox() {
    this.selectedPrelevements=[];
    this.recherche();
  }

}
