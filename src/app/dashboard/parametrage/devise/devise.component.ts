import {Component, OnInit} from '@angular/core';
import {fromDevise} from "../../../shared/reducers";
import {DeviseDTO} from "../../../../../swagger-api";
import {Store} from "@ngrx/store";
import {ConfirmationService, MessageService} from "primeng/api";
import {DeviseActions} from "../../../shared/actions";
import {skipWhile, takeWhile} from "rxjs";

@Component({
  selector: 'app-devise',
  templateUrl: './devise.component.html',
  styleUrls: ['./devise.component.scss'],
})
export class DeviseComponent implements OnInit {
  devises$ = this.store$.select(
    fromDevise.selectDevises
  );
  devises: DeviseDTO[];
  selectDevise: DeviseDTO
  devise: DeviseDTO;
  deviseDialog: boolean;
  submitted: boolean;
  _alive: boolean = true;

  constructor(private store$: Store<fromDevise.State>, private confirmationService: ConfirmationService,
              private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.store$.dispatch(DeviseActions.fetchDevises());
    this.devises$.pipe(
      skipWhile((x) => x === null || x === undefined),
      takeWhile(() => this._alive)
    ).subscribe(x =>
      this.devises = x
    );
  }

  openNew() {
    this.devise = {};
    this.submitted = false;
    this.deviseDialog = true;
  }

  getValueFilter(event: any): string {
    return event.target.value;
  }

  deleteDevise(devise: DeviseDTO) {
    this.confirmationService.confirm({
      message: $localize`Etes-vous sûr que vous voulez supprimer `+ devise.label + '?',
      header: $localize`Confirmer`,
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.store$.dispatch(DeviseActions.DeleteDevise({devise: devise}));
      }
    });
  }

  hideDialog() {
    this.deviseDialog = false;
    this.submitted = false;
  }

  editDevise(devise: DeviseDTO) {
    this.devise = {...devise};
    this.deviseDialog = true;
  }

  saveDevise() {
    this.submitted = true;
    if (this.devise.id) {
      this.store$.dispatch(DeviseActions.EditDevise({devise: this.devise}));
      this.deviseDialog = false;
    } else {
      this.store$.dispatch(DeviseActions.CreateDevise({devise: this.devise}));
      this.deviseDialog = false;
    }
  }
}
