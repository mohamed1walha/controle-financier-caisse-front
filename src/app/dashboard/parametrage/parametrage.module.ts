import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ParametrageRoutingModule} from "./parametrage-routing.module";
import {SharedModule} from "../../shared/shared.module";
import {ReactiveComponentModule} from "@ngrx/component";
import {NbCardModule, NbCheckboxModule} from "@nebular/theme";
import {RippleModule} from "primeng/ripple";
import {CheckboxModule} from "primeng/checkbox";


@NgModule({
  declarations: [
    ...ParametrageRoutingModule.components
  ],
  imports: [
    CommonModule,
    SharedModule,
    ParametrageRoutingModule,
    ReactiveComponentModule,
    NbCardModule,
    RippleModule,
    CheckboxModule,
    NbCheckboxModule
  ]

})
export class ParametrageModule {
}
