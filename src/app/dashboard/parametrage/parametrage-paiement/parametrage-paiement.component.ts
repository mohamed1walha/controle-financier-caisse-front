import {Component, OnInit} from '@angular/core';
import {ConfirmationService, MessageService} from 'primeng/api';
import * as FileSaver from 'file-saver';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import {DeviseDTO, ModepaieDTO, ModePaiementService} from "../../../../../swagger-api";
import {fromDevise, fromModePaiement} from "../../../shared/reducers";
import {Store} from "@ngrx/store";
import {DeviseActions, ModePaiementActions} from "../../../shared/actions";
import {skipWhile, takeWhile} from "rxjs";


@Component({
  selector: 'app-parametrage-paiement',
  templateUrl: './parametrage-paiement.component.html',
  styles: [`
    :host ::ng-deep .p-dialog {
      width: 150px;
      margin: 0 auto 2rem auto;
      display: block;
    }
  `],
  styleUrls: ['./parametrage-paiement.component.scss']
})
export class ParametragePaiementComponent implements OnInit {

  mpDialog: boolean;
  modePaiements$ = this.store$.select(
    fromModePaiement.selectTableData
  );

  // lastSync$ = this.store$.select(
  //   fromModePaiement.getDateSync
  // );
  _alive = true;

  mps: ModepaieDTO[] = [];
  mp: ModepaieDTO;
  selectedMps: ModepaieDTO[];
  submitted: boolean;
  actifs: any;
  lastDateSync: string;
  private sync: boolean;
  private loading: boolean;
  public devises: DeviseDTO[] = [];
  devises$ = this.store$.select(
    fromDevise.selectDevises
  );

  constructor(private mpTrueService: ModePaiementService, private store$: Store<fromModePaiement.State & fromDevise.State>,
              private messageService: MessageService, private confirmationService: ConfirmationService) {
  }

  ngOnInit(): void {
    this.store$.dispatch(ModePaiementActions.fetchData());
    this.modePaiements$.pipe(
      skipWhile((x) => x === null || x === undefined),
      takeWhile(() => this._alive)
    ).subscribe(x =>
      this.mps = x
    );
    this.store$.dispatch(DeviseActions.fetchDevises());
    this.devises$.pipe(
      skipWhile((x) => x === null || x === undefined),
      takeWhile(() => this._alive)
    ).subscribe(x => {
      this.devises.push(...x);
    });
    this.actifs = [
      {label: 'Actif', value: true},
      {label: 'Non Actif', value: false}
    ]
  }

  exportPdf() {
    const doc = new jsPDF();
    const columns = [["First Column", "Second Column", "Third Column"]];
    const data = [
      ["Data 1", "Data 2", "Data 3"],
      ["Data 1", "Data 2", "Data 3"]
    ];

    autoTable(doc, {
      head: columns,
      body: data,
      didDrawPage: dataArg => {
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.text("PAGE", dataArg.settings.margin.left, 10);
      }
    });
    doc.save("table.pdf");
  }
  exportExcel() {
    import("xlsx").then(xlsx => {
      const worksheet = xlsx.utils.json_to_sheet(this.mps);
      const workbook = {Sheets: {'data': worksheet}, SheetNames: ['data']};
      const excelBuffer: any = xlsx.write(workbook, { bookType: 'xlsx', type: 'array' });
      this.saveAsExcelFile(excelBuffer, "mps");
    });
  }
  saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }


  editMp(mp: ModepaieDTO) {
    this.mp = {...mp};
    this.mpDialog = true;
  }

  deleteMp(mp: ModepaieDTO) {
    this.confirmationService.confirm({
      message: $localize`Etes-vous sûr que vous voulez supprimer ` + mp.mpModepaie + '?',
      header: $localize`Confirmer`,
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.store$.dispatch(ModePaiementActions.DeleteModePaiement({mp: mp}));
      }
    })
  }

  hideDialog() {
    this.mpDialog = false;
    this.submitted = false;
  }

  saveMp() {
    this.submitted = true;
    if (this.mp.id) {
      this.mp.actif = this.getBoolean(this.mp.actif);
      this.store$.dispatch(ModePaiementActions.EditModePaiement({mp: this.mp}));
      this.mpDialog = false;
    }
  }

  getBoolean(value) {
    switch (value) {
      case true:
      case "true":
      case 1:
      case "1":
        return true;
      default:
        return false;
    }
  }

  getValueFilter(event: any): string {
    return event.target.value;
  }
}
