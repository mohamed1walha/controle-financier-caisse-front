import {Component, OnInit} from '@angular/core';
import {fromMotif} from "../../../shared/reducers";
import {MotifDTO} from "../../../../../swagger-api";
import {Store} from "@ngrx/store";
import {ConfirmationService, MessageService} from "primeng/api";
import {MotifActions} from "../../../shared/actions";
import {skipWhile, takeWhile} from "rxjs";

@Component({
  selector: 'app-motif',
  templateUrl: './motif.component.html',
  styleUrls: ['./motif.component.scss'],
})
export class MotifComponent implements OnInit {
  motifs$ = this.store$.select(
    fromMotif.selectMotifs
  );
  motifs: MotifDTO[];
  selectMotif: MotifDTO
  motif: MotifDTO;
  motifDialog: boolean;
  submitted: boolean;
  _alive: boolean = true;

  constructor(private store$: Store<fromMotif.State>, private confirmationService: ConfirmationService,
              private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.store$.dispatch(MotifActions.fetchMotifs());
    this.motifs$.pipe(
      skipWhile((x) => x === null || x === undefined),
      takeWhile(() => this._alive)
    ).subscribe(x =>
      this.motifs = x
    );
  }

  openNew() {
    this.motif = {};
    this.submitted = false;
    this.motifDialog = true;
  }

  getValueFilter(event: any): string {
    return event.target.value;
  }

  deleteMotif(motif: MotifDTO) {
    this.confirmationService.confirm({
      message: $localize`Etes-vous sûr que vous voulez supprimer ` + motif.libelle + '?',
      header: $localize`Confirmer`,
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.store$.dispatch(MotifActions.DeleteMotif({motif: motif}));
      }
    });
  }

  hideDialog() {
    this.motifDialog = false;
    this.submitted = false;
  }

  editMotif(motif: MotifDTO) {
    this.motif = {...motif};
    this.motifDialog = true;
  }

  saveMotif() {
    this.submitted = true;
    if (this.motif.id) {
      this.store$.dispatch(MotifActions.EditMotif({motif: this.motif}));
      this.motifDialog = false;
    } else {
      this.store$.dispatch(MotifActions.CreateMotif({motif: this.motif}));
      this.motifDialog = false;
    }
  }
}
