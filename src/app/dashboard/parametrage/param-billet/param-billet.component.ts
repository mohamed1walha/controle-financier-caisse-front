import {Component, OnDestroy, OnInit} from '@angular/core';
import {fromBlade, fromParamBillet} from "../../../shared/reducers";
import {ParamBilletDTO, ParamBilletService, TypeBilletDTO} from "../../../../../swagger-api";
import {Store} from "@ngrx/store";
import {ConfirmationService, MessageService} from "primeng/api";
import {ParamBilletActions} from "../../../shared/actions";
import {skipWhile, takeWhile} from "rxjs";

@Component({
  selector: 'app-param-billet',
  templateUrl: './param-billet.component.html',
  styleUrls: ['./param-billet.component.scss'],
})
export class ParamBilletComponent implements OnInit,OnDestroy {
 /* ParamBillets$ = this.store$.select(
    fromParamBillet.selectParamBillets
  );*/
  TypeBillets$ = this.store$.select(
    fromParamBillet.selectTypeBillets
  );
  ParamBillets: ParamBilletDTO[];
  TypeBillets: TypeBilletDTO[];
  selectParamBillet: ParamBilletDTO
  ParamBillet: ParamBilletDTO;
  ParamBilletDialog: boolean;
  submitted: boolean;
  _alive: boolean = true;
  visibleinput: boolean;
  validForm:boolean;
  constructor(private paramBilletService : ParamBilletService ,private store$: Store<fromParamBillet.State & fromBlade.State>, private confirmationService: ConfirmationService,
              private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.refresh();
    //this.store$.dispatch(ParamBilletActions.fetchParamBillets());
    this.store$.dispatch(ParamBilletActions.fetchTypeBillets());
    /*this.ParamBillets$.pipe(
      skipWhile((x) => x === null || x === undefined),
      takeWhile(() => this._alive)
    ).subscribe(x =>
      this.ParamBillets = x
    );*/

    this.TypeBillets$.pipe(
      skipWhile((x) => x === null || x === undefined),
      takeWhile(() => this._alive)
    ).subscribe(x =>
      this.TypeBillets = x
    );
  }

  refresh(){
    this.paramBilletService.paramBilletGetAllGet().subscribe(res => {
      this.ParamBillets=res;
    })
  }
  openNew() {
    this.visibleinput =true;
    this.ParamBillet = {};
    this.submitted = false;
    this.ParamBilletDialog = true;
  }

  getValueFilter(event: any): string {
    return event.target.value;
  }

  deleteParamBillet(ParamBillet: ParamBilletDTO) {
    this.confirmationService.confirm({
      message: $localize`Etes-vous sûr que vous voulez supprimer ` + ParamBillet.label + '?',
      header: $localize`Confirmer`,
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.paramBilletService.paramBilletDeleteDelete(ParamBillet.id).subscribe(x=>{
          this.refresh();

        })
        //this.store$.dispatch(ParamBilletActions.DeleteParamBillet({ParamBillet: ParamBillet}));
      }
    });
  }

  hideDialog() {
    this.ParamBilletDialog = false;
    this.submitted = false;
  }

  editParamBillet(ParamBillet: ParamBilletDTO) {
    if(ParamBillet.visible ==1){
      this.visibleinput =true;
    }else{
      this.visibleinput =false;
    }
    this.ParamBillet = {...ParamBillet};
    this.ParamBilletDialog = true;
  }

  saveParamBillet() {
      if(this.visibleinput == true){
        this.ParamBillet.visible = 1;
      }else{
        this.ParamBillet.visible = 0;
      }

      this.submitted = true;
      if (this.ParamBillet.id) {
        this.paramBilletService.paramBilletUpdatePut(this.ParamBillet).subscribe(x=>{
          this.refresh();
        })
        //this.store$.dispatch(ParamBilletActions.EditParamBillet({ParamBillet: this.ParamBillet}));
        this.ParamBilletDialog = false;
      } else {
        this.paramBilletService.paramBilletCreatePost(this.ParamBillet).subscribe(x=>{
          this.refresh();
          this.ParamBilletDialog = false;
        })
        //this.store$.dispatch(ParamBilletActions.CreateParamBillet({ParamBillet: this.ParamBillet}));
      }

  }


  ngOnDestroy(): void {
    this._alive=false;
  }
}
