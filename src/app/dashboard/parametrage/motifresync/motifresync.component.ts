import {Component, OnInit} from '@angular/core';
import {fromBlade, fromMotifresync} from "../../../shared/reducers";
import {MotifresyncDTO} from "../../../../../swagger-api";
import {Store} from "@ngrx/store";
import {ConfirmationService, MessageService} from "primeng/api";
import {MotifresyncActions} from "../../../shared/actions";
import {skipWhile, takeWhile} from "rxjs";

@Component({
  selector: 'app-chaufeur',
  templateUrl: './motifresync.component.html',
  styleUrls: ['./motifresync.component.scss'],
})
export class MotifresyncComponent implements OnInit {
  Motifresyncs$ = this.store$.select(
    fromMotifresync.selectMotifresyncs
  );
  Motifresyncs: MotifresyncDTO[];
  selectMotifresync: MotifresyncDTO
  Motifresync: MotifresyncDTO;
  MotifresyncDialog: boolean;
  submitted: boolean;
  _alive: boolean = true;

  constructor(private store$: Store<fromMotifresync.State & fromBlade.State>, private confirmationService: ConfirmationService,
              private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.store$.dispatch(MotifresyncActions.fetchMotifresyncs());
    this.Motifresyncs$.pipe(
      skipWhile((x) => x === null || x === undefined),
      takeWhile(() => this._alive)
    ).subscribe(x =>
      this.Motifresyncs = x
    );
  }

  openNew() {
    this.Motifresync = {};
    this.submitted = false;
    this.MotifresyncDialog = true;
  }

  getValueFilter(event: any): string {
    return event.target.value;
  }

  deleteMotifresync(Motifresync: MotifresyncDTO) {
    this.confirmationService.confirm({
      message: $localize`Etes-vous sûr que vous voulez supprimer ` + Motifresync.motifReSyncDescription + '?',
      header: $localize`Confirmer`,
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.store$.dispatch(MotifresyncActions.DeleteMotifresync({Motifresync: Motifresync}));
      }
    });
  }

  hideDialog() {
    this.MotifresyncDialog = false;
    this.submitted = false;
  }

  editMotifresync(Motifresync: MotifresyncDTO) {
    this.Motifresync = {...Motifresync};
    this.MotifresyncDialog = true;
  }

  saveMotifresync() {
    this.submitted = true;
    if (this.Motifresync.id) {
      this.store$.dispatch(MotifresyncActions.EditMotifresync({Motifresync: this.Motifresync}));
      this.MotifresyncDialog = false;
    } else {
      this.store$.dispatch(MotifresyncActions.CreateMotifresync({Motifresync: this.Motifresync}));
      this.MotifresyncDialog = false;
    }
  }
}
