import {Component, OnInit} from '@angular/core';
import {fromEmetteur} from "../../../shared/reducers";
import {EmetteurDTO} from "../../../../../swagger-api";
import {Store} from "@ngrx/store";
import {ConfirmationService, MessageService} from "primeng/api";
import {EmetteurActions} from "../../../shared/actions";
import {skipWhile, takeWhile} from "rxjs";

@Component({
  selector: 'app-emetteur',
  templateUrl: './emetteur.component.html',
  styleUrls: ['./emetteur.component.scss'],
})
export class EmetteurComponent implements OnInit {
  emetteurs$ = this.store$.select(
    fromEmetteur.selectEmetteurs
  );
  emetteurs: EmetteurDTO[];
  selectEmetteur: EmetteurDTO
  emetteur: EmetteurDTO;
  emetteurDialog: boolean;
  submitted: boolean;
  _alive: boolean = true;

  constructor(private store$: Store<fromEmetteur.State>, private confirmationService: ConfirmationService,
              private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.store$.dispatch(EmetteurActions.fetchEmetteurs());
    this.emetteurs$.pipe(
      skipWhile((x) => x === null || x === undefined),
      takeWhile(() => this._alive)
    ).subscribe(x =>
      this.emetteurs = x
    );
  }

  openNew() {
    this.emetteur = {};
    this.submitted = false;
    this.emetteurDialog = true;
  }

  getValueFilter(event: any): string {
    return event.target.value;
  }

  deleteEmetteur(emetteur: EmetteurDTO) {
    this.confirmationService.confirm({
      message: $localize`Etes-vous sûr que vous voulez supprimer ` + emetteur.emetteurLibelle + '?',
      header: $localize`Confirmer`,
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.store$.dispatch(EmetteurActions.DeleteEmetteur({emetteur: emetteur}));
      }
    });
  }

  hideDialog() {
    this.emetteurDialog = false;
    this.submitted = false;
  }

  editEmetteur(emetteur: EmetteurDTO) {
    this.emetteur = {...emetteur};
    this.emetteurDialog = true;
  }

  saveEmetteur() {
    this.submitted = true;
    if (this.emetteur.id) {
      this.store$.dispatch(EmetteurActions.EditEmetteur({emetteur: this.emetteur}));
      this.emetteurDialog = false;
    } else {
      this.store$.dispatch(EmetteurActions.CreateEmetteur({emetteur: this.emetteur}));
      this.emetteurDialog = false;
    }
  }
}
