import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EmetteurComponent} from "./emetteur/emetteur.component";
import {ChaufeurComponent} from "./chaufeur/chaufeur.component";
import {MotifresyncComponent} from "./motifresync/motifresync.component";
import {MotifComponent} from "./motif/motif.component";
import {ParametragePaiementComponent} from "./parametrage-paiement/parametrage-paiement.component";
import {DeviseComponent} from "./devise/devise.component";
import {ParamBilletComponent} from "./param-billet/param-billet.component";


const routes: Routes = [
  {
    path: 'emetteur',
    component: EmetteurComponent
  },
  {
    path: 'chauffeur',
    component: ChaufeurComponent
  },
  {
    path: 'motifresync',
    component: MotifresyncComponent
  },
  {
    path: 'motif',
    component: MotifComponent
  },
  {
    path: 'parametrage-paiement',
    component: ParametragePaiementComponent
  },
  {
    path: 'devise',
    component: DeviseComponent
  },
  {
    path: 'param-billet',
    component: ParamBilletComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ParametrageRoutingModule {
  static components = [
    MotifComponent,
    ChaufeurComponent,
    MotifresyncComponent,
    EmetteurComponent,
    ParametragePaiementComponent,
    DeviseComponent,
    ParamBilletComponent
  ];
}

