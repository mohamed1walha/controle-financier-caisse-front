import {Component, OnInit} from '@angular/core';
import {fromBlade, fromChauffeur} from "../../../shared/reducers";
import {ChauffeurDTO} from "../../../../../swagger-api";
import {Store} from "@ngrx/store";
import {ConfirmationService, MessageService} from "primeng/api";
import {ChauffeurActions} from "../../../shared/actions";
import {skipWhile, takeWhile} from "rxjs";

@Component({
  selector: 'app-chaufeur',
  templateUrl: './chaufeur.component.html',
  styleUrls: ['./chaufeur.component.scss'],
})
export class ChaufeurComponent implements OnInit {
  chauffeurs$ = this.store$.select(
    fromChauffeur.selectChauffeurs
  );
  chauffeurs: ChauffeurDTO[];
  selectChauffeur: ChauffeurDTO
  chauffeur: ChauffeurDTO;
  chauffeurDialog: boolean;
  submitted: boolean;
  _alive: boolean = true;

  constructor(private store$: Store<fromChauffeur.State & fromBlade.State>, private confirmationService: ConfirmationService,
              private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.store$.dispatch(ChauffeurActions.fetchChauffeurs());
    this.chauffeurs$.pipe(
      skipWhile((x) => x === null || x === undefined),
      takeWhile(() => this._alive)
    ).subscribe(x =>
      this.chauffeurs = x
    );
  }

  openNew() {
    this.chauffeur = {};
    this.submitted = false;
    this.chauffeurDialog = true;
  }

  getValueFilter(event: any): string {
    return event.target.value;
  }

  deleteChauffeur(chauffeur: ChauffeurDTO) {
    this.confirmationService.confirm({
      message: $localize`Etes-vous sûr que vous voulez supprimer ` + chauffeur.chauffeurLibelle + '?',
      header: $localize`Confirmer`,
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.store$.dispatch(ChauffeurActions.DeleteChauffeur({chauffeur: chauffeur}));
      }
    });
  }

  hideDialog() {
    this.chauffeurDialog = false;
    this.submitted = false;
  }

  editChauffeur(chauffeur: ChauffeurDTO) {
    this.chauffeur = {...chauffeur};
    this.chauffeurDialog = true;
  }

  saveChauffeur() {
    this.submitted = true;
    if (this.chauffeur.id) {
      this.store$.dispatch(ChauffeurActions.EditChauffeur({chauffeur: this.chauffeur}));
      this.chauffeurDialog = false;
    } else {
      this.store$.dispatch(ChauffeurActions.CreateChauffeur({chauffeur: this.chauffeur}));
      this.chauffeurDialog = false;
    }
  }
}
