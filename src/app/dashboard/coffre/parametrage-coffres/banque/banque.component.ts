import {Component, OnInit} from '@angular/core';
import {fromBanque, fromBlade} from "../../../../shared/reducers";
import {BanqueDTO} from "../../../../../../swagger-api";
import {Store} from "@ngrx/store";
import {ConfirmationService, MessageService} from "primeng/api";
import {BanqueActions} from "../../../../shared/actions";
import {skipWhile, takeWhile} from "rxjs";

@Component({
  selector: 'app-banque',
  templateUrl: './banque.component.html',
  styleUrls: ['./banque.component.scss']
})
export class BanqueComponent implements OnInit {
  banques$ = this.store$.select(
    fromBanque.selectBanques
  );
  banques: BanqueDTO[];
  selectBanque: BanqueDTO
  banque: BanqueDTO;
  banqueDialog: boolean;
  submitted: boolean;
  _alive: boolean = true;

  constructor(private store$: Store<fromBanque.State & fromBlade.State>, private confirmationService: ConfirmationService,
              private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.store$.dispatch(BanqueActions.fetchBanques());
    this.banques$.pipe(
      skipWhile((x) => x === null || x === undefined),
      takeWhile(() => this._alive)
    ).subscribe(x =>
      this.banques = x
    );
  }

  openNew() {
    this.banque = {};
    this.submitted = false;
    this.banqueDialog = true;
  }

  getValueFilter(event: any): string {
    return event.target.value;
  }

  deleteBanque(banque: BanqueDTO) {
    this.confirmationService.confirm({
      message: $localize`Etes-vous sûr que vous voulez supprimer ` + banque.libelle + '?',
      header: $localize`Confirmer`,
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.store$.dispatch(BanqueActions.DeleteBanque({banque: banque}));
      },
      reject: () => {
        this.confirmationService.close();
      }
    });
  }

  hideDialog() {
    this.banqueDialog = false;
    this.submitted = false;
  }

  editBanque(banque: BanqueDTO) {
    this.banque = {...banque};
    this.banqueDialog = true;
  }

  saveBanque() {
    this.submitted = true;
    if (this.banque.id) {
      this.store$.dispatch(BanqueActions.EditBanque({banque: this.banque}));
      this.banqueDialog = false;
    } else {
      this.store$.dispatch(BanqueActions.CreateBanque({banque: this.banque}));
      this.banqueDialog = false;
    }
  }
}

