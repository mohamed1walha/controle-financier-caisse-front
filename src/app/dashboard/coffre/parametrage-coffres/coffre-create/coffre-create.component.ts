import {Component, OnInit, TemplateRef} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {fromCoffre, fromModePaiement} from "../../../../shared/reducers";
import {Store} from "@ngrx/store";
import {CoffreActions, ModePaiementActions} from "../../../../shared/actions";
import {skipWhile, takeWhile} from "rxjs";
import {CoffreDTO, ModepaieDTO} from "../../../../../../swagger-api";
import {ConfirmationService} from "primeng/api";
import {NbDialogService} from "@nebular/theme";

@Component({
  selector: 'app-coffre-create',
  templateUrl: './coffre-create.component.html',
  styleUrls: ['./coffre-create.component.scss'],
  providers: [NbDialogService]
})
export class CoffreCreateComponent implements OnInit {
  coffreForm: FormGroup;
  _alive = true;
  mps: ModepaieDTO[] = [];
  modePaiements$ = this.store$.select(
    fromModePaiement.selectTableData
  );
  coffres$ = this.store$.select(
    fromCoffre.selectCoffres
  );
  coffres: CoffreDTO[];
  selectCoffre: CoffreDTO
  coffre: CoffreDTO;

  constructor(private fb: FormBuilder,
              private dialogService: NbDialogService,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private toastr: ToastrService,
              private store$: Store<fromModePaiement.State & fromCoffre.State>, private confirmationService: ConfirmationService,) {
  }

  get code() {
    return this.coffreForm.get('code');
  }

  get id() {
    return this.coffreForm.get('id');
  }

  set id(value: any) {
    this.coffreForm.get('id')?.setValue(value);
  }

  get libelle() {
    return this.coffreForm.get('libelle');
  }

  ngOnInit(): void {
    this.prepareForm();
    this.store$.dispatch(ModePaiementActions.fetchData());
    this.modePaiements$.pipe(
      skipWhile((x) => x === null || x === undefined),
      takeWhile(() => this._alive)
    ).subscribe(x =>
      this.mps = x
    );
    this.store$.dispatch(CoffreActions.fetchCoffres());
    this.coffres$.pipe(
      skipWhile((x) => x === null || x === undefined),
      takeWhile(() => this._alive)
    ).subscribe(x =>
      this.coffres = x
    );
  }

  prepareForm() {
    this.coffreForm = this.fb.group({
      id: this.fb.control(''),
      code: this.fb.control('', [Validators.required]),
      libelle: this.fb.control('', [Validators.required]),
      modePaie: new FormControl(this.mps, [Validators.required]),
    });

  }

  createCoffre(ref: any) {
    if (this.coffreForm.valid) {
      if (this.coffre.id) {
        this.id = this.coffre.id;
        this.store$.dispatch(CoffreActions.EditCoffre({coffre: this.coffreForm.value}));
      } else {
        this.id = 0;
        this.store$.dispatch(CoffreActions.CreateCoffre({coffre: this.coffreForm.value}));
      }


      this.coffreForm.reset();
      ref.close();
    }

  }

  openNew(dialog: TemplateRef<any>) {
    this.coffreForm.reset();
    this.coffre = {};
    this.dialogService.open(dialog, {context: $localize`Il s'agit de données supplémentaires transmises à la boîte de dialogue`});

  }

  getValueFilter(event: any): string {
    return event.target.value;
  }

  deleteCoffre(coffre: CoffreDTO) {
    this.confirmationService.confirm({
      message: $localize`Etes-vous sûr que vous voulez supprimer ` + coffre.libelle + '?',
      header: $localize`Confirmer`,
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.store$.dispatch(CoffreActions.DeleteCoffre({coffre: coffre}));

      },
      reject: () => {
        this.confirmationService.close();
      }
    });

  }


  editCoffre(coffre: CoffreDTO, coffreDialog: TemplateRef<any>) {
    this.coffre = {...coffre};
    this.coffreForm.patchValue({...coffre})
    this.dialogService.open(coffreDialog, {context: this.coffre});


  }


}

