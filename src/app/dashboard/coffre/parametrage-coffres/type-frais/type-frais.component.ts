import { Component, OnInit } from '@angular/core';
import {fromTypeFrais, fromBlade} from "../../../../shared/reducers";
import {TypeFraisDTO} from "../../../../../../swagger-api";
import {Store} from "@ngrx/store";
import {ConfirmationService, MessageService} from "primeng/api";
import {TypeFraisActions} from "../../../../shared/actions";
import {skipWhile, takeWhile} from "rxjs";

@Component({
  selector: 'app-type-frais',
  templateUrl: './type-frais.component.html',
  styleUrls: ['./type-frais.component.scss']
})
export class TypeFraisComponent implements OnInit {

  TypeFraiss$ = this.store$.select(
    fromTypeFrais.selectTypeFraiss
  );
  TypeFraiss: TypeFraisDTO[];
  selectTypeFrais: TypeFraisDTO
  TypeFrais: TypeFraisDTO;
  TypeFraisDialog: boolean;
  submitted: boolean;
  _alive: boolean = true;

  constructor(private store$: Store<fromTypeFrais.State & fromBlade.State>, private confirmationService: ConfirmationService,
              private messageService: MessageService) {
  }

  ngOnInit(): void {
    this.store$.dispatch(TypeFraisActions.fetchTypeFraiss());
    this.TypeFraiss$.pipe(
      skipWhile((x) => x === null || x === undefined),
      takeWhile(() => this._alive)
    ).subscribe(x =>
      this.TypeFraiss = x
    );
  }

  openNew() {
    this.TypeFrais = {};
    this.submitted = false;
    this.TypeFraisDialog = true;
  }

  getValueFilter(event: any): string {
    return event.target.value;
  }

  deleteTypeFrais(TypeFrais: TypeFraisDTO) {
    this.confirmationService.confirm({
      message: $localize`Etes-vous sûr que vous voulez supprimer ` + TypeFrais.typeFrais1 + '?',
      header: $localize`Confirmer`,
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.store$.dispatch(TypeFraisActions.DeleteTypeFrais({TypeFrais: TypeFrais}));
      },
      reject: () => {
        this.confirmationService.close();
      }
    });
  }

  hideDialog() {
    this.TypeFraisDialog = false;
    this.submitted = false;
  }

  editTypeFrais(TypeFrais: TypeFraisDTO) {
    this.TypeFrais = {...TypeFrais};
    this.TypeFraisDialog = true;
  }

  saveTypeFrais() {
    this.submitted = true;
    if (this.TypeFrais.id) {
      this.store$.dispatch(TypeFraisActions.EditTypeFrais({TypeFrais: this.TypeFrais}));
      this.TypeFraisDialog = false;
    } else {
      this.store$.dispatch(TypeFraisActions.CreateTypeFrais({TypeFrais: this.TypeFrais}));
      this.TypeFraisDialog = false;
    }
  }

}
