import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ParametrageCoffresComponent} from "./parametrage-coffres/parametrage-coffres.component";
import {AlimentationCoffresComponent} from "./alimentation-coffres/alimentation-coffres.component";
import {SortieCoffreComponent} from "./sortie-coffre/sortie-coffre.component";
import {SoldeCoffreComponent} from "./solde-coffre/solde-coffre.component";
import {EnveloppeNonCompterComponent} from "../comptage/enveloppe-non-compter/enveloppe-non-compter.component";
import {ComptageLigneComponent} from "../comptage/ComptageLigne/comptage-ligne/comptage-ligne.component";
import {SortieListComponent} from "./solde-coffre/sortie-list/sortie-list.component";
import {AlimentationListComponent} from "./solde-coffre/alimentation-list/alimentation-list.component";

const routes: Routes = [
  {
    path: 'parametrage',
    component: ParametrageCoffresComponent
  },
  {
    path: 'alimentation',
    component: AlimentationCoffresComponent
  },
  {
    path: 'sortie',
    component: SortieCoffreComponent},
  {
    path: 'solde',
    children: [
      {
        path: '',
        component: SoldeCoffreComponent
      },
      {
        path: 'sorties',
        outlet: 'blade',
        component: SortieListComponent
      },
      {
        path: 'alimentations',
        outlet: 'blade',
        component: AlimentationListComponent
      }

    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class CoffreRoutingModule {
  static components = [
    ParametrageCoffresComponent,
    AlimentationCoffresComponent,
    SortieCoffreComponent,
    SoldeCoffreComponent
  ];
}




