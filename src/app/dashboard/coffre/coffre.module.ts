import { NgModule } from '@angular/core';
import { SharedModule } from 'src/app/shared/shared.module';

import { CoffreRoutingModule } from './coffre-routing.module';
import {
  NbButtonModule,
  NbCardModule,
  NbInputModule,
  NbSelectModule,
  NbTabsetModule,
} from '@nebular/theme';
import { BanqueComponent } from './parametrage-coffres/banque/banque.component';
import { CoffreCreateComponent } from './parametrage-coffres/coffre-create/coffre-create.component';
import { RippleModule } from 'primeng/ripple';
import { SortieListComponent } from './solde-coffre/sortie-list/sortie-list.component';
import { AlimentationListComponent } from './solde-coffre/alimentation-list/alimentation-list.component';
import { TypeFraisComponent } from './parametrage-coffres/type-frais/type-frais.component';
import { NgxFileDropModule } from 'ngx-file-drop';

@NgModule({
  declarations: [
    ...CoffreRoutingModule.components,
    BanqueComponent,
    CoffreCreateComponent,
    SortieListComponent,
    AlimentationListComponent,
    TypeFraisComponent,
  ],
  imports: [
    SharedModule,
    CoffreRoutingModule,
    NbCardModule,
    NbTabsetModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    RippleModule,
    NgxFileDropModule,
  ],
})
export class CoffreModule {}
