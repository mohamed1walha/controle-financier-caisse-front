import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { fromBlade, fromCoffre } from '../../../../shared/reducers';
import {
  ConfirmationService,
  FilterMetadata,
  MessageService,
} from 'primeng/api';
import { FormBuilder } from '@angular/forms';
import { NotEmptyPipe } from '../../../../shared/utils/noEmpty';
import { CoffreDTO, CoffresortieDTO } from '../../../../../../swagger-api';
import { Observable, skipWhile, takeWhile } from 'rxjs';
import { BladeActions, CoffreActions } from '../../../../shared/actions';
import { EditableRow } from 'primeng/table';
import { ActivatedRoute, Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { Calendar } from 'primeng/calendar';

Calendar.prototype.getDateFormat = () => 'dd/mm/yy';

@Component({
  selector: 'app-sortie-list',
  templateUrl: './sortie-list.component.html',
  providers: [EditableRow],
  styleUrls: ['./sortie-list.component.scss'],
})
export class SortieListComponent implements OnInit, OnDestroy {
  @ViewChild('table') table: ElementRef;
  _alive: boolean = true;
  coffre$: Observable<any> = this.store$.select(fromCoffre.selectCoffre);
  selectedCoffre: CoffreDTO = {};
  sorties: CoffresortieDTO[];
  montantTot: number = 0;
  montantTotConv: number = 0;

  constructor(
    private store$: Store<fromBlade.State & fromCoffre.State>,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private fb: FormBuilder,
    private noEmpty: NotEmptyPipe,
    private toastService: NbToastrService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    setTimeout(() => {
      this.montantTot = 0;
      this.montantTotConv = 0;

      this.store$.dispatch(BladeActions.openBlade());
      this.store$.dispatch(BladeActions.setBladeWidth({ width: 90 }));

      this.coffre$
        .pipe(
          skipWhile((x) => x === null),
          takeWhile(() => this._alive)
        )
        .subscribe((x) => {
          this.selectedCoffre = x;
        });
      if (this.selectedCoffre.id !== undefined) {
        let coffreId = this.selectedCoffre.id;
        this.store$.dispatch(
          CoffreActions.fetchSorties({ coffreId: coffreId })
        );
        this.store$
          .select(fromCoffre.selectSorties)
          .pipe(
            skipWhile((x) => x === null || x === undefined),
            takeWhile(() => this._alive)
          )
          .subscribe((x) => {
            this.sorties = x;
            x.forEach((item) => {
              this.montantTot += item?.montant!;
              this.montantTotConv += item?.montantConvertie!;
            });
          });
      }
    });
  }

  ngOnDestroy() {
    this.store$.dispatch(BladeActions.closeBlade());
    this.store$.dispatch(CoffreActions.fetchSorties({ coffreId: 0 }));
  }

  getValueFilter(event: any): string {
    return event.target.value;
  }

  deleteSortie(sortie: any) {
    this.store$.dispatch(CoffreActions.DeleteSortie({ sortieId: sortie?.id! }));
    this.montantTot = 0;
    this.montantTotConv = 0;
  }

  handleFilter(
    filteredData: any[],
    filters: { [p: string]: FilterMetadata | FilterMetadata[] }
  ) {
    this.montantTot = 0;
    this.montantTotConv = 0;
    filteredData?.forEach((data) => {
      this.montantTot += data?.montant;
      this.montantTotConv += data?.montantConvertie;
    });
  }
}
