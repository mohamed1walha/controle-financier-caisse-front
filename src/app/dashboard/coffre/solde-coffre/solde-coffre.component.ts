import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import {
  fromBlade,
  fromCoffre,
  fromModePaiement,
} from '../../../shared/reducers';
import { CoffreDTO, ModepaieDTO } from '../../../../../swagger-api';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { NbDialogService } from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Store } from '@ngrx/store';
import { ConfirmationService, FilterMetadata } from 'primeng/api';
import { CoffreActions, ModePaiementActions } from '../../../shared/actions';
import { skipWhile, takeWhile } from 'rxjs';
import { ExportService } from '../../../core/services/export.service';
import { Table } from 'primeng/table';

@Component({
  selector: 'app-solde-coffre',
  templateUrl: './solde-coffre.component.html',
  styleUrls: ['./solde-coffre.component.scss'],
  providers: [ExportService],
})
export class SoldeCoffreComponent implements OnInit {
  _alive = true;
  coffres$ = this.store$.select(fromCoffre.selectCoffres);
  @ViewChild('dt') dt: Table;

  mps: ModepaieDTO[] = [];
  modePaiements$ = this.store$.select(fromModePaiement.selectTableData);
  montantTot: number = 0;
  montantTotConv: number = 0;

  coffres: CoffreDTO[];
  selectCoffre: CoffreDTO;
  coffreForm: FormGroup;
  coffre: CoffreDTO;
  columns = [
    { title: $localize`coffre`, dataKey: 'libelle' },
    { title: $localize`Montant`, dataKey: 'montant' },
  ];

  get code() {
    return this.coffreForm.get('code');
  }

  get id() {
    return this.coffreForm.get('id');
  }

  set id(value: any) {
    this.coffreForm.get('id')?.setValue(value);
  }

  get libelle() {
    return this.coffreForm.get('libelle');
  }

  constructor(
    private fb: FormBuilder,
    private dialogService: NbDialogService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private toastr: ToastrService,
    private exportService: ExportService,
    private store$: Store<
      fromBlade.State & fromModePaiement.State & fromCoffre.State
    >,
    private confirmationService: ConfirmationService
  ) {}

  ngOnInit(): void {
    this.store$.dispatch(CoffreActions.setSorties({ sorties: [] }));
    this.prepareForm();
    this.store$.dispatch(ModePaiementActions.fetchData());
    this.modePaiements$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => (this.mps = x));
    this.store$.dispatch(CoffreActions.fetchCoffres());
    this.coffres$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => {
        this.coffres = x;
        this.montantTot = 0;
        this.montantTotConv = 0;
        x.forEach((item) => {
          this.montantTot += item?.montant!;
          this.montantTotConv += item?.montantConvertie!;
        });
      });
  }

  prepareForm() {
    this.coffreForm = this.fb.group({
      id: this.fb.control(''),
      code: this.fb.control('', [Validators.required]),
      libelle: this.fb.control('', [Validators.required]),
      modePaie: new FormControl(this.mps, [Validators.required]),
    });
  }

  createCoffre(ref: any) {
    if (this.coffreForm.valid) {
      if (this.coffre.id) {
        this.id = this.coffre.id;
        this.store$.dispatch(
          CoffreActions.EditCoffre({ coffre: this.coffreForm.value })
        );
      } else {
        this.id = 0;
        this.store$.dispatch(
          CoffreActions.CreateCoffre({ coffre: this.coffreForm.value })
        );
      }

      this.coffreForm.reset();
      ref.close();
    }
  }

  openNew(dialog: TemplateRef<any>) {
    this.coffreForm.reset();
    this.coffre = {};
    this.dialogService.open(dialog, {
      context: $localize`Il s'agit de données supplémentaires transmises à la boîte de dialogue`,
    });
  }

  getValueFilter(event: any): string {
    return event.target.value;
  }

  deleteCoffre(coffre: CoffreDTO) {
    this.confirmationService.confirm({
      message:
        $localize`Etes-vous sûr que vous voulez supprimer ` +
        coffre.libelle +
        '?',
      header: $localize`Confirmer`,
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.store$.dispatch(CoffreActions.DeleteCoffre({ coffre: coffre }));
      },
      reject: () => {
        this.confirmationService.close();
      },
    });
  }

  onSelectionChange(event: any) {}

  editCoffre(coffre: CoffreDTO, coffreDialog: TemplateRef<any>) {
    this.coffre = { ...coffre };
    this.coffreForm.patchValue({ ...coffre });
    this.dialogService.open(coffreDialog, { context: this.coffre });
  }

  exportExcel() {
    //this.exportService.exportExcel(this.dt, this.selectedJourCaisses, this.joursEnAttentes);
  }

  exportPdf() {
    // this.exportService.exportPdfReception(this.dt, null, this.selectedProducts, this.columns/*, dateFrom, dateTo*/);
  }

  sortie(cmpt: CoffreDTO) {
    this.router.navigate(
      [
        './',
        {
          outlets: {
            blade: ['sorties'],
          },
        },
      ],
      { relativeTo: this.activatedRoute }
    );

    this.store$.dispatch(CoffreActions.setCoffre({ coffre: cmpt }));
  }

  alimentation(cmpt: CoffreDTO) {
    this.router.navigate(
      [
        './',
        {
          outlets: {
            blade: ['alimentations'],
          },
        },
      ],
      { relativeTo: this.activatedRoute }
    );

    this.store$.dispatch(CoffreActions.setCoffre({ coffre: cmpt }));
  }

  handleFilter(
    filteredData: any[],
    filters: { [p: string]: FilterMetadata | FilterMetadata[] }
  ) {
    this.montantTotConv = 0;
    this.montantTot = 0;
    filteredData?.forEach((data) => {
      this.montantTot += data?.montant!;
      this.montantTotConv += data?.montantConvertie!;
    });
  }
}
