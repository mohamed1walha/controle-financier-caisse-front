import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import { fromBlade, fromCoffre } from '../../../../shared/reducers';
import { ConfirmationService, MessageService } from 'primeng/api';
import { FormBuilder } from '@angular/forms';
import { NotEmptyPipe } from '../../../../shared/utils/noEmpty';
import {
  CoffrealimentationDTO,
  CoffreDTO,
} from '../../../../../../swagger-api';
import { Observable, skipWhile, takeWhile } from 'rxjs';
import {
  BladeActions,
  CoffreActions,
  CoffreAlimentationActions,
} from '../../../../shared/actions';
import { EditableRow, Table } from 'primeng/table';
import { ActivatedRoute, Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { Calendar } from 'primeng/calendar';

Calendar.prototype.getDateFormat = () => 'dd/mm/yy';

@Component({
  selector: 'app-alimentation-list',
  templateUrl: './alimentation-list.component.html',
  providers: [EditableRow],
  styleUrls: ['./alimentation-list.component.scss'],
})
export class AlimentationListComponent implements OnInit, OnDestroy {
  @ViewChild('dt') dt: Table;
  _alive: boolean = true;
  coffre$: Observable<any> = this.store$.select(fromCoffre.selectCoffre);
  selectedCoffre: CoffreDTO = {};
  alimentations: CoffrealimentationDTO[];
  montantTot: number = 0;
  montantTotConv: number = 0;

  constructor(
    private store$: Store<fromBlade.State & fromCoffre.State>,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private fb: FormBuilder,
    private noEmpty: NotEmptyPipe,
    private toastService: NbToastrService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    setTimeout(() => {
      this.store$.dispatch(BladeActions.openBlade());
      this.store$.dispatch(BladeActions.setBladeWidth({ width: 90 }));

      this.coffre$
        .pipe(
          skipWhile((x) => x === null),
          takeWhile(() => this._alive)
        )
        .subscribe((x) => {
          this.selectedCoffre = x;
          if (this.selectedCoffre.id !== undefined) {
            let coffreId = this.selectedCoffre.id;
            this.store$.dispatch(
              CoffreActions.fetchAlimentations({ coffreId: coffreId })
            );
            this.store$
              .select(fromCoffre.selectAlimentations)
              .pipe(
                skipWhile((x) => x === null),
                takeWhile(() => this._alive)
              )
              .subscribe((x) => {
                this.alimentations = x;
                this.montantTot = 0;
                this.montantTotConv = 0;

                x.forEach((item) => {
                  this.montantTot += item?.montantalimenter!;
                  this.montantTot;
                  this.montantTotConv += item?.montantConvertie!;
                  this.montantTotConv;
                });
              });
          }
        });
    });
  }

  ngOnDestroy() {
    this.store$.dispatch(BladeActions.closeBlade());
    this.store$.dispatch(CoffreActions.fetchAlimentations({ coffreId: 0 }));
  }

  getValueFilter(event: any): string {
    return event.target.value;
  }

  deleteAlimentation(alimentation: CoffrealimentationDTO) {
    this.store$.dispatch(
      CoffreAlimentationActions.DeleteAlimentation({
        alimentationId: alimentation?.id!,
      })
    );
    // this.confirmationService.confirm({
    //   message: 'Are you sure you want to delete ' + alimentation.montantalimenter + '?',
    //   header: 'Confirm',
    //   icon: 'pi pi-exclamation-triangle',
    //   accept: () => {
    //     this.store$.dispatch(CoffreAlimentationActions.DeleteAlimentation({alimentationId: alimentation?.id!}));
    //     this.store$.dispatch(CoffreActions.fetchAlimentations({coffreId: alimentation.coffreId!}));
    //   }
    // });
  }
}
