import { Component, OnInit, ViewChild } from '@angular/core';
import { Table } from 'primeng/table';
import {
  BilletDetailAliExcepDTO,
  CoffrealimentationDTO,
  CoffreDTO,
  EtablissementDTO,
  JoursCaisseService,
  ModepaieDTO,
  PiedecheDTO,
} from '../../../../../swagger-api';
import { Observable, skipWhile, takeWhile } from 'rxjs';
import {
  fromBlade,
  fromCoffre,
  fromCoffreAlimentation,
  fromEtablissement,
  fromModePaiement,
  fromPiedeche,
} from '../../../shared/reducers';
import {
  ConfirmationService,
  FilterMatchMode,
  FilterMetadata,
  FilterService,
  SelectItem,
} from 'primeng/api';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Store } from '@ngrx/store';
import { ActivatedRoute } from '@angular/router';
import { ExportService } from '../../../core/services/export.service';
import { DatePipe } from '@angular/common';
import {
  CoffreActions,
  CoffreAlimentationActions,
  EtablissementActions,
  ModePaiementActions,
  PiedecheActions,
} from '../../../shared/actions';
import { deepCopy } from '../../../shared/utils/deep-copy';
import { NbDialogService, NbToastrService } from '@nebular/theme';
import { Calendar } from 'primeng/calendar';
import { DialogCalculatePromptComponent } from '../../../shared/components/dialog-calculate-prompt/dialog-calculate-prompt.component';
import { SeparateurPipe } from '../../../shared/utils/separateur.pipe';

Calendar.prototype.getDateFormat = () => 'dd/mm/yy';

@Component({
  selector: 'app-alimentation-coffres',
  templateUrl: './alimentation-coffres.component.html',
  styleUrls: ['./alimentation-coffres.component.scss'],
  providers: [ExportService, DatePipe],
})
export class AlimentationCoffresComponent implements OnInit {
  modePId: (number | null | undefined)[];
  modePExpId: (number | null | undefined)[];
  @ViewChild('dt') dt: Table;
  headerRow: {
    displayName: string;
    key: string;
    hasSort: boolean;
    type: string;
  }[] = [
    {
      displayName: $localize`Etablissement`,
      key: 'etablissement.etLibelle',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Date d'ouverture`,
      key: 'jourcaisse.gjcDateouv',
      hasSort: true,
      type: 'date',
    },
    {
      displayName: $localize`Caisse`,
      key: 'jourcaisse.gjcCaisse',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Référence Z`,
      key: 'jourcaisse.gjcNumzcaisse',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Mode Paiement`,
      key: 'modepaie.libelleAppFinance',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Devise`,
      key: 'modepaie.deviseLabel',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Montant Compté`,
      key: 'montantCompte',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Montant Convertie`,
      key: 'montantConvertie',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Montant Alimenté`,
      key: 'montantAlimenter',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Reste à alimenté`,
      key: 'montantCompte',
      hasSort: true,
      type: 'text',
    },
  ];
  _alive: boolean = true;

  etablissement$: Observable<EtablissementDTO[]> = this.store$.select(
    fromEtablissement.selectEtablissements
  );
  etablissements: EtablissementDTO[];
  etablissement: EtablissementDTO;
  modePaiments$: Observable<ModepaieDTO[]> = this.store$.select(
    fromModePaiement.selectTableData
  );
  modePaiments: ModepaieDTO[];
  modePaiment: ModepaieDTO;
  currentUser = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .currentUser;
  montantTot: number = 0;

  piedeches$: Observable<PiedecheDTO[]> = this.store$.select(
    fromPiedeche.selectPiedeches
  );
  piedeches: PiedecheDTO[] = [];
  selectedPiedeche: PiedecheDTO[];
  alimentationDialog: boolean;

  coffres$ = this.store$.select(fromCoffre.selectCoffres);
  coffres: CoffreDTO[];
  selectCoffre: CoffreDTO;
  coffre: CoffreDTO;
  coffAllFrom: FormGroup;
  coffAllExcepForm: FormGroup;
  matchModeOptions: SelectItem[];
  matchModeOptionsDate: SelectItem[];
  somme: number = 0;
  montantCompter: number = 0;
  montantConvertie: number = 0;
  montantAlim: number = 0;
  alimentationExcep: boolean = false;
  calculette: any;

  constructor(
    private store$: Store<
      fromBlade.State &
        fromCoffre.State &
        fromPiedeche.State &
        fromEtablissement.State &
        fromCoffreAlimentation.State
    >,
    private filterService: FilterService,
    private confirmationService: ConfirmationService,
    private dialogService: NbDialogService,
    private joursCaisseService: JoursCaisseService,
    public toast: NbToastrService,
    private fb: FormBuilder,
    private numberPipe: SeparateurPipe,
    private activatedRoute: ActivatedRoute,
    private exportService: ExportService,
    private datePipe: DatePipe
  ) {}

  ngOnDestroy(): void {
    this._alive = false;
  }

  get piedecheIds() {
    return this.coffAllFrom?.get('piedecheIds');
  }

  handleFilter(
    filteredData: any[],
    filters: { [p: string]: FilterMetadata | FilterMetadata[] }
  ) {
    this.montantCompter = 0;
    this.montantAlim = 0;
    this.montantConvertie = 0;
    this.montantTot = 0;
    filteredData?.forEach((data) => {
      this.montantCompter += data?.montantCompte;
      this.montantAlim += data?.montantAlimenter;
      this.montantConvertie += data?.montantConvertie;
      this.montantTot = this.montantCompter - this.montantAlim;
    });
  }

  set piedecheIds(value) {
    this.coffAllFrom?.get('piedecheIds')?.setValue(value);
  }

  get Montantalimenter() {
    return this.coffAllFrom?.get('Montantalimenter');
  }

  get coffreId() {
    return this.coffAllFrom?.get('coffreId');
  }

  get motif() {
    return this.coffAllFrom?.get('motif');
  }

  get expCoffreId() {
    return this.coffAllExcepForm?.get('expCoffreId');
  }
  get expMpId() {
    return this.coffAllExcepForm?.get('expMpId');
  }
  get expMntP() {
    return this.coffAllExcepForm?.get('expMntP');
  }
  get expMotifP() {
    return this.coffAllExcepForm?.get('expMotifP');
  }
  get expDtP() {
    return this.coffAllExcepForm?.get('expDtP');
  }

  ngOnInit(): void {
    this.store$.dispatch(PiedecheActions.fetchAllByCoffreNull());
    this.store$.dispatch(EtablissementActions.fetchEtablissements());
    this.store$.dispatch(ModePaiementActions.fetchData());
    this.etablissement$
      .pipe(
        takeWhile((x) => this._alive),
        skipWhile((x) => x == null)
      )
      .subscribe((x) => {
        this.etablissements = x;
      });
    this.modePaiments$
      .pipe(
        takeWhile((x) => this._alive),
        skipWhile((x) => x == null)
      )
      .subscribe((x) => {
        this.modePaiments = x;
      });
    this.matchModeOptionsDate = [
      { label: $localize`Date supérieur`, value: FilterMatchMode.DATE_AFTER },
      { label: $localize`Date inférieur`, value: FilterMatchMode.DATE_BEFORE },
      { label: $localize`Date Egale`, value: FilterMatchMode.DATE_IS },
    ];
    this.matchModeOptions = [
      { label: $localize`Début avec`, value: FilterMatchMode.STARTS_WITH },
      { label: $localize`Contient`, value: FilterMatchMode.CONTAINS },
    ];
    this.piedeches$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => {
        this.piedeches = deepCopy(x);
        this.montantCompter = 0;
        this.montantAlim = 0;
        this.montantTot = 0;
        this.montantConvertie = 0;
        this.piedeches.forEach((item) => {
          const j = deepCopy(item?.jourcaisse);
          if (item?.jourcaisse != undefined || item?.jourcaisse != null) {
            j.gjcDateouv = new Date(item?.jourcaisse?.gjcDateouv!);
          }
          item.jourcaisse = j;

          this.montantCompter += item?.montantCompte!;
          this.montantAlim += item?.montantAlimenter!;
          this.montantConvertie += item?.montantConvertie!;
          this.montantTot = this.montantCompter - this.montantAlim;
          //this.montantTot.toFixed(3);
        });
      });

    this.store$.dispatch(CoffreActions.fetchCoffres());
    this.coffres$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => (this.coffres = deepCopy(x)));
    this.createForm();
    this.createExpForm();
  }

  hideDialog() {
    this.somme = 0;
    this.alimentationDialog = false;
    this.coffAllFrom.reset();
    this.selectedPiedeche = [];
  }

  hideDialogExp() {
    this.alimentationExcep = false;
    this.coffAllExcepForm.reset();
  }

  createForm() {
    this.coffAllFrom = this.fb.group({
      Username: this.fb.control(this.currentUser),
      coffreId: this.fb.control('', [Validators.required]),
      Montantalimenter: this.fb.control('', [Validators.required]),
      motif: [],
      piedecheIds: new FormControl([]),
    });
  }

  createExpForm() {
    this.coffAllExcepForm = this.fb.group({
      Username: this.fb.control(this.currentUser),
      expMpId: this.fb.control('', [Validators.required]),
      expCoffreId: this.fb.control('', [Validators.required]),
      expMntP: this.fb.control('', [Validators.required]),
      expMotifP: [],
      expDtP: this.fb.control(new Date()),
    });
  }

  onSelectionChange(event: any) {
    this.selectedPiedeche = event;
  }

  alimenter() {
    this.store$.dispatch(CoffreActions.fetchCoffres());
    this.coffres$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => (this.coffres = deepCopy(x)));
    this.somme = 0;
    this.selectedPiedeche.forEach((x) => {
      // this.montantCompter-=x.montantCompte!
      // this.montantAlim-=x.montantAlimenter!
      // this.montantTot = this.montantCompter - this.montantAlim;
      this.somme += x.montantCompte! - x.montantAlimenter!;
    });
    this.Montantalimenter!.setValue(this.numberPipe.transform(this.somme));
    this.alimentationDialog = true;
    this.modePId = Array.from(this.selectedPiedeche.map((x) => x.modepaieId));
    this.store$.dispatch(
      CoffreActions.fetchCoffresFromModePaie({ modePaie: this.modePId })
    );
  }

  save() {
    if (this.motif?.value === null) {
      this.motif.setValue(' ');
    }
    if (this.coffAllFrom.valid) {
      var sum = 0;
      this.selectedPiedeche.forEach((x) => {
        sum += x.montantCompte! - x.montantAlimenter!;
      });
      if (sum >= this.Montantalimenter?.value) {
        let piedecheIds = Array.from(this.selectedPiedeche.map((x) => x.id));
        this.piedecheIds?.setValue(piedecheIds);
        this.store$.dispatch(
          CoffreAlimentationActions.Createcoffrealimentation({
            coffrealimentations: this.coffAllFrom.value,
          })
        );
        this.coffAllFrom.reset();
        this.selectedPiedeche = [];
        this.hideDialog();
      } else {
        this.toast.warning(
          $localize` Le Montant Alimenter doit etre < ` + sum,
          $localize`Avertissement`,
          {
            duration: 3000,
          }
        );
      }
    } else {
      let h = '';
      if (this.coffreId?.invalid) {
        h += $localize`choisissez une coffre,  `;
      }
      if (this.Montantalimenter?.invalid) {
        h += $localize`saisie le montant ,`;
      }
      this.toast.warning(h, $localize`Avertissement`, {
        duration: 3000,
      });
    }
  }

  alimenterExcep() {
    this.coffres = [];
    this.alimentationExcep = true;
  }

  convertCalculetteExp(calculette: any) {
    let billetDetailAliExcepDTO: BilletDetailAliExcepDTO[] = [];
    if (calculette?.items.length > 0) {
      calculette.items.forEach((ligne) => {
        let billet: BilletDetailAliExcepDTO = {
          billetId: ligne.billetId,
          quantite: Number(ligne.quantite),
          montant: ligne.montant,
          total: Number(ligne.quantite) * ligne.montant,
          alimentationId: 0,
        };
        billetDetailAliExcepDTO.push(billet);
      });
    }
    return billetDetailAliExcepDTO;
  }

  saveExp() {
    if (this.expMotifP?.value === null) {
      this.expMotifP.setValue(' ');
    }
    if (this.coffAllExcepForm.valid) {
      let coffreAlimentationDTO: CoffrealimentationDTO = {
        username: this.currentUser,
        coffreId: this.expCoffreId?.value,
        alimentationDate: this.expDtP?.value,
        montantalimenter: this.expMntP?.value,
        motif: this.expMotifP?.value,
        modePaieId: this.expMpId?.value,
        billetDetailAliExcepDTOs: this.convertCalculetteExp(this.calculette),
      };

      this.store$.dispatch(
        CoffreAlimentationActions.CreatecoffrealimentationExp({
          coffreAlimentationDTO,
        })
      );
      this.coffAllExcepForm.reset();
      this.hideDialogExp(), (this.calculette = null);
      /*else {

       this.toast.warning(" Le Montant Alimenter doit etre < " + sum, 'Avertissement', {
         duration: 3000
       });
     }*/
    } else {
      let h = '';
      if (this.expCoffreId?.invalid) {
        h += $localize`choisissez un coffre, `;
      }
      if (this.expMntP?.invalid) {
        h += $localize`saisie le montant ,`;
      }
      this.toast.warning(h, $localize`Avertissement`, {
        duration: 3000,
      });
    }
  }

  loadCoffre(mp) {
    this.modePExpId = [];
    this.modePExpId.push(mp.value);
    this.store$.dispatch(
      CoffreActions.fetchCoffresFromModePaie({ modePaie: this.modePExpId })
    );
    this.coffres$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => (this.coffres = deepCopy(x)));
  }

  /*openCalculate() {
    this.alimentationDialog = false;
    this.dialogService.open(DialogCalculatePromptComponent, {
      context: {
        calculette: this.calculette
      }})
      .onClose.subscribe(calculette => {
        if(calculette?.somme !== 0){
          this.Montantalimenter?.setValue(calculette?.somme);
          this.calculette = calculette;
        }
      this.alimentationDialog = true;
    });
  }*/

  openCalculateExp() {
    this.alimentationExcep = false;
    this.dialogService
      .open(DialogCalculatePromptComponent, {
        context: {
          calculette: this.calculette,
        },
      })
      .onClose.subscribe((calculette) => {
        if (
          Number(calculette?.somme) !== 0 &&
          calculette?.somme !== undefined
        ) {
          this.expMntP?.setValue(calculette?.somme);
          this.expMntP?.disable();
          this.calculette = calculette;
        } else if (Number(calculette?.somme) === 0) {
          this.expMntP?.setValue(calculette?.somme);
          this.expMntP?.enable();

          this.calculette = calculette;
        }

        this.alimentationExcep = true;
      });
  }
}
