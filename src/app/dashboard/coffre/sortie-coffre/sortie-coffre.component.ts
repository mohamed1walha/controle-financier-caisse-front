import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import {
  NbDialogService,
  NbGlobalPhysicalPosition,
  NbToastrService,
} from '@nebular/theme';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Store } from '@ngrx/store';
import {
  fromBanque,
  fromBlade,
  fromCoffre,
  fromDevise,
  fromModePaiement,
  fromTypeFrais,
} from '../../../shared/reducers';
import {
  ConfirmationService,
  FilterMatchMode,
  FilterMetadata,
  SelectItem,
} from 'primeng/api';
import {
  BanqueDTO,
  BilletDetailSortieDTO,
  BilletDetailSortieService,
  CoffreDTO,
  CoffresortieDTO,
  DeviseDTO,
  TypeFraisDTO,
} from '../../../../../swagger-api';
import {
  BanqueActions,
  CoffreActions,
  DeviseActions,
  TypeFraisActions,
} from '../../../shared/actions';
import { Observable, skipWhile, takeWhile } from 'rxjs';
import { FileUpload } from 'primeng/fileupload';
import { NgxFileDropEntry } from 'ngx-file-drop';
import { environment } from '../../../../environments/environment';
import { deepCopy } from '../../../shared/utils/deep-copy';
import { Calendar } from 'primeng/calendar';
import { DialogCalculatePromptComponent } from '../../../shared/components/dialog-calculate-prompt/dialog-calculate-prompt.component';

Calendar.prototype.getDateFormat = () => 'dd/mm/yy';

@Component({
  selector: 'app-sortie-coffre',
  templateUrl: './sortie-coffre.component.html',
  styleUrls: ['./sortie-coffre.component.scss'],
})
export class SortieCoffreComponent implements OnInit {
  headerRow: {
    displayName: string;
    key: string;
    hasSort: boolean;
    type: string;
  }[] = [
    {
      displayName: $localize`Coffre`,
      key: 'coffre.libelle',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Type Sortie`,
      key: 'typeSortieLibelle',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Date`,
      key: 'sortieDate',
      hasSort: true,
      type: 'date',
    },
    {
      displayName: $localize`Banque`,
      key: 'banqueLibelle',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Type Frais`,
      key: 'typeFraisLibelle',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Site`,
      key: 'siteConcernee',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Service`,
      key: 'serviceConcernee',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Alimenté par`,
      key: 'username',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Commentaire`,
      key: 'commentaire',
      hasSort: false,
      type: 'text',
    },
    {
      displayName: $localize`Justificatif`,
      key: 'sortie',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Montant`,
      key: 'sortie.montant',
      hasSort: true,
      type: 'text',
    },
    {
      displayName: $localize`Montant Convertie`,
      key: 'sortie.montantConvertie',
      hasSort: true,
      type: 'text',
    },
    { displayName: $localize`Action`, key: '', hasSort: false, type: 'text' },
  ];
  calculette: any;
  show: boolean = false;
  matchModeOptions: SelectItem[];
  matchModeOptionsDate: SelectItem[];
  @ViewChild('fileInput') fileInput: FileUpload;
  public sortie: CoffresortieDTO = {};
  env = environment.apiUrl;
  fileType: string =
    "['.PPTX','.PPT','.DOCX','.XLSX','.XLS','.CSV','.PDF','.JPG','.JPEG','.BMP','.PNG']";
  files: NgxFileDropEntry[] = [];
  tempDocName: string;
  fileSize: any;
  public banques: BanqueDTO[] = [];
  public coffres: CoffreDTO[] = [];
  public typeFrais: TypeFraisDTO[] = [];
  displayModal = false;
  _alive = true;
  public submitted: boolean;
  public sortieDialog: boolean;
  public showMontant: boolean = false;
  sorties: CoffresortieDTO[] = [];
  devises$ = this.store$.select(fromDevise.selectDevises);
  devises: DeviseDTO[];
  coffresSorties$ = this.store$.select(fromCoffre.selectAllSorties);
  sortie$: Observable<CoffresortieDTO> = this.store$.select(
    fromCoffre.selectSortie
  );
  sortieCoff: CoffresortieDTO;
  coffres$ = this.store$.select(fromCoffre.selectCoffres);
  banques$ = this.store$.select(fromBanque.selectBanques);
  typeFrais$ = this.store$.select(fromTypeFrais.selectTypeFraiss);
  pieceJustificatif: number;
  listFileUpload: any[] = [];
  reader: FileReader;
  typeSortie: any[] = [
    { id: 1, type: $localize`Remise en banque` },
    { id: 2, type: $localize`Frais divers` },
  ];
  montantTot: number = 0;
  montantTotConv: number = 0;
  fileName: string;
  extension: string | undefined;
  fileBase64: string;
  coffresFilter: CoffreDTO[];
  AllCalculetteBySortieIdGet: BilletDetailSortieDTO[] = [];

  constructor(
    private fb: FormBuilder,
    private dialogService: NbDialogService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private billetDetailSortieService: BilletDetailSortieService,
    private toastService: NbToastrService,
    private toastr: ToastrService,
    private store$: Store<
      fromBlade.State &
        fromModePaiement.State &
        fromCoffre.State &
        fromBanque.State &
        fromTypeFrais.State &
        fromDevise.State
    >,
    private confirmationService: ConfirmationService
  ) {
    this.reader = new FileReader();
  }

  ngOnInit(): void {
    this.matchModeOptions = [
      { label: $localize`Début avec`, value: FilterMatchMode.STARTS_WITH },
      { label: $localize`Contient`, value: FilterMatchMode.CONTAINS },
    ];
    this.matchModeOptionsDate = [
      { label: $localize`Date supérieur`, value: FilterMatchMode.DATE_AFTER },
      { label: $localize`Date inférieur`, value: FilterMatchMode.DATE_BEFORE },
      { label: $localize`Date Egale`, value: FilterMatchMode.DATE_IS },
    ];
    this.store$.dispatch(CoffreActions.fetchAllSorties());
    this.coffresSorties$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => {
        this.sorties = x;
        this.montantTot = 0;
        this.montantTotConv = 0;
        x.forEach((item) => {
          this.montantTot += item?.montant!;
          this.montantTotConv += item?.montantConvertie!;
        });
      });
    this.sortie$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => {
        this.sortie = deepCopy(x);
        this.sortie.sortieDate = new Date(x.sortieDate!);
      });
    this.store$.dispatch(BanqueActions.fetchBanques());
    this.banques$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => {
        this.banques = x;
      });
    this.store$.dispatch(TypeFraisActions.fetchTypeFraiss());
    this.typeFrais$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => {
        this.typeFrais = x;
      });
    this.store$.dispatch(CoffreActions.fetchCoffres());
    this.coffres$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => {
        this.coffresFilter = x;
        this.coffres = x;
        this.coffres = this.coffres.filter((coffre) => coffre.montant !== 0);
        if (this.coffres?.length) {
          this.onChange(this.coffres[0].id);
        }
      });
    this.devises$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => (this.devises = x));
  }

  openNew() {
    this.displayModal = true;
    this.sortie = { sortieDate: new Date() };
    this.sortie.justificatif = false;
    this.sortie.typeSortie = 1;
    this.submitted = false;
    this.sortieDialog = true;
  }

  hideDialog() {
    this.sortieDialog = false;
    this.submitted = false;
    this.store$.dispatch(
      CoffreActions.fetchCalculetteBySortieId({ sortieId: 0 })
    );
  }

  convertCalculetteExp(calculette: any) {
    let billetDetailSortieDTO: BilletDetailSortieDTO[] = [];
    if (calculette?.items.length > 0) {
      calculette.items.forEach((ligne) => {
        let billet: BilletDetailSortieDTO = {
          billetId: ligne.billetId,
          quantite: Number(ligne.quantite),
          montant: ligne.montant,
          total: Number(ligne.quantite) * ligne.montant,
          sortieId: 0,
        };
        billetDetailSortieDTO.push(billet);
      });
    }
    return billetDetailSortieDTO;
  }

  saveSortie(value) {
    this.submitted = value;
    if (this.sortie.coffreId) {
      let soldeCoffre = this.coffres.find(
        (x) => x.id === this.sortie.coffreId
      )?.montant;
      let soldeCoffreConv = this.coffres.find(
        (x) => x.id === this.sortie.coffreId
      )?.montantConvertie;
      const taux: number =
        this.devises.find((x) => x.id == this.sortie.deviseId)?.tauxChange! *
        this.sortie.montant!;
      if (
        Number(soldeCoffre) >= Number(this.sortie.montant) &&
        Number(soldeCoffreConv) >= Number(taux)
      ) {
        if (this.sortie.justificatif) {
          this.sortie.justificatifName = this.fileName;
          this.sortie.pieceJustificatif = this.fileBase64;
          this.sortie.extention = '.' + this.extension;
        }
        if (this.sortie.typeSortie == 1) {
          this.sortie.typeFraisId = null;
          this.sortie.siteConcernee = null;
          this.sortie.serviceConcernee = null;
        } else {
          this.sortie.banqueId = null;
        }
        this.sortie.username = JSON.parse(
          localStorage.getItem('currentUser') || '{}'
        ).currentUser;
        if (this.sortie.montant != null && this.sortie.montant > 0) {
          this.sortie.billetDetailSortieDTOs = this.convertCalculetteExp(
            this.calculette
          );
          this.store$.dispatch(
            CoffreActions.CreateSortie({ sortie: this.sortie })
          );
          this.store$.dispatch(
            CoffreActions.fetchCalculetteBySortieId({ sortieId: 0 })
          );
          this.calculette = null;
        }
        if (value) {
          this.sortieDialog = false;
          this.sortie = {};
          this.fileSize = null;
          this.fileName = '';
          this.extension = '';
          this.fileBase64 = '';
        }
        this.store$.dispatch(CoffreActions.fetchCoffres());
      } else {
        this.toastService.danger(
          $localize`s'il vous plait Verifier le montant de sortie`,
          $localize`Erreur`,
          {
            duration: 3000,
            position: NbGlobalPhysicalPosition.BOTTOM_RIGHT,
          }
        );
      }
    }
  }

  public dropped(files: NgxFileDropEntry[]) {
    this.files = files;
    for (const droppedFile of files) {
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          this.fileSize = file.size;
          this.fileName = file.name.substring(0, file.name.lastIndexOf('.'));
          this.extension = file.name.split('.').pop();
          const reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = () => {
            this.fileBase64 = (reader.result as string)
              .replace('data:', '')
              .replace(/^.+,/, '');

            if (
              [
                'PPTX',
                'PPT',
                'DOCX',
                'DOC',
                'XLSX',
                'XLS',
                'CSV',
                'PDF',
                'JPG',
                'JPEG',
                'BMP',
                'PNG',
              ].find(
                (value) =>
                  value === file?.name?.split('.')?.pop()?.toUpperCase()
              )
            ) {
              {
                if (this.sortie.justificatif) {
                  this.sortie.justificatifName = this.fileName;
                  this.sortie.pieceJustificatif = this.fileBase64;
                  this.sortie.extention = '.' + this.extension;
                }
                this.sortie.billetDetailSortieDTOs = this.convertCalculetteExp(
                  this.calculette
                );
                this.store$.dispatch(
                  CoffreActions.CreateSortie({ sortie: this.sortie })
                );
                this.store$.dispatch(
                  CoffreActions.fetchCalculetteBySortieId({ sortieId: 0 })
                );
                this.calculette = null;

                //  this.sortie.id=this.sortieCoff?.id!=null?this.sortieCoff?.id:0;
                // this.sortie.commentaire=this.sortieCoff.commentaire

                // this.recordService.recordUpload(this.recordForm.value).subscribe((value) => {
                //   this.toastr.success($localize`:@@OperationSuccessful:Operation successful`);
                //   this.tempDocName = value.fileName;
                // });
              }
            }
          };
        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
      }
    }
  }

  selectFilesToUpload() {
    this.listFileUpload = [];
    for (var i = 0; i < this.fileInput.files.length; i++) {
      this.listFileUpload.push(this.fileInput.files[i]);
    }
  }

  handleFilter(
    filteredData: any[],
    filters: { [p: string]: FilterMetadata | FilterMetadata[] }
  ) {
    this.montantTot = 0;
    this.montantTotConv = 0;
    filteredData?.forEach((data) => {
      this.montantTot += data?.montant;
      this.montantTotConv += data?.montantConvertie;
    });
  }

  editSortie(sortie: CoffresortieDTO) {
    this.sortie = { ...sortie, sortieDate: new Date(sortie.sortieDate!) };
    this.sortieDialog = true;

    this.billetDetailSortieService
      .billetDetailSortieGetAllBySortieIdGet(this.sortie.id)
      .subscribe((allCalculettes) => {
        this.AllCalculetteBySortieIdGet = allCalculettes;
      });
  }

  onChange(event) {
    this.show = true;

    this.store$.dispatch(
      DeviseActions.fetchDevisesByCoffreId({ coffreId: event })
    );
  }

  openCalculate(AllCalculetteBySortieIdGet) {
    this.sortieDialog = false;
    this.dialogService
      .open(DialogCalculatePromptComponent, {
        context: {
          calculette: this.calculette,
          calculSortie: AllCalculetteBySortieIdGet,
        },
      })
      .onClose.subscribe((calculette) => {
        if (
          Number(calculette?.somme) !== 0 &&
          calculette?.somme !== undefined
        ) {
          this.showMontant = true;

          this.sortie.montant = calculette?.somme;
          this.calculette = calculette;
        } else if (Number(calculette?.somme) === 0) {
          this.sortie.montant = calculette?.somme;
          this.calculette = calculette;

          this.showMontant = false;
        }
        this.sortieDialog = true;
      });
  }
}
