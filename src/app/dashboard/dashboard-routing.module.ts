import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {DashboardComponent} from './dashboard.component';
import {HomeComponent} from './home/home.component';
import {ProfileComponent} from "./profile/profile.component";

export const DashboardRoutes: Routes = [
  {

    path: '',
    component: DashboardComponent,
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },
      {
        path: 'home',
        component: HomeComponent
      },
      {
        path: 'interfacage',
        loadChildren: () => import('./interfacage/interfacage.module').then(m => m.InterfacageModule)
      },
      {
        path: 'parametrage',
        loadChildren: () => import('./parametrage/parametrage.module').then(m => m.ParametrageModule)
      },
      {
        path: 'comptage',
        loadChildren: () => import('./comptage/comptage-enveloppe.module').then(m => m.ComptageEnveloppeModule)
      },
      {
        path: 'coffre',
        loadChildren: () => import('./coffre/coffre.module').then(m => m.CoffreModule)
      },
      {
        path: 'reception-enveloppe',
        loadChildren: () => import('./reception-enveloppe/reception-enveloppe.module').then(m => m.ReceptionEnveloppeModule)
      }, {
        path: 'users',
        loadChildren: () => import('./users/users.module').then(m => m.UsersModule)
      },
      {
        path: 'profile',
        component: ProfileComponent
      },
      {
        path: 'depense',
        loadChildren: () => import('./depense/depense.module').then(m => m.DepenseModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(DashboardRoutes)],
  exports: [RouterModule]
})

export class DashboarRoutingModule {
  static components = [DashboardComponent, HomeComponent, ProfileComponent];
}
