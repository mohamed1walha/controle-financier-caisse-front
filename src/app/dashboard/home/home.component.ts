import { Component, OnInit } from '@angular/core';
import { Observable, skipWhile, takeWhile } from 'rxjs';
import {
  fromCoffre,
  fromJoursCaisse,
  fromModePaiement,
} from '../../shared/reducers';
import {
  CoffreActions,
  JoursCaisseActions,
  ModePaiementActions,
} from '../../shared/actions';
import { CoffreDTO, ModepaieDTO, StatsDTO } from '../../../../swagger-api';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  products: any;
  stats$: Observable<any> = this.store$.select(fromJoursCaisse.selectStats);
  stats: StatsDTO;
  _alive = true;

  coffres$ = this.store$.select(fromCoffre.selectCoffres);
  mps: ModepaieDTO[] = [];
  modePaiements$ = this.store$.select(fromModePaiement.selectTableData);
  montantTot: number = 0;

  coffres: CoffreDTO[];
  selectCoffre: CoffreDTO;
  coffre: CoffreDTO;

  constructor(
    private store$: Store<
      fromJoursCaisse.State & fromModePaiement.State & fromCoffre.State
    >
  ) {}

  ngOnInit() {
    this.store$.dispatch(JoursCaisseActions.fetchStats());
    this.stats$
      .pipe(skipWhile((x) => x === null || x === undefined))
      .subscribe((x) => {
        this.stats = x;
      });
    this.store$.dispatch(ModePaiementActions.fetchData());
    this.modePaiements$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => (this.mps = x));
    this.store$.dispatch(CoffreActions.fetchCoffres());
    this.coffres$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => {
        this.coffres = x;
        this.montantTot = 0;
        x.forEach((item) => {
          this.montantTot += item?.montant!;
          this.montantTot;
        });
      });
  }
}
