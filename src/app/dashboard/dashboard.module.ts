import { InjectionToken, NgModule } from '@angular/core';

import { DashboarRoutingModule } from './dashboard-routing.module';
import { SharedModule } from '../shared/shared.module';
import { SidebarModule } from 'primeng/sidebar';
import {
  NbCardModule,
  NbDatepickerModule,
  NbIconModule,
  NbLayoutModule,
  NbMenuModule,
  NbSidebarModule,
  NbToastrModule,
} from '@nebular/theme';
import { CoreModule } from '../core/core.module';
import {
  fromArticle,
  fromBanque,
  fromBilletDetailAliExcep,
  fromBilletDetailComptage,
  fromBilletDetailSortie,
  fromBlade,
  fromChauffeur,
  fromCoffre,
  fromCoffreAlimentation,
  fromCompteDepense,
  fromDevise,
  fromEmetteur,
  fromEnteteDepense,
  fromEntetePrelevement,
  fromEtablissement,
  fromFournisseur,
  fromJoursCaisse,
  fromLastSyncApp,
  fromLigneDepense,
  fromLignePrelevement,
  fromMailDestinataire,
  fromModePaiement,
  fromMotif,
  fromMotifresync,
  fromParamBillet,
  fromParcaisse,
  fromPiedeche,
  fromReportBillet,
  fromReports,
  fromRoles,
  fromStatus,
  fromTypeFrais,
  fromUsers,
} from '../shared/reducers';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import {
  ArticleEffects,
  BanqueEffects,
  BilletDetailAliExcepEffects,
  BilletDetailComptageEffects,
  BilletDetailSortieEffects,
  ChauffeurEffects,
  CoffreAlimentationEffects,
  CoffreEffects,
  CompteDepenseEffects,
  DeviseEffects,
  EmetteurEffects,
  EnteteDepenseEffects,
  EntetePrelevementEffects,
  EtablissementEffects,
  FournisseurEffects,
  JoursCaisseEffects,
  LigneDepenseEffects,
  LignePrelevementEffects,
  MailDestinataireEffects,
  ModePaiementEffects,
  MotifEffects,
  MotifresyncEffects,
  ParamBilletEffects,
  ParcaisseEffects,
  PiedecheEffects,
  ReportEffects,
  RolesEffects,
  StatusEffects,
  TypeFraisEffects,
  UsersEffects,
} from '../shared/effects';
import { LastSyncAppEffects } from '../shared/effects/lastSyncApp.effects';
import { BladeEffects } from '../shared/effects/blade.effects';
import { IndividualConfig, ToastrService } from 'ngx-toastr';
import { UsersComponent } from './users/usersList/users.component';
import { RippleModule } from 'primeng/ripple';
import { CheckboxModule } from 'primeng/checkbox';
import { UsersModule } from './users/users.module';
import { ReportBilletEffects } from '../shared/effects/reportBillet.effects';

interface State {
  [fromJoursCaisse.joursCaisseFeatureKey]: fromJoursCaisse.State;
  [fromPiedeche.piedecheFeatureKey]: fromPiedeche.State;
  [fromModePaiement.modePaiementFeatureKey]: fromModePaiement.State;
  [fromEtablissement.etablissementFeatureKey]: fromEtablissement.State;
  [fromLastSyncApp.LastSyncAppFeatureKey]: fromLastSyncApp.State;
  [fromMotif.motifFeatureKey]: fromMotif.State;
  [fromChauffeur.chauffeurFeatureKey]: fromChauffeur.State;
  [fromEmetteur.emetteurFeatureKey]: fromEmetteur.State;
  [fromStatus.statusFeatureKey]: fromStatus.State;
  [fromBlade.bladeFeatureKey]: fromBlade.State;
  [fromBanque.banqueFeatureKey]: fromBanque.State;
  [fromTypeFrais.TypeFraisFeatureKey]: fromTypeFrais.State;
  [fromCoffre.coffreFeatureKey]: fromCoffre.State;
  [fromCoffreAlimentation.coffrealimentationFeatureKey]: fromCoffreAlimentation.State;
  [fromMotifresync.MotifresyncFeatureKey]: fromMotifresync.State;
  [fromReports.reportFeatureKey]: fromReports.State;
  [fromDevise.deviseFeatureKey]: fromDevise.State;
  [fromBilletDetailSortie.billetDetailSortieFeatureKey]: fromBilletDetailSortie.State;
  [fromBilletDetailComptage.billetDetailComptageFeatureKey]: fromBilletDetailComptage.State;
  [fromBilletDetailAliExcep.billetDetailAliExcepFeatureKey]: fromBilletDetailAliExcep.State;
  [fromParamBillet.paramBilletFeatureKey]: fromParamBillet.State;
  [fromArticle.articleFeatureKey]: fromArticle.State;
  [fromFournisseur.fournisseurFeatureKey]: fromFournisseur.State;
  [fromCompteDepense.compteDepenseFeatureKey]: fromCompteDepense.State;
  [fromMailDestinataire.mailDestinataireFeatureKey]: fromMailDestinataire.State;

  [fromEnteteDepense.enteteDepenseFeatureKey]: fromEnteteDepense.State;
  [fromEntetePrelevement.entetePrelevementFeatureKey]: fromEntetePrelevement.State;
  [fromLignePrelevement.lignePrelevementFeatureKey]: fromLignePrelevement.State;
  [fromLigneDepense.ligneDepenseFeatureKey]: fromLigneDepense.State;
  [fromParcaisse.parcaisseFeatureKey]: fromParcaisse.State;
  [fromReportBillet.reportBilletFeatureKey]: fromReportBillet.State;
}

export const reducers: ActionReducerMap<State> = {
  [fromJoursCaisse.joursCaisseFeatureKey]: fromJoursCaisse.reducer,
  [fromPiedeche.piedecheFeatureKey]: fromPiedeche.reducer,
  [fromModePaiement.modePaiementFeatureKey]: fromModePaiement.reducer,
  [fromEtablissement.etablissementFeatureKey]: fromEtablissement.reducer,
  [fromLastSyncApp.LastSyncAppFeatureKey]: fromLastSyncApp.reducer,
  [fromMotif.motifFeatureKey]: fromMotif.reducer,
  [fromEmetteur.emetteurFeatureKey]: fromEmetteur.reducer,
  [fromChauffeur.chauffeurFeatureKey]: fromChauffeur.reducer,
  [fromStatus.statusFeatureKey]: fromStatus.reducer,
  [fromBlade.bladeFeatureKey]: fromBlade.reducer,
  [fromBanque.banqueFeatureKey]: fromBanque.reducer,
  [fromTypeFrais.TypeFraisFeatureKey]: fromTypeFrais.reducer,
  [fromCoffre.coffreFeatureKey]: fromCoffre.reducer,
  [fromCoffreAlimentation.coffrealimentationFeatureKey]:
    fromCoffreAlimentation.reducer,
  [fromMotifresync.MotifresyncFeatureKey]: fromMotifresync.reducer,
  [fromReports.reportFeatureKey]: fromReports.reducer,
  [fromDevise.deviseFeatureKey]: fromDevise.reducer,
  [fromBilletDetailSortie.billetDetailSortieFeatureKey]:
    fromBilletDetailSortie.reducer,
  [fromBilletDetailComptage.billetDetailComptageFeatureKey]:
    fromBilletDetailComptage.reducer,
  [fromBilletDetailAliExcep.billetDetailAliExcepFeatureKey]:
    fromBilletDetailAliExcep.reducer,
  [fromParamBillet.paramBilletFeatureKey]: fromParamBillet.reducer,
  [fromArticle.articleFeatureKey]: fromArticle.reducer,
  [fromFournisseur.fournisseurFeatureKey]: fromFournisseur.reducer,
  [fromCompteDepense.compteDepenseFeatureKey]: fromCompteDepense.reducer,
  [fromMailDestinataire.mailDestinataireFeatureKey]:
    fromMailDestinataire.reducer,
  [fromEnteteDepense.enteteDepenseFeatureKey]: fromEnteteDepense.reducer,
  [fromEntetePrelevement.entetePrelevementFeatureKey]:
    fromEntetePrelevement.reducer,
  [fromLignePrelevement.lignePrelevementFeatureKey]:
    fromLignePrelevement.reducer,
  [fromLigneDepense.ligneDepenseFeatureKey]: fromLigneDepense.reducer,
  [fromParcaisse.parcaisseFeatureKey]: fromParcaisse.reducer,
  [fromReportBillet.reportBilletFeatureKey]: fromReportBillet.reducer,
};

interface StateUsers {
  [fromUsers.userFeatureKey]: fromUsers.State;
  [fromRoles.roleFeatureKey]: fromRoles.State;
}

export const reducersUsers: ActionReducerMap<StateUsers> = {
  [fromUsers.userFeatureKey]: fromUsers.reducer,
  [fromRoles.roleFeatureKey]: fromRoles.reducer,
};
const REDUCERS = new InjectionToken<ActionReducerMap<State>>('interfacage');
const REDUCERS_Users = new InjectionToken<ActionReducerMap<StateUsers>>(
  'users'
);
const toastrService = {
  success: (
    message?: string,
    title?: string,
    override?: Partial<IndividualConfig>
  ) => {},
  error: (
    message?: string,
    title?: string,
    override?: Partial<IndividualConfig>
  ) => {},
};

@NgModule({
  imports: [
    SharedModule,
    DashboarRoutingModule,
    SidebarModule,
    NbLayoutModule,
    NbMenuModule,
    NbSidebarModule,
    NbIconModule,
    CoreModule,
    NbToastrModule.forRoot(),

    NbDatepickerModule.forRoot(),
    StoreModule.forRoot(reducers),
    StoreModule.forFeature('interfacage', REDUCERS),
    StoreModule.forFeature('users', REDUCERS_Users),
    EffectsModule.forFeature([
      JoursCaisseEffects,
      ReportEffects,
      PiedecheEffects,
      ModePaiementEffects,
      EtablissementEffects,
      LastSyncAppEffects,
      MotifEffects,
      DeviseEffects,
      ChauffeurEffects,
      CompteDepenseEffects,
      MotifresyncEffects,
      EmetteurEffects,
      BladeEffects,
      StatusEffects,
      BanqueEffects,
      TypeFraisEffects,
      CoffreEffects,
      CoffreAlimentationEffects,
      ArticleEffects,
      FournisseurEffects,
      BilletDetailSortieEffects,
      BilletDetailComptageEffects,
      BilletDetailAliExcepEffects,
      ParamBilletEffects,
      MailDestinataireEffects,
      EnteteDepenseEffects,
      EntetePrelevementEffects,
      ParcaisseEffects,
      LignePrelevementEffects,
      LigneDepenseEffects,
    ]),
    EffectsModule.forRoot([
      JoursCaisseEffects,
      ReportEffects,
      PiedecheEffects,
      ModePaiementEffects,
      EtablissementEffects,
      BladeEffects,
      LastSyncAppEffects,
      MotifEffects,
      ChauffeurEffects,
      MotifresyncEffects,
      EmetteurEffects,
      StatusEffects,
      CoffreEffects,
      CoffreAlimentationEffects,
      UsersEffects,
      ArticleEffects,
      CompteDepenseEffects,
      MailDestinataireEffects,
      FournisseurEffects,
      RolesEffects,
      BanqueEffects,
      BilletDetailSortieEffects,
      BilletDetailComptageEffects,
      BilletDetailAliExcepEffects,
      ParamBilletEffects,
      EnteteDepenseEffects,
      EntetePrelevementEffects,
      LigneDepenseEffects,
      LignePrelevementEffects,
      ParcaisseEffects,
      ReportBilletEffects,
    ]),
    NbCardModule,
    CheckboxModule,
    RippleModule,
    UsersModule,
  ],
  providers: [
    { provide: REDUCERS, useValue: reducers },
    { provide: REDUCERS_Users, useValue: reducersUsers },
    { provide: ToastrService, useValue: toastrService },
  ],
  declarations: [...DashboarRoutingModule.components, UsersComponent],
})
export class DashboardModule {}
