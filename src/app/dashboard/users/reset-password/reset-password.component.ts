import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  NB_AUTH_OPTIONS,
  nbAuthCreateToken,
  NbAuthOAuth2JWTToken,
  NbAuthResult,
  NbAuthService,
  NbTokenService,
} from '@nebular/auth';
import { filter, finalize, map, switchMap } from 'rxjs/operators';
import { of as observableOf } from 'rxjs';
import { getDeepFromObject } from '../../../shared/utils/deep-copy';
import { AuthenticationService } from '../../../core/services/authentication.service';

@Component({
  selector: 'ngx-reset-password-page',
  styleUrls: ['./reset-password.component.scss'],
  templateUrl: './reset-password.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NgxResetPasswordComponent implements OnInit {
  minLength: number = this.getConfigValue(
    'forms.validation.password.minLength'
  );
  maxLength: number = this.getConfigValue(
    'forms.validation.password.maxLength'
  );
  redirectDelay: number = this.getConfigValue(
    'forms.resetPassword.redirectDelay'
  );
  showMessages: any = this.getConfigValue('forms.resetPassword.showMessages');
  strategy: string = this.getConfigValue('forms.resetPassword.strategy');
  isPasswordRequired: boolean = this.getConfigValue(
    'forms.validation.password.required'
  );

  submitted = false;
  errors: string[] = [];
  messages: string[] = [];
  user: any = {};
  resetPasswordForm: FormGroup;
  token: string;

  constructor(
    protected service: NbAuthService,
    private authenticationService: AuthenticationService,

    @Inject(NB_AUTH_OPTIONS) protected options = {},
    protected cd: ChangeDetectorRef,
    protected fb: FormBuilder,
    protected router: Router,
    private route: ActivatedRoute,
    protected tokenService: NbTokenService
  ) {}

  get userName() {
    return this.resetPasswordForm.get('userName');
  }

  get newPassword() {
    return this.resetPasswordForm.get('newPassword');
  }

  get confirmPassword() {
    return this.resetPasswordForm.get('confirmPassword');
  }

  ngOnInit(): void {
    const passwordValidators = [
      Validators.minLength(this.minLength),
      Validators.maxLength(this.maxLength),
    ];
    if (this.isPasswordRequired) {
      passwordValidators.push(Validators.required);
    }

    this.resetPasswordForm = this.fb.group({
      userName: this.fb.control('', [Validators.required]),
      newPassword: this.fb.control('', [...passwordValidators]),
      confirmPassword: this.fb.control('', [...passwordValidators]),
    });

    this.route.queryParams.pipe().subscribe((params) => {
      this.token = JSON.parse(
        localStorage.getItem('currentUser') || '{}'
      ).token;
      console.log(JSON.parse(localStorage.getItem('currentUser') || '{}'));
      console.log('params ' + JSON.stringify(params));
    });
  }

  resetPass(): void {
    this.errors = this.messages = [];
    this.submitted = true;
    this.user = { ...this.resetPasswordForm.value, token: this.token };
    this.service
      .resetPassword(this.strategy, this.user)
      .pipe(finalize(() => (this.submitted = false)))
      .subscribe((result) => {
        if (result) {
          this.messages = result.getMessages();
        } else {
          this.errors = result;
        }

        const redirect = result.getRedirect();
        if (redirect) {
          setTimeout(() => {
            this.authenticationService.logout();
            this.router.navigate(['auth/login']);
          }, this.redirectDelay);
        }
        this.cd.detectChanges();
      });
  }

  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }
}
