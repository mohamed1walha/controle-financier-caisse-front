import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { RolesComponent } from './roles/roles.component';
import { NgxResetPasswordComponent } from './reset-password/reset-password.component';
import {
  NbAlertModule,
  NbButtonModule,
  NbCardModule,
  NbInputModule,
  NbSpinnerModule,
} from '@nebular/theme';
import { ComponentsModule } from '../../core/@components/components.module';
import { ReactiveFormsModule } from '@angular/forms';
import { ChangePasswordComponent } from './change-password/change-password.component';

@NgModule({
  declarations: [
    RolesComponent,
    NgxResetPasswordComponent,
    ChangePasswordComponent,
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    NbAlertModule,
    NbCardModule,
    ComponentsModule,
    NbInputModule,
    ReactiveFormsModule,
    NbButtonModule,
    NbSpinnerModule,
  ],
  exports: [NgxResetPasswordComponent, ChangePasswordComponent],
})
export class UsersModule {}
