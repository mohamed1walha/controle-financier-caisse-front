import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Store } from '@ngrx/store';
import { ChangePasswordDTO, UserService } from 'swagger-api';
import { NB_AUTH_OPTIONS, NbAuthService } from '@nebular/auth';
import { finalize } from 'rxjs/operators';
import { AuthenticationService } from '../../../core/services/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { getDeepFromObject } from '../../../shared/utils/deep-copy';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';

@Component({
  selector: 'sds-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {
  changePasswordForm: FormGroup;
  readonly minLength: number = 4;
  readonly maxLength: number = 25;
  submitClicked = false;
  errors: string[] = [];
  messages: string[] = [];
  strategy: string = this.getConfigValue('forms.resetPassword.strategy');

  constructor(
    private fb: FormBuilder,
    @Inject(NB_AUTH_OPTIONS) protected options = {},

    protected userService: UserService,
    protected service: NbAuthService,
    private authenticationService: AuthenticationService,
    protected router: Router,
    private route: ActivatedRoute,
    private toastService: NbToastrService,

    protected cd: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.buildUserForm();
  }

  get(key: string): AbstractControl {
    return this.changePasswordForm.get(key)!;
  }
  buildUserForm() {
    const minMaxLength = [
      Validators.required,
      Validators.minLength(this.minLength),
      Validators.maxLength(this.maxLength),
    ];
    const passwordValidators = [Validators.required, ...minMaxLength];
    this.changePasswordForm = this.fb.group({
      oldPassword: ['', [...passwordValidators]],
      password: ['', [...passwordValidators]],
      confirmPassword: ['', [...passwordValidators]],
    });
  }

  changeUserPassword() {
    this.submitClicked = true;
    this.errors = this.messages = [];

    const changePassword: ChangePasswordDTO = this.changePasswordForm
      .value as ChangePasswordDTO;

    this.service
      .requestPassword(this.strategy, changePassword)
      .pipe(finalize(() => (this.submitClicked = false)))
      .subscribe((result) => {
        if (result) {
          console.log(JSON.stringify(result));
          this.messages = result.getMessages();
        } else {
          console.log(JSON.stringify(result));

          this.errors = result;
        }

        const redirect = result.getRedirect();
        if (redirect) {
          setTimeout(() => {
            this.toastService.success(
              $localize`Mot de passe modifié avec succès`,
              $localize`Succès`,
              {
                duration: 3000,
                position: NbGlobalPhysicalPosition.BOTTOM_RIGHT,
              }
            );
            this.changePasswordForm.reset();
          }, 100);
        } else {
          this.toastService.danger(
            $localize`Vérifier mot de pass`,
            $localize`Erreur`,
            {
              duration: 3000,
              position: NbGlobalPhysicalPosition.BOTTOM_RIGHT,
            }
          );
        }
        this.cd.detectChanges();
      });
    // setTimeout(() => {
    //   this.userService.change({ changePassword }));
    //   this.submitClicked = false;
    // }, 1000);
  }
  getConfigValue(key: string): any {
    return getDeepFromObject(this.options, key, null);
  }
}
