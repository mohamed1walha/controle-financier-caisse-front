import { Component, OnInit } from '@angular/core';
import {
  fromBlade,
  fromEtablissement,
  fromModePaiement,
  fromRoles,
  fromUsers,
} from '../../../shared/reducers';
import {
  EtablissementDTO,
  ModepaieDTO,
  RolesDTO,
  UserDTO,
} from '../../../../../swagger-api';
import { Store } from '@ngrx/store';
import { ConfirmationService, MessageService } from 'primeng/api';
import {
  EtablissementActions,
  ModePaiementActions,
  RolesActions,
  UsersActions,
} from '../../../shared/actions';
import { Observable, skipWhile, takeWhile } from 'rxjs';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
})
export class UsersComponent implements OnInit {
  users$ = this.store$.select(fromUsers.selectUsers);
  etablissement$: Observable<EtablissementDTO[]> = this.store$.select(
    fromEtablissement.selectEtablissements
  );
  etablissements: EtablissementDTO[];
  roles$: Observable<RolesDTO[]> = this.store$.select(fromRoles.selectRoles);
  modePaies$: Observable<ModepaieDTO[]> = this.store$.select(
    fromModePaiement.selectTableData
  );
  roles: RolesDTO[];
  modePaies: ModepaieDTO[];
  users: UserDTO[];
  selectUser: UserDTO;
  user: UserDTO;
  usersDialog: boolean;
  submitted: boolean;
  _alive: boolean = true;

  constructor(
    private store$: Store<
      fromUsers.State &
        fromBlade.State &
        fromEtablissement.State &
        fromModePaiement.State
    >,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.store$.dispatch(EtablissementActions.fetchEtablissements());
    this.etablissement$
      .pipe(skipWhile((x) => x === null || x === undefined))
      .subscribe((x) => {
        this.etablissements = x;
      });
    this.store$.dispatch(ModePaiementActions.fetchData());

    this.store$.dispatch(UsersActions.fetchUsers());
    this.store$.dispatch(RolesActions.fetchRoles());
    this.users$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => (this.users = x));
    this.modePaies$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => (this.modePaies = x));
    this.roles$
      .pipe(
        skipWhile((x) => x === null || x === undefined),
        takeWhile(() => this._alive)
      )
      .subscribe((x) => (this.roles = x));
  }

  openNew() {
    this.user = {};
    this.user.isAdmin = false;
    this.submitted = false;
    this.usersDialog = true;
  }

  getValueFilter(event: any): string {
    return event.target.value;
  }

  deleteUser(user: UserDTO) {
    this.confirmationService.confirm({
      message:
        $localize`Etes-vous sûr que vous voulez supprimer ` +
        user.username +
        '?',
      header: $localize`Confirmer`,
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.store$.dispatch(UsersActions.DeleteUser({ user: user }));
      },
      reject: () => {
        this.confirmationService.close();
      },
    });
  }

  hideDialog() {
    this.usersDialog = false;
    this.submitted = false;
  }

  editUser(users: UserDTO) {
    this.user = { ...users };
    this.usersDialog = true;
  }

  saveUser() {
    this.submitted = true;
    if (this.user.id) {
      this.store$.dispatch(UsersActions.EditUser({ user: this.user }));
      this.usersDialog = false;
    } else {
      this.user.password = this.user.firstName + '.' + this.user.lastName;
      this.store$.dispatch(UsersActions.CreateUser({ user: this.user }));
      this.usersDialog = false;
    }
  }
}
