import {Component, OnInit} from '@angular/core';
import {NbMenuService, NbSidebarService} from "@nebular/theme";
import {LayoutService} from "../../shared/utils/layout.service";
import {AuthenticationService} from "../services/authentication.service";
import {Router} from "@angular/router";
import {LocalService} from "../services/locale.service";

@Component({
  selector: 'ngx-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  userMenu = [{title: 'Profile'}, {title: 'Log out'}];
  currentUser = (JSON.parse(localStorage.getItem('currentUser') || '{}')).currentUser;
  siteLanguage: string = $localize`Français`;

  siteLocale: string;

  languageList :any[] = [
    { code: 'en', label: $localize`Anglais` },
    { code: 'fr', label: $localize`Français` }
  ];
  constructor(private sidebarService: NbSidebarService,
              private layoutService: LayoutService,
              private menuService: NbMenuService,
              private authenticationService: AuthenticationService,
              private router: Router,
              private localStore: LocalService
  ) {
  }

  ngOnInit(): void {
    this.siteLocale = window.location.pathname.split('/')[1];
    if(this.localStore.getData('language') === null){
      this.localStore.saveData('language',this.siteLocale)
    }else{
      this.localStore.removeData('language');
      this.localStore.saveData('language',this.siteLocale)
    }
    //this.siteLanguage = this.languageList.find(f => f?.code === this.siteLocale).label;
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['auth/login']);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');
    //this.layoutService.changeLayoutSize();

    return false;
  }

  navigateHome() {
    this.menuService.navigateHome();
    return false;
  }

  saveCurrentLangue(langue: string) {
    if(this.localStore.getData('language') === null){
      this.localStore.saveData('language',langue)
    }else{
      this.localStore.removeData('language');
      this.localStore.saveData('language',langue)
    }
  }
}
