import { Injectable } from '@angular/core';
import jsPDF from 'jspdf';
import autoTable from 'jspdf-autotable';
import * as FileSaver from 'file-saver';
import { DatePipe } from '@angular/common';
import { deepCopy } from '../../shared/utils/deep-copy';
import { LocalService } from './locale.service';
import { SeparateurPipe } from '../../shared/utils/separateur.pipe';
import {
  ReportBilletDTO,
  ReportBilletRequestDTO,
} from '../../../../swagger-api';
import * as XLSX from 'xlsx';

@Injectable({
  providedIn: 'root',
})
export class ExportService {
  constructor(
    private datePipe: DatePipe,
    private numberPipe: SeparateurPipe,
    private localStore: LocalService
  ) {}

  exportExcelCustom(dt, selectedjoursCaisses, joursCaisses, columns) {
    import('xlsx').then((xlsx) => {
      dt.filteredValue === null
        ? (selectedjoursCaisses = joursCaisses)
        : (selectedjoursCaisses = deepCopy(dt.filteredValue));
      var mappedItems = [] as any;

      //dt.filteredValue === null ? selectedjoursCaisses = joursCaisses : selectedjoursCaisses = deepCopy(dt.filteredValue);
      function getDateFrom(etLibelle: string | null | undefined) {
        let selectionEt = joursCaisses.filter((x) => {
          return x.gjcEtablissement?.etLibelle === etLibelle;
        });
        let x = new Date(
          Math.min(...selectionEt.map((e) => new Date(e.gjcDateouv)))
        );
        return x;
      }

      function getDateTo(etLibelle: string | null | undefined) {
        let selectionEt = joursCaisses.filter((x) => {
          return x.gjcEtablissement?.etLibelle === etLibelle;
        });
        let x = new Date(
          Math.max(...selectionEt.map((e) => new Date(e.gjcDateouv)))
        );
        return x;
      }

      const result = [
        ...joursCaisses
          .reduce((r, o) => {
            const key =
              o.gjcEtablissement?.etLibelle +
              '-' +
              o.emetteur?.emetteurLibelle +
              '-' +
              o.chauffeur?.chauffeurLibelle;
            //console.log(key)
            const item =
              r.get(key) ||
              Object.assign({}, o, {
                instances: 0,
                dateFrom: null,
                dateTo: null,
              });
            item.dateFrom = this.datePipe.transform(
              getDateFrom(o.gjcEtablissement?.etLibelle),
              'dd/MM/yyyy'
            );
            item.dateTo = this.datePipe.transform(
              getDateTo(o.gjcEtablissement?.etLibelle),
              'dd/MM/yyyy'
            );

            item.instances += 1;
            return r.set(key, item);
          }, new Map())
          .values(),
      ];

      if (this.localStore.getData('language') === 'fr') {
        result.forEach((item, key) => {
          mappedItems.push({
            Etablissement: item.gjcEtablissement?.etLibelle,
            Période: 'DU ' + item.dateFrom + ' AU ' + item.dateTo,
            'Nbre d"enveloppe': item.instances || '',
            Remarques: '',
            'Nom Emetteur': item.emetteur?.emetteurLibelle || '',
            'Signature Emetteur': '',
            'Cachet Emetteur': '',
            'Nom Chauffeur': item.chauffeur?.chauffeurLibelle || '',
            'Signature Chauffeur': '',
            'Date Reception':
              item.statusReceptionObj?.code === 'RCP'
                ? this.datePipe.transform(item.receptionDate, 'dd/MM/yyyy')
                : '',
          });
        });
      }
      if (this.localStore.getData('language') === 'en') {
        result.forEach((item, key) => {
          mappedItems.push({
            Etablissement: item.gjcEtablissement?.etLibelle,
            Period:
              $localize`DU ` + item.dateFrom + $localize` AU ` + item.dateTo,
            'Number of envelopes': item.instances || '',
            Remarks: '',
            'Name Issuer': item.emetteur?.emetteurLibelle || '',
            'Signature Issuer': '',
            'Stamp Issuer': '',
            'Name Driver': item.chauffeur?.chauffeurLibelle || '',
            'Signature Driver': '',
            'Date Received':
              item.statusReceptionObj?.code === 'RCP'
                ? this.datePipe.transform(item.receptionDate, 'dd/MM/yyyy')
                : '',
          });
        });
      }

      const worksheet = xlsx.utils.json_to_sheet(mappedItems);
      const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, {
        bookType: 'xlsx',
        type: 'array',
      });
      this.saveAsExcelFile(excelBuffer, $localize`JoursCaisses`);
    });
  }

  exportExcel(dt, selectedjoursCaisses, joursCaisses) {
    import('xlsx').then((xlsx) => {
      dt.filteredValue === null
        ? (selectedjoursCaisses = joursCaisses)
        : (selectedjoursCaisses = deepCopy(dt.filteredValue));

      const worksheet = xlsx.utils.json_to_sheet(selectedjoursCaisses);
      const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, {
        bookType: 'xlsx',
        type: 'array',
      });
      this.saveAsExcelFile(excelBuffer, $localize`JoursCaisses`);
    });
  }

  saveAsExcelFile(buffer: any, fileName: string): void {
    let EXCEL_TYPE =
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    let EXCEL_EXTENSION = '.xlsx';
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE,
    });
    FileSaver.saveAs(
      data,
      fileName +
        '_export_' +
        this.datePipe.transform(new Date(), 'YYYYMMdd hh:mm:ss') +
        EXCEL_EXTENSION
    );
  }

  exportPdf(dt, selectedjoursCaisses, joursCaisses, columns) {
    var mappedItems = [] as any;
    var foot = [] as any;
    let sum = 0;
    dt.filteredValue === null
      ? (selectedjoursCaisses = joursCaisses)
      : (selectedjoursCaisses = deepCopy(dt.filteredValue));
    selectedjoursCaisses.forEach((item, key) => {
      if (item.montantCaisse != null) {
        sum += item.montantCaisse!;
      }
      mappedItems.push({
        gjcEtablissementlabel: item.gjcEtablissement?.etLibelle,
        gjcCaisse: item.gjcCaisse,
        gjcNumzcaisse: item.gjcNumzcaisse,
        gjcDateouv: this.datePipe.transform(item.gjcDateouv, 'dd-MMM-YYYY'),
        gjcHeureouv: item.gjcHeureouv,
        gjcDateferme: this.datePipe.transform(item.gjcDateferme, 'dd-MMM-YYYY'),
        gjcHeureferme: item.gjcHeureferme,
        gjcUserferme: item.gjcUserferme,
        dateCreate: this.datePipe.transform(item.dateCreate, 'dd-MMM-YYYY'),
        montantCaisse: this.numberPipe.transform(item.montantCaisse),
      });
    });

    foot.push({
      gjcEtablissementLabel: '',
      gjcCaisse: $localize`Montant Total:`,
      gjcNumzcaisse: '',
      gjcDateouv: '',
      gjcHeureouv: '',
      gjcDateferme: '',
      gjcHeureferme: '',
      gjcUserferme: '',
      dateCreate: '',
      montantCaisse: this.numberPipe.transform(sum),
    });
    const doc = new jsPDF('p');
    autoTable(doc, {
      columns: columns,
      body: mappedItems,
      foot: foot,
      margin: { top: 15, bottom: 10 },
      pageBreak: 'auto',
      showHead: 'everyPage',
      columnStyles: {
        0: { cellWidth: 'auto' },
        1: { cellWidth: 'auto' },
        2: { cellWidth: 'auto' },
        3: { cellWidth: 'auto' },
        4: { cellWidth: 'auto' },
        5: { cellWidth: 'auto' },
        6: { cellWidth: 'auto' },
        7: { cellWidth: 'auto' },
        8: { cellWidth: 'auto' },
        9: { cellWidth: 'auto', halign: 'right' },
      },

      headStyles: { fontSize: 7, fontStyle: 'bolditalic' },
      rowPageBreak: 'auto',
      footStyles: {
        fontSize: 10,
        fontStyle: 'bolditalic',
        lineColor: '',
        halign: 'right',
      },
      tableWidth: 'auto',
      styles: { overflow: 'linebreak', fontStyle: 'normal', fontSize: 7 },

      didDrawPage: (dataArg) => {
        doc.setFontSize(12);
        doc.setTextColor(20);
        doc.text($localize`Jours Caisses`, dataArg.settings.margin.left, 10);
      },
    });
    doc.save(
      $localize`JoursCaisses_export_` +
        this.datePipe.transform(new Date(), 'YYYYMMdd hh:mm:ss') +
        '.pdf'
    );
  }

  getDateFrom(s) {}

  exportPdfReception(dt, selectedjoursCaisses, joursCaisses, columns) {
    var mappedItems = [] as any;

    //dt.filteredValue === null ? selectedjoursCaisses = joursCaisses : selectedjoursCaisses = deepCopy(dt.filteredValue);
    function getDateFrom(etLibelle: string | null | undefined) {
      let selectionEt = joursCaisses.filter((x) => {
        return x.gjcEtablissement?.etLibelle === etLibelle;
      });
      let x = new Date(
        Math.min(...selectionEt.map((e) => new Date(e.gjcDateouv)))
      );
      return x;
    }

    function getDateTo(etLibelle: string | null | undefined) {
      let selectionEt = joursCaisses.filter((x) => {
        return x.gjcEtablissement?.etLibelle === etLibelle;
      });
      let x = new Date(
        Math.max(...selectionEt.map((e) => new Date(e.gjcDateouv)))
      );
      return x;
    }

    const result = [
      ...joursCaisses
        .reduce((r, o) => {
          const key =
            o.gjcEtablissement?.etLibelle +
            '-' +
            o.emetteur?.emetteurLibelle +
            '-' +
            o.chauffeur?.chauffeurLibelle;
          //console.log(key)
          const item =
            r.get(key) ||
            Object.assign({}, o, {
              instances: 0,
              dateFrom: null,
              dateTo: null,
            });
          item.dateFrom = this.datePipe.transform(
            getDateFrom(o.gjcEtablissement?.etLibelle),
            'dd/MM/yyyy'
          );
          item.dateTo = this.datePipe.transform(
            getDateTo(o.gjcEtablissement?.etLibelle),
            'dd/MM/yyyy'
          );

          item.instances += 1;
          return r.set(key, item);
        }, new Map())
        .values(),
    ];

    result.forEach((item, key) => {
      mappedItems.push({
        gjcEtablissementlabel: item.gjcEtablissement?.etLibelle,
        periode: $localize`DU ` + item.dateFrom + $localize` AU ` + item.dateTo,
        nbrEnv: item.instances || '',
        nomEm: item.emetteur?.emetteurLibelle || '',
        nomChauf: item.chauffeur?.chauffeurLibelle || '',
        receipDate:
          item.statusReceptionObj?.code === 'RCP'
            ? this.datePipe.transform(item.receptionDate, 'dd/MM/yyyy')
            : '',
      });
    });
    const doc = new jsPDF('p');
    autoTable(doc, {
      columns: columns,
      body: mappedItems,
      margin: { top: 15, bottom: 10 },
      pageBreak: 'auto',
      showHead: 'everyPage',
      columnStyles: {
        0: { cellWidth: 'wrap' },
      },
      styles: { overflow: 'linebreak', fontStyle: 'normal', fontSize: 7 },
      theme: 'grid',

      didDrawPage: (dataArg) => {
        doc.setFontSize(8);
        doc.setTextColor(20);
        doc.text(
          $localize`Fiche de collecte des caisses`,
          dataArg.settings.margin.left,
          6
        );
        doc.text(
          $localize`Date: ` + this.datePipe.transform(Date.now(), 'dd/MM/yyyy'),
          dataArg.settings.margin.left,
          12
        );
      },
    });
    doc.save(
      $localize`JoursCaisses_export_` +
        this.datePipe.transform(new Date(), 'YYYYMMdd hh:mm:ss') +
        '.pdf'
    );
  }

  exportReportPdf(
    selectedReport,
    dateMinEx,
    dateMaxEx,
    columns,
    MontantTotal,
    MontantCompte,
    MontantVerse,
    Depense,
    Ecart
  ) {
    var foot = [] as any;
    var mappedItems = [] as any;

    let dateMin = dateMinEx;
    let dateMax = dateMaxEx;

    selectedReport.forEach((item, key) => {
      mappedItems.push({
        etablissement: item.etablissement,
        caisse: item.caisse,
        referenceZ: item.referenceZ,
        dateOuverture: item.dateOuverture,
        heureOuverture: item.heureOuverture,
        modePaiement: item.modePaiement,
        montantTotal: this.numberPipe.transform(item.montantTotal),
        montantCompte: this.numberPipe.transform(item.montantCompte),
        montantVerse: this.numberPipe.transform(item.montantVerse),
        depense: this.numberPipe.transform(item.depense),
        ecart: this.numberPipe.transform(item.ecart),
        statut: item.statut,
      });
    });

    foot.push({
      etablissement: '',
      caisse: '',
      referenceZ: '',
      dateOuverture: '',
      heureOuverture: '',
      modePaiement: $localize`Total`,
      montantTotal: this.numberPipe.transform(MontantTotal),
      montantCompte: this.numberPipe.transform(MontantCompte),
      montantVerse: this.numberPipe.transform(MontantVerse),
      depense: this.numberPipe.transform(Depense),
      ecart: this.numberPipe.transform(Ecart),
      statut: '',
    });
    const doc = new jsPDF('p');
    autoTable(doc, {
      columns: columns,
      body: mappedItems,
      foot: foot,
      margin: { top: 15, bottom: 10 },
      pageBreak: 'auto',
      showHead: 'everyPage',
      columnStyles: {
        0: { cellWidth: 'auto' },
        1: { cellWidth: 'auto' },
        2: { cellWidth: 'auto' },
        3: { cellWidth: 'auto' },
        4: { cellWidth: 'auto' },
        5: { cellWidth: 'auto', halign: 'right' },
        6: { cellWidth: 'auto', halign: 'right' },
        7: { cellWidth: 'auto', halign: 'right' },
        8: { cellWidth: 'auto', halign: 'right' },
        9: { cellWidth: 'auto', halign: 'right' },
        10: { cellWidth: 'auto' },
      },

      headStyles: { fontSize: 6, fontStyle: 'bolditalic' },
      rowPageBreak: 'auto',
      footStyles: {
        fontSize: 8,
        fontStyle: 'bolditalic',
        lineColor: '',
        halign: 'right',
      },
      tableWidth: 'auto',
      styles: { overflow: 'linebreak', fontStyle: 'normal', fontSize: 7 },

      didDrawPage: (dataArg) => {
        doc.setFontSize(9);
        doc.setTextColor(15);
        doc.text('Rapport', dataArg.settings.margin.left, 9);
        if (dateMin.length > 0 && dateMax.length > 0) {
          doc.text(
            $localize`Période : De ` + dateMin + $localize` A ` + dateMax,
            dataArg.settings.margin.left,
            13
          );
        }

        if (dateMin.length > 0 && dateMax.length === 0) {
          doc.text('Période : De ' + dateMin, dataArg.settings.margin.left, 13);
        }

        if (dateMin.length === 0 && dateMax.length > 0) {
          doc.text(
            $localize`Période :  A ` + dateMax,
            dataArg.settings.margin.left,
            13
          );
        }
      },
    });
    doc.save(
      $localize`Report_export_` +
        this.datePipe.transform(new Date(), 'YYYYMMdd hh:mm:ss') +
        '.pdf'
    );
  }

  exportExcelReport(
    selectedReport,
    dateOuverture,
    dateOuvertureTo,
    columns,
    MontantTotal,
    MontantCompte,
    MontantVerse,
    Depense,
    Ecart
  ) {
    import('xlsx').then((xlsx) => {
      var mappedItems = [] as any;

      let dateMin = new Date(
        Math.min(...selectedReport.map((e) => new Date(e.dateOuverture)))
      );
      let dateMax = new Date(
        Math.max(...selectedReport.map((e) => new Date(e.dateOuverture)))
      );

      if (this.localStore.getData('language') === 'fr') {
        selectedReport.forEach((item, key) => {
          mappedItems.push({
            Etablissement: item.etablissement,
            Caisse: item.caisse,
            'Reference Z': item.referenceZ,
            'Date Ouverture': item.dateOuverture,
            'Heure Ouverture': item.heureOuverture,
            'Mode Paiement': item.modePaiement,
            'Montant Total': this.numberPipe.transform(item.montantTotal),
            'Montant Comptée': this.numberPipe.transform(item.montantCompte),
            'Montant Versée': this.numberPipe.transform(item.montantVerse),
            Dépense: this.numberPipe.transform(item.depense),
            Ecart: this.numberPipe.transform(item.ecart),
            Statut: item.statut,
          });
        });
        if (dateOuverture.length > 0 && dateOuvertureTo.length > 0) {
          mappedItems.push({
            Etablissement: '',
            Caisse: '',
            'Reference Z': '',
            'Date Ouverture':
              $localize`Période : De ` +
              dateOuverture +
              $localize` A ` +
              dateOuvertureTo,
            'Heure Ouverture': '',
            'Mode Paiement': $localize`Total :`,
            'Montant Total': this.numberPipe.transform(MontantTotal),
            'Montant Comptée': this.numberPipe.transform(MontantCompte),
            'Montant Versée': this.numberPipe.transform(MontantVerse),
            Dépense: this.numberPipe.transform(Depense),
            Ecart: this.numberPipe.transform(Ecart),
            Statut: '',
          });
        }
        if (dateOuverture.length === 0 && dateOuvertureTo.length > 0) {
          mappedItems.push({
            Etablissement: '',
            Caisse: '',
            'Reference Z': '',
            'Date Ouverture':
              $localize`Période : ` + $localize` A ` + dateOuvertureTo,
            'Heure Ouverture': '',
            'Mode Paiement': $localize`Total :`,
            'Montant Total': this.numberPipe.transform(MontantTotal),
            'Montant Comptée': this.numberPipe.transform(MontantCompte),
            'Montant Versée': this.numberPipe.transform(MontantVerse),
            Dépense: this.numberPipe.transform(Depense),
            Ecart: this.numberPipe.transform(Ecart),
            Statut: '',
          });
        }
        if (dateOuverture.length > 0 && dateOuvertureTo.length === 0) {
          mappedItems.push({
            Etablissement: '',
            Caisse: '',
            'Reference Z': '',
            'Date Ouverture':
              $localize`Période : ` + $localize` De ` + dateOuverture,
            'Heure Ouverture': '',
            'Mode Paiement': $localize`Total :`,
            'Montant Total': this.numberPipe.transform(MontantTotal),
            'Montant Comptée': this.numberPipe.transform(MontantCompte),
            'Montant Versée': this.numberPipe.transform(MontantVerse),
            Dépense: this.numberPipe.transform(Depense),
            Ecart: this.numberPipe.transform(Ecart),
            Statut: '',
          });
        }
      }
      if (this.localStore.getData('language') === 'en') {
        selectedReport.forEach((item, key) => {
          mappedItems.push({
            Establishment: item.etablissement,
            'Cash register': item.caisse,
            'Reference Z': item.referenceZ,
            'Opening date': item.dateOuverture,
            'Opening time': item.heureOuverture,
            'Payment method': item.modePaiement,
            'Total amount': this.numberPipe.transform(item.montantTotal),
            'Amount Counted': this.numberPipe.transform(item.montantCompte),
            'Amount Paid': this.numberPipe.transform(item.montantVerse),
            Spent: this.numberPipe.transform(item.depense),
            Difference: this.numberPipe.transform(item.ecart),
            Status: item.statutEN,
          });
        });
        if (dateOuverture.length > 0 && dateOuvertureTo.length > 0) {
          mappedItems.push({
            Establishment: '',
            'Cash register': '',
            'Reference Z': '',
            'Opening date':
              $localize`Période : De ` +
              dateOuverture +
              $localize` A ` +
              dateOuvertureTo,
            'Opening time': '',
            'Payment method': $localize`Total :`,
            'Total amount': this.numberPipe.transform(MontantTotal),
            'Amount Counted': this.numberPipe.transform(MontantCompte),
            'Amount Paid': this.numberPipe.transform(MontantVerse),
            Spent: this.numberPipe.transform(Depense),
            Difference: this.numberPipe.transform(Ecart),
            Status: '',
          });
        }
        if (dateOuverture.length === 0 && dateOuvertureTo.length > 0) {
          mappedItems.push({
            Establishment: '',
            'Cash register': '',
            'Reference Z': '',
            'Opening date':
              $localize`Période : ` + $localize` A ` + dateOuvertureTo,
            'Opening time': '',
            'Payment method': $localize`Total :`,
            'Total amount': this.numberPipe.transform(MontantTotal),
            'Amount Counted': this.numberPipe.transform(MontantCompte),
            'Amount Paid': this.numberPipe.transform(MontantVerse),
            Spent: this.numberPipe.transform(Depense),
            Difference: this.numberPipe.transform(Ecart),
            Status: '',
          });
        }
        if (dateOuverture.length > 0 && dateOuvertureTo.length === 0) {
          mappedItems.push({
            Establishment: '',
            'Cash register': '',
            'Reference Z': '',
            'Opening date':
              $localize`Période : ` + $localize` De ` + dateOuverture,
            'Opening time': '',
            'Payment method': $localize`Total :`,
            'Total amount': this.numberPipe.transform(MontantTotal),
            'Amount Counted': this.numberPipe.transform(MontantCompte),
            'Amount Paid': this.numberPipe.transform(MontantVerse),
            Spent: this.numberPipe.transform(Depense),
            Difference: this.numberPipe.transform(Ecart),
            Status: '',
          });
        }
      }

      const worksheet = xlsx.utils.json_to_sheet(mappedItems);
      const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
      const excelBuffer: any = xlsx.write(workbook, {
        bookType: 'xlsx',
        type: 'array',
      });
      this.saveAsExcelFile(
        excelBuffer,
        $localize`Report_export_` +
          this.datePipe.transform(new Date(), 'YYYYMMdd hh:mm:ss')
      );
    });
  }
  // exportExcelReportBillet(
  //   selectedReport: ReportBilletDTO[],
  //   RequestReport: ReportBilletRequestDTO,
  //   dateOuverture,
  //   dateOuvertureTo,
  //   columns
  // ) {
  //   import('xlsx').then((xlsx) => {
  //     var mappedItems = [] as any;
  //
  //     let dateMin = new Date(
  //       Math.min(new Date(RequestReport?.startDate!).getDate())
  //     );
  //     let dateMax = new Date(
  //       Math.max(new Date(RequestReport?.endDate!).getDate())
  //     );
  //     console.log(
  //       "this.localStore.getData('language') " +
  //         this.localStore.getData('language')
  //     );
  //     const filterDetails = {
  //       'Start Date': RequestReport.startDate || 'N/A',
  //       'End Date': RequestReport.endDate || 'N/A',
  //       'Etablissment IDs': RequestReport.etablissmentIds
  //         ? RequestReport.etablissmentIds.join(', ')
  //         : 'N/A',
  //       'Caisse IDs': RequestReport.caisseIds
  //         ? RequestReport.caisseIds.join(', ')
  //         : 'N/A',
  //     };
  //
  //     mappedItems.push(filterDetails);
  //     // if (this.localStore.getData('language') === 'fr') {
  //     selectedReport?.forEach((item, key) => {
  //       mappedItems.push({
  //         note: item.note,
  //         montantTotal: item.montantTotal,
  //         quantiteTotal: item.quantiteTotal,
  //       });
  //     });
  //     if (dateOuverture.length > 0 && dateOuvertureTo.length > 0) {
  //       mappedItems.push({
  //         note: 'note',
  //         montantTotal: 'montantTotal',
  //         quantiteTotal: 'quantiteTotal',
  //       });
  //     }
  //
  //     const worksheet = xlsx.utils.json_to_sheet(mappedItems);
  //     const workbook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
  //     const excelBuffer: any = xlsx.write(workbook, {
  //       bookType: 'xlsx',
  //       type: 'array',
  //     });
  //     this.saveAsExcelFile(
  //       excelBuffer,
  //       $localize`Report_export_` +
  //         this.datePipe.transform(new Date(), 'YYYYMMdd hh:mm:ss')
  //     );
  //   });
  // }
  // exportExcelReportBillet(
  //   selectedReport: ReportBilletDTO[],
  //   RequestReport: ReportBilletRequestDTO,
  //   dateOuverture,
  //   dateOuvertureTo,
  //   columns
  // ) {
  //   XLSX.writeFile(
  //     {
  //       SheetNames: ['data'],
  //       Sheets: {
  //         data: {
  //           ...this.generateWorksheet(
  //             selectedReport,
  //             RequestReport,
  //             dateOuverture,
  //             dateOuvertureTo,
  //             columns
  //           ),
  //           '!cols': [{ width: 15 }, { width: 15 }, { width: 15 }], // Définir la largeur des colonnes
  //           '!rows': [{ hpt: 20 }], // Définir la hauteur de la première ligne
  //         },
  //       },
  //     },
  //     'exported_report.xlsx'
  //   );
  // }
  //
  // generateWorksheet(
  //   selectedReport,
  //   RequestReport,
  //   dateOuverture,
  //   dateOuvertureTo,
  //   columns
  // ) {
  //   const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(selectedReport);
  //
  //   // Appliquer un style à la première ligne (lignes 1)
  //   ws['!rows'] = [{ hpt: 30 }]; // Hauteur de la première ligne
  //   ws['A1'].s = { font: { bold: true }, alignment: { horizontal: 'center' } }; // Style de la cellule A1
  //
  //   // Appliquer un style aux données du rapport (à partir de la ligne 2)
  //   for (let r = 2; r <= selectedReport.length + 1; r++) {
  //     const cell = `A${r}`;
  //     ws[cell].s = { alignment: { horizontal: 'left' } }; // Style de l'alignement horizontal
  //   }
  //
  //   return ws;
  // }
  // exportExcelReportBillet(
  //   selectedReport: ReportBilletDTO[],
  //   RequestReport: ReportBilletRequestDTO,
  //   dateOuverture,
  //   dateOuvertureTo,
  //   columns
  // ) {
  //   var mappedItems = [] as any;
  //   mappedItems.push(selectedReport);
  //   selectedReport?.forEach((item, key) => {
  //     mappedItems.push({
  //       note: item.note,
  //       montantTotal: item.montantTotal,
  //       quantiteTotal: item.quantiteTotal,
  //     });
  //   });
  //   if (dateOuverture.length > 0 && dateOuvertureTo.length > 0) {
  //     mappedItems.push({
  //       note: 'note',
  //       montantTotal: 'montantTotal',
  //       quantiteTotal: 'quantiteTotal',
  //     });
  //   }
  //
  //   const worksheet = XLSX.utils.json_to_sheet(mappedItems);
  //   const workbook = XLSX.utils.book_new();
  //
  //   // Ajouter un titre
  //   XLSX.utils.book_append_sheet(workbook, worksheet, 'Report');
  //   const titleCell = worksheet['A1'];
  //   titleCell.v = 'Etablissements : ';
  //
  //   titleCell.s = { font: { bold: true }, alignment: { horizontal: 'left' } };
  //   const ValuetitleCell = worksheet['B1'];
  //   ValuetitleCell.v = +(RequestReport?.etablissmentIds
  //     ? RequestReport?.etablissmentIds!?.join(', ')
  //     : 'N/A');
  //   ValuetitleCell.s = {
  //     font: { bold: true },
  //     alignment: { horizontal: 'left' },
  //   };
  //
  //   // Ajouter un sous-titre
  //   const subtitleCell = worksheet['A2'];
  //   subtitleCell.v = 'Caisses : ';
  //   subtitleCell.s = {
  //     font: { bold: true },
  //     alignment: { horizontal: 'left' },
  //   };
  //   const ValueSubtitleCell = worksheet['B2'];
  //   ValueSubtitleCell.v = RequestReport?.caisseIds
  //     ? RequestReport?.caisseIds!?.join(', ')
  //     : 'N/A';
  //   ValueSubtitleCell.s = {
  //     font: { bold: true },
  //     alignment: { horizontal: 'left' },
  //   };
  //   const dateCell = worksheet['A3'];
  //   dateCell.v = 'Date :  ';
  //   dateCell.s = {
  //     font: { bold: true },
  //     alignment: { horizontal: 'left' },
  //   };
  //   const dateStartCell = worksheet['B3'];
  //   dateStartCell.v = 'De ' + dateOuverture + ' à ' + dateOuvertureTo;
  //   dateStartCell.s = {
  //     font: { bold: true },
  //     alignment: { horizontal: 'left' },
  //   };
  //
  //   // Ajouter des bordures aux cellules du tableau
  //   const range = XLSX.utils.decode_range(worksheet['!ref']!);
  //   for (let R = range.s.r; R <= range.e.r; ++R) {
  //     for (let C = range.s.c; C <= range.e.c; ++C) {
  //       const cell = worksheet[XLSX.utils.encode_cell({ r: R, c: C })];
  //       if (R === 0 || C === 0) {
  //         // Appliquer un style différent aux cellules du titre
  //         cell.s = {
  //           border: { right: { style: 'thin' }, bottom: { style: 'thin' } },
  //         };
  //       } else {
  //         cell.s = {
  //           border: { right: { style: 'thin' }, bottom: { style: 'thin' } },
  //         };
  //       }
  //     }
  //   }
  //
  //   // Générer le fichier Excel
  //   XLSX.writeFile(workbook, 'exported_report.xlsx');
  // }
  exportExcelReportBillet(
    selectedReport: ReportBilletDTO[],
    requestReport: ReportBilletRequestDTO,
    etablissementIds: (string | null)[],
    dateOuverture,
    dateOuvertureTo,
    columns
  ) {
    const titleStyle = {
      font: { bold: true },
      alignment: { horizontal: 'left' },
      border: {
        top: { style: 'solid' },
        bottom: { style: 'solid' },
        left: { style: 'thin' },
        right: { style: 'thin' },
      },
      fill: {
        fgColor: { rgb: 'FFFF00' }, // Couleur jaune
      },
    };
    var totale = 0;
    const workbook = XLSX.utils.book_new();
    const worksheet = XLSX.utils.aoa_to_sheet([]); // Créez une feuille vide
    var mappedItems = [] as any;
    mappedItems.push(selectedReport);
    mappedItems.push({
      quantiteTotal: 'quantiteTotal',

      note: 'note',
      montantTotal: 'montantTotal',
    });
    selectedReport?.forEach((item, key) => {
      totale += item?.montantTotal!;
      mappedItems.push({
        quantiteTotal: item.quantiteTotal,
        note: item.note,
        montantTotal: item.montantTotal,
      });
    });

    // Ajouter un titre
    XLSX.utils.sheet_add_aoa(worksheet, [['Etablissements : ']], {
      origin: 'A1',
    });
    XLSX.utils.sheet_add_aoa(
      worksheet,
      [[etablissementIds ? etablissementIds.join(', ') : 'N/A']],
      { origin: 'B1' }
    );

    // Ajouter un sous-titre
    XLSX.utils.sheet_add_aoa(worksheet, [['Caisses : ']], { origin: 'A2' });
    XLSX.utils.sheet_add_aoa(
      worksheet,
      [[requestReport?.caisseIds ? requestReport.caisseIds.join(', ') : 'N/A']],
      { origin: 'B2' }
    );

    // Ajouter la date
    XLSX.utils.sheet_add_aoa(worksheet, [['Date : ']], { origin: 'A3' });
    XLSX.utils.sheet_add_aoa(
      worksheet,
      [
        [
          'De ' + requestReport?.startDate ? dateOuverture : 'N/A',
          +' à ' + requestReport?.endDate! ? dateOuvertureTo : 'N/A',
        ],
      ],
      { origin: 'B3' }
    );

    // Appliquer des styles aux cellules du titre et du sous-titre
    worksheet['A1'].s = titleStyle;
    worksheet['B1'].s = titleStyle;
    worksheet['A2'].s = titleStyle;
    worksheet['B2'].s = titleStyle;
    worksheet['A3'].s = titleStyle;
    worksheet['B3'].s = titleStyle;

    for (let r = 0; r < mappedItems.length; r++) {
      const rowData = mappedItems[r];
      XLSX.utils.sheet_add_aoa(worksheet, [Object.values(rowData)], {
        origin: `B${r + 4}`,
      }); // Démarrez à la ligne 4
    }
    XLSX.utils.sheet_add_aoa(worksheet, [['Total']], {
      origin: 'B' + (mappedItems.length + 4).toString(),
    });
    XLSX.utils.sheet_add_aoa(worksheet, [[totale!]], {
      origin: 'D' + (mappedItems.length + 4).toString(),
    });

    XLSX.utils.book_append_sheet(workbook, worksheet, 'Report');
    XLSX.writeFile(
      workbook,
      'report-billets-' +
        this.datePipe.transform(new Date(), 'YYYYMMdd hh:mm:ss') +
        '.xlsx'
    );
  }
}
