import {NgModule} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import {NotFoundComponent} from './layouts/not-found/not-found.component';
import {JwtInterceptor} from './http/jwt.interceptor';
import {ErrorInterceptor} from './http/error.interceptor';
import {HeaderComponent} from './header/header.component';
import {
  NbActionsModule,
  NbContextMenuModule,
  NbIconModule,
  NbLayoutModule,
  NbMenuModule,
  NbSearchModule,
  NbSelectModule,
  NbSidebarModule,
  NbUserModule
} from "@nebular/theme";
import {OneColumnLayoutComponent} from './one-column-layout/one-column-layout.component';
import {RouterModule} from "@angular/router";
import {CommonModule} from "@angular/common";
import {SharedModule} from "../shared/shared.module";

@NgModule({
  declarations: [
    NotFoundComponent,
    HeaderComponent,
    OneColumnLayoutComponent
  ],
  imports: [
    HttpClientModule,
    NbActionsModule,
    NbSelectModule,
    NbIconModule,
    NbSearchModule,
    NbUserModule,
    NbContextMenuModule,
    NbLayoutModule,
    RouterModule,
    CommonModule,
    NbSidebarModule,
    NbMenuModule,
    SharedModule,
  ],

  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true}
  ],
  exports: [
    HeaderComponent,
    OneColumnLayoutComponent
  ]
})
export class CoreModule { }
