import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { CoreModule } from './core/core.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ApiModule, Configuration } from '../../swagger-api';
import {
  NbDialogModule,
  NbIconModule,
  NbLayoutModule,
  NbMenuModule,
  NbMenuService,
  NbSidebarModule,
  NbSidebarService,
  NbThemeModule,
} from '@nebular/theme';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { MatToolbarModule } from '@angular/material/toolbar';
import { SharedModule } from './shared/shared.module';
import { LayoutService } from './shared/utils/layout.service';
import { NbAuthModule, NbPasswordAuthStrategy } from '@nebular/auth';
import { environment } from '../environments/environment';
import { IntlModule } from '@progress/kendo-angular-intl';
import { LabelModule } from '@progress/kendo-angular-label';
import { ButtonsModule } from '@progress/kendo-angular-buttons';
import { DateInputsModule } from '@progress/kendo-angular-dateinputs';
import '@progress/kendo-angular-intl/locales/fr/all';
import { NgxLoaderModule } from '@tusharghoshbd/ngx-loader';
import { CustomNbAuthOAuth2JWTToken } from './auth/CustomNbAuthOAuth2JWTToken';

export function APIConfigurationFactory(): Configuration {
  return new Configuration({ basePath: environment.apiUrl });
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    NgxLoaderModule,
    IntlModule,
    LabelModule,
    ButtonsModule,
    DateInputsModule,
    BrowserModule,
    AppRoutingModule, // Main routes for application
    CoreModule,
    NbThemeModule.forRoot({ name: 'gray' }),
    ApiModule.forRoot(APIConfigurationFactory),
    BrowserAnimationsModule,
    NbLayoutModule,
    NbEvaIconsModule,
    MatToolbarModule,
    SharedModule,
    NbSidebarModule,
    NbMenuModule.forRoot(),
    // ToastrModule.forRoot({
    //   timeOut: 4000,
    //   positionClass: 'toast-bottom-left',
    //   preventDuplicates: true,
    // }),
    NbDialogModule.forRoot(),

    FontAwesomeModule,
    NbIconModule,
    NbAuthModule.forRoot({
      strategies: [
        NbPasswordAuthStrategy.setup({
          name: 'email',
          baseEndpoint: environment.apiUrl,
          token: {
            class: CustomNbAuthOAuth2JWTToken,
            key: 'token',
          },
          login: {
            endpoint: '/login',
            method: 'post',
          },
          register: {
            endpoint: '/auth/sign-up',
            method: 'post',
          },
          logout: {
            endpoint: '/auth/sign-out',
            method: 'post',
            redirect: {
              success: 'auth/login',
            },
          },
          requestPass: {
            endpoint: '/User/request-pass',
            method: 'post',
            redirect: {
              success: 'auth/login',
            },
          },
          resetPass: {
            endpoint: '/User/RestorePassword',
            method: 'post',
            redirect: {
              success: 'auth/login',
              failure: 'auth/login',
            },
          },
          refreshToken: {
            endpoint: '/auth/refresh-token',
            method: 'post',
            requireValidToken: true,
            redirect: {
              failure: 'auth/login',
            },
          },
        }),
      ],

      forms: {},
    }),
  ],
  providers: [
    NbSidebarService,
    LayoutService,
    NbMenuService,
    {
      provide: LOCALE_ID,
      useValue: 'fr-FR',
    },
  ],
  exports: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
