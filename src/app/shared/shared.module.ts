import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';

import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material.module';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import { FileUploadPreviewComponent } from './components/file-upload-preview/file-upload-preview.component';
import { SidenavComponent } from './components/sidenav/sidenav.component';
import { DndDirective } from './components/file-upload-preview/dnd.directive';
import { ConfirmDialogComponent } from './utils/dialogs/confirm-dialog/confirm-dialog.component';
import { AlertDialogComponent } from './utils/dialogs/alert-dialog/alert-dialog.component';
import { PrimeNGModule } from './primeng.module';
import { DraggableDirective } from './utils/draggable.directive';
import { NgLetDirective } from './utils/dialogs/ng-let.directive';
import { NotEmptyPipe } from './utils/noEmpty';
import { AddRowDirective } from '../core/services/add-row.directive';

import localeFr from '@angular/common/locales/fr';
import { DialogCalculatePromptComponent } from './components/dialog-calculate-prompt/dialog-calculate-prompt.component';
import { NbCardModule } from '@nebular/theme';
import { ThreeDigitNumberDirective } from './utils/three-digit-number.directive';
import { SeparateurPipe } from './utils/separateur.pipe';
import { RippleModule } from 'primeng/ripple';

registerLocaleData(localeFr);

const COMPONENTS = [
  AlertDialogComponent,
  ConfirmDialogComponent,
  FileUploadPreviewComponent,
  SidenavComponent,
  DialogCalculatePromptComponent,
];

@NgModule({
  declarations: [
    ...COMPONENTS,
    DndDirective,
    DraggableDirective,
    ThreeDigitNumberDirective,
    NgLetDirective,
    NotEmptyPipe,
    AddRowDirective,
    SeparateurPipe,
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MaterialModule,
    PrimeNGModule,
    NbCardModule,
    RippleModule,
  ],

  exports: [
    CommonModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    PrimeNGModule,
    PerfectScrollbarModule,
    ...COMPONENTS,
    DraggableDirective,
    NgLetDirective,
    NotEmptyPipe,
    AddRowDirective,
    ThreeDigitNumberDirective,
    SeparateurPipe,
  ],

  entryComponents: [
    AlertDialogComponent,
    ConfirmDialogComponent,
    DialogCalculatePromptComponent,
  ],
  providers: [
    NotEmptyPipe,
    SeparateurPipe,
    { provide: LOCALE_ID, useValue: 'fr-FR' },
  ],
})
export class SharedModule {}
