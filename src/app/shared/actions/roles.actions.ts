import {createAction, props} from "@ngrx/store";
import {RolesDTO} from "../../../../swagger-api";

export const fetchRoles = createAction('[Role] fetch Roles');
export const setRoles = createAction('[Roles] set Roles', props<{ roles: RolesDTO[] }>());

export const DeleteRole = createAction('[Roles] delete  Role', props<{ role: RolesDTO }>());
export const EditRole = createAction('[Roles] Edit  Role', props<{ role: RolesDTO }>());
export const CreateRole = createAction('[Roles] create  Role', props<{ role: RolesDTO }>());
