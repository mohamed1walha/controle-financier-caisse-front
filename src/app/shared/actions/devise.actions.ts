import {createAction, props} from "@ngrx/store";
import {DeviseDTO} from "../../../../swagger-api";

export const fetchDevises = createAction('[Devise] fetch Devises');
export const fetchDevisesByCoffreId = createAction('[Devise] fetch Devises by coffre Id', props<{ coffreId: number }>());
export const setDevises = createAction('[Devises] set Devises', props<{ devises: DeviseDTO[] }>());

export const DeleteDevise = createAction('[Devises] delete  Devise', props<{ devise: DeviseDTO }>());
export const EditDevise = createAction('[Devises] Edit  Devise', props<{ devise: DeviseDTO }>());
export const CreateDevise = createAction('[Devises] create  Devise', props<{ devise: DeviseDTO }>());
