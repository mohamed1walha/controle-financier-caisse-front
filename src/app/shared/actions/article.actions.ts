import {createAction, props} from "@ngrx/store";
import {ArticleDTO} from "../../../../swagger-api";

export const fetchArticles = createAction('[Article] fetch Articles');
export const fetchArticlesWithControlSolde = createAction('[Article] fetch Articles With ControlSolde');
export const setArticles = createAction('[Article] set Articles', props<{ articles: ArticleDTO[] }>());

export const DeleteArticle = createAction('[Article] delete  Article', props<{ article: ArticleDTO }>());
export const EditArticle = createAction('[Article] Edit  Article', props<{ article: ArticleDTO }>());
export const CreateArticle = createAction('[Article] create  Article', props<{ article: ArticleDTO }>());
