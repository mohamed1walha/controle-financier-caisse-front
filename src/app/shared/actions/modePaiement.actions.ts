import {createAction, props} from "@ngrx/store";
import {ModepaieDTO} from "../../../../swagger-api";

export const fetchData = createAction('[ModePaiement] fetch data');
export const setData = createAction('[ModePaiement] Set Table Data', props<{ data: ModepaieDTO[] }>());
export const resetDataTableStore = createAction('[ModePaiement] Reset Store');
export const DeleteModePaiement = createAction('[ModePaiement] delete mode paiement', props<{ mp: ModepaieDTO }>());
export const EditModePaiement = createAction('[ModePaiement] Edit Mode Paiement', props<{ mp: ModepaieDTO }>());


