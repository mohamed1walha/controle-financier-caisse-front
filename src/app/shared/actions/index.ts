import * as JoursCaisseActions from 'src/app/shared/actions/joursCaisse.actions';
import * as PiedecheActions from 'src/app/shared/actions/piedeche.actions';
import * as ModePaiementActions from 'src/app/shared/actions/modePaiement.actions';
import * as EtablissementActions from 'src/app/shared/actions/etablissement.actions';
import * as LastSyncAppActions from 'src/app/shared/actions/lastSyncApp.actions';
import * as MotifActions from 'src/app/shared/actions/motif.actions';
import * as ChauffeurActions from 'src/app/shared/actions/chauffeur.actions';
import * as MotifresyncActions from 'src/app/shared/actions/motifresync.actions';
import * as EmetteurActions from 'src/app/shared/actions/emetteur.actions';
import * as StatusActions from 'src/app/shared/actions/status.actions';
import * as BladeActions from 'src/app/shared/actions/blade.actions';
import * as BanqueActions from 'src/app/shared/actions/banque.actions';
import * as CoffreActions from 'src/app/shared/actions/coffre.actions';
import * as CoffreAlimentationActions from 'src/app/shared/actions/alimentation.actions';
import * as TypeFraisActions from 'src/app/shared/actions/typefrais.actions';
import * as ReportActions from 'src/app/shared/actions/report.actions';
import * as DeviseActions from 'src/app/shared/actions/devise.actions';

import * as BilletDetailSortieActions from 'src/app/shared/actions/billetDetailSortie.actions';
import * as BilletDetailComptageActions from 'src/app/shared/actions/billetDetailComptage.actions';
import * as BilletDetailAliExcepActions from 'src/app/shared/actions/billetDetailAliExcep.actions';
import * as ParamBilletActions from 'src/app/shared/actions/paramBillet.actions';
import * as ArticleActions from 'src/app/shared/actions/article.actions';
import * as FournisseurActions from 'src/app/shared/actions/fournisseur.actions';

import * as UsersActions from 'src/app/shared/actions/users.actions';
import * as RolesActions from 'src/app/shared/actions/roles.actions';
import * as CompteDepenseActions from 'src/app/shared/actions/compteDepense.actions';
import * as MailDestinataireActions from 'src/app/shared/actions/mailDestinataire.actions';
import * as EnteteDepenseActions from 'src/app/shared/actions/enteteDepense.actions';
import * as ParcaisseActions from 'src/app/shared/actions/parcaisse.actions';
import * as EntetePrelevementActions from 'src/app/shared/actions/entetePrelevement.actions';
import * as LignePrelevementActions from 'src/app/shared/actions/lignePrelevement.actions';
import * as LigneDepenseActions from 'src/app/shared/actions/ligneDepense.actions';
import * as ReportBilletActions from 'src/app/shared/actions/reportBillet.actions';

export {
  JoursCaisseActions,
  PiedecheActions,
  ModePaiementActions,
  EtablissementActions,
  LastSyncAppActions,
  MotifActions,
  ChauffeurActions,
  MotifresyncActions,
  EmetteurActions,
  StatusActions,
  BladeActions,
  BanqueActions,
  CoffreActions,
  CoffreAlimentationActions,
  TypeFraisActions,
  ReportActions,
  DeviseActions,
  BilletDetailSortieActions,
  BilletDetailComptageActions,
  BilletDetailAliExcepActions,
  UsersActions,
  RolesActions,
  ParamBilletActions,
  ArticleActions,
  FournisseurActions,
  CompteDepenseActions,
  MailDestinataireActions,
  EntetePrelevementActions,
  ParcaisseActions,
  EnteteDepenseActions,
  LignePrelevementActions,
  LigneDepenseActions,
  ReportBilletActions,
};
