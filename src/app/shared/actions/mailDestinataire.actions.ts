import {createAction, props} from "@ngrx/store";
import {MailDestinataireDTO} from "../../../../swagger-api";

export const fetchMailDestinataires = createAction('[MailDestinataire] fetch MailDestinataires');
export const setMailDestinataires = createAction('[MailDestinataire] set MailDestinataires', props<{ mailDestinataires: MailDestinataireDTO[] }>());

export const DeleteMailDestinataire = createAction('[MailDestinataire] delete  MailDestinataire', props<{ mailDestinataire: MailDestinataireDTO }>());
export const EditMailDestinataire = createAction('[MailDestinataire] Edit  MailDestinataire', props<{ mailDestinataire: MailDestinataireDTO }>());
export const CreateMailDestinataire = createAction('[MailDestinataire] create  MailDestinataire', props<{ mailDestinataire: MailDestinataireDTO }>());
