import {createAction, props} from "@ngrx/store";
import {FournisseurDTO} from "../../../../swagger-api";

export const fetchFournisseurs = createAction('[Fournisseur] fetch Fournisseurs');
export const fetchIsDefaultFournisseur = createAction('[Fournisseur] fetch is Default Fournisseurs');
export const setFournisseurs = createAction('[Fournisseur] set Fournisseurs', props<{ fournisseurs: FournisseurDTO[] }>());
export const setFournisseur = createAction('[Fournisseur] set Fournisseur', props<{ fournisseur: FournisseurDTO }>());

export const DeleteFournisseur = createAction('[Fournisseur] delete  Fournisseur', props<{ fournisseur: FournisseurDTO }>());
export const EditFournisseur = createAction('[Fournisseur] Edit  Fournisseur', props<{ fournisseur: FournisseurDTO }>());
export const CreateFournisseur = createAction('[Fournisseur] create  Fournisseur', props<{ fournisseur: FournisseurDTO }>());
