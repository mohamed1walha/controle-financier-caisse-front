import {createAction, props} from "@ngrx/store";
import {StatusDTO} from "../../../../swagger-api";

export const fetchStatuss = createAction('[Status] fetch Status');
export const setStatuss = createAction('[Status] set Status', props<{ statuss: StatusDTO[] }>());
export const fetchStatus = createAction('[Status] fetch Status joursCaisse', props<{ id: any }>());
export const setStatus = createAction('[Status] set Status joursCaisse', props<{ status: StatusDTO }>());
