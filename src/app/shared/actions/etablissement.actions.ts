import { createAction, props } from '@ngrx/store';
import { EtablissementDTO } from '../../../../swagger-api';

export const fetchEtablissements = createAction(
  '[Etablissement] fetch all Etablissement'
);
export const setEtablissements = createAction(
  '[Etablissement] set etablissements',
  props<{ etablissements: EtablissementDTO[] }>()
);
export const setEtablissement = createAction(
  '[Etablissement] set etablissement',
  props<{ etablissement: EtablissementDTO }>()
);
export const fetchEtablissementById = createAction(
  '[Etablissement] fetch Etablissement by id',
  props<{ etablissementId: number }>()
);
