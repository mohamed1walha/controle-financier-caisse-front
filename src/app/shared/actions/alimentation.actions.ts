import {createAction, props} from "@ngrx/store";
import {CoffrealimentationDTO, CoffrealimentationEntryDTO} from "../../../../swagger-api";


export const fetchcoffrealimentations = createAction('[coffrealimentation] fetch coffrealimentations');
export const setcoffrealimentations = createAction('[coffrealimentations] set coffrealimentations',
  props<{ coffrealimentations: CoffrealimentationDTO[] }>());


export const DeleteAlimentation = createAction('[coffrealimentations] delete Alimentation', props<{ alimentationId: number }>());

export const Editcoffrealimentation = createAction('[coffrealimentations] Edit  coffrealimentation',
  props<{ coffrealimentation: CoffrealimentationDTO }>());
export const Createcoffrealimentation = createAction('[coffrealimentations] create  coffrealimentation',
  props<{ coffrealimentations: CoffrealimentationEntryDTO }>());

export const CreatecoffrealimentationExp = createAction('[coffrealimentations] create  coffrealimentationExp',
  props<{ coffreAlimentationDTO: CoffrealimentationDTO }>());

