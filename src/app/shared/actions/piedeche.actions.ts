import {createAction, props} from "@ngrx/store";
import {PiedecheDTO} from "../../../../swagger-api/model/piedecheDTO";


export const resetDataTable = createAction('[Piedeche] reset store');
export const fetchPiedecheByJoursCaisse = createAction('[Piedeche] fetch by jourCaisse caisse ', props<{ id: any }>());
export const fetchPiedecheByJoursCaisseID = createAction('[Piedeche] fetch by jourCaisse caisse  id');
export const fetchPiedecheById = createAction('[Piedeche] fetch by id  ', props<{ id: number | undefined }>());
export const fetchAllByCoffreNull = createAction('[Piedeche] fetch by fetchAllByCoffreNull ');
export const setPiedeches = createAction('[Piedeche] set Piedeches', props<{ piedeches: PiedecheDTO[] }>());
export const setPiedeche = createAction('[Piedeche] set Piedeche', props<{ piedeche: PiedecheDTO }>());
export const updatepiedeche = createAction('[Piedeche] update Piedeche', props<{ piedeche: PiedecheDTO }>());
export const setSideBar = createAction('[Piedeche] side Bar', props<{ sidebar: boolean }>());

