import {createAction, props} from '@ngrx/store';

export const openBlade = createAction('[Blade] open blade');
export const setBladeWidth = createAction(
  '[Blade] set blade width',
  props<{ width: number }>()
);
export const closeBlade = createAction('[Blade] close blade');

