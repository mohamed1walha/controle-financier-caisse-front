import {createAction, props} from "@ngrx/store";
import {BilletDetailSortieDTO} from "../../../../swagger-api";

export const fetchBilletDetailSorties = createAction('[BilletDetailSortie] fetch BilletDetailSorties');
export const setBilletDetailSorties = createAction('[BilletDetailSorties] set BilletDetailSorties', props<{ BilletDetailSorties: BilletDetailSortieDTO[] }>());

export const DeleteBilletDetailSortie = createAction('[BilletDetailSorties] delete  BilletDetailSortie', props<{ BilletDetailSortie: BilletDetailSortieDTO }>());
export const EditBilletDetailSortie = createAction('[BilletDetailSorties] Edit  BilletDetailSortie', props<{ BilletDetailSortie: BilletDetailSortieDTO }>());
export const CreateBilletDetailSortie = createAction('[BilletDetailSorties] create  BilletDetailSortie', props<{ BilletDetailSortie: BilletDetailSortieDTO }>());


