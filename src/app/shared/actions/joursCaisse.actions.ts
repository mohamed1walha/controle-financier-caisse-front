import {createAction, props} from "@ngrx/store";
import {FilterMetadata} from "primeng/api";
import {JoursCaiseDTO, StatsDTO} from "../../../../swagger-api";

// export const copierDatabase = createAction('[copierDatabase] Copie Database');
// export const fetchJoursCaisses = createAction('[JoursCaisse] fetch JoursCaisses');
// export const setJoursCaisses = createAction('[JoursCaisse] set JoursCaisses', props<{ joursCaisse: JoursCaiseDTO[] }>());

export const fetchData = createAction('[JoursCaisse] fetch Filter By Pr');
export const UpdateJoursCaisse = createAction('[JoursCaisse] Update JoursCaisse', props<{ jourCaisse: JoursCaiseDTO }>());
export const changeStatus = createAction('[JoursCaisse] changeStatus', props<{ joursCaisse: JoursCaiseDTO[] }>());
export const fetchById = createAction('[JoursCaisse]  fetch By Id');
export const fetchByIdParams = createAction('[JoursCaisse]  fetch By Id params', props<{ id: any }>());
export const fetchStats = createAction('[JoursCaisse] fetch Stats By Pr');
export const fetchDataWithFilters = createAction('[JoursCaisse] fetch Filter By using filters', props<{ filters: { [p: string]: FilterMetadata | FilterMetadata[] } }>());
export const receptionEnveloppes = createAction('[JoursCaisse] fetch reception enveloppe', props<{ enveloppes:JoursCaiseDTO[], chauffeurId: number | undefined, emetteurId: number | undefined }>());
export const receptionReSyncEnveloppes = createAction('[JoursCaisse] fetch reception resync enveloppe', props<{ enveloppes:JoursCaiseDTO[], /*chauffeurId: number | undefined, emetteurId: number | undefined,*/motifId: number | undefined,comments: string | undefined }>());
export const fetchAllDataStatusCpmtNone = createAction('[JoursCaisse] fetch jourcaisse with statusNone');
export const fetchAllDataStatusEnAttente = createAction('[JoursCaisse] fetch jourcaisse with status En Attente');
export const fetchAllDataStatusReceptionne = createAction('[JoursCaisse] fetch jourcaisse with status En Receptionne');
export const fetchAllDataStatusReceptionneNonValide = createAction('[JoursCaisse] fetch jourcaisse with status En Receptionne non valide');
export const fetchAllDataStatusCompter = createAction('[JoursCaisse] fetch jourcaisse with status En compter');
export const fetchDataByDate = createAction('[JoursCaisse] fetch data by date', props<{ date: any }>());
export const fetchNumZByEtablissement = createAction('[JoursCaisse] fetch NumZ By Etablissement', props<{ etabId: number | undefined }>());
export const fetchCaisseByEtablissement = createAction('[JoursCaisse] fetch Caisse By Etablissement', props<{ etabId: number | undefined }>());
export const setData = createAction('[JoursCaisse] Set Table Data', props<{ data: JoursCaiseDTO[] }>());
export const setStats = createAction('[JoursCaisse] Set Stats', props<{ stats: StatsDTO }>());
export const setNumZByEtablissement = createAction('[JoursCaisse] set NumZ By Etablissement  Data', props<{ data: number[] }>());
export const setJoursCaisse = createAction('[JoursCaisse] set JoursCaisse selcted', props<{ joursCaisse: JoursCaiseDTO }>());
export const setCaisseByEtablissement = createAction('[JoursCaisse] set Caisse By Etablissement  Data', props<{ data: string[] }>());
export const setSortKey = createAction('[JoursCaisse] Set Sort Key', props<{ sortKey: string }>());
export const resetDataTableStore = createAction('[JoursCaisse] Reset Store');
export const setFilterBy = createAction('[JoursCaisse] Set Filter By Properties and Query', props<{ filters: { filterBy: string[], query: string } }>());

export const fetchByFilters = createAction('[JoursCaisse] fetch by filters', props<{ filters: { [s: string]: FilterMetadata | FilterMetadata[] } }>());
export const setDataFilters = createAction('[JoursCaisse] set by filters', props<{ filters: { [s: string]: FilterMetadata | FilterMetadata[] } }>());
