import {createAction, props} from "@ngrx/store";
import {TypeFraisDTO} from "../../../../swagger-api";

export const fetchTypeFraiss = createAction('[TypeFrais] fetch TypeFraiss');
export const setTypeFraiss = createAction('[TypeFraiss] set TypeFraiss', props<{ TypeFraiss: TypeFraisDTO[] }>());

export const DeleteTypeFrais = createAction('[TypeFraiss] delete  TypeFrais', props<{ TypeFrais: TypeFraisDTO }>());
export const EditTypeFrais = createAction('[TypeFraiss] Edit  TypeFrais', props<{ TypeFrais: TypeFraisDTO }>());
export const CreateTypeFrais = createAction('[TypeFraiss] create  TypeFrais', props<{ TypeFrais: TypeFraisDTO }>());
