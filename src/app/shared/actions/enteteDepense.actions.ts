import {createAction, props} from '@ngrx/store';
import {EnteteDepenseDTO} from '../../../../swagger-api';

export const fetchAllEnteteDepenses = createAction(
  '[EnteteDepense] fetch all EnteteDepenses'
);
export const fetchAllEnteteDepensesWithEtabAndUserId = createAction(
  '[EnteteDepense] fetch all EnteteDepenses with Etab And UserId',
  props<{ etablissementId: number; userId: number }>()
);

export const fetchAllEnteteDepensesWithValidatorId = createAction(
  '[EnteteDepense] fetch all EnteteDepenses with ValidatorId',
  props<{ userId: number }>()
);
export const fetchAllEnteteDepensesWithEtabAndNumZRefAndcaisseId = createAction(
  '[EnteteDepense] fetch all EnteteDepenses with Etab And caisseId and numZref',
  props<{ etabId: number; numZRef: number; caisseId: any }>()
);
export const fetchEnteteDepense = createAction(
  '[EnteteDepense] fetch EnteteDepense',
  props<{ id: number }>()
);
export const deleteFile = createAction(
  '[EnteteDepense]  delete File',
  props<{ id: number }>()
);
export const setEnteteDepenses = createAction(
  '[EnteteDepenses] set EnteteDepenses',
  props<{ enteteDepenses: any  }>()
);
export const setEnteteValidationDepenses = createAction(
  '[EnteteDepenses] set Entete validation Depenses',
  props<{ enteteValidationDepenses: EnteteDepenseDTO[] }>()
);

export const setEnteteDepense = createAction(
  '[EnteteDepenses] set EnteteDepense',
  props<{ enteteDepense: EnteteDepenseDTO }>()
);
export const NoActions = createAction(
  '[EnteteDepenses]  NoActions',
  props<{ enteteDepense: EnteteDepenseDTO }>()
);

export const DeleteEnteteDepense = createAction(
  '[EnteteDepenses] delete  EnteteDepense',
  props<{ enteteDepense: EnteteDepenseDTO }>()
);
export const EditEnteteDepense = createAction(
  '[EnteteDepenses] Edit  EnteteDepense',
  props<{ enteteDepense: EnteteDepenseDTO }>()
);
export const CreateEnteteDepense = createAction(
  '[EnteteDepenses] create  EnteteDepense',
  props<{ enteteDepense: EnteteDepenseDTO }>()
);
export const UpdateEnteteDepense = createAction(
  '[EnteteDepenses] update  EnteteDepense',
  props<{ enteteDepense: EnteteDepenseDTO }>()
);
