import {createAction, props} from "@ngrx/store";
import {BilletDetailSortieDTO, CoffrealimentationDTO, CoffreDTO, CoffresortieDTO} from "../../../../swagger-api";

export let fetchCoffresFromModePaie = createAction('[Coffres] get  fetchCoffres From ModePaie', props<{ modePaie: any[] }>());


export const fetchCoffres = createAction('[Coffre] fetch Coffres');
export const setCoffres = createAction('[Coffres] set Coffres', props<{ coffres: CoffreDTO[] }>());

export const DeleteCoffre = createAction('[Coffres] delete  Coffre', props<{ coffre: CoffreDTO }>());
export const EditCoffre = createAction('[Coffres] Edit  Coffre', props<{ coffre: CoffreDTO }>());
export const CreateCoffre = createAction('[Coffres] create  Coffre', props<{ coffre: CoffreDTO }>());


export const setCoffre = createAction('[Coffre] set Coffre selcted', props<{ coffre: CoffreDTO }>());


export const DeleteSortie = createAction('[Sorties] DeleteSortie Sorties', props<{ sortieId: number }>());
export const fetchSorties = createAction('[Sorties] fetch Sorties', props<{ coffreId: number }>());
export const setSorties = createAction('[Sorties] set Sorties', props<{ sorties: CoffresortieDTO[] }>());

export const fetchAlimentations = createAction('[Alimentations] fetch Alimentations', props<{ coffreId: number }>());
export const setAlimentations = createAction('[Alimentations] set Alimentations', props<{ alimentations: CoffrealimentationDTO[] }>());


export const fetchAllSorties = createAction('[Sorties] fetch all Sorties');
export const setAllSorties = createAction('[Sorties] set all Sorties', props<{ allSorties: CoffresortieDTO[] }>());
export const CreateSortie = createAction('[Sorties] create  Sortie', props<{ sortie: CoffresortieDTO }>());
export const setCoffreSortie = createAction('[Sorties] set  Sortie', props<{ sortie: CoffresortieDTO }>());



export const fetchCalculetteBySortieId = createAction('[Sorties] fetch Calculette BySortie Id', props<{ sortieId: number | undefined }>());
export const setCalculetteBySortieId = createAction('[Sorties] set Calculette BySortie Id', props<{ allCalculetteBySortieId: BilletDetailSortieDTO[] }>());
