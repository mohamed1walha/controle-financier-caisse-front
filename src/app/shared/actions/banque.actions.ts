import {createAction, props} from "@ngrx/store";
import {BanqueDTO} from "../../../../swagger-api";

export const fetchBanques = createAction('[Banque] fetch Banques');
export const setBanques = createAction('[Banques] set Banques', props<{ banques: BanqueDTO[] }>());

export const DeleteBanque = createAction('[Banques] delete  Banque', props<{ banque: BanqueDTO }>());
export const EditBanque = createAction('[Banques] Edit  Banque', props<{ banque: BanqueDTO }>());
export const CreateBanque = createAction('[Banques] create  Banque', props<{ banque: BanqueDTO }>());
