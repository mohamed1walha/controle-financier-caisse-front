import { createAction, props } from '@ngrx/store';
import {
  EntetePrelevementDTO,
  FilterPrelevementDTO,
} from '../../../../swagger-api';

export const fetchAllEntetePrelevements = createAction(
  '[EntetePrelevement] fetch all EntetePrelevements'
);
export const setFilterPrelevements = createAction(
  '[EntetePrelevement] set Filter Prelevements',
  props<{ filter: FilterPrelevementDTO }>()
);
export const fetchAllEntetePrelevementsWithEtabAndUserId = createAction(
  '[EntetePrelevement] fetch all EntetePrelevements with Etab And UserId',
  props<{ etablissementId: number; userId: number }>()
);
export const fetchAllEntetePrelevementsWithEtabAndNumZRefAndcaisseId =
  createAction(
    '[EntetePrelevement] fetch all EntetePrelevements with Etab And caisseId and numZref',
    props<{ etabId: number; numZRef: number; caisseId: any }>()
  );
export const fetchAllEntetePrelevementsWithEtabAndUserIdDemandeur =
  createAction(
    '[EntetePrelevement] fetch all EntetePrelevements with Etab And UserId demandeur',
    props<{ etablissementId: number; userId: number }>()
  );

export const fetchAllEntetePrelevementsWithValidatorId = createAction(
  '[EntetePrelevement] fetch all EntetePrelevements with ValidatorId',
  props<{ userId: number }>()
);

export const fetchEntetePrelevement = createAction(
  '[EntetePrelevement] fetch EntetePrelevement',
  props<{ id: number }>()
);
export const deleteFile = createAction(
  '[EntetePrelevement]  delete File',
  props<{ id: number }>()
);
export const setEntetePrelevements = createAction(
  '[EntetePrelevements] set EntetePrelevements',
  props<{ entetePrelevements: EntetePrelevementDTO[] }>()
);
export const setEnteteValidationPrelevements = createAction(
  '[EntetePrelevements] set Entete Validation Prelevements',
  props<{ enteteValidationPrelevements: EntetePrelevementDTO[] }>()
);
export const setEntetePrelevement = createAction(
  '[EntetePrelevements] set EntetePrelevement',
  props<{ entetePrelevement: EntetePrelevementDTO }>()
);

export const DeleteEntetePrelevement = createAction(
  '[EntetePrelevements] delete  EntetePrelevement',
  props<{ entetePrelevement: EntetePrelevementDTO }>()
);
export const EditEntetePrelevement = createAction(
  '[EntetePrelevements] Edit  EntetePrelevement',
  props<{ entetePrelevement: EntetePrelevementDTO }>()
);
export const CreateEntetePrelevement = createAction(
  '[EntetePrelevements] create  EntetePrelevement',
  props<{ entetePrelevement: EntetePrelevementDTO }>()
);
export const NoActions = createAction(
  '[EntetePrelevements] no action  EntetePrelevement',
  props<{ entetePrelevement: EntetePrelevementDTO }>()
);
export const UpdateEntetePrelevement = createAction(
  '[EntetePrelevements] update  EntetePrelevement',
  props<{ entetePrelevement: EntetePrelevementDTO }>()
);

export const generateComptable = createAction(
  '[EntetePrelevement] Generate Comptable',
  props<{ Prelevements: EntetePrelevementDTO[] }>()
);

export const setAnnexesFile = createAction(
  '[EntetePrelevements] set Annexes File',
  props<{ files: any }>()
);
