import {createAction, props} from "@ngrx/store";
import {BilletDetailAliExcepDTO} from "../../../../swagger-api";

export const fetchBilletDetailAliExceps = createAction('[BilletDetailAliExcep] fetch BilletDetailAliExceps');
export const setBilletDetailAliExceps = createAction('[BilletDetailAliExceps] set BilletDetailAliExceps', props<{ BilletDetailAliExceps: BilletDetailAliExcepDTO[] }>());

export const DeleteBilletDetailAliExcep = createAction('[BilletDetailAliExceps] delete  BilletDetailAliExcep', props<{ BilletDetailAliExcep: BilletDetailAliExcepDTO }>());
export const EditBilletDetailAliExcep = createAction('[BilletDetailAliExceps] Edit  BilletDetailAliExcep', props<{ BilletDetailAliExcep: BilletDetailAliExcepDTO }>());
export const CreateBilletDetailAliExcep = createAction('[BilletDetailAliExceps] create  BilletDetailAliExcep', props<{ BilletDetailAliExcep: BilletDetailAliExcepDTO }>());
