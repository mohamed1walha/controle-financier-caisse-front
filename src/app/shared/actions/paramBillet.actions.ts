import {createAction, props} from "@ngrx/store";
import {ParamBilletDTO, TypeBilletDTO} from "../../../../swagger-api";

export const fetchParamBillets = createAction('[ParamBillet] fetch ParamBillets');
export const fetchTypeBillets = createAction('[ParamBillet] fetch TypeBillets');
export const setParamBillets = createAction('[ParamBillets] set ParamBillets', props<{ ParamBillets: ParamBilletDTO[] }>());
export const setTypeBillets = createAction('[ParamBillets] set ParamBillets', props<{ TypeBillets: TypeBilletDTO[] }>());

export const DeleteParamBillet = createAction('[ParamBillets] delete  ParamBillet', props<{ ParamBillet: ParamBilletDTO }>());
export const EditParamBillet = createAction('[ParamBillets] Edit  ParamBillet', props<{ ParamBillet: ParamBilletDTO }>());
export const CreateParamBillet = createAction('[ParamBillets] create  ParamBillet', props<{ ParamBillet: ParamBilletDTO }>());
