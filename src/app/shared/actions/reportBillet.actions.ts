import { createAction, props } from '@ngrx/store';
import {
  ReportBilletDTO,
  ReportBilletRequestDTO,
} from '../../../../swagger-api';

export const fetchReportBillets = createAction(
  '[ReportBillet] fetch ReportBillets',
  props<{ reportBillets: ReportBilletRequestDTO }>()
);
export const setReportBillets = createAction(
  '[ReportBillets] set ReportBillets',
  props<{ reportBillets: ReportBilletDTO[] }>()
);
