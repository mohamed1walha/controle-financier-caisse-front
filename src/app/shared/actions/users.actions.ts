import {createAction, props} from "@ngrx/store";
import {UserDTO} from "../../../../swagger-api";

export const fetchUsers = createAction('[User] fetch Users');
export const fetchUsersSimpleData = createAction('[User] fetch Users simple Data');

export const fetchUsersValidators = createAction('[User] fetch Users Validators');
export const fetchUserByUserName = createAction('[User] fetch User by user Name', props<{ userName: string }>());
export const setUsersSimpleData = createAction('[Users] set Users simple data', props<{ usersSimpleData: UserDTO[] }>());
export const setUsers = createAction('[Users] set Users', props<{ users: UserDTO[] }>());
export const setUser = createAction('[Users] set User', props<{ user: UserDTO }>());

export const DeleteUser = createAction('[Users] delete  User', props<{ user: UserDTO }>());
export const EditUser = createAction(
  '[Users] Edit  User',
  props<{ user: UserDTO }>()
);
export const CreateUser = createAction(
  '[Users] create  User',
  props<{ user: UserDTO }>()
);
export const fetchUsersByEtablissmentId = createAction(
  '[Users] fetch Users By EtablissmentId',
  props<{ etabId: number }>()
);
