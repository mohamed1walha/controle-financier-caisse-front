import { createAction, props } from '@ngrx/store';
import { CompteDepenseDTO } from '../../../../swagger-api';

export const fetchCompteDepenses = createAction(
  '[CompteDepense] fetch CompteDepenses'
);
export const setCompteDepenses = createAction(
  '[CompteDepense] set CompteDepenses',
  props<{ compteDepenses: CompteDepenseDTO[] }>()
);

export const DeleteCompteDepense = createAction(
  '[CompteDepense] delete  CompteDepense',
  props<{ compteDepense: CompteDepenseDTO }>()
);
export const EditCompteDepense = createAction(
  '[CompteDepense] Edit  CompteDepense',
  props<{ compteDepense: CompteDepenseDTO }>()
);
export const CreateCompteDepense = createAction(
  '[CompteDepense] create  CompteDepense',
  props<{ compteDepense: CompteDepenseDTO }>()
);
export const fetchCompteDepenseById = createAction(
  '[CompteDepense] fetch CompteDepenses by id',
  props<{ id: number }>()
);
export const setCompteDepense = createAction(
  '[CompteDepenses] set CompteDepense',
  props<{ compteDepense: CompteDepenseDTO }>()
);
