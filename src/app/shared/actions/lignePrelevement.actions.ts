import { createAction, props } from '@ngrx/store';
import { LignePrelevementDTO } from '../../../../swagger-api';

export const fetchLignePrelevements = createAction(
  '[LignePrelevement] fetch LignePrelevements'
);
export const fetchLignePrelevementsWithEnteteId = createAction(
  '[LignePrelevement] fetch LignePrelevements With enteteId',
  props<{ enteteId: any }>()
);
export const setLignePrelevements = createAction(
  '[LignePrelevement] set LignePrelevements',
  props<{ lignePrelevements: LignePrelevementDTO[] }>()
);

export const DeleteLignePrelevement = createAction(
  '[LignePrelevement] delete  LignePrelevement',
  props<{ lignePrelevement: LignePrelevementDTO }>()
);
export const EditLignePrelevement = createAction(
  '[LignePrelevement] Edit  LignePrelevement',
  props<{ lignePrelevement: LignePrelevementDTO }>()
);
export const CreateLignePrelevement = createAction(
  '[LignePrelevement] create  LignePrelevement',
  props<{ lignePrelevement: LignePrelevementDTO }>()
);
