import { createAction, props } from '@ngrx/store';
import { ReportDTO } from '../../../../swagger-api';

export const fetchReports = createAction(
  '[Report] fetch Reports',
  props<{
    dateOuverture?: any;
    dateOuvertureTo?: any;
    etablissementId?: number;
  }>()
);
export const setReports = createAction(
  '[Report] set Reports',
  props<{ reports: ReportDTO[] }>()
);
