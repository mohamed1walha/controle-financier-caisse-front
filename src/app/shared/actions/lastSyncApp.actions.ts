import {createAction, props} from "@ngrx/store";

export const fetchlastSync = createAction('[LastSyncApp] fetch last sync date');
export const setlastSync = createAction('[LastSyncApp] last sync ', props<{ lastsync: Date }>());
