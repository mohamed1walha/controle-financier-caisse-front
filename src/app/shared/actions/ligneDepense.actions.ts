import { createAction, props } from '@ngrx/store';
import { LigneDepenseDTO } from '../../../../swagger-api';

export const fetchLigneDepenses = createAction(
  '[LigneDepense] fetch LigneDepenses'
);
export const fetchLigneDepensesWithEnteteId = createAction(
  '[LigneDepense] fetch LigneDepenses With enteteId',
  props<{ enteteId: any }>()
);
export const setLigneDepenses = createAction(
  '[LigneDepense] set LigneDepenses',
  props<{ ligneDepenses: LigneDepenseDTO[] }>()
);

export const DeleteLigneDepense = createAction(
  '[LigneDepense] delete  LigneDepense',
  props<{ ligneDepense: LigneDepenseDTO }>()
);
export const EditLigneDepense = createAction(
  '[LigneDepense] Edit  LigneDepense',
  props<{ ligneDepense: LigneDepenseDTO }>()
);
export const CreateLigneDepense = createAction(
  '[LigneDepense] create  LigneDepense',
  props<{ ligneDepense: LigneDepenseDTO }>()
);
