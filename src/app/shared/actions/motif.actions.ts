import {createAction, props} from "@ngrx/store";
import {MotifDTO} from "../../../../swagger-api";

export const fetchMotifs = createAction('[Motifs] fetch Motifs');
export const setMotifs = createAction('[Motifs] set Motifs', props<{ motifs: MotifDTO[] }>());

export const DeleteMotif = createAction('[Motifs] delete  Motif', props<{ motif: MotifDTO }>());
export const EditMotif = createAction('[Motifs] Edit  Motif', props<{ motif: MotifDTO }>());
export const CreateMotif = createAction('[Motifs] create  Motif', props<{ motif: MotifDTO }>());
