import {createAction, props} from "@ngrx/store";
import {MotifresyncDTO} from "../../../../swagger-api";

export const fetchMotifresyncs = createAction('[Motifresync] fetch Motifresyncs');
export const setMotifresyncs = createAction('[Motifresyncs] set Motifresyncs', props<{ Motifresyncs: MotifresyncDTO[] }>());

export const DeleteMotifresync = createAction('[Motifresyncs] delete  Motifresync', props<{ Motifresync: MotifresyncDTO }>());
export const EditMotifresync = createAction('[Motifresyncs] Edit  Motifresync', props<{ Motifresync: MotifresyncDTO }>());
export const CreateMotifresync = createAction('[Motifresyncs] create  Motifresync', props<{ Motifresync: MotifresyncDTO }>());
