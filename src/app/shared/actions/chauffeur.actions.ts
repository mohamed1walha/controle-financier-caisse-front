import {createAction, props} from "@ngrx/store";
import {ChauffeurDTO} from "../../../../swagger-api";

export const fetchChauffeurs = createAction('[Chauffeur] fetch Chauffeurs');
export const setChauffeurs = createAction('[Chauffeurs] set Chauffeurs', props<{ chauffeurs: ChauffeurDTO[] }>());

export const DeleteChauffeur = createAction('[Chauffeurs] delete  Chauffeur', props<{ chauffeur: ChauffeurDTO }>());
export const EditChauffeur = createAction('[Chauffeurs] Edit  Chauffeur', props<{ chauffeur: ChauffeurDTO }>());
export const CreateChauffeur = createAction('[Chauffeurs] create  Chauffeur', props<{ chauffeur: ChauffeurDTO }>());
