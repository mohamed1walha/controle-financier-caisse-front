import {createAction, props} from "@ngrx/store";
import {EmetteurDTO} from "../../../../swagger-api";

export const fetchEmetteurs = createAction('[Emetteur] fetch Emetteurs');
export const setEmetteurs = createAction('[Emetteurs] set Emetteurs', props<{ emetteurs: EmetteurDTO[] }>());

export const DeleteEmetteur = createAction('[Emetteurs] delete  Emetteur', props<{ emetteur: EmetteurDTO }>());
export const EditEmetteur = createAction('[Emetteurs] Edit  Emetteur', props<{ emetteur: EmetteurDTO }>());
export const CreateEmetteur = createAction('[Emetteurs] create  Emetteur', props<{ emetteur: EmetteurDTO }>());
