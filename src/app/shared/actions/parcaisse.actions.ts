import { createAction, props } from '@ngrx/store';
import { ParcaisseDTO } from '../../../../swagger-api';

export const fetchParcaisses = createAction('[Parcaisse] fetch Parcaisses');
export const fetchParcaissesByEtabId = createAction(
  '[Parcaisses] fetch Parcaisses By EtabId',
  props<{ etabId: any }>()
);
export const fetchParcaissesByEtabIds = createAction(
  '[Parcaisses] fetch Parcaisses By EtabIds',
  props<{ etabIds: any[] }>()
);
export const setParcaisses = createAction(
  '[Parcaisses] set Parcaisses',
  props<{ parcaisses: ParcaisseDTO[] }>()
);

export const DeleteParcaisse = createAction(
  '[Parcaisses] delete  Parcaisse',
  props<{ parcaisse: ParcaisseDTO }>()
);
export const EditParcaisse = createAction(
  '[Parcaisses] Edit  Parcaisse',
  props<{ parcaisse: ParcaisseDTO }>()
);
export const CreateParcaisse = createAction(
  '[Parcaisses] create  Parcaisse',
  props<{ parcaisse: ParcaisseDTO }>()
);
