import {createAction, props} from "@ngrx/store";
import {BilletDetailComptageDTO} from "../../../../swagger-api";

export const fetchBilletDetailComptages = createAction('[BilletDetailComptage] fetch BilletDetailComptages');
export const setBilletDetailComptages = createAction('[BilletDetailComptages] set BilletDetailComptages', props<{ BilletDetailComptages: BilletDetailComptageDTO[] }>());

export const DeleteBilletDetailComptage = createAction('[BilletDetailComptages] delete  BilletDetailComptage', props<{ BilletDetailComptage: BilletDetailComptageDTO }>());
export const EditBilletDetailComptage = createAction('[BilletDetailComptages] Edit  BilletDetailComptage', props<{ BilletDetailComptage: BilletDetailComptageDTO }>());
export const CreateBilletDetailComptage = createAction('[BilletDetailComptages] create  BilletDetailComptage', props<{ BilletDetailComptage: BilletDetailComptageDTO }>());
