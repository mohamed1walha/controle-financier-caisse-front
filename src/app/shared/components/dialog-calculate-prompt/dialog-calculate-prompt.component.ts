import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import {
  BilletDetailComptageDTO,
  BilletDetailSortieDTO,
  ParamBilletDTO,
  ParamBilletService,
  TypeBilletDTO,
} from '../../../../../swagger-api';
import { Observable } from 'rxjs';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { SeparateurPipe } from '../../utils/separateur.pipe';

@Component({
  selector: 'app-dialog-calculate-prompt',
  templateUrl: 'dialog-calculate-prompt.component.html',
  styleUrls: ['dialog-calculate-prompt.component.scss'],
})
export class DialogCalculatePromptComponent implements OnInit, OnDestroy {
  calculette: any;
  calculSortie: BilletDetailSortieDTO[] = [];
  calculComptage: BilletDetailComptageDTO[] = [];
  ParamBillets$: Observable<ParamBilletDTO[]> =
    this.paramBilletService.paramBilletGetAllGet();
  ParamBillets: ParamBilletDTO[];
  TypeBillets$: Observable<TypeBilletDTO[]> =
    this.paramBilletService.paramBilletGetAllTypeBilletGet();
  TypeBillets: TypeBilletDTO[];
  _alive: boolean = true;
  sommeGlobal: number = 0;
  orderForm: FormGroup;
  items: FormArray;

  constructor(
    private fb: FormBuilder,
    private numberPipe: SeparateurPipe,
    protected ref: NbDialogRef<DialogCalculatePromptComponent>,
    private paramBilletService: ParamBilletService
  ) {}

  refresh() {
    this.TypeBillets$.subscribe((res) => {
      this.TypeBillets = res;
    });

    this.ParamBillets$.subscribe((res) => {
      this.ParamBillets = res;
      this.addItem(this.ParamBillets);
    });
  }
  cancel() {
    this.ref.close(0);
  }

  submit(somme) {
    this.somme?.setValue(somme);
    this.ref.close(this.orderForm.value);
  }

  ngOnInit(): void {
    this.orderForm = this.fb.group({
      somme: '',
      items: this.fb.array([]),
    });
    this.refresh();
  }
  ngOnDestroy(): void {
    this._alive = false;
  }

  createItem(paramBillet: ParamBilletDTO, qte: any): FormGroup {
    return this.fb.group({
      billetId: [paramBillet.id, ''],
      billetLabel: [paramBillet.label, ''],
      montant: [paramBillet.montant, ''],
      typeId: [paramBillet.type, ''],
      quantite: [qte, ''],
    });
  }

  addItem(allParamBillets: ParamBilletDTO[]): void {
    this.items = this.orderForm.get('items') as FormArray;
    allParamBillets.forEach((paramBillet) => {
      let qte: any = 0;
      if (this.calculSortie.length > 0) {
        qte = this.calculSortie.find(
          (x) => x.billetId === paramBillet.id
        )?.quantite;
      }
      if (this.calculComptage.length > 0) {
        qte = this.calculComptage.find(
          (x) => x.billetId === paramBillet.id
        )?.quantite;
      }
      this.items.push(this.createItem(paramBillet, qte));
    });
    if (this.calculette) {
      this.orderForm.patchValue(this.calculette);
    }
    this.onChange();
  }

  getControls() {
    return (this.orderForm.get('items') as FormArray).controls;
  }

  get somme() {
    return this.orderForm.get('somme');
  }

  calculItem(item: any) {
    const x = item.quantite * item.montant;
    return this.numberPipe.transform(x);
  }

  calculSomme(): number {
    let somme = 0;
    this.orderForm.value.items.forEach((item) => {
      somme = somme + item.quantite * item.montant;
    });
    return somme;
  }

  onChange() {
    this.sommeGlobal = this.calculSomme();
  }
}
