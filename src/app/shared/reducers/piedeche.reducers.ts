import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";
import {PiedecheActions} from "../actions";
import {PiedecheDTO} from "../../../../swagger-api/model/piedecheDTO";

export const piedecheFeatureKey = 'Piedeche';


export interface State {
  piedeches: PiedecheDTO[] | any[];
  piedeche: PiedecheDTO;
  isLoading: boolean
  sidebar: boolean;
}

const initialState: State = {
  piedeches: [],
  piedeche: {},
  isLoading: false,
  sidebar:false
};
export const reducer = createReducer(
  initialState,
  on(PiedecheActions.fetchPiedecheByJoursCaisse, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(PiedecheActions.setPiedeches, (state: State, {piedeches}) => {
    return {...state, piedeches, isLoading: false};
  }),
 on(PiedecheActions.setPiedeche, (state: State, {piedeche}) => {
    return {...state, piedeche, isLoading: false};
  }),
  on(PiedecheActions.setSideBar, (state: State, {sidebar}) => {
    return {...state, sidebar, isLoading: false};
  }),

  on(PiedecheActions.resetDataTable, state => {
    return {
      ...state,
      ...initialState
    };
  }),
);
export const piedecheStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('interfacage'),
  state => state[piedecheFeatureKey]);

export const selectPiedeches = createSelector(piedecheStateSelector, (state: State) => state.piedeches);
export const selectPiedeche = createSelector(piedecheStateSelector, (state: State) => state.piedeche);
export const selectIsLoading = createSelector(piedecheStateSelector, (state: State) => state.isLoading);
export const selectSideBar = createSelector(piedecheStateSelector, (state: State) => state.sidebar);


