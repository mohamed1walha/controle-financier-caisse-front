import {
  createFeatureSelector,
  createReducer,
  createSelector,
  MemoizedSelector,
  on,
} from '@ngrx/store';
import { LignePrelevementActions } from '../actions';
import { LignePrelevementDTO } from '../../../../swagger-api';

export const lignePrelevementFeatureKey = 'LignePrelevement';

export interface State {
  lignePrelevements: LignePrelevementDTO[];
  lignePrelevement: LignePrelevementDTO;
  isLoading: boolean;
}

const initialState: State = {
  lignePrelevements: [],
  lignePrelevement: {},
  isLoading: false,
};
export const reducer = createReducer(
  initialState,
  on(LignePrelevementActions.fetchLignePrelevements, (state: State) => {
    return { ...state, isLoading: true };
  }),
  on(
    LignePrelevementActions.setLignePrelevements,
    (state: State, { lignePrelevements }) => {
      return { ...state, lignePrelevements, isLoading: false };
    }
  )
);
export const lignePrelevementStateSelector: MemoizedSelector<object, State> =
  createSelector(
    createFeatureSelector('interfacage'),
    (state) => state[lignePrelevementFeatureKey]
  );

export const selectLignePrelevements = createSelector(
  lignePrelevementStateSelector,
  (state: State) => state.lignePrelevements
);
export const selectIsLoading = createSelector(
  lignePrelevementStateSelector,
  (state: State) => state.isLoading
);
