import {ModePaiementActions} from "../actions";
import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";
import {ModepaieDTO} from "../../../../swagger-api";

export const modePaiementFeatureKey = 'ModePaiement';

export const INITIAL_FILTER_KEY = {filterKey: '', query: ''}

export interface State {
  tableData: ModepaieDTO[];
}

const initialState: State = {
  tableData: [],
};
export const reducer = createReducer(
  initialState,
  on(ModePaiementActions.setData, (state, {data}) => {
    return {
      ...state,
      tableData: data
    };
  }),

  on(ModePaiementActions.resetDataTableStore, state => {
    return {
      ...state,
      ...initialState
    };
  })
);


export const modePaiementStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('interfacage'),
  state => state[modePaiementFeatureKey]);

export const selectTableData = createSelector(modePaiementStateSelector, (state: State) => state.tableData);







