import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";
import {EmetteurActions} from "../actions";
import {EmetteurDTO} from "../../../../swagger-api";

export const emetteurFeatureKey = 'Emetteur';


export interface State {
  emetteurs: EmetteurDTO[];
  emetteur: EmetteurDTO;
  isLoading: boolean
}

const initialState: State = {
  emetteurs: [],
  emetteur: {},
  isLoading: false
};
export const reducer = createReducer(
  initialState,
  on(EmetteurActions.fetchEmetteurs, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(EmetteurActions.setEmetteurs, (state: State, {emetteurs}) => {
    return {...state, emetteurs, isLoading: false};
  }),
);
export const emetteurStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('interfacage'),
  state => state[emetteurFeatureKey]);

export const selectEmetteurs = createSelector(emetteurStateSelector, (state: State) => state.emetteurs);
export const selectIsLoading = createSelector(emetteurStateSelector, (state: State) => state.isLoading);


