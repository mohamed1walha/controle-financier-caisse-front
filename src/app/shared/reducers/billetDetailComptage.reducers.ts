import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";
import {BilletDetailComptageActions} from "../actions";
import {BilletDetailComptageDTO} from "../../../../swagger-api";

export const billetDetailComptageFeatureKey = 'BilletDetailComptage';


export interface State {
  BilletDetailComptages: BilletDetailComptageDTO[];
  BilletDetailComptage: BilletDetailComptageDTO;
  isLoading: boolean
}

const initialState: State = {
  BilletDetailComptages: [],
  BilletDetailComptage: {},
  isLoading: false
};
export const reducer = createReducer(
  initialState,
  on(BilletDetailComptageActions.fetchBilletDetailComptages, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(BilletDetailComptageActions.setBilletDetailComptages, (state: State, {BilletDetailComptages}) => {
    return {...state, BilletDetailComptages, isLoading: false};
  }),
);
export const billetDetailComptageStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('interfacage'),
  state => state[billetDetailComptageFeatureKey]);

export const selectBilletDetailComptages = createSelector(billetDetailComptageStateSelector, (state: State) => state.BilletDetailComptages);
export const selectIsLoading = createSelector(billetDetailComptageStateSelector, (state: State) => state.isLoading);


