import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";
import {ArticleActions} from "../actions";
import {ArticleDTO} from "../../../../swagger-api";

export const articleFeatureKey = 'Article';


export interface State {
  articles: ArticleDTO[];
  article: ArticleDTO;
  isLoading: boolean
}

const initialState: State = {
  articles: [],
  article: {},
  isLoading: false
};
export const reducer = createReducer(
  initialState,
  on(ArticleActions.fetchArticles, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(ArticleActions.setArticles, (state: State, {articles}) => {
    return {...state, articles, isLoading: false};
  }),
);
export const articleStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('interfacage'),
  state => state[articleFeatureKey]);

export const selectArticles = createSelector(articleStateSelector, (state: State) => state.articles);
export const selectIsLoading = createSelector(articleStateSelector, (state: State) => state.isLoading);


