import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";
import {ParamBilletActions} from "../actions";
import {ParamBilletDTO, TypeBilletDTO} from "../../../../swagger-api";

export const paramBilletFeatureKey = 'ParamBillet';


export interface State {
  ParamBillets: ParamBilletDTO[];
  TypeBillets: TypeBilletDTO[];
  ParamBillet: ParamBilletDTO;
  isLoading: boolean
}

const initialState: State = {
  ParamBillets: [],
  TypeBillets: [],
  ParamBillet: {},
  isLoading: false
};
export const reducer = createReducer(
  initialState,
  on(ParamBilletActions.fetchParamBillets, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(ParamBilletActions.fetchTypeBillets, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(ParamBilletActions.setParamBillets, (state: State, {ParamBillets}) => {
    return {...state, ParamBillets, isLoading: false};
  }),
  on(ParamBilletActions.setTypeBillets, (state: State, {TypeBillets}) => {
    return {...state, TypeBillets, isLoading: false};
  }),
);
export const paramBilletStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('interfacage'),
  state => state[paramBilletFeatureKey]);

export const selectParamBillets = createSelector(paramBilletStateSelector, (state: State) => state.ParamBillets);
export const selectTypeBillets = createSelector(paramBilletStateSelector, (state: State) => state.TypeBillets);
export const selectIsLoading = createSelector(paramBilletStateSelector, (state: State) => state.isLoading);


