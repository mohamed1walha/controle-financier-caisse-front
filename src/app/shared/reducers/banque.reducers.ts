import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";
import {BanqueActions} from "../actions";
import {BanqueDTO} from "../../../../swagger-api";

export const banqueFeatureKey = 'Banque';


export interface State {
  banques: BanqueDTO[];
  banque: BanqueDTO;
  isLoading: boolean
}

const initialState: State = {
  banques: [],
  banque: {},
  isLoading: false
};
export const reducer = createReducer(
  initialState,
  on(BanqueActions.fetchBanques, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(BanqueActions.setBanques, (state: State, {banques}) => {
    return {...state, banques, isLoading: false};
  }),
);
export const banqueStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('interfacage'),
  state => state[banqueFeatureKey]);

export const selectBanques = createSelector(banqueStateSelector, (state: State) => state.banques);
export const selectIsLoading = createSelector(banqueStateSelector, (state: State) => state.isLoading);


