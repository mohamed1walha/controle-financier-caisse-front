import {LastSyncAppActions} from "../actions";
import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";

export const LastSyncAppFeatureKey = 'LastSyncApp';

export const INITIAL_FILTER_KEY = {filterKey: '', query: ''}

export interface State {
  lastsync?: Date;
}

const initialState: State = {
  lastsync: new Date()
};
export const reducer = createReducer(
  initialState,


  on(LastSyncAppActions.setlastSync, (state, {lastsync}) => ({
    ...state,
    lastsync: lastsync
  }))
);

export const lastSyncAppStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('interfacage'),
  state => state[LastSyncAppFeatureKey]);


export const getDateSync = createSelector(
  lastSyncAppStateSelector,
  (state: State) => state.lastsync
);





