import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";
import {MotifActions} from "../actions";
import {MotifDTO} from "../../../../swagger-api";

export const motifFeatureKey = 'Motif';


export interface State {
  motifs: MotifDTO[];
  motif: MotifDTO;
  isLoading: boolean
}

const initialState: State = {
  motifs: [],
  motif: {},
  isLoading: false
};
export const reducer = createReducer(
  initialState,
  on(MotifActions.fetchMotifs, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(MotifActions.setMotifs, (state: State, {motifs}) => {
    return {...state, motifs, isLoading: false};
  }),
);
export const motifStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('interfacage'),
  state => state[motifFeatureKey]);

export const selectMotifs = createSelector(motifStateSelector, (state: State) => state.motifs);
export const selectIsLoading = createSelector(motifStateSelector, (state: State) => state.isLoading);


