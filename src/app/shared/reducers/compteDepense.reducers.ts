import {
  createFeatureSelector,
  createReducer,
  createSelector,
  MemoizedSelector,
  on,
} from '@ngrx/store';
import { CompteDepenseActions } from '../actions';
import { CompteDepenseDTO } from '../../../../swagger-api';

export const compteDepenseFeatureKey = 'CompteDepense';

export interface State {
  compteDepenses: CompteDepenseDTO[];
  compteDepense: CompteDepenseDTO;
  isLoading: boolean;
}

const initialState: State = {
  compteDepenses: [],
  compteDepense: {},
  isLoading: false,
};
export const reducer = createReducer(
  initialState,
  on(CompteDepenseActions.fetchCompteDepenses, (state: State) => {
    return { ...state, isLoading: true };
  }),
  on(CompteDepenseActions.fetchCompteDepenseById, (state: State) => {
    return { ...state, isLoading: true };
  }),
  on(
    CompteDepenseActions.setCompteDepenses,
    (state: State, { compteDepenses }) => {
      return { ...state, compteDepenses, isLoading: false };
    }
  ),
  on(
    CompteDepenseActions.setCompteDepense,
    (state: State, { compteDepense }) => {
      return { ...state, compteDepense, isLoading: false };
    }
  )
);
export const compteDepenseStateSelector: MemoizedSelector<object, State> =
  createSelector(
    createFeatureSelector('interfacage'),
    (state) => state[compteDepenseFeatureKey]
  );

export const selectCompteDepenses = createSelector(
  compteDepenseStateSelector,
  (state: State) => state.compteDepenses
);
export const selectCompteDepense = createSelector(
  compteDepenseStateSelector,
  (state: State) => state.compteDepense
);
export const selectIsLoading = createSelector(
  compteDepenseStateSelector,
  (state: State) => state.isLoading
);
