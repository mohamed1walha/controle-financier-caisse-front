import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";
import {MailDestinataireActions} from "../actions";
import {MailDestinataireDTO} from "../../../../swagger-api";

export const mailDestinataireFeatureKey = 'MailDestinataire';

export interface State {
  mailDestinataires: MailDestinataireDTO[];
  mailDestinataire: MailDestinataireDTO;
  isLoading: boolean
}

const initialState: State = {
  mailDestinataires: [],
  mailDestinataire: {},
  isLoading: false
};
export const reducer = createReducer(
  initialState,
  on(MailDestinataireActions.fetchMailDestinataires, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(MailDestinataireActions.setMailDestinataires, (state: State, {mailDestinataires}) => {
    return {...state, mailDestinataires, isLoading: false};
  }),
);
export const mailDestinataireStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('interfacage'),
  state => state[mailDestinataireFeatureKey]);

export const selectMailDestinataires = createSelector(mailDestinataireStateSelector, (state: State) => state.mailDestinataires);
export const selectIsLoading = createSelector(mailDestinataireStateSelector, (state: State) => state.isLoading);


