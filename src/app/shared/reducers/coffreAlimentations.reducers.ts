import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";
import {CoffrealimentationDTO} from "../../../../swagger-api";
import {CoffreAlimentationActions} from "../actions";

export const coffrealimentationFeatureKey = 'coffrealimentation';


export interface State {
  coffrealimentations: CoffrealimentationDTO[];
  coffrealimentation: CoffrealimentationDTO;
  isLoading: boolean
}

const initialState: State = {
  coffrealimentations: [],
  coffrealimentation: {},
  isLoading: false
};
export const reducer = createReducer(
  initialState,
  on(CoffreAlimentationActions.fetchcoffrealimentations, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(CoffreAlimentationActions.setcoffrealimentations, (state: State, {coffrealimentations}) => {
    return {...state, coffrealimentations, isLoading: false};
  }),
);
export const coffrealimentationStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('interfacage'),
  state => state[coffrealimentationFeatureKey]);

export const selectcoffrealimentations = createSelector(coffrealimentationStateSelector, (state: State) => state.coffrealimentations);
export const selectIsLoading = createSelector(coffrealimentationStateSelector, (state: State) => state.isLoading);


