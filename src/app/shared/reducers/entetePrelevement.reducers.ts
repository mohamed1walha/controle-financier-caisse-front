import {
  createFeatureSelector,
  createReducer,
  createSelector,
  MemoizedSelector,
  on,
} from '@ngrx/store';
import { EntetePrelevementActions } from '../actions';
import {
  EntetePrelevementDTO,
  FilterPrelevementDTO,
} from '../../../../swagger-api';

export const entetePrelevementFeatureKey = 'EntetePrelevement';

export interface State {
  entetePrelevements: EntetePrelevementDTO[];
  enteteValidationPrelevements: EntetePrelevementDTO[];
  entetePrelevement: EntetePrelevementDTO;
  filter: FilterPrelevementDTO;
  isLoading: boolean;
  files : any;
}

const initialState: State = {
  entetePrelevements: [],
  enteteValidationPrelevements: [],
  entetePrelevement: {},
  filter: {},
  isLoading: false,
  files: null
};
export const reducer = createReducer(
  initialState,
  on(EntetePrelevementActions.fetchAllEntetePrelevements, (state: State) => {
    return { ...state, isLoading: true };
  }),
  on(
    EntetePrelevementActions.setEntetePrelevements,
    (state: State, { entetePrelevements }) => {
      return { ...state, entetePrelevements, isLoading: false };
    }
  ),
  on(
    EntetePrelevementActions.setFilterPrelevements,
    (state: State, { filter }) => {
      return { ...state, filter, isLoading: false };
    }
  ),
  on(
    EntetePrelevementActions.setEnteteValidationPrelevements,
    (state: State, { enteteValidationPrelevements }) => {
      return { ...state, enteteValidationPrelevements, isLoading: false };
    }
  ),
  on(
    EntetePrelevementActions.setEntetePrelevement,
    (state: State, { entetePrelevement }) => {
      return { ...state, entetePrelevement, isLoading: false };
    }
  ),
  on(
    EntetePrelevementActions.setAnnexesFile,
    (state: State, { files }) => {
      return { ...state, files, isLoading: false };
    }
  )
);
export const entetePrelevementStateSelector: MemoizedSelector<object, State> =
  createSelector(
    createFeatureSelector('interfacage'),
    (state) => state[entetePrelevementFeatureKey]
  );

export const selectEntetePrelevements = createSelector(
  entetePrelevementStateSelector,
  (state: State) => state.entetePrelevements
);

export const selectEnteteValidationPrelevements = createSelector(
  entetePrelevementStateSelector,
  (state: State) => state.enteteValidationPrelevements
);

export const selectEntetePrelevement = createSelector(
  entetePrelevementStateSelector,
  (state: State) => state.entetePrelevement
);
export const selectIsLoading = createSelector(
  entetePrelevementStateSelector,
  (state: State) => state.isLoading
);
export const selectFilterPrelevement = createSelector(
  entetePrelevementStateSelector,
  (state: State) => state.filter
);

export const selectAnnexesFile = createSelector(
  entetePrelevementStateSelector,
  (state: State) => state.files
);

