import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";
import {RolesDTO} from "../../../../swagger-api";
import {RolesActions} from "../actions";

export const roleFeatureKey = 'Roles';


export interface State {
  roles: RolesDTO[];
  role: RolesDTO;
  isLoading: boolean
}

const initialState: State = {
  roles: [],
  role: {},
  isLoading: false
};
export const reducer = createReducer(
  initialState,
  on(RolesActions.fetchRoles, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(RolesActions.setRoles, (state: State, {roles}) => {
    return {...state, roles, isLoading: false};
  }),
);
export const roleStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('users'),
  state => state[roleFeatureKey]);

export const selectRoles = createSelector(roleStateSelector, (state: State) => state.roles);
export const selectIsLoading = createSelector(roleStateSelector, (state: State) => state.isLoading);


