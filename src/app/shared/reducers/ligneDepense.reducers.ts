import {
  createFeatureSelector,
  createReducer,
  createSelector,
  MemoizedSelector,
  on,
} from '@ngrx/store';
import { LigneDepenseActions } from '../actions';
import { LigneDepenseDTO } from '../../../../swagger-api';

export const ligneDepenseFeatureKey = 'LigneDepense';

export interface State {
  ligneDepenses: LigneDepenseDTO[];
  ligneDepense: LigneDepenseDTO;
  isLoading: boolean;
}

const initialState: State = {
  ligneDepenses: [],
  ligneDepense: {},
  isLoading: false,
};
export const reducer = createReducer(
  initialState,
  on(LigneDepenseActions.fetchLigneDepenses, (state: State) => {
    return { ...state, isLoading: true };
  }),
  on(
    LigneDepenseActions.setLigneDepenses,
    (state: State, { ligneDepenses }) => {
      return { ...state, ligneDepenses, isLoading: false };
    }
  )
);
export const ligneDepenseStateSelector: MemoizedSelector<object, State> =
  createSelector(
    createFeatureSelector('interfacage'),
    (state) => state[ligneDepenseFeatureKey]
  );

export const selectLigneDepenses = createSelector(
  ligneDepenseStateSelector,
  (state: State) => state.ligneDepenses
);
export const selectIsLoading = createSelector(
  ligneDepenseStateSelector,
  (state: State) => state.isLoading
);
