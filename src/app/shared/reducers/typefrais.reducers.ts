import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";
import {TypeFraisActions} from "../actions";
import {TypeFraisDTO} from "../../../../swagger-api";

export const TypeFraisFeatureKey = 'TypeFrais';


export interface State {
  TypeFraiss: TypeFraisDTO[];
  TypeFrais: TypeFraisDTO;
  isLoading: boolean
}

const initialState: State = {
  TypeFraiss: [],
  TypeFrais: {},
  isLoading: false
};
export const reducer = createReducer(
  initialState,
  on(TypeFraisActions.fetchTypeFraiss, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(TypeFraisActions.setTypeFraiss, (state: State, {TypeFraiss}) => {
    return {...state, TypeFraiss, isLoading: false};
  }),
);
export const TypeFraisStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('interfacage'),
  state => state[TypeFraisFeatureKey]);

export const selectTypeFraiss = createSelector(TypeFraisStateSelector, (state: State) => state.TypeFraiss);
export const selectIsLoading = createSelector(TypeFraisStateSelector, (state: State) => state.isLoading);


