import {
  createFeatureSelector,
  createReducer,
  createSelector,
  MemoizedSelector,
  on,
} from '@ngrx/store';
import { ReportBilletActions } from '../actions';
import { ReportBilletDTO } from '../../../../swagger-api';

export const reportBilletFeatureKey = 'ReportBillet';

export interface State {
  reportBillets: ReportBilletDTO[];

  isLoading: boolean;
}

const initialState: State = {
  reportBillets: [],
  isLoading: false,
};
export const reducer = createReducer(
  initialState,
  on(ReportBilletActions.fetchReportBillets, (state: State) => {
    return { ...state, isLoading: true };
  }),
  on(
    ReportBilletActions.setReportBillets,
    (state: State, { reportBillets }) => {
      return { ...state, reportBillets, isLoading: false };
    }
  )
);
export const reportBilletStateSelector: MemoizedSelector<object, State> =
  createSelector(
    createFeatureSelector('interfacage'),
    (state) => state[reportBilletFeatureKey]
  );

export const selectReportBillets = createSelector(
  reportBilletStateSelector,
  (state: State) => state.reportBillets
);
export const selectIsLoading = createSelector(
  reportBilletStateSelector,
  (state: State) => state.isLoading
);
