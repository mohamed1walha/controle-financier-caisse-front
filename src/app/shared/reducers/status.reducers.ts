import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";
import {StatusActions} from "../actions";
import {StatusDTO} from "../../../../swagger-api";

export const statusFeatureKey = 'Status';


export interface State {
  statuss: StatusDTO[];
  status: StatusDTO;

}

const initialState: State = {
  statuss: [],
  status: {},

};
export const reducer = createReducer(
  initialState,
  on(StatusActions.setStatuss, (state, {statuss}) => {
      return {
        ...state,
        statuss
      };
    }
  ),
  on(StatusActions.setStatus, (state, {status}) => {
      return {
        ...state,
        status
      };
    }
  )
);
export const statusStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('interfacage'),
  state => state[statusFeatureKey]);

export const selectStatuss = createSelector(statusStateSelector, (state: State) => state.statuss);
export const selectStatus = createSelector(statusStateSelector, (state: State) => state.status);



