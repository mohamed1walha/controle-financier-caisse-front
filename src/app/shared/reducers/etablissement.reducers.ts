import {
  createFeatureSelector,
  createReducer,
  createSelector,
  MemoizedSelector,
  on,
} from '@ngrx/store';
import { EtablissementDTO } from '../../../../swagger-api';
import { EtablissementActions } from '../actions';

export const etablissementFeatureKey = 'Etablissement';

export interface State {
  etablissements: EtablissementDTO[];
  selectedEtablissement: EtablissementDTO;
  isLoading: boolean;
}

const initialState: State = {
  etablissements: [],
  selectedEtablissement: {},
  isLoading: false,
};
export const reducer = createReducer(
  initialState,
  on(EtablissementActions.fetchEtablissements, (state: State) => {
    return { ...state, isLoading: true };
  }),
  on(
    EtablissementActions.setEtablissements,
    (state: State, { etablissements }) => {
      return { ...state, etablissements, isLoading: false };
    }
  ),
  on(
    EtablissementActions.setEtablissement,
    (state: State, { etablissement }) => {
      return {
        ...state,
        selectedEtablissement: etablissement,
        isLoading: false,
      };
    }
  )
);
export const etablissementStateSelector: MemoizedSelector<object, State> =
  createSelector(
    createFeatureSelector('interfacage'),
    (state) => state[etablissementFeatureKey]
  );

export const selectEtablissements = createSelector(
  etablissementStateSelector,
  (state: State) => state.etablissements
);
export const selectIsLoading = createSelector(
  etablissementStateSelector,
  (state: State) => state.isLoading
);

export const selectEtablissement = createSelector(
  etablissementStateSelector,
  (state: State) => state.selectedEtablissement
);
