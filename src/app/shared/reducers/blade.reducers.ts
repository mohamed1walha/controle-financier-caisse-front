import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from '@ngrx/store';
import {BladeActions} from "../actions";

export const bladeFeatureKey = 'blades';

export interface State {
  isBladeExpanded: boolean;
  bladeWidth: number;

}

const initialState: State = {
  isBladeExpanded: false,
  bladeWidth: 0,

};

export const reducer = createReducer(
  initialState,
  on(BladeActions.openBlade, (state: State) => {
    return {...state, bladeWidth: 60, isBladeExpanded: true};
  }),
  on(BladeActions.setBladeWidth, (state: State, {width}) => ({
    ...state,
    bladeWidth: width
  })),
  on(BladeActions.closeBlade, (state: State) => {
    return {...state, bladeWidth: 0, isBladeExpanded: false};
  })
);

const commonsStateSelector: MemoizedSelector<object, State> =
  createFeatureSelector<State>(bladeFeatureKey);
export const selectBladeIsExpanded = createSelector(
  commonsStateSelector,
  (state: State) => state.isBladeExpanded
);
export const selectBladeWidth = createSelector(
  commonsStateSelector,
  (state: State) => state.bladeWidth
);
