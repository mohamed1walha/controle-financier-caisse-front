import {
  createFeatureSelector,
  createReducer,
  createSelector,
  MemoizedSelector,
  on,
} from '@ngrx/store';
import {ParcaisseActions} from '../actions';
import {ParcaisseDTO} from '../../../../swagger-api';

export const parcaisseFeatureKey = 'Parcaisse';

export interface State {
  parcaisses: ParcaisseDTO[];
  parcaisse: ParcaisseDTO;
  isLoading: boolean;
}

const initialState: State = {
  parcaisses: [],
  parcaisse: {},
  isLoading: false,
};
export const reducer = createReducer(
  initialState,
  on(ParcaisseActions.fetchParcaisses, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(ParcaisseActions.setParcaisses, (state: State, {parcaisses}) => {
    return {...state, parcaisses, isLoading: false};
  })
);
export const parcaisseStateSelector: MemoizedSelector<object, State> =
  createSelector(
    createFeatureSelector('interfacage'),
    (state) => state[parcaisseFeatureKey]
  );

export const selectParcaisses = createSelector(
  parcaisseStateSelector,
  (state: State) => state.parcaisses
);
export const selectIsLoading = createSelector(
  parcaisseStateSelector,
  (state: State) => state.isLoading
);


