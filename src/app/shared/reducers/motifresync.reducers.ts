import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";
import {MotifresyncActions} from "../actions";
import {MotifresyncDTO} from "../../../../swagger-api";

export const MotifresyncFeatureKey = 'Motifresync';


export interface State {
  Motifresyncs: MotifresyncDTO[];
  Motifresync: MotifresyncDTO;
  isLoading: boolean
}

const initialState: State = {
  Motifresyncs: [],
  Motifresync: {},
  isLoading: false
};
export const reducer = createReducer(
  initialState,
  on(MotifresyncActions.fetchMotifresyncs, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(MotifresyncActions.setMotifresyncs, (state: State, {Motifresyncs}) => {
    return {...state, Motifresyncs, isLoading: false};
  }),
);
export const MotifresyncStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('interfacage'),
  state => state[MotifresyncFeatureKey]);

export const selectMotifresyncs = createSelector(MotifresyncStateSelector, (state: State) => state.Motifresyncs);
export const selectIsLoading = createSelector(MotifresyncStateSelector, (state: State) => state.isLoading);


