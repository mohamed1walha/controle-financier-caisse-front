import {
  createFeatureSelector,
  createReducer,
  createSelector,
  MemoizedSelector,
  on,
} from '@ngrx/store';
import { FournisseurActions } from '../actions';
import { FournisseurDTO } from '../../../../swagger-api';

export const fournisseurFeatureKey = 'Fournisseur';

export interface State {
  fournisseurs: FournisseurDTO[];
  fournisseur: FournisseurDTO;
  isLoading: boolean;
}

const initialState: State = {
  fournisseurs: [],
  fournisseur: {},
  isLoading: false,
};
export const reducer = createReducer(
  initialState,
  on(FournisseurActions.fetchFournisseurs, (state: State) => {
    return { ...state, isLoading: true };
  }),
  on(FournisseurActions.setFournisseurs, (state: State, { fournisseurs }) => {
    return { ...state, fournisseurs, isLoading: false };
  }),
  on(FournisseurActions.setFournisseur, (state: State, { fournisseur }) => {
    return { ...state, fournisseur, isLoading: false };
  })
);
export const fournisseurStateSelector: MemoizedSelector<object, State> =
  createSelector(
    createFeatureSelector('interfacage'),
    (state) => state[fournisseurFeatureKey]
  );

export const selectFournisseurs = createSelector(
  fournisseurStateSelector,
  (state: State) => state.fournisseurs
);
export const selectFournisseur = createSelector(
  fournisseurStateSelector,
  (state: State) => state.fournisseur
);
export const selectIsLoading = createSelector(
  fournisseurStateSelector,
  (state: State) => state.isLoading
);
