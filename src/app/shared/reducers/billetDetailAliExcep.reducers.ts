import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";
import {BilletDetailAliExcepActions} from "../actions";
import {BilletDetailAliExcepDTO} from "../../../../swagger-api";

export const billetDetailAliExcepFeatureKey = 'BilletDetailAliExcep';


export interface State {
  BilletDetailAliExceps: BilletDetailAliExcepDTO[];
  BilletDetailAliExcep: BilletDetailAliExcepDTO;
  isLoading: boolean
}

const initialState: State = {
  BilletDetailAliExceps: [],
  BilletDetailAliExcep: {},
  isLoading: false
};
export const reducer = createReducer(
  initialState,
  on(BilletDetailAliExcepActions.fetchBilletDetailAliExceps, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(BilletDetailAliExcepActions.setBilletDetailAliExceps, (state: State, {BilletDetailAliExceps}) => {
    return {...state, BilletDetailAliExceps, isLoading: false};
  }),
);
export const billetDetailAliExcepStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('interfacage'),
  state => state[billetDetailAliExcepFeatureKey]);

export const selectBilletDetailAliExceps = createSelector(billetDetailAliExcepStateSelector, (state: State) => state.BilletDetailAliExceps);
export const selectIsLoading = createSelector(billetDetailAliExcepStateSelector, (state: State) => state.isLoading);


