import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";
import {BanqueActions, ReportActions} from "../actions";
import {BanqueDTO, ReportDTO} from "../../../../swagger-api";

export const reportFeatureKey = 'Report';


export interface State {
  reports: ReportDTO[];
  report: ReportDTO;
  isLoading: boolean
}

const initialState: State = {
  reports: [],
  report: {},
  isLoading: false
};
export const reducer = createReducer(
  initialState,
  on(ReportActions.fetchReports, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(ReportActions.setReports, (state: State, {reports}) => {
    return {...state, reports, isLoading: false};
  }),
);
export const reportStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('interfacage'),
  state => state[reportFeatureKey]);

export const selectReports = createSelector(reportStateSelector, (state: State) => state.reports);
export const selectIsLoading = createSelector(reportStateSelector, (state: State) => state.isLoading);


