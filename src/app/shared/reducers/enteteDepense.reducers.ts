import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";
import {EnteteDepenseActions} from "../actions";
import {EnteteDepenseDTO} from "../../../../swagger-api";

export const enteteDepenseFeatureKey = 'EnteteDepense';


export interface State {
  enteteDepenses: EnteteDepenseDTO[];
  enteteValidationDepenses: EnteteDepenseDTO[];
  enteteDepense: EnteteDepenseDTO;
  isLoading: boolean
}

const initialState: State = {
  enteteDepenses: [],
  enteteValidationDepenses: [],
  enteteDepense: {},
  isLoading: false
};
export const reducer = createReducer(
  initialState,
 /* on(EnteteDepenseActions.fetchAllEnteteDepenses, (state: State) => {
    return {...state, isLoading: true};
  }),*/
  on(EnteteDepenseActions.setEnteteDepenses, (state: State, {enteteDepenses}) => {
    return {...state, enteteDepenses, isLoading: false};
  }),
  on(EnteteDepenseActions.setEnteteValidationDepenses, (state: State, {enteteValidationDepenses}) => {
    return {...state, enteteValidationDepenses, isLoading: false};
  }),
  on(EnteteDepenseActions.setEnteteDepense, (state: State, {enteteDepense}) => {
    return {...state, enteteDepense, isLoading: false};
  }),
);
export const enteteDepenseStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('interfacage'),
  state => state[enteteDepenseFeatureKey]);

export const selectEnteteDepenses = createSelector(enteteDepenseStateSelector, (state: State) => state.enteteDepenses);
export const selectEnteteValidationDepenses = createSelector(enteteDepenseStateSelector, (state: State) => state.enteteValidationDepenses);
export const selectEnteteDepense = createSelector(enteteDepenseStateSelector, (state: State) => state.enteteDepense);
export const selectIsLoading = createSelector(enteteDepenseStateSelector, (state: State) => state.isLoading);


