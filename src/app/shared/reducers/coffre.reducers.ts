import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";
import {CoffreActions} from "../actions";
import {BilletDetailSortieDTO, CoffrealimentationDTO, CoffreDTO, CoffresortieDTO} from "../../../../swagger-api";

export const coffreFeatureKey = 'Coffre';


export interface State {
  coffres: CoffreDTO[];
  sorties: CoffresortieDTO[];
  alimentations: CoffrealimentationDTO[];
  allSorties: CoffresortieDTO[];
  allCalculetteBySortieId: BilletDetailSortieDTO[];
  coffre: CoffreDTO;
  sortie: CoffresortieDTO;
  isLoading: boolean
}

const initialState: State = {
  coffres: [],
  sorties: [],
  alimentations: [],
  allSorties: [],
  allCalculetteBySortieId: [],
  coffre: {},
  sortie: {},
  isLoading: false
};
export const reducer = createReducer(
  initialState,
  on(CoffreActions.fetchCoffres, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(CoffreActions.fetchAllSorties, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(CoffreActions.fetchCalculetteBySortieId, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(CoffreActions.setCoffres, (state: State, {coffres}) => {
    return {...state, coffres, isLoading: false};
  }),
  on(CoffreActions.setCoffreSortie, (state: State, {sortie}) => {
    return {...state, sortie};
  }),
  on(CoffreActions.fetchSorties, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(CoffreActions.setSorties, (state: State, {sorties}) => {
    return {...state, sorties, isLoading: false};
  }),
  on(CoffreActions.fetchAlimentations, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(CoffreActions.setAlimentations, (state: State, {alimentations}) => {
    return {...state, alimentations, isLoading: false};
  }),
  on(CoffreActions.setAllSorties, (state: State, {allSorties}) => {
    return {...state, allSorties, isLoading: false};
  }),
  on(CoffreActions.setCalculetteBySortieId, (state: State, {allCalculetteBySortieId}) => {
    return {...state, allCalculetteBySortieId, isLoading: false};
  }),
  on(CoffreActions.setCoffre, (state: State, {coffre}) => {
    return {...state, coffre, isLoading: false};
  }),
);
export const coffreStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('interfacage'),
  state => state[coffreFeatureKey]);

export const selectCoffres = createSelector(coffreStateSelector, (state: State) => state.coffres);
export const selectIsLoading = createSelector(coffreStateSelector, (state: State) => state.isLoading);
export const selectCoffre = createSelector(coffreStateSelector, (state: State) => state.coffre);
export const selectSorties = createSelector(coffreStateSelector, (state: State) => state.sorties);
export const selectSortie = createSelector(coffreStateSelector, (state: State) => state.sortie);
export const selectAlimentations = createSelector(coffreStateSelector, (state: State) => state.alimentations);
export const selectAllSorties = createSelector(coffreStateSelector, (state: State) => state.allSorties);
export const selectCalculetteBySortieId = createSelector(coffreStateSelector, (state: State) => state.allCalculetteBySortieId);


