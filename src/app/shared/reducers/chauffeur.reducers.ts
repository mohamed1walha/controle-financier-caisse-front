import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";
import {ChauffeurActions} from "../actions";
import {ChauffeurDTO} from "../../../../swagger-api";

export const chauffeurFeatureKey = 'Chauffeur';


export interface State {
  chauffeurs: ChauffeurDTO[];
  chauffeur: ChauffeurDTO;
  isLoading: boolean
}

const initialState: State = {
  chauffeurs: [],
  chauffeur: {},
  isLoading: false
};
export const reducer = createReducer(
  initialState,
  on(ChauffeurActions.fetchChauffeurs, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(ChauffeurActions.setChauffeurs, (state: State, {chauffeurs}) => {
    return {...state, chauffeurs, isLoading: false};
  }),
);
export const chauffeurStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('interfacage'),
  state => state[chauffeurFeatureKey]);

export const selectChauffeurs = createSelector(chauffeurStateSelector, (state: State) => state.chauffeurs);
export const selectIsLoading = createSelector(chauffeurStateSelector, (state: State) => state.isLoading);


