import { JoursCaisseActions } from '../actions';
import {
  createFeatureSelector,
  createReducer,
  createSelector,
  MemoizedSelector,
  on,
} from '@ngrx/store';
import { JoursCaiseDTO, StatsDTO } from '../../../../swagger-api';
import { FilterMetadata } from 'primeng/api';

export const joursCaisseFeatureKey = 'JoursCaisse';

export const INITIAL_FILTER_KEY = { filterKey: '', query: '' };

export interface State {
  tableData: JoursCaiseDTO[];
  loading: boolean;
  sortDirection: string;
  sortKey: string;
  filterQuery: string;
  filterBy: string[];
  filters: { [s: string]: FilterMetadata | FilterMetadata[] };
  numZByEtablissement: number[];
  caisseByEtablissement: string[];
  stats: StatsDTO;
  joursCaisse: JoursCaiseDTO;
}

const initialState: State = {
  tableData: [],
  loading: false,
  sortDirection: '',
  sortKey: '',
  filterQuery: '',
  filterBy: [],
  filters: {},
  numZByEtablissement: [],
  caisseByEtablissement: [],
  stats: {},
  joursCaisse: {},
};
export const reducer = createReducer(
  initialState,
  on(JoursCaisseActions.fetchAllDataStatusCpmtNone, (state) => {
    return {
      ...state,
      loading: true,
    };
  }),
  on(JoursCaisseActions.fetchAllDataStatusCompter, (state) => {
    return {
      ...state,
      loading: true,
    };
  }),
  on(JoursCaisseActions.fetchAllDataStatusEnAttente, (state) => {
    return {
      ...state,
      loading: true,
    };
  }),
  on(JoursCaisseActions.fetchAllDataStatusReceptionneNonValide, (state) => {
    return {
      ...state,
      loading: true,
    };
  }),
  on(JoursCaisseActions.fetchAllDataStatusReceptionne, (state) => {
    return {
      ...state,
      loading: true,
    };
  }),
  on(JoursCaisseActions.setData, (state, { data }) => {
    return {
      ...state,
      tableData: data,
      loading: false,
    };
  }),
  on(JoursCaisseActions.setStats, (state, { stats }) => {
    return {
      ...state,
      stats: stats,
      loading: false,
    };
  }),
  on(JoursCaisseActions.fetchByIdParams, (state) => {
    return {
      ...state,
      loading: true,
    };
  }),

  on(JoursCaisseActions.setSortKey, (state, { sortKey }) => {
    sortKey = sortKey?.toLowerCase();

    let sortDirection;
    if (sortKey !== state.sortKey) {
      sortDirection = 'asc';
    } else {
      sortDirection = setSortDirection(state.sortDirection);
    }
    return {
      ...state,
      sortKey,
      sortDirection,
    };
  }),
  on(JoursCaisseActions.resetDataTableStore, (state) => {
    return {
      ...state,
      ...initialState,
    };
  }),

  on(JoursCaisseActions.fetchByFilters, (state, { filters }) => {
    return {
      ...state,
      filters,
      loading: true,
    };
  }),
  on(JoursCaisseActions.setDataFilters, (state, { filters }) => {
    return {
      ...state,
      filters,
      loading: false,
    };
  }),

  on(JoursCaisseActions.setFilterBy, (state, { filters }) => {
    return {
      ...state,
      sortDirection: '',
      sortKey: '',
      filterQuery: filters.query,
      filterBy: filters.filterBy,
      loading: false,
    };
  }),
  on(JoursCaisseActions.setJoursCaisse, (state: State, { joursCaisse }) => {
    return { ...state, joursCaisse, loading: false };
  }),
  on(JoursCaisseActions.setCaisseByEtablissement, (state: State, { data }) => {
    return { ...state, caisseByEtablissement: data, loading: false };
  })
);

export const joursCaisseStateSelector: MemoizedSelector<object, State> =
  createSelector(
    createFeatureSelector('interfacage'),
    (state) => state[joursCaisseFeatureKey]
  );

export const selectSortDirection = createSelector(
  joursCaisseStateSelector,
  (state: State) => state.sortDirection
);
export const selectStats = createSelector(
  joursCaisseStateSelector,
  (state: State) => state.stats
);
export const selectSortKey = createSelector(
  joursCaisseStateSelector,
  (state: State) => state.sortKey
);
export const selectTableData = createSelector(
  joursCaisseStateSelector,
  (state: State) => state.tableData
);

export const selectFilterQuery = createSelector(
  joursCaisseStateSelector,
  (state: State) => state.filterQuery
);
export const selectFilterBy = createSelector(
  joursCaisseStateSelector,
  (state: State) => state.filterBy
);
export const selectLoading = createSelector(
  joursCaisseStateSelector,
  (state: State) => state.loading
);

export const selectNumZByEtablissement = createSelector(
  joursCaisseStateSelector,
  (state: State) => state.numZByEtablissement
);
export const selectCaisseByEtablissement = createSelector(
  joursCaisseStateSelector,
  (state: State) => state.caisseByEtablissement
);
export const selectJoursCaisse = createSelector(
  joursCaisseStateSelector,
  (state: State) => state.joursCaisse
);
export const selectIsLoading = createSelector(
  joursCaisseStateSelector,
  (state: State) => state.loading
);

export const selectData = createSelector(
  selectTableData,
  selectSortDirection,
  selectSortKey,
  selectFilterQuery,
  selectFilterBy,

  (tableData, sortDirection, sortKey, filterQuery, filterBy) => {
    let filteredData = [...tableData];

    // Filter Array
    if (filterQuery !== '') {
      filteredData = filteredData.filter((item) => {
        const result = filterBy
          .map((filterBy) => {
            return item[filterBy]
              ?.toString()
              .toLowerCase()
              .includes(filterQuery.toString());
          })
          .some((item) => item);
        return result;
      });
    }

    if (sortDirection === '') {
      return filteredData;
    }

    const sortedData = [...filteredData].sort((a, b) => {
      const paramA = a[sortKey];
      const paramB = b[sortKey];
      return compare(paramA, paramB, sortDirection);
    });
    return sortedData;
  }
);

// Utils
export function compare(a: any, b: any, sortDirection: string): number {
  if (a > b) {
    return sortDirection === 'asc' ? 1 : -1;
  }
  if (a < b) {
    return sortDirection === 'desc' ? 1 : -1;
  }
  return 0;
}

// export function DataTableReducer(state: DataTableState, action: Action) {
//   return dataTableReducer(state, action);
// }

// Utils
export function setSortDirection(sortDirection: string): string {
  switch (sortDirection) {
    case 'asc':
      return 'desc';
    case 'desc':
      return '';
    case '':
      return 'asc';
    default:
      return '';
  }
}

export const selectFilters: MemoizedSelector<
  State,
  { [p: string]: FilterMetadata | FilterMetadata[] }
> = createSelector(joursCaisseStateSelector, (state: State) => state.filters);
