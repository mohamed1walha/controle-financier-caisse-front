import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";
import {BilletDetailSortieActions} from "../actions";
import {BilletDetailSortieDTO} from "../../../../swagger-api";

export const billetDetailSortieFeatureKey = 'BilletDetailSortie';


export interface State {
  BilletDetailSorties: BilletDetailSortieDTO[];
  BilletDetailSortie: BilletDetailSortieDTO;
  isLoading: boolean
}

const initialState: State = {
  BilletDetailSorties: [],
  BilletDetailSortie: {},
  isLoading: false
};
export const reducer = createReducer(
  initialState,
  on(BilletDetailSortieActions.fetchBilletDetailSorties, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(BilletDetailSortieActions.setBilletDetailSorties, (state: State, {BilletDetailSorties}) => {
    return {...state, BilletDetailSorties, isLoading: false};
  }),
);
export const billetDetailSortieStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('interfacage'),
  state => state[billetDetailSortieFeatureKey]);

export const selectBilletDetailSorties = createSelector(billetDetailSortieStateSelector, (state: State) => state.BilletDetailSorties);
export const selectIsLoading = createSelector(billetDetailSortieStateSelector, (state: State) => state.isLoading);


