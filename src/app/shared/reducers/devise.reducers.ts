import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";
import {DeviseActions} from "../actions";
import {DeviseDTO} from "../../../../swagger-api";

export const deviseFeatureKey = 'Devise';


export interface State {
  devises: DeviseDTO[];
  devise: DeviseDTO;
  isLoading: boolean
}

const initialState: State = {
  devises: [],
  devise: {},
  isLoading: false
};
export const reducer = createReducer(
  initialState,
  on(DeviseActions.fetchDevises, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(DeviseActions.setDevises, (state: State, {devises}) => {
    return {...state, devises, isLoading: false};
  }),
);
export const deviseStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('interfacage'),
  state => state[deviseFeatureKey]);

export const selectDevises = createSelector(deviseStateSelector, (state: State) => state.devises);
export const selectIsLoading = createSelector(deviseStateSelector, (state: State) => state.isLoading);


