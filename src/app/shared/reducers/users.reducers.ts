import {createFeatureSelector, createReducer, createSelector, MemoizedSelector, on} from "@ngrx/store";
import {UserDTO} from "../../../../swagger-api";
import {UsersActions} from "../actions";

export const userFeatureKey = 'Users';


export interface State {
  users: UserDTO[];
  usersSimpleData: UserDTO[];
  user: UserDTO;
  isLoading: boolean
}

const initialState: State = {
  users: [],
  usersSimpleData: [],
  user: {},
  isLoading: false
};
export const reducer = createReducer(
  initialState,
  on(UsersActions.fetchUsers, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(UsersActions.fetchUserByUserName, (state: State) => {
    return {...state, isLoading: true};
  }),
  on(UsersActions.setUsers, (state: State, {users}) => {
    return {...state, users, isLoading: false};
  }),
  on(UsersActions.setUsersSimpleData, (state: State, {usersSimpleData}) => {
    return {...state, usersSimpleData, isLoading: false};
  }),
  on(UsersActions.setUser, (state: State, {user}) => {
    return {...state, user, isLoading: false};
  }),
);
export const userStateSelector: MemoizedSelector<object, State> = createSelector(
  createFeatureSelector('users'),
  state => state[userFeatureKey]);

export const selectUsers = createSelector(userStateSelector, (state: State) => state.users);
export const selectUsersSimpleData = createSelector(userStateSelector, (state: State) => state.usersSimpleData);

export const selectUser = createSelector(userStateSelector, (state: State) => state.user);
export const selectIsLoading = createSelector(userStateSelector, (state: State) => state.isLoading);


