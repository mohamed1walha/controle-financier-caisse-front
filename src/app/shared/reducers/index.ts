import * as fromJoursCaisse from 'src/app/shared/reducers/joursCaisse.reducers';
import * as fromPiedeche from 'src/app/shared/reducers/piedeche.reducers';
import * as fromModePaiement from 'src/app/shared/reducers/modePaiement.reducers';
import * as fromEtablissement from 'src/app/shared/reducers/etablissement.reducers';
import * as fromLastSyncApp from 'src/app/shared/reducers/lastSyncApp.reducers';
import * as fromMotif from 'src/app/shared/reducers/motif.reducers';
import * as fromChauffeur from 'src/app/shared/reducers/chauffeur.reducers';
import * as fromMotifresync from 'src/app/shared/reducers/motifresync.reducers';
import * as fromEmetteur from 'src/app/shared/reducers/emetteur.reducers';
import * as fromStatus from 'src/app/shared/reducers/status.reducers';
import * as fromBlade from 'src/app/shared/reducers/blade.reducers';
import * as fromBanque from 'src/app/shared/reducers/banque.reducers';
import * as fromTypeFrais from 'src/app/shared/reducers/typefrais.reducers';
import * as fromCoffre from 'src/app/shared/reducers/coffre.reducers';
import * as fromCoffreAlimentation from 'src/app/shared/reducers/coffreAlimentations.reducers';
import * as fromReports from 'src/app/shared/reducers/report.reducers';
import * as fromDevise from 'src/app/shared/reducers/devise.reducers';
import * as fromBilletDetailSortie from 'src/app/shared/reducers/billetDetailSortie.reducers';
import * as fromBilletDetailComptage from 'src/app/shared/reducers/billetDetailComptage.reducers';
import * as fromBilletDetailAliExcep from 'src/app/shared/reducers/billetDetailAliExcep.reducers';
import * as fromParamBillet from 'src/app/shared/reducers/paramBillet.reducers';
import * as fromUsers from 'src/app/shared/reducers/users.reducers';
import * as fromRoles from 'src/app/shared/reducers/roles.reducers';
import * as fromArticle from 'src/app/shared/reducers/article.reducers';
import * as fromFournisseur from 'src/app/shared/reducers/fournisseur.reducers';
import * as fromCompteDepense from 'src/app/shared/reducers/compteDepense.reducers';
import * as fromMailDestinataire from 'src/app/shared/reducers/mailDestinataire.reducers';
import * as fromParcaisse from 'src/app/shared/reducers/parcaisse.reducers';
import * as fromEntetePrelevement from 'src/app/shared/reducers/entetePrelevement.reducers';
import * as fromLignePrelevement from 'src/app/shared/reducers/lignePrelevement.reducers';
import * as fromEnteteDepense from 'src/app/shared/reducers/enteteDepense.reducers';
import * as fromLigneDepense from 'src/app/shared/reducers/ligneDepense.reducers';
import * as fromReportBillet from 'src/app/shared/reducers/reportBillet.reducers';

export {
  fromJoursCaisse,
  fromPiedeche,
  fromModePaiement,
  fromEtablissement,
  fromLastSyncApp,
  fromMotif,
  fromChauffeur,
  fromEmetteur,
  fromStatus,
  fromBlade,
  fromBanque,
  fromCoffre,
  fromCoffreAlimentation,
  fromTypeFrais,
  fromMotifresync,
  fromReports,
  fromDevise,
  fromBilletDetailSortie,
  fromBilletDetailComptage,
  fromBilletDetailAliExcep,
  fromArticle,
  fromFournisseur,
  fromParamBillet,
  fromUsers,
  fromRoles,
  fromCompteDepense,
  fromMailDestinataire,
  fromEnteteDepense,
  fromEntetePrelevement,
  fromParcaisse,
  fromLignePrelevement,
  fromLigneDepense,
  fromReportBillet,
};
