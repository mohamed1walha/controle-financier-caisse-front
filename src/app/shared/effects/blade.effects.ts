import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {ActivatedRoute, Router} from '@angular/router';
import {tap} from 'rxjs/operators';
import {BladeActions} from "../actions";

@Injectable()
export class BladeEffects {
  closeBlade$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(
          BladeActions.closeBlade,
        ),
        tap(() => {
          const segments = this.router.url.split('(');
          if (segments.length === 2) {
            let [completeUrl, bladeSegment] = segments;
            if (bladeSegment?.includes('//')) {
              completeUrl += bladeSegment.split('//')[0];
              this.router.navigateByUrl(completeUrl);
              return;
            }
            this.router.navigate([completeUrl], {
              relativeTo: this.activatedRoute,
              queryParamsHandling: 'preserve'
            });
          }
        })
      ),
    {dispatch: false}
  );

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private actions$: Actions
  ) {
  }
}
