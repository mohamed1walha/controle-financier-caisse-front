import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { exhaustMap, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { EnteteDepenseActions } from '../actions';
import { EnteteDepenseService } from '../../../../swagger-api';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable()
export class EnteteDepenseEffects {
  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EnteteDepenseActions.fetchAllEnteteDepenses),
      switchMap(({}) => {
        return this.enteteDepenseService.enteteDepenseGetAllGet();
      }),
      map((enteteDepenses) => {
        return EnteteDepenseActions.setEnteteDepenses({ enteteDepenses });
      })
    )
  );
  fetchEnteteDepenses$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EnteteDepenseActions.fetchAllEnteteDepensesWithEtabAndUserId),
      switchMap(({ etablissementId, userId }) => {
        return this.enteteDepenseService.enteteDepenseGetAllByEtablissementIdAndUserIdEtablissementIdUserIdGet(
          etablissementId,
          userId
        );
      }),
      map((enteteDepenses) => {
        return EnteteDepenseActions.setEnteteDepenses({ enteteDepenses });
      })
    )
  );
  fetchAllEnteteDepensesWithEtabAndNumZRefAndcaisseId$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        EnteteDepenseActions.fetchAllEnteteDepensesWithEtabAndNumZRefAndcaisseId
      ),
      switchMap(({ etabId, caisseId, numZRef }) => {
        return this.enteteDepenseService.enteteDepenseGetAllByEtabIdCaisseIdNumZRefGet(
          etabId,
          caisseId?.toString(),
          numZRef
        );
      }),
      map((enteteDepenses) => {
        return EnteteDepenseActions.setEnteteDepenses({
          enteteDepenses,
        });
      })
    )
  );
  fetchEnteteDepensesWithValidatorId$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EnteteDepenseActions.fetchAllEnteteDepensesWithValidatorId),
      switchMap(({ userId }) => {
        return this.enteteDepenseService.enteteDepenseGetAllByValidatorIdUserIdGet(
          userId
        );
      }),
      map((enteteDepenses) => {
        return EnteteDepenseActions.setEnteteDepenses({
          enteteDepenses,
        });
      })
    )
  );
  fetchEnteteDepense$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EnteteDepenseActions.fetchEnteteDepense),
      switchMap(({ id }) => {
        return this.enteteDepenseService.enteteDepenseGetByIdGet(id);
      }),
      map((enteteDepense) => {
        return EnteteDepenseActions.setEnteteDepense({ enteteDepense });
      })
    )
  );
  downloadFile$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EnteteDepenseActions.deleteFile),
      switchMap(({ id }) => {
        return this.enteteDepenseService.enteteDepenseDeleteFileIdGet(id);
      }),
      map((enteteDepense) => {
        return EnteteDepenseActions.setEnteteDepense({ enteteDepense });
      })
    )
  );

  deletedModePaei$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EnteteDepenseActions.DeleteEnteteDepense),

      exhaustMap(({ enteteDepense }) =>
        this.enteteDepenseService.enteteDepenseDeleteDelete(enteteDepense.id)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'operation réussie', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [EnteteDepenseActions.fetchAllEnteteDepenses()];
      })
    )
  );
  editEnteteDepense$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EnteteDepenseActions.EditEnteteDepense),
      switchMap(({ enteteDepense }) =>
        this.enteteDepenseService.enteteDepenseUpdatePut(enteteDepense)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'operation réussie', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((enteteDepense) => {
        return [EnteteDepenseActions.setEnteteDepense({ enteteDepense })];
      })
    )
  );
  createEnteteDepense$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EnteteDepenseActions.CreateEnteteDepense),
      switchMap(({ enteteDepense }) =>
        this.enteteDepenseService.enteteDepenseCreatePost(enteteDepense)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.router.navigate(['dashboard/depense/edit', op?.id], {
            relativeTo: this.activatedRoute.parent,
          });
          this.snackBar.open('Success !', 'operation réussie', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((enteteDepense) => {
        return [EnteteDepenseActions.NoActions({ enteteDepense })];
      })
    )
  );
  UpdateEnteteDepense$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EnteteDepenseActions.UpdateEnteteDepense),
      switchMap(({ enteteDepense }) =>
        this.enteteDepenseService.enteteDepenseUpdatePut(enteteDepense)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'operation réussie', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((enteteDepense) => {
        return [EnteteDepenseActions.setEnteteDepense({ enteteDepense })];
      })
    )
  );

  constructor(
    private router: Router,
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private enteteDepenseService: EnteteDepenseService,
    private activatedRoute: ActivatedRoute
  ) {}
}
