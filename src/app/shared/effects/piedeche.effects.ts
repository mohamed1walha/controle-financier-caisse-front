import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { PiedecheActions, StatusActions } from '../actions';
import { deepCopy } from '../utils/deep-copy';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PiedecheDTO, PiedecheService } from '../../../../swagger-api';
import { NbGlobalPhysicalPosition, NbToastrService } from '@nebular/theme';
import { fromJoursCaisse } from '../reducers';
import { Store } from '@ngrx/store';

@Injectable()
export class PiedecheEffects {
  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PiedecheActions.fetchPiedecheByJoursCaisse),
      switchMap(({ id }) => {
        return id
          ? this.piedecheService.piedecheGetAllByJoursCaisseIdPost(id)
          : [];
      }),
      map((piedeches) => {
        return PiedecheActions.setPiedeches({ piedeches: deepCopy(piedeches) });
      })
    )
  );
  fetchDataID$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PiedecheActions.fetchPiedecheByJoursCaisseID),
      switchMap(() => this.store$.select(fromJoursCaisse.selectJoursCaisse)),
      switchMap((joursCaisse) => {
        return joursCaisse?.id
          ? this.piedecheService.piedecheGetAllByJoursCaisseIdPost(
              joursCaisse?.id!
            )
          : [];
      }),
      map((piedeches) => {
        return PiedecheActions.setPiedeches({ piedeches: deepCopy(piedeches) });
      })
    )
  );

  fetchById$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PiedecheActions.fetchPiedecheById),
      switchMap(({ id }) => {
        return this.piedecheService.piedecheGetByIdGet(id);
      }),
      map((piedeche) => {
        return PiedecheActions.setPiedeche({
          piedeche: deepCopy(piedeche) as PiedecheDTO,
        });
      })
    )
  );
  fetchAllByCoffreNull$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PiedecheActions.fetchAllByCoffreNull),
      switchMap(({}) => {
        return this.piedecheService.piedecheGetAllByCoffreNullGet();
      }),
      map((piedeches) => {
        return PiedecheActions.setPiedeches({ piedeches: deepCopy(piedeches) });
      })
    )
  );
  updatepiedeche$ = createEffect(() =>
    this.actions$.pipe(
      ofType(PiedecheActions.updatepiedeche),
      switchMap(({ piedeche }) => {
        return this.piedecheService.piedecheUpdatePost(piedeche);
      }),
      tap((op) => {
        if (op != null && op != undefined) {
          this.toastService.success(
            $localize`Opération effectuée`,
            $localize`Succès`,
            {
              duration: 3000,
              position: NbGlobalPhysicalPosition.BOTTOM_RIGHT,
            }
          );
        }
      }),
      mergeMap((piedeche) => [
        StatusActions.fetchStatus({ id: piedeche.jourcaisseId }),
        PiedecheActions.fetchPiedecheByJoursCaisse({
          id: piedeche.jourcaisseId,
        }),
        PiedecheActions.setPiedeche({
          piedeche: deepCopy(piedeche) as PiedecheDTO,
        }),
      ])
    )
  );
  // fetchDataDocCompt$ = createEffect(() =>
  //   this.actions$.pipe(
  //     ofType(PiedecheActions.fetchPiedecheByJoursCaisseDocComptageLigne),
  //     switchMap(({id}) => {
  //       return this.piedecheService.piedecheGetAllByJoursCaisseDocumentCpmtIdPost(id);
  //     }),
  //     map((documentComptageLignes) => {
  //       console.log(documentComptageLignes);
  //       return PiedecheActions.setDocComp({documentComptageLignes})
  //     })
  //   )
  // );

  constructor(
    private actions$: Actions,
    private snackBar: MatSnackBar,
    private piedecheService: PiedecheService,
    private toastService: NbToastrService,
    private store$: Store<fromJoursCaisse.State>
  ) {}
}
