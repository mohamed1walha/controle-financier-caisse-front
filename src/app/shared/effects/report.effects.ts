import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {exhaustMap, map, mergeMap, switchMap, tap} from "rxjs/operators";
import {BanqueActions, ReportActions} from "../actions";
import {BanqueService, JoursCaisseService} from "../../../../swagger-api";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable()
export class ReportEffects {

  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ReportActions.fetchReports),
      switchMap(({dateOuverture, dateOuvertureTo,etablissementId}) => {
        return this.reportService.chargerJoursCaisseReportPost(dateOuverture,dateOuvertureTo,etablissementId);
      }),
      map((reports) => {
        return ReportActions.setReports({ reports })
      })
    )
  );




  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private reportService: JoursCaisseService
  ) {
  }
}
