import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {exhaustMap, map, mergeMap, switchMap, tap} from "rxjs/operators";
import {EmetteurActions} from "../actions";
import {EmetteurService} from "../../../../swagger-api";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable()
export class EmetteurEffects {

  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EmetteurActions.fetchEmetteurs),
      switchMap(({}) => {
        return this.emetteurService.emetteurGetAllGet();
      }),
      map((emetteurs) => {
        return EmetteurActions.setEmetteurs({emetteurs})
      })
    )
  );


  deletedModePaei$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EmetteurActions.DeleteEmetteur),

      exhaustMap(({emetteur}) =>
        this.emetteurService.emetteurDeleteDelete(emetteur.id)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'Emetteur Deleted', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          EmetteurActions.fetchEmetteurs()
        ];
      }),
    )
  );
  editEmetteur$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EmetteurActions.EditEmetteur),
      switchMap(({emetteur}) =>
        this.emetteurService.emetteurUpdatePut(emetteur)
      ),
      tap((op) => {
        if (op != null && op != undefined) {

          this.snackBar.open('Success !', 'Emetteur Updated', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          EmetteurActions.fetchEmetteurs()
        ];
      })
    )
  );
  createEmetteur$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EmetteurActions.CreateEmetteur),
      switchMap(({emetteur}) =>
        this.emetteurService.emetteurCreatePost(emetteur)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'Emetteur Created', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          EmetteurActions.fetchEmetteurs()
        ];
      })
    )
  );


  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private emetteurService: EmetteurService
  ) {
  }
}
