import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {exhaustMap, map, mergeMap, switchMap, tap} from "rxjs/operators";
import {BilletDetailSortieActions} from "../actions";
import {BilletDetailSortieService} from "../../../../swagger-api";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable()
export class BilletDetailSortieEffects {

  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BilletDetailSortieActions.fetchBilletDetailSorties),
      switchMap(({}) => {
        return this.BilletDetailSortieService.billetDetailSortieGetAllGet();
      }),
      map((BilletDetailSorties) => {
        return BilletDetailSortieActions.setBilletDetailSorties({BilletDetailSorties})
      })
    )
  );


  deletedModePaei$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BilletDetailSortieActions.DeleteBilletDetailSortie),

      exhaustMap(({BilletDetailSortie}) =>
        this.BilletDetailSortieService.billetDetailSortieDeleteDelete(BilletDetailSortie.id)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'BilletDetailSortie Deleted', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          BilletDetailSortieActions.fetchBilletDetailSorties()
        ];
      }),
    )
  );
  editBilletDetailSortie$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BilletDetailSortieActions.EditBilletDetailSortie),
      switchMap(({BilletDetailSortie}) =>
        this.BilletDetailSortieService.billetDetailSortieUpdatePut(BilletDetailSortie)
      ),
      tap((op) => {
        if (op != null && op != undefined) {

          this.snackBar.open('Success !', 'BilletDetailSortie Updated', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          BilletDetailSortieActions.fetchBilletDetailSorties()
        ];
      })
    )
  );
  createBilletDetailSortie$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BilletDetailSortieActions.CreateBilletDetailSortie),
      switchMap(({BilletDetailSortie}) =>
        this.BilletDetailSortieService.billetDetailSortieCreatePost(BilletDetailSortie)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'BilletDetailSortie Created', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          BilletDetailSortieActions.fetchBilletDetailSorties()
        ];
      })
    )
  );


  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private BilletDetailSortieService: BilletDetailSortieService
  ) {
  }
}
