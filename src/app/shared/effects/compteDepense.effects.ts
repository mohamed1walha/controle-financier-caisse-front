import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { exhaustMap, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { CompteDepenseActions } from '../actions';
import { CompteDepenseService } from '../../../../swagger-api';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class CompteDepenseEffects {
  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CompteDepenseActions.fetchCompteDepenses),
      switchMap(({}) => {
        return this.compteDepenseService.compteDepenseGetAllGet();
      }),
      map((compteDepenses) => {
        return CompteDepenseActions.setCompteDepenses({ compteDepenses });
      })
    )
  );
  fetchCompteDepenseById$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CompteDepenseActions.fetchCompteDepenseById),
      switchMap(({ id }) => {
        return this.compteDepenseService.compteDepenseGetByIdGet(id);
      }),
      map((compteDepense) => {
        return CompteDepenseActions.setCompteDepense({ compteDepense });
      })
    )
  );

  deletedModePaei$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CompteDepenseActions.DeleteCompteDepense),

      exhaustMap(({ compteDepense }) =>
        this.compteDepenseService.compteDepenseDeleteDelete(compteDepense.id)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'compteDepense Deleted', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [CompteDepenseActions.fetchCompteDepenses()];
      })
    )
  );
  editCompteDepense$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CompteDepenseActions.EditCompteDepense),
      switchMap(({ compteDepense }) =>
        this.compteDepenseService.compteDepenseUpdatePut(compteDepense)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'compteDepense Updated', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [CompteDepenseActions.fetchCompteDepenses()];
      })
    )
  );
  createCompteDepense$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CompteDepenseActions.CreateCompteDepense),
      switchMap(({ compteDepense }) =>
        this.compteDepenseService.compteDepenseCreatePost(compteDepense)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'compteDepense Created', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [CompteDepenseActions.fetchCompteDepenses()];
      })
    )
  );

  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private compteDepenseService: CompteDepenseService
  ) {}
}
