import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { exhaustMap, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { LigneDepenseActions } from '../actions';
import { LigneDepenseService } from '../../../../swagger-api';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class LigneDepenseEffects {
  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LigneDepenseActions.fetchLigneDepenses),
      switchMap(({}) => {
        return this.ligneDepenseService.ligneDepenseGetAllGet();
      }),
      map((ligneDepenses) => {
        return LigneDepenseActions.setLigneDepenses({ ligneDepenses });
      })
    )
  );
  fetchLigneDepensesWithEnteteId$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LigneDepenseActions.fetchLigneDepensesWithEnteteId),
      switchMap(({ enteteId }) => {
        return this.ligneDepenseService.ligneDepenseGetByEnteteIdGet(enteteId);
      }),
      map((ligneDepenses) => {
        return LigneDepenseActions.setLigneDepenses({ ligneDepenses });
      })
    )
  );

  deletedModePaei$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LigneDepenseActions.DeleteLigneDepense),

      exhaustMap(({ ligneDepense }) =>
        this.ligneDepenseService.ligneDepenseDeleteDelete(ligneDepense.id)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'ligneDepense Deleted', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [LigneDepenseActions.fetchLigneDepenses()];
      })
    )
  );
  editLigneDepense$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LigneDepenseActions.EditLigneDepense),
      switchMap(({ ligneDepense }) =>
        this.ligneDepenseService.ligneDepenseUpdatePut(ligneDepense)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'LigneDepense Updated', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [LigneDepenseActions.fetchLigneDepenses()];
      })
    )
  );
  createLigneDepense$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LigneDepenseActions.CreateLigneDepense),
      switchMap(({ ligneDepense }) =>
        this.ligneDepenseService.ligneDepenseCreatePost(ligneDepense)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'LigneDepense Created', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [LigneDepenseActions.fetchLigneDepenses()];
      })
    )
  );

  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private ligneDepenseService: LigneDepenseService
  ) {}
}
