import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';
import { EtablissementActions } from '../actions';
import { EtablissementService } from '../../../../swagger-api';

@Injectable()
export class EtablissementEffects {
  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EtablissementActions.fetchEtablissements),
      switchMap(() => {
        return this.etablissementService.etablissementGetAllPost();
      }),
      map((etablissements) => {
        return EtablissementActions.setEtablissements({ etablissements });
      })
    )
  );
  fetchEtablissementById$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EtablissementActions.fetchEtablissementById),
      switchMap(({ etablissementId }) => {
        return this.etablissementService.etablissementGetByIdGet(
          etablissementId
        );
      }),
      map((etablissement) => {
        return EtablissementActions.setEtablissement({ etablissement });
      })
    )
  );

  constructor(
    private actions$: Actions,
    private etablissementService: EtablissementService
  ) {}
}
