import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { exhaustMap, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { ParcaisseActions } from '../actions';
import { ParcaisseService } from '../../../../swagger-api';
import { MatSnackBar } from '@angular/material/snack-bar';
import { fetchParcaissesByEtabId } from '../actions/parcaisse.actions';

@Injectable()
export class ParcaisseEffects {
  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ParcaisseActions.fetchParcaisses),
      switchMap(({}) => {
        return this.parcaisseService.parcaisseGetAllGet();
      }),
      map((parcaisses) => {
        return ParcaisseActions.setParcaisses({ parcaisses });
      })
    )
  );
  fetchParcaissesByEtabId$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ParcaisseActions.fetchParcaissesByEtabId),
      switchMap(({ etabId }) => {
        return this.parcaisseService.parcaisseGetAllByEtabIdEtabIdGet(etabId);
      }),
      map((parcaisses) => {
        return ParcaisseActions.setParcaisses({ parcaisses });
      })
    )
  );
  fetchParcaissesByEtabIds$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ParcaisseActions.fetchParcaissesByEtabIds),
      switchMap(({ etabIds }) => {
        console.log(etabIds);
        return this.parcaisseService.parcaisseGetAllByEtabIdsPost(etabIds);
      }),
      map((parcaisses) => {
        return ParcaisseActions.setParcaisses({ parcaisses });
      })
    )
  );

  deletedModePaei$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ParcaisseActions.DeleteParcaisse),

      exhaustMap(({ parcaisse }) =>
        this.parcaisseService.parcaisseDeleteDelete(parcaisse.id)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'parcaisse Deleted', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [ParcaisseActions.fetchParcaisses()];
      })
    )
  );
  editParcaisse$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ParcaisseActions.EditParcaisse),
      switchMap(({ parcaisse }) =>
        this.parcaisseService.parcaisseUpdatePut(parcaisse)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'Parcaisse Updated', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [ParcaisseActions.fetchParcaisses()];
      })
    )
  );
  createParcaisse$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ParcaisseActions.CreateParcaisse),
      switchMap(({ parcaisse }) =>
        this.parcaisseService.parcaisseCreatePost(parcaisse)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'Parcaisse Created', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [ParcaisseActions.fetchParcaisses()];
      })
    )
  );

  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private parcaisseService: ParcaisseService
  ) {}
}
