import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {exhaustMap, map, mergeMap, switchMap, tap} from "rxjs/operators";
import {ChauffeurActions} from "../actions";
import {ChauffeurService} from "../../../../swagger-api";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable()
export class ChauffeurEffects {

  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ChauffeurActions.fetchChauffeurs),
      switchMap(({}) => {
        return this.chauffeurService.chauffeurGetAllGet();
      }),
      map((chauffeurs) => {
        return ChauffeurActions.setChauffeurs({chauffeurs})
      })
    )
  );


  deletedModePaei$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ChauffeurActions.DeleteChauffeur),

      exhaustMap(({chauffeur}) =>
        this.chauffeurService.chauffeurDeleteDelete(chauffeur.id)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'chauffeur Deleted', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          ChauffeurActions.fetchChauffeurs()
        ];
      }),
    )
  );
  editChauffeur$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ChauffeurActions.EditChauffeur),
      switchMap(({chauffeur}) =>
        this.chauffeurService.chauffeurUpdatePut(chauffeur)
      ),
      tap((op) => {
        if (op != null && op != undefined) {

          this.snackBar.open('Success !', 'Chauffeur Updated', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          ChauffeurActions.fetchChauffeurs()
        ];
      })
    )
  );
  createChauffeur$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ChauffeurActions.CreateChauffeur),
      switchMap(({chauffeur}) =>
        this.chauffeurService.chauffeurCreatePost(chauffeur)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'Chauffeur Created', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          ChauffeurActions.fetchChauffeurs()
        ];
      })
    )
  );


  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private chauffeurService: ChauffeurService
  ) {
  }
}
