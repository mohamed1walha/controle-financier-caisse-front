import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {exhaustMap, map, mergeMap, switchMap, tap} from 'rxjs/operators';
import {EntetePrelevementActions} from '../actions';
import {MatSnackBar} from '@angular/material/snack-bar';
import {ActivatedRoute, Router} from '@angular/router';
import {EntetePrelevementDTO, EntetePrelevmentService, OperationResult,} from '../../../../swagger-api';
import {fromEntetePrelevement} from '../reducers';
import {Store} from '@ngrx/store';

@Injectable()
export class EntetePrelevementEffects {
/*  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EntetePrelevementActions.fetchAllEntetePrelevements),
      mergeMap(() =>
        this.store$.select(fromEntetePrelevement.selectFilterPrelevement)
      ),
      switchMap((filter) => {
        return this.entetePrelevementService.entetePrelevementGetAllWithFilterPost(
          filter
        );
      }),
      map((entetePrelevements) => {
        return EntetePrelevementActions.setEntetePrelevements({
          entetePrelevements,
        });
      })
    )
  );*/
  fetchEntetePrelevements$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        EntetePrelevementActions.fetchAllEntetePrelevementsWithEtabAndUserId
      ),
      switchMap(({ etablissementId, userId }) => {
        return this.entetePrelevementService.entetePrelevementGetAllByEtablissementIdAndUserIdEtablissementIdUserIdGet(
          etablissementId,
          userId
        );
      }),
      map((entetePrelevements) => {
        return EntetePrelevementActions.setEntetePrelevements({
          entetePrelevements,
        });
      })
    )
  );
  fetchAllEntetePrelevementsWithEtabAndNumZRefAndcaisseId$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        EntetePrelevementActions.fetchAllEntetePrelevementsWithEtabAndNumZRefAndcaisseId
      ),
      switchMap(({ etabId, caisseId, numZRef }) => {
        return this.entetePrelevementService.entetePrelevementGetAllByEtabIdCaisseIdNumZRefGet(
          etabId,
          caisseId?.toString(),
          numZRef?.toString()
        );
      }),
      map((entetePrelevements) => {
        return EntetePrelevementActions.setEntetePrelevements({
          entetePrelevements,
        });
      })
    )
  );
  fetchAllEntetePrelevementsWithEtabAndUserIdDemandeur$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        EntetePrelevementActions.fetchAllEntetePrelevementsWithEtabAndUserIdDemandeur
      ),
      switchMap(({ etablissementId, userId }) => {
        return this.entetePrelevementService.entetePrelevementGetAllByDemandeurUserIdGet(
          userId
        );
      }),
      map((entetePrelevements) => {
        return EntetePrelevementActions.setEntetePrelevements({
          entetePrelevements,
        });
      })
    )
  );

  fetchEntetePrelevementswithvalidator$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        EntetePrelevementActions.fetchAllEntetePrelevementsWithValidatorId
      ),
      switchMap(({ userId }) => {
        return this.entetePrelevementService.entetePrelevementGetAllByValidateurUserIdGet(
          userId
        );
      }),
      map((entetePrelevements) => {
        return EntetePrelevementActions.setEntetePrelevements({
          entetePrelevements,
        });
      })
    )
  );
  fetchEntetePrelevement$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EntetePrelevementActions.fetchEntetePrelevement),
      switchMap(({ id }) => {
        return this.entetePrelevementService.entetePrelevementGetByIdGet(id);
      }),
      map((entetePrelevement) => {
        return EntetePrelevementActions.setEntetePrelevement({
          entetePrelevement,
        });
      })
    )
  );
  downloadFile$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EntetePrelevementActions.deleteFile),
      switchMap(({ id }) => {
        return this.entetePrelevementService.entetePrelevementDeleteFileIdGet(
          id
        );
      }),
      map((entetePrelevement) => {
        return EntetePrelevementActions.setEntetePrelevement({
          entetePrelevement,
        });
      })
    )
  );

  deletedModePaei$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EntetePrelevementActions.DeleteEntetePrelevement),

      exhaustMap(({ entetePrelevement }) =>
        this.entetePrelevementService.entetePrelevementDeleteDelete(
          entetePrelevement.id
        )
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'operation réussie', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [EntetePrelevementActions.fetchAllEntetePrelevements()];
      })
    )
  );
  editEntetePrelevement$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EntetePrelevementActions.EditEntetePrelevement),
      switchMap(({ entetePrelevement }) =>
        this.entetePrelevementService.entetePrelevementUpdatePut(
          entetePrelevement
        )
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'operation réussie', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [EntetePrelevementActions.fetchAllEntetePrelevements()];
      })
    )
  );
  createEntetePrelevement$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EntetePrelevementActions.CreateEntetePrelevement),
      switchMap(({ entetePrelevement }) =>
        this.entetePrelevementService.entetePrelevementCreatePost(
          entetePrelevement
        )
      ),
      tap((op: OperationResult) => {
        if (op.errorOccured == false) {
          const data = op?.data as any[];
          console.log('dattaa   ' + data);
          this.router.navigate(['dashboard/prelevement/edit', data], {
            relativeTo: this.activatedRoute.parent,
          });
          this.snackBar.open('Success !', 'operation réussie', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        } else {
          this.snackBar.open('Failed !', 'échec de creation', {
            duration: 5000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((op) => {
        return [
          EntetePrelevementActions.NoActions({
            entetePrelevement: op?.data as EntetePrelevementDTO,
          }),
        ];
      })
    )
  );
  UpdateEntetePrelevement$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EntetePrelevementActions.UpdateEntetePrelevement),
      switchMap(({ entetePrelevement }) =>
        this.entetePrelevementService.entetePrelevementUpdatePut(
          entetePrelevement
        )
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'operation réussie', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((entetePrelevement) => {
        return [
          EntetePrelevementActions.setEntetePrelevement({ entetePrelevement }),
        ];
      })
    )
  );

  generateComptable$ = createEffect(() =>
    this.actions$.pipe(
      ofType(EntetePrelevementActions.generateComptable),

      switchMap(({ Prelevements }) =>
        this.entetePrelevementService.entetePrelevementGenerateComptablePost(
          Prelevements
        )
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'operation réussie', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult:OperationResult) => {
        return [EntetePrelevementActions.fetchAllEntetePrelevements(),
          EntetePrelevementActions.setAnnexesFile({ files : operationResult?.data }),
        ];
      })
    )
  );


  constructor(
    private router: Router,
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private store$: Store<fromEntetePrelevement.State>,
    private entetePrelevementService: EntetePrelevmentService,
    private activatedRoute: ActivatedRoute
  ) {}
}
