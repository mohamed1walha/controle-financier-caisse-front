import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';
import { ReportBilletActions } from '../actions';
import { MatSnackBar } from '@angular/material/snack-bar';
import { JoursCaisseService } from '../../../../swagger-api';

@Injectable()
export class ReportBilletEffects {
  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ReportBilletActions.fetchReportBillets),
      switchMap(({ reportBillets }) => {
        return this.reportBilletService.chargerJoursCaisseReportDetailsBilletPost(
          reportBillets
        );
      }),
      map((reportBillets) => {
        return ReportBilletActions.setReportBillets({ reportBillets });
      })
    )
  );

  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private reportBilletService: JoursCaisseService
  ) {}
}
