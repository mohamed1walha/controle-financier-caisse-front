import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { exhaustMap, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { ArticleActions } from '../actions';
import { ArticleService } from '../../../../swagger-api';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class ArticleEffects {
  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ArticleActions.fetchArticles),
      switchMap(({}) => {
        return this.articleService.articleGetAllGet();
      }),
      map((articles) => {
        return ArticleActions.setArticles({ articles });
      })
    )
  );
  fetchArticlesWithControlSolde$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ArticleActions.fetchArticlesWithControlSolde),
      switchMap(({}) => {
        return this.articleService.articleGetAllWithControlSoldeGet();
      }),
      map((articles) => {
        return ArticleActions.setArticles({ articles });
      })
    )
  );

  deletedModePaei$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ArticleActions.DeleteArticle),

      exhaustMap(({ article }) =>
        this.articleService.articleDeleteDelete(article.id)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'article Deleted', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [ArticleActions.fetchArticles()];
      })
    )
  );
  editArticle$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ArticleActions.EditArticle),
      switchMap(({ article }) => this.articleService.articleUpdatePut(article)),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'Article Updated', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [ArticleActions.fetchArticles()];
      })
    )
  );
  createArticle$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ArticleActions.CreateArticle),
      switchMap(({ article }) =>
        this.articleService.articleCreatePost(article)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'Article Created', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [ArticleActions.fetchArticles()];
      })
    )
  );

  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private articleService: ArticleService
  ) {}
}
