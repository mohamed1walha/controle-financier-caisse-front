import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {map, mergeMap, switchMap} from "rxjs/operators";
import {JoursCaisseActions, PiedecheActions, StatusActions} from "../actions";
import {StatusService} from "../../../../swagger-api";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Store} from "@ngrx/store";
import {fromJoursCaisse} from "../reducers";

@Injectable()
export class StatusEffects {

//   fetchData$ = createEffect(() =>
//     this.actions$.pipe(
//       ofType(StatusActions.fetchStatuss),
//       switchMap(() =>
//         this.statusService.statusGetAllGet()
//       )
//   map({statuss}) => {
//   StatusActions.setStatuss({statuss})}
//
// );
  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(StatusActions.fetchStatuss),
      switchMap(({}) => {
        return this.statusService.statusGetAllGet();
      }),
      map((statuss) => StatusActions.setStatuss({statuss}))
    ));
  fetchStatusJourCaisse$ = createEffect(() =>
    this.actions$.pipe(
      ofType(StatusActions.fetchStatus),
      switchMap(({id}) => {
        return this.statusService.statusCheckStatusPost(id);
      }),
      mergeMap((status) => [JoursCaisseActions.fetchById(),
        PiedecheActions.fetchPiedecheByJoursCaisseID(),
        StatusActions.setStatus({status})])
    ));

  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private statusService: StatusService,
    private store$: Store<fromJoursCaisse.State>
  ) {
  }
}
