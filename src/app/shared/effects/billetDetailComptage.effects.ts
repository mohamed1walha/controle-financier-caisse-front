import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {exhaustMap, map, mergeMap, switchMap, tap} from "rxjs/operators";
import {BilletDetailComptageActions} from "../actions";
import {BilletDetailComptageService} from "../../../../swagger-api";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable()
export class BilletDetailComptageEffects {

  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BilletDetailComptageActions.fetchBilletDetailComptages),
      switchMap(({}) => {
        return this.BilletDetailComptageService.billetDetailComptageGetAllGet();
      }),
      map((BilletDetailComptages) => {
        return BilletDetailComptageActions.setBilletDetailComptages({BilletDetailComptages})
      })
    )
  );


  deletedModePaei$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BilletDetailComptageActions.DeleteBilletDetailComptage),

      exhaustMap(({BilletDetailComptage}) =>
        this.BilletDetailComptageService.billetDetailComptageDeleteDelete(BilletDetailComptage.id)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'BilletDetailComptage Deleted', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          BilletDetailComptageActions.fetchBilletDetailComptages()
        ];
      }),
    )
  );
  editBilletDetailComptage$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BilletDetailComptageActions.EditBilletDetailComptage),
      switchMap(({BilletDetailComptage}) =>
        this.BilletDetailComptageService.billetDetailComptageUpdatePut(BilletDetailComptage)
      ),
      tap((op) => {
        if (op != null && op != undefined) {

          this.snackBar.open('Success !', 'BilletDetailComptage Updated', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          BilletDetailComptageActions.fetchBilletDetailComptages()
        ];
      })
    )
  );
  createBilletDetailComptage$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BilletDetailComptageActions.CreateBilletDetailComptage),
      switchMap(({BilletDetailComptage}) =>
        this.BilletDetailComptageService.billetDetailComptageCreatePost(BilletDetailComptage)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'BilletDetailComptage Created', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          BilletDetailComptageActions.fetchBilletDetailComptages()
        ];
      })
    )
  );


  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private BilletDetailComptageService: BilletDetailComptageService
  ) {
  }
}
