import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {exhaustMap, map, mergeMap, switchMap, tap} from "rxjs/operators";
import {BanqueActions} from "../actions";
import {BanqueService} from "../../../../swagger-api";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable()
export class BanqueEffects {

  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BanqueActions.fetchBanques),
      switchMap(({}) => {
        return this.banqueService.banqueGetAllGet();
      }),
      map((banques) => {
        return BanqueActions.setBanques({banques})
      })
    )
  );


  deletedModePaei$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BanqueActions.DeleteBanque),

      exhaustMap(({banque}) =>
        this.banqueService.banqueDeleteDelete(banque.id)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'banque Deleted', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          BanqueActions.fetchBanques()
        ];
      }),
    )
  );
  editBanque$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BanqueActions.EditBanque),
      switchMap(({banque}) =>
        this.banqueService.banqueUpdatePut(banque)
      ),
      tap((op) => {
        if (op != null && op != undefined) {

          this.snackBar.open('Success !', 'Banque Updated', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          BanqueActions.fetchBanques()
        ];
      })
    )
  );
  createBanque$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BanqueActions.CreateBanque),
      switchMap(({banque}) =>
        this.banqueService.banqueCreatePost(banque)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'Banque Created', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          BanqueActions.fetchBanques()
        ];
      })
    )
  );


  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private banqueService: BanqueService
  ) {
  }
}
