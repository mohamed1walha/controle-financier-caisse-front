import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {exhaustMap, map, mergeMap, switchMap, tap} from "rxjs/operators";
import {DeviseActions} from "../actions";
import {DeviseService} from "../../../../swagger-api";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable()
export class DeviseEffects {

  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(DeviseActions.fetchDevises),
      switchMap(({}) => {
        return this.deviseService.deviseGetAllGet();
      }),
      map((devises) => {
        return DeviseActions.setDevises({devises})
      })
    )
  );

  fetchDataByCoffreId$ = createEffect(() =>
    this.actions$.pipe(
      ofType(DeviseActions.fetchDevisesByCoffreId),
      switchMap(({coffreId}) => {
        return this.deviseService.deviseGetAllByCoffreGet(coffreId);
      }),
      map((devises) => {
        return DeviseActions.setDevises({devises})
      })
    )
  );


  deletedModePaei$ = createEffect(() =>
    this.actions$.pipe(
      ofType(DeviseActions.DeleteDevise),

      exhaustMap(({devise}) =>
        this.deviseService.deviseDeleteDelete(devise.id)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'devise Deleted', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          DeviseActions.fetchDevises()
        ];
      }),
    )
  );
  editDevise$ = createEffect(() =>
    this.actions$.pipe(
      ofType(DeviseActions.EditDevise),
      switchMap(({devise}) =>
        this.deviseService.deviseUpdatePut(devise)
      ),
      tap((op) => {
        if (op != null && op != undefined) {

          this.snackBar.open('Success !', 'Devise Updated', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          DeviseActions.fetchDevises()
        ];
      })
    )
  );
  createDevise$ = createEffect(() =>
    this.actions$.pipe(
      ofType(DeviseActions.CreateDevise),
      switchMap(({devise}) =>
        this.deviseService.deviseCreatePost(devise)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'Devise Created', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          DeviseActions.fetchDevises()
        ];
      })
    )
  );


  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private deviseService: DeviseService
  ) {
  }
}
