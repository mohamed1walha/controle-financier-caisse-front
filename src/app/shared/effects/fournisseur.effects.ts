import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {exhaustMap, map, mergeMap, switchMap, tap} from "rxjs/operators";
import { FournisseurActions} from "../actions";
import {FournisseurService} from "../../../../swagger-api";
import {MatSnackBar} from "@angular/material/snack-bar";


@Injectable()
export class FournisseurEffects {

  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(FournisseurActions.fetchFournisseurs),
      switchMap(({}) => {
        return this.fournisseurService.fournisseurGetAllGet();
      }),
      map((fournisseurs) => {
        return FournisseurActions.setFournisseurs({fournisseurs})
      })
    )
  );
  fetchIsDefaultFournisseur$ = createEffect(() =>
    this.actions$.pipe(
      ofType(FournisseurActions.fetchIsDefaultFournisseur),
      switchMap(({}) => {
        return this.fournisseurService.fournisseurGetIsDefaultGet();
      }),
      map((fournisseur) => {
        return FournisseurActions.setFournisseur({fournisseur})
      })
    )
  );


  deletedModePaei$ = createEffect(() =>
    this.actions$.pipe(
      ofType(FournisseurActions.DeleteFournisseur),

      exhaustMap(({fournisseur}) =>
        this.fournisseurService.fournisseurDeleteDelete(fournisseur.id)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'fournisseur Deleted', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          FournisseurActions.fetchFournisseurs()
        ];
      }),
    )
  );
  editFournisseur$ = createEffect(() =>
    this.actions$.pipe(
      ofType(FournisseurActions.EditFournisseur),
      switchMap(({fournisseur}) =>
        this.fournisseurService.fournisseurUpdatePut(fournisseur)
      ),
      tap((op) => {
        if (op != null && op != undefined) {

          this.snackBar.open('Success !', 'fournisseur Updated', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          FournisseurActions.fetchFournisseurs()
        ];
      })
    )
  );
  createFournisseur$ = createEffect(() =>
    this.actions$.pipe(
      ofType(FournisseurActions.CreateFournisseur),
      switchMap(({fournisseur}) =>
        this.fournisseurService.fournisseurCreatePost(fournisseur)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'fournisseur Created', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          FournisseurActions.fetchFournisseurs()
        ];
      })
    )
  );


  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private fournisseurService: FournisseurService
  ) {
  }
}
