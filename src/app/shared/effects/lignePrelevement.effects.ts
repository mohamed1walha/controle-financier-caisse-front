import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { exhaustMap, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { LignePrelevementActions } from '../actions';
import { LignePrelevementService } from '../../../../swagger-api';
import { MatSnackBar } from '@angular/material/snack-bar';

@Injectable()
export class LignePrelevementEffects {
  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LignePrelevementActions.fetchLignePrelevements),
      switchMap(({}) => {
        return this.lignePrelevementService.lignePrelevementGetAllGet();
      }),
      map((lignePrelevements) => {
        return LignePrelevementActions.setLignePrelevements({
          lignePrelevements,
        });
      })
    )
  );
  fetchLignePrelevementsWithEnteteId$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LignePrelevementActions.fetchLignePrelevementsWithEnteteId),
      switchMap(({ enteteId }) => {
        return this.lignePrelevementService.lignePrelevementGetByEnteteIdGet(
          enteteId
        );
      }),
      map((lignePrelevements) => {
        return LignePrelevementActions.setLignePrelevements({
          lignePrelevements,
        });
      })
    )
  );

  deletedModePaei$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LignePrelevementActions.DeleteLignePrelevement),

      exhaustMap(({ lignePrelevement }) =>
        this.lignePrelevementService.lignePrelevementDeleteDelete(
          lignePrelevement.id
        )
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'lignePrelevement Deleted', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [LignePrelevementActions.fetchLignePrelevements()];
      })
    )
  );
  editLignePrelevement$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LignePrelevementActions.EditLignePrelevement),
      switchMap(({ lignePrelevement }) =>
        this.lignePrelevementService.lignePrelevementUpdatePut(lignePrelevement)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'LignePrelevement Updated', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [LignePrelevementActions.fetchLignePrelevements()];
      })
    )
  );
  createLignePrelevement$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LignePrelevementActions.CreateLignePrelevement),
      switchMap(({ lignePrelevement }) =>
        this.lignePrelevementService.lignePrelevementCreatePost(
          lignePrelevement
        )
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'LignePrelevement Created', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [LignePrelevementActions.fetchLignePrelevements()];
      })
    )
  );

  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private lignePrelevementService: LignePrelevementService
  ) {}
}
