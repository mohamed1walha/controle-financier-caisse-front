import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {exhaustMap, map, mergeMap, switchMap, tap} from "rxjs/operators";
import {ParamBilletActions} from "../actions";
import {ParamBilletService} from "../../../../swagger-api";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable()
export class ParamBilletEffects {

  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ParamBilletActions.fetchParamBillets),
      switchMap(({}) => {
        return this.ParamBilletService.paramBilletGetAllGet();
      }),
      map((ParamBillets) => {
        return ParamBilletActions.setParamBillets({ParamBillets})
      })
    )
  );

  fetchTypeBillet$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ParamBilletActions.fetchTypeBillets),
      switchMap(({}) => {
        return this.ParamBilletService.paramBilletGetAllTypeBilletGet();
      }),
      map((TypeBillets) => {
        return ParamBilletActions.setTypeBillets({TypeBillets})
      })
    )
  );


  deletedModePaei$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ParamBilletActions.DeleteParamBillet),

      exhaustMap(({ParamBillet}) =>
        this.ParamBilletService.paramBilletDeleteDelete(ParamBillet.id)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'ParamBillet Deleted', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          ParamBilletActions.fetchParamBillets()
        ];
      }),
    )
  );
  editParamBillet$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ParamBilletActions.EditParamBillet),
      switchMap(({ParamBillet}) =>
        this.ParamBilletService.paramBilletUpdatePut(ParamBillet)
      ),
      tap((op) => {
        if (op != null && op != undefined) {

          this.snackBar.open('Success !', 'ParamBillet Updated', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          ParamBilletActions.fetchParamBillets()
        ];
      })
    )
  );
  createParamBillet$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ParamBilletActions.CreateParamBillet),
      switchMap(({ParamBillet}) =>
        this.ParamBilletService.paramBilletCreatePost(ParamBillet)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'ParamBillet Created', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          ParamBilletActions.fetchParamBillets()
        ];
      })
    )
  );


  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private ParamBilletService: ParamBilletService
  ) {
  }
}
