import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {exhaustMap, map, mergeMap, switchMap, tap} from "rxjs/operators";
import {CoffreActions} from "../actions";
import {
  BilletDetailSortieService,
  CoffreAlimentationService,
  CoffreService,
  CoffresortieDTO,
  CoffreSortieService
} from "../../../../swagger-api";
import {MatSnackBar} from "@angular/material/snack-bar";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";

@Injectable()
export class CoffreEffects {

  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CoffreActions.fetchCoffres),
      switchMap(({}) => {
        return this.coffreService.coffreGetAllGet();
      }),
      map((coffres) => {
        return CoffreActions.setCoffres({coffres})
      })
    )
  );
  fetchDataCoffre$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CoffreActions.fetchCoffresFromModePaie),
      switchMap(({modePaie}) => {
        //console.log(modePaie);
        return this.coffreService.coffreGetAllByModePaiePost(modePaie);
      }),
      map((coffres) => {
        return CoffreActions.setCoffres({coffres})
      })
    )
  );

  // fetchSorties$ = createEffect(() =>
  //   this.actions$.pipe(
  //     ofType(CoffreActions.fetchSorties),
  //     switchMap(({coffreId}) => {
  //       return this.coffreSortieService.coffreSortieGetByCoffreIDCoffreIdGet(coffreId);
  //     }),
  //     map((sorties) => {
  //       return CoffreActions.setSorties({sorties})
  //     })
  //   )
  // );

  deletedSortie$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CoffreActions.DeleteSortie),

      exhaustMap(({sortieId}) =>
        this.coffreSortieService.coffreSortieDeleteDelete(sortieId)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.toastService.success($localize`Opération effectuée`, $localize`Succès`, {
            duration: 3000, position: NbGlobalPhysicalPosition.BOTTOM_RIGHT
          });
        }
      }),
      mergeMap((id) => {
        return [
          CoffreActions.fetchSorties({coffreId: id}),
          CoffreActions.fetchCoffres()
        ];
      })
    )
  );
  deletedModePaei$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CoffreActions.DeleteCoffre),

      exhaustMap(({coffre}) =>
        this.coffreService.coffreDeleteDelete(coffre.id)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'coffre Deleted', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          CoffreActions.fetchCoffres()
        ];
      }),
    )
  );
  editCoffre$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CoffreActions.EditCoffre),
      switchMap(({coffre}) =>
        this.coffreService.coffreUpdatePut(coffre)
      ),
      tap((op) => {
        if (op != null && op != undefined) {

          this.snackBar.open('Success !', 'Coffre Updated', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          CoffreActions.fetchCoffres()
        ];
      })
    )
  );
  createCoffre$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CoffreActions.CreateCoffre),
      switchMap(({coffre}) =>
        this.coffreService.coffreCreatePost(coffre)
      ),
      tap((op) => {
        if (op.errorOccured == true) {
          this.toastService.danger(op.message, $localize`Erreur`, {

            duration: 3000, position: NbGlobalPhysicalPosition.BOTTOM_RIGHT
          });
        } else {
          this.toastService.success($localize`Opération effectuée`, $localize`Succès`, {

            duration: 3000, position: NbGlobalPhysicalPosition.BOTTOM_RIGHT
          });

        }
      }),
      mergeMap((operationResult) => {
        return [
          CoffreActions.fetchCoffres()
        ];
      })
    )
  );

  fetchAllSorties$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CoffreActions.fetchAllSorties),
      switchMap(({}) => {
        return this.coffreSortieService.coffreSortieGetAllGet();
      }),
      map((coffresSorties) => {
        return CoffreActions.setAllSorties({allSorties:coffresSorties})
      })
    )
  );

  fetchAllSortiesByCoffreId$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CoffreActions.fetchSorties),
      switchMap(({coffreId}) => {
        return this.coffreSortieService.coffreSortieGetByCoffreIDCoffreIdGet(coffreId);
      }),
      map((coffresSorties) => {
        return CoffreActions.setSorties({sorties:coffresSorties})
      })
    )
  );

  fetchAllAlientationsByCoffreId$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CoffreActions.fetchAlimentations),
      switchMap(({coffreId}) => {
        return this.coffreAlimentationService.coffreAlimentationGetByCoffreIDCoffreIdGet(coffreId);
      }),
      map((coffresAlimentations) => {
        return CoffreActions.setAlimentations({alimentations:coffresAlimentations})
      })
    )
  );

  createSortie$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CoffreActions.CreateSortie),
      switchMap(({sortie}) =>
        this.coffreSortieService.coffreSortieCreatePost(sortie)
      ),
      tap((op) => {
        if (op.errorOccured == true) {
          this.toastService.danger(op.message, $localize`Erreur`, {

            duration: 3000, position: NbGlobalPhysicalPosition.BOTTOM_RIGHT
          });
        } else {
          this.toastService.success($localize`Opération effectuée`, $localize`Succès`, {

            duration: 3000, position: NbGlobalPhysicalPosition.BOTTOM_RIGHT
          });

        }
      }),
      mergeMap((operationResult) => {
        return [
          CoffreActions.setCoffreSortie({sortie: operationResult.data as CoffresortieDTO}),
          CoffreActions.fetchAllSorties()
        ];
      })
    )
  );

  fetchGetAllBySortieId$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CoffreActions.fetchCalculetteBySortieId),
      switchMap(({sortieId}) => {
        return this.billetDetailSortieService.billetDetailSortieGetAllBySortieIdGet(sortieId);
      }),
      map((allCalculetteBySortieId) => {
        return CoffreActions.setCalculetteBySortieId({allCalculetteBySortieId})
      })
    )
  );


  constructor(
    private toastService: NbToastrService,
    private snackBar: MatSnackBar,
    private actions$: Actions,
    private coffreService: CoffreService,
    private coffreSortieService: CoffreSortieService,
    private billetDetailSortieService: BilletDetailSortieService,
    private coffreAlimentationService: CoffreAlimentationService,
  ) {
  }
}
