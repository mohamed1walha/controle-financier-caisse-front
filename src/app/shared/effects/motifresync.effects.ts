import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {exhaustMap, map, mergeMap, switchMap, tap} from "rxjs/operators";
import {MotifresyncActions} from "../actions";
import {MotifresyncService} from "../../../../swagger-api";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable()
export class MotifresyncEffects {

  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MotifresyncActions.fetchMotifresyncs),
      switchMap(({}) => {
        return this.MotifresyncService.motifresyncGetAllGet();
      }),
      map((Motifresyncs) => {
        return MotifresyncActions.setMotifresyncs({Motifresyncs})
      })
    )
  );


  deletedModePaei$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MotifresyncActions.DeleteMotifresync),

      exhaustMap(({Motifresync}) =>
        this.MotifresyncService.motifresyncDeleteDelete(Motifresync.id)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'Motifresync Deleted', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          MotifresyncActions.fetchMotifresyncs()
        ];
      }),
    )
  );
  editMotifresync$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MotifresyncActions.EditMotifresync),
      switchMap(({Motifresync}) =>
        this.MotifresyncService.motifresyncUpdatePut(Motifresync)
      ),
      tap((op) => {
        if (op != null && op != undefined) {

          this.snackBar.open('Success !', 'Motifresync Updated', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          MotifresyncActions.fetchMotifresyncs()
        ];
      })
    )
  );
  createMotifresync$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MotifresyncActions.CreateMotifresync),
      switchMap(({Motifresync}) =>
        this.MotifresyncService.motifresyncCreatePost(Motifresync)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'Motifresync Created', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          MotifresyncActions.fetchMotifresyncs()
        ];
      })
    )
  );


  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private MotifresyncService: MotifresyncService
  ) {
  }
}
