import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {exhaustMap, map, mergeMap, switchMap, tap} from "rxjs/operators";
import {BilletDetailAliExcepActions} from "../actions";
import {BilletDetailAliExcepService} from "../../../../swagger-api";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable()
export class BilletDetailAliExcepEffects {

  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BilletDetailAliExcepActions.fetchBilletDetailAliExceps),
      switchMap(({}) => {
        return this.BilletDetailAliExcepService.billetDetailAliExcepGetAllGet();
      }),
      map((BilletDetailAliExceps) => {
        return BilletDetailAliExcepActions.setBilletDetailAliExceps({BilletDetailAliExceps})
      })
    )
  );


  deletedModePaei$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BilletDetailAliExcepActions.DeleteBilletDetailAliExcep),

      exhaustMap(({BilletDetailAliExcep}) =>
        this.BilletDetailAliExcepService.billetDetailAliExcepDeleteDelete(BilletDetailAliExcep.id)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'BilletDetailAliExcep Deleted', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          BilletDetailAliExcepActions.fetchBilletDetailAliExceps()
        ];
      }),
    )
  );
  editBilletDetailAliExcep$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BilletDetailAliExcepActions.EditBilletDetailAliExcep),
      switchMap(({BilletDetailAliExcep}) =>
        this.BilletDetailAliExcepService.billetDetailAliExcepUpdatePut(BilletDetailAliExcep)
      ),
      tap((op) => {
        if (op != null && op != undefined) {

          this.snackBar.open('Success !', 'BilletDetailAliExcep Updated', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          BilletDetailAliExcepActions.fetchBilletDetailAliExceps()
        ];
      })
    )
  );
  createBilletDetailAliExcep$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BilletDetailAliExcepActions.CreateBilletDetailAliExcep),
      switchMap(({BilletDetailAliExcep}) =>
        this.BilletDetailAliExcepService.billetDetailAliExcepCreatePost(BilletDetailAliExcep)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'BilletDetailAliExcep Created', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          BilletDetailAliExcepActions.fetchBilletDetailAliExceps()
        ];
      })
    )
  );


  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private BilletDetailAliExcepService: BilletDetailAliExcepService
  ) {
  }
}
