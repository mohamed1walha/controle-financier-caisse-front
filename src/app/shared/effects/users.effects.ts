import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {exhaustMap, map, mergeMap, switchMap, tap} from "rxjs/operators";
import {UsersActions} from "../actions";
import {UserService} from "../../../../swagger-api";
import {MatSnackBar} from '@angular/material/snack-bar';

@Injectable()
export class UsersEffects {
  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UsersActions.fetchUsers),
      switchMap(({}) => {
        return this.userService.userGetAllGet();
      }),
      map((users) => {
        return UsersActions.setUsers({users});
      })
    )
  );

  fetchDataSimpleData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UsersActions.fetchUsersSimpleData),
      switchMap(({}) => {
        return this.userService.userGetAllSimpleDataGet();
      }),
      map((usersSimpleData) => {
        return UsersActions.setUsersSimpleData({usersSimpleData});
      })
    )
  );

  fetchUsersByEtablissmentId$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UsersActions.fetchUsersByEtablissmentId),
      switchMap(({etabId}) => {
        return this.userService.userGetAllByEtabIdEtabIdGet(etabId);
      }),
      map((users) => {
        return UsersActions.setUsers({users});
      })
    )
  );
  fetchUsersValidators$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UsersActions.fetchUsersValidators),
      switchMap(({}) => {
        return this.userService.userGetAllValidateurGet();
      }),
      map((users) => {
        return UsersActions.setUsers({users});
      })
    )
  );
  fetchUserByUserName$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UsersActions.fetchUserByUserName),
      switchMap(({userName}) => {
        return this.userService.userGetByUserNameGet(userName);
      }),
      map((user) => {
        return UsersActions.setUser({user});
      })
    )
  );

  deleted$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UsersActions.DeleteUser),

      exhaustMap(({user}) => this.userService.userDeleteDelete(user.id)),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'user Deleted', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [UsersActions.fetchUsers()];
      })
    )
  );
  editUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UsersActions.EditUser),
      switchMap(({user}) => this.userService.userUpdatePut(user)),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'User Updated', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [UsersActions.fetchUsers()];
      })
    )
  );
  createUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UsersActions.CreateUser),
      switchMap(({user}) => this.userService.userCreatePost(user)),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'User Created', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [UsersActions.fetchUsers()];
      })
    )
  );

  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private userService: UserService
  ) {
  }
}
