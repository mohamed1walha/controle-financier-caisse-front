import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {exhaustMap, map, mergeMap, switchMap, tap} from "rxjs/operators";
import {RolesActions} from "../actions";
import {RolesService} from "../../../../swagger-api";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable()
export class RolesEffects {

  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RolesActions.fetchRoles),
      switchMap(({}) => {
        return this.roleService.rolesGetAllGet();
      }),
      map((roles) => {
        return RolesActions.setRoles({roles})
      })
    )
  );


  deleted$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RolesActions.DeleteRole),

      exhaustMap(({role}) =>
        this.roleService.rolesDeleteDelete(role.id)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'role Deleted', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          RolesActions.fetchRoles()
        ];
      }),
    )
  );
  editRole$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RolesActions.EditRole),
      switchMap(({role}) =>
        this.roleService.rolesUpdatePut(role)
      ),
      tap((op) => {
        if (op != null && op != undefined) {

          this.snackBar.open('Success !', 'Role Updated', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          RolesActions.fetchRoles()
        ];
      })
    )
  );
  createRole$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RolesActions.CreateRole),
      switchMap(({role}) =>
        this.roleService.rolesCreatePost(role)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'Role Created', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          RolesActions.fetchRoles()
        ];
      })
    )
  );


  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private roleService: RolesService
  ) {
  }
}
