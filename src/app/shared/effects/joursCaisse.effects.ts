import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {map, switchMap, take} from "rxjs/operators";
import {JoursCaisseActions} from "../actions";
import {JoursCaiseDTO, JoursCaisseService} from "../../../../swagger-api";
import {Store} from "@ngrx/store";
import {fromJoursCaisse} from "../reducers";

@Injectable()
export class JoursCaisseEffects {
  // fetchData$ = createEffect(() =>
  //   this.actions$.pipe(
  //     ofType(JoursCaisseActions.fetchDataByDate),
  //     switchMap(({ date }) => {
  //       this.joursCaisseService.joursCaisseGetAllPost(date)),
  //       map((joursCaisse) => {console.log(joursCaisse);return JoursCaisseActions.setData({ data:joursCaisse })})
  //   )
  // );

  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(JoursCaisseActions.fetchDataByDate),
      switchMap(({date}) => {
        return this.joursCaisseService.joursCaisseGetAllPost(date);
      }),
      map((joursCaisse: JoursCaiseDTO[]) => {
        return JoursCaisseActions.setData({data: joursCaisse})
      })
    )
  );
  fetchDatabyStatusCpmt$ = createEffect(() =>
    this.actions$.pipe(
      ofType(JoursCaisseActions.fetchAllDataStatusCpmtNone),
      switchMap(() => {

        return this.joursCaisseService.joursCaisseGetAllByStatusCmptNonePost();
      }),
      map((joursCaisse: JoursCaiseDTO[]) => {
        return JoursCaisseActions.setData({data: joursCaisse})
      })
    )
  );

  receptionEnveloppe$ = createEffect(() =>
    this.actions$.pipe(
      ofType(JoursCaisseActions.receptionEnveloppes),
      switchMap(({enveloppes,chauffeurId,emetteurId}) => {

        return this.joursCaisseService.chargerJoursCaisseReceptionEnveloppesPost(chauffeurId,emetteurId,enveloppes);
      }),
      map((joursCaisse: JoursCaiseDTO[]) => {
        return JoursCaisseActions.setData({data: joursCaisse})
      })
    )
  );

  receptionResyncEnveloppe$ = createEffect(() =>
    this.actions$.pipe(
      ofType(JoursCaisseActions.receptionReSyncEnveloppes),
      switchMap(({enveloppes,/*chauffeurId,emetteurId,*/motifId,comments}) => {

        return this.joursCaisseService.chargerJoursCaisseReceptionReSyncEnveloppesPost(/*chauffeurId,emetteurId,*/motifId,comments,enveloppes);
      }),
      map((joursCaisse: JoursCaiseDTO[]) => {
        return JoursCaisseActions.setData({data: joursCaisse})
      })
    )
  );

  fetchDatabyStatusEnAttente$ = createEffect(() =>
    this.actions$.pipe(
      ofType(JoursCaisseActions.fetchAllDataStatusEnAttente),
      switchMap(() => {

        return this.joursCaisseService.joursCaisseGetAllByStatusEnAttentePost();
      }),
      map((joursCaisse) => {
        return JoursCaisseActions.setData({data: joursCaisse})
      })
    )
  );

  fetchDatabyStatusReceptionne$ = createEffect(() =>
    this.actions$.pipe(
      ofType(JoursCaisseActions.fetchAllDataStatusReceptionne),
      switchMap(() => {

        return this.joursCaisseService.joursCaisseGetAllByStatusReceptionnePost();
      }),
      map((joursCaisse) => {
        return JoursCaisseActions.setData({data: joursCaisse})
      })
    )
  );
  fetchGetAllByStatusReceptionneNonValide$ = createEffect(() =>
    this.actions$.pipe(
      ofType(JoursCaisseActions.fetchAllDataStatusReceptionneNonValide),
      switchMap(() => {

        return this.joursCaisseService.joursCaisseGetAllByStatusReceptionneNonValidePost();
      }),
      map((joursCaisse) => {
        return JoursCaisseActions.setData({data: joursCaisse})
      })
    )
  );
  fetchAllDataStatusCompter$ = createEffect(() =>
    this.actions$.pipe(
      ofType(JoursCaisseActions.fetchAllDataStatusCompter),
      switchMap(() => {

        return this.joursCaisseService.joursCaisseGetAllByStatusCompterPost();
      }),
      map((joursCaisse) => {
        return JoursCaisseActions.setData({data: joursCaisse})
      })
    )
  );

  fetchDatabwithFilters$ = createEffect(() =>
    this.actions$.pipe(
      ofType(JoursCaisseActions.fetchByFilters),
      switchMap((filters) => {

        return this.joursCaisseService.joursCaisseGetAllByStatusCmptNonePost();
      }),
      map((joursCaisse) => {
        return JoursCaisseActions.setData({data: joursCaisse})
      })
    )
  );

  fetchStats$ = createEffect(() =>
    this.actions$.pipe(
      ofType(JoursCaisseActions.fetchStats),
      switchMap(({}) => {
        return this.joursCaisseService.chargerJoursCaisseStatDashboardGet();
      }),
      map((stats) => {
        return JoursCaisseActions.setStats({stats: stats})
      })
    )
  );
  fetchNumZByEtablissement$ = createEffect(() =>
    this.actions$.pipe(
      ofType(JoursCaisseActions.fetchNumZByEtablissement),
      switchMap(({etabId}) => {
        return this.joursCaisseService.chargerJoursCaisseGetAllZByEtablissementGet(etabId);
      }),
      map((numZs) => {
        return JoursCaisseActions.setNumZByEtablissement({data: numZs})
      })
    )
  );

  fetchCaisseByEtablissement$ = createEffect(() =>
    this.actions$.pipe(
      ofType(JoursCaisseActions.fetchCaisseByEtablissement),
      switchMap(({etabId}) => {
        return this.joursCaisseService.chargerJoursCaisseGetAllCaisseByEtablissementGet(etabId);
      }),
      map((caisses) => {
        return JoursCaisseActions.setCaisseByEtablissement({data: caisses})
      })
    )
  );
  updateJourCaisse$ = createEffect(() =>
    this.actions$.pipe(
      ofType(JoursCaisseActions.UpdateJoursCaisse),
      switchMap(({jourCaisse}) => {
        return this.joursCaisseService.joursCaisseUpdatePut(jourCaisse);
      }),
      map((jourCaisse) =>

        JoursCaisseActions.setJoursCaisse({joursCaisse: jourCaisse})
      )
    )
  );
  changeStatusImprime$ = createEffect(() =>
    this.actions$.pipe(
      ofType(JoursCaisseActions.changeStatus),
      switchMap(({joursCaisse}) => {
        return this.joursCaisseService.joursCaisseChangeStatusImprimePost(joursCaisse);
      }),
      map((jourCaisse) =>

        JoursCaisseActions.setData({data: jourCaisse})
      )
    )
  );
  FetchById$ = createEffect(() =>
    this.actions$.pipe(
      ofType(JoursCaisseActions.fetchById),
      switchMap(() => this.store$.select(fromJoursCaisse.selectJoursCaisse).pipe(take(1))),
      switchMap((joursCaisse) => {
        return this.joursCaisseService.chargerJoursCaisseGetByIdGet(joursCaisse.id);
      }),
      map((jourCaisse) =>

        JoursCaisseActions.setJoursCaisse({joursCaisse: jourCaisse})
      )
    )
  );
  FetchByIdparams$ = createEffect(() =>
    this.actions$.pipe(
      ofType(JoursCaisseActions.fetchByIdParams),
      switchMap(({id}) => {
        return this.joursCaisseService.chargerJoursCaisseGetByIdGet(id);
      }),
      map((jourCaisse) =>

        JoursCaisseActions.setJoursCaisse({joursCaisse: jourCaisse})
      )
    )
  );
  //   return this.actions$.pipe(
  //
  //     ofType(JoursCaisseActions.fetchDataByDate),
  //     switchMap(({date}) => this.joursCaisseService.joursCaisseGetAllPost(date)),
  //
  //     map((joursCaisse) => {console.log(joursCaisse);return JoursCaisseActions.setData({ data:joursCaisse })})
  //
  //     /** An EMPTY observable only emits completion. Replace with your own observable API request */
  //   );
  // });
  // fetchJoursCaisse$ = createEffect(() =>
  //   this.actions$.pipe(
  //     ofType(JoursCaisseActions.fetchJoursCaisses),
  //     switchMap(() => this.joursCaisseService.joursCaisseGetAllGet()),
  //
  //      map((joursCaisse) => {console.log(joursCaisse);return JoursCaisseActions.setJoursCaisses({ joursCaisse })})
  //   )
  // );
  constructor(
    // private store$: Store<fromDocument.State & fromDocumentVersion.State>,
    private actions$: Actions,
    private joursCaisseService: JoursCaisseService,
    private store$: Store<fromJoursCaisse.State>
  ) {
  }
}
