import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {exhaustMap, map, mergeMap, switchMap, tap} from "rxjs/operators";
import {TypeFraisActions} from "../actions";
import {TypeFraisService} from "../../../../swagger-api";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable()
export class TypeFraisEffects {

  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(TypeFraisActions.fetchTypeFraiss),
      switchMap(({}) => {
        return this.TypeFraisService.typeFraisGetAllGet();
      }),
      map((TypeFraiss) => {
        return TypeFraisActions.setTypeFraiss({TypeFraiss})
      })
    )
  );


  deletedModePaei$ = createEffect(() =>
    this.actions$.pipe(
      ofType(TypeFraisActions.DeleteTypeFrais),

      exhaustMap(({TypeFrais}) =>
        this.TypeFraisService.typeFraisDeleteDelete(TypeFrais.id)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'TypeFrais Deleted', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          TypeFraisActions.fetchTypeFraiss()
        ];
      }),
    )
  );
  editTypeFrais$ = createEffect(() =>
    this.actions$.pipe(
      ofType(TypeFraisActions.EditTypeFrais),
      switchMap(({TypeFrais}) =>
        this.TypeFraisService.typeFraisUpdatePut(TypeFrais)
      ),
      tap((op) => {
        if (op != null && op != undefined) {

          this.snackBar.open('Success !', 'TypeFrais Updated', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          TypeFraisActions.fetchTypeFraiss()
        ];
      })
    )
  );
  createTypeFrais$ = createEffect(() =>
    this.actions$.pipe(
      ofType(TypeFraisActions.CreateTypeFrais),
      switchMap(({TypeFrais}) =>
        this.TypeFraisService.typeFraisCreatePost(TypeFrais)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'TypeFrais Created', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          TypeFraisActions.fetchTypeFraiss()
        ];
      })
    )
  );


  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private TypeFraisService: TypeFraisService
  ) {
  }
}
