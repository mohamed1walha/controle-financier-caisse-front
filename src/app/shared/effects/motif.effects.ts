import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {exhaustMap, map, mergeMap, switchMap, tap} from "rxjs/operators";
import {MotifActions} from "../actions";
import {MotifService} from "../../../../swagger-api";
import {NbGlobalPhysicalPosition, NbToastrService} from "@nebular/theme";

@Injectable()
export class MotifEffects {

  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MotifActions.fetchMotifs),
      switchMap(({}) => {
        return this.motifService.motifGetAllGet();
      }),
      map((motifs) => {
        return MotifActions.setMotifs({motifs})
      })
    )
  );


  deletedModePaei$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MotifActions.DeleteMotif),

      exhaustMap(({motif}) =>
        this.motifService.motifDeleteDelete(motif.id)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.toastService.success($localize`Motif Supprimée`, $localize`Succès`, {
            duration: 3000, position: NbGlobalPhysicalPosition.BOTTOM_RIGHT
          });

        }
      }),
      mergeMap((operationResult) => {
        return [
          MotifActions.fetchMotifs()
        ];
      }),
    )
  );
  editMotif$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MotifActions.EditMotif),
      switchMap(({motif}) =>
        this.motifService.motifUpdatePut(motif)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.toastService.success($localize`Motif Modifié`, $localize`Succès`, {
            duration: 3000, position: NbGlobalPhysicalPosition.BOTTOM_RIGHT
          });

        }
      }),
      mergeMap((operationResult) => {
        return [
          MotifActions.fetchMotifs()
        ];
      })
    )
  );
  createMotif$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MotifActions.CreateMotif),
      switchMap(({motif}) =>
        this.motifService.motifCreatePost(motif)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.toastService.success($localize`Opération effectuée`, $localize`Succès`, {
            duration: 3000, position: NbGlobalPhysicalPosition.BOTTOM_RIGHT
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          MotifActions.fetchMotifs()
        ];
      })
    )
  );


  constructor(
    private actions$: Actions,
    private motifService: MotifService,
    private toastService: NbToastrService
  ) {
  }
}
