import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {exhaustMap, map, mergeMap, switchMap, tap} from "rxjs/operators";
import {CoffreActions, CoffreAlimentationActions, PiedecheActions} from "../actions";
import {CoffreAlimentationService} from "../../../../swagger-api";
import {NbToastrService} from "@nebular/theme";
import {Store} from "@ngrx/store";
import {fromCoffre, fromCoffreAlimentation} from "../reducers";

@Injectable()
export class CoffreAlimentationEffects {

  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CoffreAlimentationActions.fetchcoffrealimentations),
      switchMap(({}) => {
        return this.coffrealimentationService.coffreAlimentationGetAllGet();
      }),
      map((coffrealimentations) => {
        return CoffreAlimentationActions.setcoffrealimentations({coffrealimentations})
      })
    )
  );


  deletedAlimentation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CoffreAlimentationActions.DeleteAlimentation),

      exhaustMap(({alimentationId}) =>
        this.coffrealimentationService.coffreAlimentationDeleteDelete(alimentationId)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.toastService.success($localize`Opération effectuée`, $localize`Succès`, {
            duration: 3000
          });
        }
      }),
      mergeMap((id) => {
        return [
          CoffreActions.fetchAlimentations({coffreId: id}),
          CoffreActions.fetchCoffres()
        ];
      })
    )
  );
  editCoffrealimentation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CoffreAlimentationActions.Editcoffrealimentation),
      switchMap(({coffrealimentation}) =>
        this.coffrealimentationService.coffreAlimentationUpdatePut(coffrealimentation)
      ),
      tap((op) => {
        if (op != null && op != undefined) {

          this.toastService.success($localize`Opération effectuée`, $localize`Succès`, {
            duration: 3000
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          CoffreAlimentationActions.fetchcoffrealimentations()
        ];
      })
    )
  );
  createCoffrealimentation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CoffreAlimentationActions.Createcoffrealimentation),
      switchMap(({coffrealimentations}) => {
          //console.log(coffrealimentations);
          return this.coffrealimentationService.coffreAlimentationCreatePost(coffrealimentations)
        }
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.toastService.success($localize`Opération effectuée`, $localize`Succès`, {
            duration: 3000
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          PiedecheActions.fetchAllByCoffreNull(),
          CoffreAlimentationActions.fetchcoffrealimentations()
        ];
      })
    )
  );

  createCoffrealimentationExp$ = createEffect(() =>
    this.actions$.pipe(
      ofType(CoffreAlimentationActions.CreatecoffrealimentationExp),
      switchMap(({coffreAlimentationDTO}) => {
          return this.coffrealimentationService.coffreAlimentationCreateExpPost(coffreAlimentationDTO)
        }
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.toastService.success($localize`Opération effectuée`, $localize`Succès`, {
            duration: 3000
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          PiedecheActions.fetchAllByCoffreNull(),
          CoffreAlimentationActions.fetchcoffrealimentations()
        ];
      })
    )
  );



  constructor(
    private toastService: NbToastrService,
    private actions$: Actions,
    private store$: Store<fromCoffreAlimentation.State & fromCoffre.State>,
    private coffrealimentationService: CoffreAlimentationService
  ) {
  }
}
