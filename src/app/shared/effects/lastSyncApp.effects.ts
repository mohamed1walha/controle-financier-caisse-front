import {Injectable} from "@angular/core";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Actions, createEffect, ofType} from "@ngrx/effects";
import {LastSyncAppService} from "../../../../swagger-api";
import {map, switchMap} from "rxjs/operators";
import {LastSyncAppActions} from "../actions";

@Injectable()
export class LastSyncAppEffects {

  getSyncDate$ = createEffect(() =>
    this.actions$.pipe(
      ofType(LastSyncAppActions.fetchlastSync),
      switchMap(() => {
        return this.lastSyncAppService.lastSyncAppLastSyncModePaieGet()
      }),
      map((lastDate) => {
        return LastSyncAppActions.setlastSync({lastsync: lastDate})
      })
    )
  );


  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private lastSyncAppService: LastSyncAppService
  ) {
  }
}
