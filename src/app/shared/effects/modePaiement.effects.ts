import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { exhaustMap, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { ModePaiementActions } from '../actions';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ModePaiementService } from '../../../../swagger-api';

@Injectable()
export class ModePaiementEffects {
  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ModePaiementActions.fetchData),
      switchMap(({}) => {
        return this.modePaiementService.modePaiementGetALLGet();
      }),
      map((modePaiement) => {
        return ModePaiementActions.setData({ data: modePaiement });
      })
    )
  );

  deletedModePaei$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ModePaiementActions.DeleteModePaiement),

      exhaustMap(({ mp }) =>
        this.modePaiementService.modePaiementDeleteDelete(mp.id)
      ),
      tap((op) => {
        if (op == 1) {
          this.snackBar.open('Success !', '', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [ModePaiementActions.fetchData()];
      })
    )
  );

  // getSyncDate$ = createEffect(() =>
  //   this.actions$.pipe(
  //     ofType(ModePaiementActions.fetchlastSyncModePaei),
  //     switchMap(({}) => {
  //       return this.modePaiementService.apiModePaiementModePaiementLastSyncModePaieGet();
  //     }),
  //     map((lastDate) => {
  //       return ModePaiementActions.setlastSyncModePaei({lastsync: lastDate})
  //     })
  //   )
  // );

  editModePaiement$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ModePaiementActions.EditModePaiement),
      exhaustMap(({ mp }) => this.modePaiementService.modePaiementEditPost(mp)),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', '', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom',
          });
        }
      }),
      mergeMap((operationResult) => {
        return [ModePaiementActions.fetchData()];
      })
    )
  );

  // syncModePaiement$ = createEffect(() =>
  //   this.actions$.pipe(
  //     ofType(ModePaiementActions.SyncModePaiement),
  //     exhaustMap(({  }) =>
  //       this.modePaiementService.apiModePaiementModePaiementSynchroniserPost()
  //     ),
  //     tap((op) => {
  //       if (op){
  //         this.snackBar.open('Success !', '', {
  //           duration: 3000,
  //           horizontalPosition: 'end',
  //           verticalPosition: 'bottom'
  //         });
  //       }
  //     }),
  //     mergeMap((operationResult) => {
  //       return [
  //         ModePaiementActions.fetchData(),
  //         ModePaiementActions.fetchlastSyncModePaei()
  //       ];
  //     })
  //   )
  // );

  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private modePaiementService: ModePaiementService
  ) {}
}
