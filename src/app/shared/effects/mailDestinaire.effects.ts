import {Injectable} from "@angular/core";
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {exhaustMap, map, mergeMap, switchMap, tap} from "rxjs/operators";
import {MailDestinataireActions} from "../actions";
import {MailDestinataireService} from "../../../../swagger-api";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable()
export class MailDestinataireEffects {

  fetchData$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MailDestinataireActions.fetchMailDestinataires),
      switchMap(({}) => {
        return this.mailDestinataireService.mailDestinataireGetAllGet();
      }),
      map((mailDestinataires) => {
        return MailDestinataireActions.setMailDestinataires({mailDestinataires})
      })
    )
  );


  deletedModePaei$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MailDestinataireActions.DeleteMailDestinataire),

      exhaustMap(({mailDestinataire}) =>
        this.mailDestinataireService.mailDestinataireDeleteDelete(mailDestinataire.id)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'mailDestinataire Deleted', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          MailDestinataireActions.fetchMailDestinataires()
        ];
      }),
    )
  );
  editMailDestinataire$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MailDestinataireActions.EditMailDestinataire),
      switchMap(({mailDestinataire}) =>
        this.mailDestinataireService.mailDestinataireUpdatePut(mailDestinataire)
      ),
      tap((op) => {
        if (op != null && op != undefined) {

          this.snackBar.open('Success !', 'mailDestinataire Updated', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          MailDestinataireActions.fetchMailDestinataires()
        ];
      })
    )
  );
  createMailDestinataire$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MailDestinataireActions.CreateMailDestinataire),
      switchMap(({mailDestinataire}) =>
        this.mailDestinataireService.mailDestinataireCreatePost(mailDestinataire)
      ),
      tap((op) => {
        if (op != null && op != undefined) {
          this.snackBar.open('Success !', 'mailDestinataire Created', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
        }
      }),
      mergeMap((operationResult) => {
        return [
          MailDestinataireActions.fetchMailDestinataires()
        ];
      })
    )
  );


  constructor(
    public snackBar: MatSnackBar,
    private actions$: Actions,
    private mailDestinataireService: MailDestinataireService
  ) {
  }
}
