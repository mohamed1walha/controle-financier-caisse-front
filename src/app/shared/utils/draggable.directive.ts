import {Directive, ElementRef, EventEmitter, HostListener, Output, Renderer2} from '@angular/core';

@Directive({
  selector: '[sdsDraggable]'
})
export class DraggableDirective {
  isHeld = false;
  draggablePosition = 50;
  minWidth = 5;
  maxWidth = 80;
  @Output() bladeWidth = new EventEmitter<number>();

  constructor(private element: ElementRef, private render: Renderer2) {
    this.render.setStyle(element.nativeElement, 'right', this.draggablePosition - 0.5 + 'vw');
  }

  @HostListener('mousedown', ['$event'])
  onMoseDown(e) {
    this.isHeld = true;
  }

  @HostListener('mouseup', ['$event'])
  onMoseUp(e) {
    this.isHeld = false;
    const screenWith = document.documentElement.clientWidth;
    let bladeWithVW = ((screenWith - this.draggablePosition) * 100) / screenWith;
    if (bladeWithVW > this.maxWidth) {
      bladeWithVW = this.maxWidth;
      this.render.removeStyle(this.element.nativeElement, 'left');
      this.render.setStyle(this.element.nativeElement, 'right', bladeWithVW - 0.5 + 'vw');
    }
    if (bladeWithVW < this.minWidth) {
      bladeWithVW = this.minWidth;
      this.render.removeStyle(this.element.nativeElement, 'left');
      this.render.setStyle(this.element.nativeElement, 'right', bladeWithVW - 0.5 + 'vw');
    }
    this.bladeWidth.emit(bladeWithVW);
  }

  @HostListener('document:mousemove', ['$event'])
  onMouseMove(e) {
    if (this.isHeld) {
      this.render.setStyle(this.element.nativeElement, 'left', e.x + 'px');
      this.draggablePosition = e.x;
    }
  }
}
