import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'separateurPipe',
})
export class SeparateurPipe implements PipeTransform {
  decimalSeparator = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .decimalSeparator;
  digitsAfterDecimal = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .digitsAfterDecimal;

  transform(value: any): string {
    if (value) {
      value = this.digitsAfterDecimal
        ? value?.toFixed(this.digitsAfterDecimal)
        : value;

      if (
        value !== undefined &&
        value !== null &&
        this.decimalSeparator === '.'
      ) {
        return value.toString().replace(',', this.decimalSeparator);
      } else if (
        value !== undefined &&
        value !== null &&
        this.decimalSeparator === ','
      ) {
        return value.toString().replace('.', this.decimalSeparator);
      } else {
        return value.toString();
      }
    }
    return '0';
  }
}
