import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appThreeDigitDecimaNumber]',
})
export class ThreeDigitNumberDirective {
  // Allow decimal numbers and negative values
  // private regex: RegExp = new RegExp(/^\d*[\.,]?\d{0,2}$/g);
  decimalSeparator =
    JSON.parse(localStorage.getItem('currentUser') || '{}').decimalSeparator ||
    '.,';
  digitsAfterDecimal = JSON.parse(localStorage.getItem('currentUser') || '{}')
    .digitsAfterDecimal;
  // private regex: RegExp = new RegExp(/^\d+[\.,]?\d{0,2}$/g);
  // private regex: RegExp = new RegExp(`^\d+[\.,]?\d{0,2}$`, 'g');
  numDecimals = +this.digitsAfterDecimal || 3; // La variable pour le nombre de chiffres décimaux
  regex = new RegExp(`^\\d+[\.,]?\\d{0,${this.numDecimals}}$`, 'g');
  // Allow key codes for special events. Reflect :
  // Backspace, tab, end, home
  private specialKeys: Array<string> = [
    'Backspace',
    'Tab',
    'End',
    'Home',
    '-',
    'ArrowLeft',
    'ArrowRight',
    'Del',
    'Delete',
  ];

  constructor(private el: ElementRef) {}

  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {
    //console.log(this.el.nativeElement.value);
    // Allow Backspace, tab, end, and home keys
    if (this.specialKeys.indexOf(event.key) !== -1) {
      return;
    }
    let current: string = this.el.nativeElement.value;
    const position = this.el.nativeElement.selectionStart;
    //console.log(event.key);
    const next: string = [
      current.slice(0, position),
      event.key == 'Decimal' ? '.' : event.key,
      current.slice(position),
    ].join('');
    console.log(event.key == 'Decimal' ? '.' : next);
    //console.log('rrrrrrrrrrr +  ' + !String(next).match(this.regex));
    if (next && !String(next).match(this.regex)) {
      if (event.key === ',') {
        event.key.replace(',', '.');
      }
      event.preventDefault();
    }
  }
}

// if (this.decimalSeparator === ',' && event.key === '.') {
//   event.key.replace('.', ',');
// } else if (this.decimalSeparator === '.' && event.key === ',') {
//   event.key.replace(',', '.');
// }
// console.log('  event.key +  ' + event.key);
