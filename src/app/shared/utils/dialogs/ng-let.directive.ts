// https://gist.github.com/AustinMatherne/659f0aed22efa094d8a55f31aebfe0d4#file-let-directive-ts
import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';

interface LetContext<T> {
  ngLet: T | null;
}

@Directive({
  selector: '[ngLet]'
})
export class NgLetDirective<T> {
  private _context: LetContext<T> = {ngLet: null};

  constructor(_viewContainer: ViewContainerRef, _templateRef: TemplateRef<LetContext<T>>) {
    _viewContainer.createEmbeddedView(_templateRef, this._context);
  }

  @Input()
  set ngLet(value: T) {
    this._context.ngLet = value;
  }
}
