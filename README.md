# Controle Financier (Angular 13)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.0.1.

> The goal of this project is to provide a starter with strong architecture concepts, best practices and several Angular features.

## Setting up the local environment and workspace

1. Install the latest LTS version of Node.js from https://nodejs.org.

1. Install the Angular CLI: `npm install -g @angular/cli`


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
